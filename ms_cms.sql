-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 16, 2018 at 06:05 AM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ms_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_address`
--

CREATE TABLE `tbl_address` (
  `CID` varchar(20) NOT NULL,
  `Perma_Address` varchar(150) NOT NULL,
  `Perma_City` varchar(20) NOT NULL,
  `Perma_Phone` varchar(15) NOT NULL,
  `Cur_Address` varchar(150) NOT NULL,
  `Cur_City` varchar(20) NOT NULL,
  `Cur_Phone` varchar(15) NOT NULL,
  `Guard_Address` varchar(150) NOT NULL,
  `Guard_City` varchar(20) NOT NULL,
  `Guard_Phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_address`
--

INSERT INTO `tbl_address` (`CID`, `Perma_Address`, `Perma_City`, `Perma_Phone`, `Cur_Address`, `Cur_City`, `Cur_Phone`, `Guard_Address`, `Guard_City`, `Guard_Phone`) VALUES
('L1F17PHDC0001', 'Town', 'Abbottabad', '03004182814', 'Iqbal Town', 'Mansehra', '03004182814', 'Iqbal Town', 'Battagram', '03004182814'),
('L1F17PHMD0012', 'Illama Iqbal Town', 'Bannu', '03004182814', 'Illama Iqbal Town', 'Bannu', '03004182814', 'Illama Iqbal Town', 'Bannu', '03004182814'),
('L1F15BSCS0348', 'Raiwind', 'Rawalpindi', '03004182814', 'Raiwind', 'Rawalpindi', '03004182814', 'Raiwind', 'Rawalpindi', '03004182814'),
('L1F15PHMD1001', 'sassaasasasasa', 'deeeee', '03000000000', 'sassaasasasasa', 'deeeee', '03000000000', 'sassaasasasasa', 'Hangu', '03000000000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admission_form`
--

CREATE TABLE `tbl_admission_form` (
  `CID` varchar(20) NOT NULL,
  `Upload_Form` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_admission_form`
--

INSERT INTO `tbl_admission_form` (`CID`, `Upload_Form`) VALUES
('L1F17PHDC0001', 'admission/L1F17PHDC0001.jpg'),
('L1F17PHMD0012', 'admission/L1F17PHMD0012.jpg'),
('L1F15BSCS0348', 'admission/L1F15BSCS0348.png'),
('L1F15PHMD1001', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_audit_courses`
--

CREATE TABLE `tbl_audit_courses` (
  `CID` varchar(20) NOT NULL,
  `Course_ID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_audit_courses`
--

INSERT INTO `tbl_audit_courses` (`CID`, `Course_ID`) VALUES
('L1F15BSCS0348', 'abc4'),
('L1F17PHDC0001', 'abc321'),
('L1F17PHDC0001', 'oop4'),
('L1F17PHMD0012', 'oop4');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_authors`
--

CREATE TABLE `tbl_authors` (
  `AID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_authors`
--

INSERT INTO `tbl_authors` (`AID`, `Name`) VALUES
(1, 'saad'),
(2, 'shahjahan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate`
--

CREATE TABLE `tbl_candidate` (
  `CID` varchar(20) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Father_Name` varchar(100) NOT NULL,
  `Picture` varchar(100) NOT NULL,
  `DOB` date NOT NULL,
  `CNIC_num` varchar(15) NOT NULL,
  `CNIC_front` varchar(100) NOT NULL,
  `CNIC_back` varchar(100) NOT NULL,
  `Gender` text NOT NULL,
  `Mobile` varchar(100) NOT NULL,
  `Domicile` text NOT NULL,
  `Religion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_candidate`
--

INSERT INTO `tbl_candidate` (`CID`, `Email`, `Name`, `Father_Name`, `Picture`, `DOB`, `CNIC_num`, `CNIC_front`, `CNIC_back`, `Gender`, `Mobile`, `Domicile`, `Religion`) VALUES
('L1F15BSCS0324', 'muhammadsobanjaved@gmail.com', 'Soban Javed', 'Soban', '5a5395e28991bpurple.jpg', '2018-01-08', '0', '5a5395e28995bpurple.jpg', '/opt/lampp/temp/php6IMymF', 'Male', '03334504341', 'Kotli', 'Islam'),
('L1F15BSCS0332', 'saad.majid@ucp.edu.pk', 'Saad Majid', 'Majid', '5a53966855952purple.jpg', '2018-01-05', '0', '5a5396685598cpurple.jpg', '/opt/lampp/temp/phpxliLnZ', 'Male', '03334504341', 'Quetta', 'Islam'),
('L1F15BSCS0348', 'razakhan@gmail.com', 'Raza Khan', 'Jamal khan', 'profilepic/L1F15BSCS0348.png', '2018-02-13', '35202-2956897-8', 'cnic/L1F15BSCS0348.png', 'cnic/L1F15BSCS0348-back.png', 'Male', '03354182814', 'Gwadar', 'Islam'),
('L1F15PHMD1001', 'sarmad_1@gmail.com', 'Sarmad', 'Ahmad', '', '1990-09-03', '36205-5684698-7', '', '', 'Male', '03000000000', 'Gwadar', 'Islam'),
('L1F17MSCS0001', 'ali@gmail', 'Ali', 'Javed', '', '2018-01-16', '3520229379', '', '', 'Male', '03354182814', 'Bolan', 'Islam'),
('L1F17PHDC0001', 'sobanjaved10@yahoo.com', 'Adil', 'Javed', 'profilepic/L1F17PHDC0001.png', '2000-06-04', '35211-526564-4', 'cnic/L1F17PHDC0001.png', 'cnic/L1F17PHDC0001-back.png', 'Male', '03334504314', 'Diamer', 'Islam'),
('L1F17PHMD0012', 'Ahmad@gmail.com', 'Ahmad Raza', 'Akhtar', 'profilepic/L1F17PHMD0012.png', '2018-02-12', '35214-51615', 'cnic/L1F17PHMD0012.png', 'cnic/L1F17PHMD0012-back.jpg', 'Male', '03004182814', 'Dera Bugti', 'Islam');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comprehensive`
--

CREATE TABLE `tbl_comprehensive` (
  `CID` varchar(20) NOT NULL,
  `Exam_Date` date NOT NULL,
  `Max_Marks` double NOT NULL,
  `Obt_Marks` double NOT NULL,
  `Paper1` varchar(100) NOT NULL,
  `Paper2` varchar(100) NOT NULL,
  `Paper3` varchar(100) NOT NULL,
  `Solution1` varchar(100) NOT NULL,
  `Comprehensive_Result` varchar(100) NOT NULL,
  `Solution2` varchar(100) NOT NULL,
  `Solution3` varchar(100) NOT NULL,
  `result1` tinyint(1) NOT NULL,
  `result2` tinyint(1) NOT NULL,
  `result3` tinyint(1) NOT NULL,
  `attempt` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_comprehensive`
--

INSERT INTO `tbl_comprehensive` (`CID`, `Exam_Date`, `Max_Marks`, `Obt_Marks`, `Paper1`, `Paper2`, `Paper3`, `Solution1`, `Comprehensive_Result`, `Solution2`, `Solution3`, `result1`, `result2`, `result3`, `attempt`) VALUES
('L1F17PHDC0001', '2018-02-15', 200, 189, 'comprehensive/L1F17PHDC0001-paper1.jpg', 'comprehensive/L1F17PHDC0001-paper2.png', 'comprehensive/L1F17PHDC0001-paper3.png', '', 'comprehensive/L1F17PHDC0001-result.jpg', '', '', 0, 0, 0, 1),
('L1F17PHMD0012', '2018-02-22', 50, 41, 'comprehensive/L1F17PHMD0012-paper1.jpg', 'comprehensive/L1F17PHMD0012-paper2.png', 'comprehensive/L1F17PHMD0012-paper3.png', '', 'comprehensive/L1F17PHMD0012-result.jpg', '', '', 0, 0, 0, 1),
('L1F15BSCS0348', '2018-02-21', 800, 689, 'comprehensive/L1F15BSCS0348-paper1.jpg', 'comprehensive/L1F15BSCS0348-paper2.png', 'comprehensive/L1F15BSCS0348-paper3.jpg', '', 'comprehensive/L1F15BSCS0348-result.png', '', '', 0, 0, 0, 1),
('L1F15PHMD1001', '2018-08-02', 0, 0, 'comprehensive/L1F15PHMD1001-paper1.jpg', '', '', 'comprehensive/L1F15PHMD1001-solution1.jpg', '', '', '', 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_courses`
--

CREATE TABLE `tbl_courses` (
  `Course_ID` varchar(20) NOT NULL,
  `Course_Name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_courses`
--

INSERT INTO `tbl_courses` (`Course_ID`, `Course_Name`) VALUES
('abc123', 'ABC'),
('abc321', 'DB'),
('abc4', 'DSA'),
('oop4', 'OOP');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_defence_notification`
--

CREATE TABLE `tbl_defence_notification` (
  `CID` varchar(255) NOT NULL,
  `Date` date NOT NULL,
  `BASR_File` varchar(255) NOT NULL,
  `Commitee_File` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_defence_notification`
--

INSERT INTO `tbl_defence_notification` (`CID`, `Date`, `BASR_File`, `Commitee_File`) VALUES
('L1F15BSCS0348', '2018-02-13', 'public_notification/L1F15BSCS0348-basr.jpg', 'public_notification/L1F15BSCS0348-Commitee_File.jpg'),
('L1F15PHMD1001', '0000-00-00', '', ''),
('L1F17PHMD0012', '2018-02-13', 'public_notification/L1F17PHMD0012-basr.png', 'public_notification/L1F17PHMD0012-Commitee_File.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_degree_completion`
--

CREATE TABLE `tbl_degree_completion` (
  `CID` varchar(255) NOT NULL,
  `Date` date NOT NULL,
  `Transcript_File` varchar(255) NOT NULL,
  `Final_Degree_File` varchar(255) NOT NULL,
  `Transcript_File_Back` varchar(255) NOT NULL,
  `Final_Degree_File_Back` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_degree_completion`
--

INSERT INTO `tbl_degree_completion` (`CID`, `Date`, `Transcript_File`, `Final_Degree_File`, `Transcript_File_Back`, `Final_Degree_File_Back`) VALUES
('L1F17PHMD0012', '2018-02-20', 'degree/L1F17PHMD0012-Transcript_File.png', 'degree/L1F17PHMD0012-final_file.jpg', '', ''),
('L1F15BSCS0348', '2018-02-22', 'degree/L1F15BSCS0348-Transcript_File.png', 'degree/L1F15BSCS0348-final_file.jpg', '', ''),
('L1F15PHMD1001', '0000-00-00', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_evaluation`
--

CREATE TABLE `tbl_evaluation` (
  `CID` varchar(20) NOT NULL,
  `Evaluator_name` varchar(150) NOT NULL,
  `Affeliations` varchar(20) NOT NULL,
  `Evaluation_Report` varchar(100) NOT NULL,
  `Approval_File` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_evaluation`
--

INSERT INTO `tbl_evaluation` (`CID`, `Evaluator_name`, `Affeliations`, `Evaluation_Report`, `Approval_File`) VALUES
('L1F17MSCS0001', 'sad majid', 'soban12', 'evaluation/L1F17MSCS0001-gpcfile.png', 'evaluation/L1F17MSCS0001-app_file.png'),
('L1F17PHMD0012', 'Soban45', 'Soban', 'evaluation/L1F17PHMD0012-gpcfile.png', 'evaluation/L1F17PHMD0012-app_file.png'),
('L1F15BSCS0348', 'Soban45', 'Saad', 'evaluation/L1F15BSCS0348-gpcfile.jpg', 'evaluation/L1F15BSCS0348-app_file.jpg'),
('L1F15PHMD1001', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gat`
--

CREATE TABLE `tbl_gat` (
  `CID` varchar(20) NOT NULL,
  `Test_Date` date NOT NULL,
  `Max_Marks` double NOT NULL,
  `Obt_Marks` double NOT NULL,
  `Result_Card` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_gat`
--

INSERT INTO `tbl_gat` (`CID`, `Test_Date`, `Max_Marks`, `Obt_Marks`, `Result_Card`) VALUES
('L1F17PHDC0001', '2017-11-11', 100, 78, 'gat/L1F17PHDC0001.jpg'),
('L1F15BSCS0324', '2018-01-09', 100, 95, 'ÿØÿà\0JFIF\0\0H\0H\0\0ÿÛ\0C\0\n\n\n\r\rÿÛ\0C'),
('L1F15BSCS0324', '2018-01-09', 100, 95, ''),
('L1F17PHMD0012', '2018-02-20', 60, 40, 'gat/L1F17PHMD0012.jpg'),
('L1F15BSCS0348', '2018-02-14', 700, 550, 'gat/L1F15BSCS0348.jpg'),
('L1F15PHMD1001', '2018-08-08', 16, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pre_req`
--

CREATE TABLE `tbl_pre_req` (
  `CID` varchar(20) NOT NULL,
  `Course_ID` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pre_req`
--

INSERT INTO `tbl_pre_req` (`CID`, `Course_ID`) VALUES
('L1F15BSCS0324', 'abc123'),
('L1F15BSCS0348', 'abc321'),
('L1F15BSCS0348', 'oop4'),
('L1F17PHDC0001', 'abc123'),
('L1F17PHDC0001', 'abc321'),
('L1F17PHMD0012', 'abc321');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_qualification`
--

CREATE TABLE `tbl_qualification` (
  `CID` varchar(20) NOT NULL,
  `Degree` varchar(100) NOT NULL,
  `Passing_Year` int(11) NOT NULL,
  `Board` varchar(100) NOT NULL,
  `Major` text NOT NULL,
  `Max_Marks` double NOT NULL,
  `Obt_Marks` double NOT NULL,
  `DMC` varchar(100) NOT NULL,
  `Original_Certificate` varchar(100) NOT NULL,
  `Distinction` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_qualification`
--

INSERT INTO `tbl_qualification` (`CID`, `Degree`, `Passing_Year`, `Board`, `Major`, `Max_Marks`, `Obt_Marks`, `DMC`, `Original_Certificate`, `Distinction`) VALUES
('L1F17PHDC0001', '6', 2016, '15', '1', 1100, 1087, 'qualification/dmc-L1F17PHDC0001.jpg', 'qualification/degree-L1F17PHDC0001.jpg', 'Second-Position'),
('L1F17PHDC0001', 'PhD', 2017, 'University of Peshawar, Peshawar', 'Arts', 1100, 1087, '', '', 'Second-Position'),
('L1F17PHDC0001', 'SSC/Equivalent', 2016, 'Board of Intermediate and Secondary Education, Kohat', 'Science', 1100, 1087, 'qualification/L1F17PHDC0001-1-dmc.png', 'qualification/L1F17PHDC0001-1-degree.png', 'Second-Position'),
('L1F17PHMD0012', '', 0, '', '', 0, 0, '', '', ''),
('L1F17PHMD0012', 'MS / M.Phil/Equivalent', 2003, 'Kohat University of Science & Technology, Kohat', 'Arts', 500, 444, 'qualification/L1F17PHMD0012-5-dmc.jpg', 'qualification/L1F17PHMD0012-5-degree.png', 'Third-Position'),
('L1F17PHMD0012', 'PhD', 2010, 'Aga Khan Educational Board, Karachi', 'Arts', 70, 61, 'qualification/L1F17PHMD0012-6-dmc.png', 'qualification/L1F17PHMD0012-6-degree.jpg', 'Third-Position'),
('L1F15BSCS0348', '', 0, '', '', 0, 0, '', '', ''),
('L1F15BSCS0348', 'Post-Doc', 2007, 'Aga Khan Educational Board, Karachi', 'Science', 900, 789, 'qualification/L1F15BSCS0348-7-dmc.jpg', 'qualification/L1F15BSCS0348-7-degree.png', 'First-Position'),
('L1F15BSCS0348', 'FA / FSc. /Equivalent', 2013, 'University of Peshawar, Peshawar', 'Science', 250, 100, '', '', 'No-Position'),
('L1F15BSCS0348', 'BA / BSc. /Equivalent', 2017, 'Board of Intermediate and Secondary Education, Kohat', 'Science', 0, 0, '', '', 'No-Position'),
('L1F15BSCS0348', 'MA / MSc. /Equivalent', 2017, 'Board of Intermediate and Secondary Education, Kohat', 'Science', 0, 0, '', '', 'No-Position'),
('L1F15BSCS0348', 'SSC/Equivalent', 2017, 'Board of Intermediate and Secondary Education, Kohat', 'Science', 500, 400, 'qualification/L1F15BSCS0348-1-dmc.png', 'qualification/L1F15BSCS0348-1-degree.jpg', 'No-Position'),
('L1F15BSCS0348', 'MS / M.Phil/Equivalent', 2014, 'Board of Intermediate and Secondary Education, Kohat', 'Science', 300, 250, 'qualification/L1F15BSCS0348-5-dmc.png', 'qualification/L1F15BSCS0348-5-degree.png', 'No-Position'),
('L1F17PHMD0012', 'SSC/Equivalent', 2017, 'Board of Intermediate and Secondary Education, Kohat', 'Science', 300, 108, 'qualification/L1F17PHMD0012-1-dmc.png', 'qualification/L1F17PHMD0012-1-degree.png', 'No-Position'),
('L1F17PHMD0012', 'FA / FSc. /Equivalent', 2017, 'Board of Intermediate and Secondary Education, Kohat', 'Science', 350, 256, 'qualification/L1F17PHMD0012-2-dmc.png', 'qualification/L1F17PHMD0012-2-degree.png', 'No-Position'),
('L1F17PHMD0012', 'BA / BSc. /Equivalent', 2017, 'Board of Intermediate and Secondary Education, Kohat', 'Science', 600, 502, 'qualification/L1F17PHMD0012-3-dmc.png', 'qualification/L1F17PHMD0012-3-degree.png', 'No-Position'),
('L1F15PHMD1001', '', 0, '', '', 0, 0, '', '', ''),
('L1F15PHMD1001', 'SSC/Equivalent', 2017, 'University of Balochistan, Quetta', 'Science', 0, 0, '', '', 'No-Position'),
('L1F15PHMD1001', 'FA / FSc. /Equivalent', 2017, 'University of Balochistan, Quetta', 'Science', 0, 0, '', '', 'No-Position'),
('L1F15PHMD1001', 'MA / MSc. /Equivalent', 2017, 'Aga Khan Educational Board, Karachi', 'Science', 0, 0, '', '', 'No-Position');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_research_paper`
--

CREATE TABLE `tbl_research_paper` (
  `PID` int(11) NOT NULL,
  `Title` varchar(255) NOT NULL,
  `Ptype` varchar(255) NOT NULL,
  `JC` varchar(255) NOT NULL,
  `JC_Name` varchar(255) NOT NULL,
  `Impact_Factor` int(11) NOT NULL,
  `ISSN` varchar(255) NOT NULL,
  `PDate` date NOT NULL,
  `PFile` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_research_paper`
--

INSERT INTO `tbl_research_paper` (`PID`, `Title`, `Ptype`, `JC`, `JC_Name`, `Impact_Factor`, `ISSN`, `PDate`, `PFile`) VALUES
(1, 'Formal And Executable Specification of Random Waypoint Mobility Model Using ', 'A', 'sad', 'sobaan', 5, '123456', '2018-01-16', ''),
(2, 'amjad', 'B', 'jc', 'alijc', 2, '78654', '2018-01-13', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_rp_author`
--

CREATE TABLE `tbl_rp_author` (
  `AID` int(11) NOT NULL,
  `PID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_rp_author`
--

INSERT INTO `tbl_rp_author` (`AID`, `PID`) VALUES
(1, 1),
(2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_semester_progress_report`
--

CREATE TABLE `tbl_semester_progress_report` (
  `CID` varchar(20) NOT NULL,
  `Semester` varchar(20) NOT NULL,
  `Year` varchar(10) NOT NULL,
  `Report_Date` date NOT NULL,
  `Student_Progress` int(11) NOT NULL,
  `Progress_File` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supervisor`
--

CREATE TABLE `tbl_supervisor` (
  `CID` varchar(20) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Affiliations` varchar(100) NOT NULL,
  `Approval_file` varchar(100) NOT NULL,
  `GPC_Approval_File` varchar(100) NOT NULL,
  `BOS_Approval_File` varchar(100) NOT NULL,
  `BASR_Approval_File` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_supervisor`
--

INSERT INTO `tbl_supervisor` (`CID`, `Name`, `Affiliations`, `Approval_file`, `GPC_Approval_File`, `BOS_Approval_File`, `BASR_Approval_File`) VALUES
('L1F17PHDC0001', 'Saad', 'Dean', 'supervisor/L1F17PHDC0001-app_file.jpg', 'supervisor/L1F17PHDC0001-gpcfile.png', 'supervisor/L1F17PHDC0001-bosfile.png', 'supervisor/L1F17PHDC0001-basrfile.png'),
('L1F17MSCS0001', 'Saad', 'Dean Ashraf', '', '', '', ''),
('L1F17PHMD0012', 'Ashraf Iqbal', 'Dean', 'supervisor/L1F17PHMD0012-app_file.png', 'supervisor/L1F17PHMD0012-gpcfile.jpg', 'supervisor/L1F17PHMD0012-bosfile.png', 'supervisor/L1F17PHMD0012-basrfile.png'),
('L1F15BSCS0348', 'Ashraf Iqbal', 'Dean', 'supervisor/L1F15BSCS0348-app_file.jpg', 'supervisor/L1F15BSCS0348-gpcfile.jpg', 'supervisor/L1F15BSCS0348-bosfile.jpg', 'supervisor/L1F15BSCS0348-basrfile.png'),
('L1F15PHMD1001', 'Ali', 'Punjab univercity', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_synopsis`
--

CREATE TABLE `tbl_synopsis` (
  `CID` varchar(20) NOT NULL,
  `Approval_Date` date NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Abstract` varchar(200) NOT NULL,
  `Supervisor` varchar(20) NOT NULL,
  `Synopsis_File` varchar(100) NOT NULL,
  `GPC_Approval_file` varchar(100) NOT NULL,
  `DPC_Approcal_File` varchar(100) NOT NULL,
  `BASR_Approval_File` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_synopsis`
--

INSERT INTO `tbl_synopsis` (`CID`, `Approval_Date`, `Title`, `Abstract`, `Supervisor`, `Synopsis_File`, `GPC_Approval_file`, `DPC_Approcal_File`, `BASR_Approval_File`) VALUES
('L1F17PHDC0001', '2018-02-15', 'Hello', '1231165', 'Saeed Iqbal', 'synopsis/L1F17PHDC0001-result.png', 'synopsis/L1F17PHDC0001-gpcfile.png', 'synopsis/L1F17PHDC0001-dpcfile.png', 'synopsis/L1F17PHDC0001-basrfile.jpg'),
('L1F17PHMD0012', '2018-02-20', 'Saad', 'ikftvjy', 'soban', 'synopsis/L1F17PHMD0012-synopsis.png', 'synopsis/L1F17PHMD0012-gpcfile.png', 'synopsis/L1F17PHMD0012-dpcfile.png', 'synopsis/L1F17PHMD0012-basrfile.jpg'),
('L1F15BSCS0348', '2018-02-28', 'Hello', 'Hey!', 'soban', 'synopsis/L1F15BSCS0348-synopsis.jpg', 'synopsis/L1F15BSCS0348-gpcfile.png', 'synopsis/L1F15BSCS0348-dpcfile.jpg', 'synopsis/L1F15BSCS0348-basrfile.png'),
('L1F15PHMD1001', '2018-08-02', 'Hello', 'abcdefghujklmnopqrstuvwxyz', 'Ali', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_thesis`
--

CREATE TABLE `tbl_thesis` (
  `CID` varchar(20) NOT NULL,
  `Date` varchar(150) NOT NULL,
  `title` varchar(20) NOT NULL,
  `gpcfile` varchar(100) NOT NULL,
  `thesisfile` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_thesis`
--

INSERT INTO `tbl_thesis` (`CID`, `Date`, `title`, `gpcfile`, `thesisfile`) VALUES
('L1F17MSCS0001', '2018-02-24', 'saad67', 'thesis/L1F17MSC', 'thesis/L1F17MSCS0001-app_file.jpg'),
('L1F17PHMD0012', '2018-02-24', 'gggalsabhas', 'thesis/L1F17PHMD0012-gpc.png', 'thesis/L1F17PHMD0012-thesis.png'),
('L1F15BSCS0348', '2018-02-22', 'hello', 'thesis/L1F15BSCS0348-gpc.png', 'thesis/L1F15BSCS0348-thesis.png'),
('L1F15PHMD1001', '2018-08-20', 'hello', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transcript_details`
--

CREATE TABLE `tbl_transcript_details` (
  `CID` varchar(20) NOT NULL,
  `Semester_Num` varchar(255) NOT NULL,
  `Result_Date` date NOT NULL,
  `Max_CGPA` double NOT NULL,
  `Obt_CGPA` double NOT NULL,
  `Result_Card` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_transcript_details`
--

INSERT INTO `tbl_transcript_details` (`CID`, `Semester_Num`, `Result_Date`, `Max_CGPA`, `Obt_CGPA`, `Result_Card`) VALUES
('L1F17PHDC0001', '0', '1984-11-11', 4, 3.91, 'transcript/L1F17PHDC0001.jpg'),
('L1F17PHMD0012', '0', '2018-02-21', 60, 49, 'transcript/FALL2017-L1F17PHMD0012.png'),
('L1F17PHMD0012', 'SPRING 2013', '2018-02-21', 60, 49, ''),
('L1F17PHMD0012', 'SPRING 2011', '2018-02-21', 60, 49, ''),
('L1F17PHMD0012', 'FALL 2017', '2018-02-21', 60, 49, 'transcript/FALL2017-L1F17PHMD0012.png'),
('L1F15BSCS0348', '', '0000-00-00', 0, 0, ''),
('L1F15BSCS0348', 'SUMMER 2004', '2018-02-23', 1000, 903, 'transcript/SUMMER2004-L1F15BSCS0348.jpg'),
('L1F15BSCS0348', 'SUMMER 2005', '2018-02-09', 1000, 919, 'transcript/SUMMER2005-L1F15BSCS0348.jpg'),
('L1F15PHMD1001', '', '0000-00-00', 0, 0, ''),
('L1F15PHMD1001', 'FALL 2017', '2018-08-13', 4, 4, '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_address`
--
ALTER TABLE `tbl_address`
  ADD KEY `CID` (`CID`);

--
-- Indexes for table `tbl_admission_form`
--
ALTER TABLE `tbl_admission_form`
  ADD KEY `CID` (`CID`);

--
-- Indexes for table `tbl_audit_courses`
--
ALTER TABLE `tbl_audit_courses`
  ADD PRIMARY KEY (`CID`,`Course_ID`),
  ADD KEY `CID` (`CID`),
  ADD KEY `Course_ID` (`Course_ID`);

--
-- Indexes for table `tbl_authors`
--
ALTER TABLE `tbl_authors`
  ADD PRIMARY KEY (`AID`);

--
-- Indexes for table `tbl_candidate`
--
ALTER TABLE `tbl_candidate`
  ADD PRIMARY KEY (`CID`);

--
-- Indexes for table `tbl_comprehensive`
--
ALTER TABLE `tbl_comprehensive`
  ADD KEY `CID` (`CID`);

--
-- Indexes for table `tbl_courses`
--
ALTER TABLE `tbl_courses`
  ADD PRIMARY KEY (`Course_ID`);

--
-- Indexes for table `tbl_defence_notification`
--
ALTER TABLE `tbl_defence_notification`
  ADD PRIMARY KEY (`CID`);

--
-- Indexes for table `tbl_evaluation`
--
ALTER TABLE `tbl_evaluation`
  ADD KEY `CID` (`CID`);

--
-- Indexes for table `tbl_gat`
--
ALTER TABLE `tbl_gat`
  ADD KEY `CID` (`CID`);

--
-- Indexes for table `tbl_pre_req`
--
ALTER TABLE `tbl_pre_req`
  ADD PRIMARY KEY (`CID`,`Course_ID`),
  ADD KEY `CID` (`CID`),
  ADD KEY `Course_ID` (`Course_ID`);

--
-- Indexes for table `tbl_qualification`
--
ALTER TABLE `tbl_qualification`
  ADD KEY `CID` (`CID`);

--
-- Indexes for table `tbl_research_paper`
--
ALTER TABLE `tbl_research_paper`
  ADD PRIMARY KEY (`PID`);

--
-- Indexes for table `tbl_rp_author`
--
ALTER TABLE `tbl_rp_author`
  ADD PRIMARY KEY (`AID`,`PID`);

--
-- Indexes for table `tbl_semester_progress_report`
--
ALTER TABLE `tbl_semester_progress_report`
  ADD KEY `CID` (`CID`);

--
-- Indexes for table `tbl_supervisor`
--
ALTER TABLE `tbl_supervisor`
  ADD KEY `CID` (`CID`);

--
-- Indexes for table `tbl_synopsis`
--
ALTER TABLE `tbl_synopsis`
  ADD KEY `CID_2` (`CID`);

--
-- Indexes for table `tbl_thesis`
--
ALTER TABLE `tbl_thesis`
  ADD KEY `CID` (`CID`);

--
-- Indexes for table `tbl_transcript_details`
--
ALTER TABLE `tbl_transcript_details`
  ADD KEY `CID` (`CID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_authors`
--
ALTER TABLE `tbl_authors`
  MODIFY `AID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_research_paper`
--
ALTER TABLE `tbl_research_paper`
  MODIFY `PID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_address`
--
ALTER TABLE `tbl_address`
  ADD CONSTRAINT `tbl_address_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_admission_form`
--
ALTER TABLE `tbl_admission_form`
  ADD CONSTRAINT `tbl_admission_form_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_audit_courses`
--
ALTER TABLE `tbl_audit_courses`
  ADD CONSTRAINT `tbl_audit_courses_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_audit_courses_ibfk_2` FOREIGN KEY (`Course_ID`) REFERENCES `tbl_courses` (`Course_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_comprehensive`
--
ALTER TABLE `tbl_comprehensive`
  ADD CONSTRAINT `tbl_comprehensive_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_evaluation`
--
ALTER TABLE `tbl_evaluation`
  ADD CONSTRAINT `tbl_evaluation_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_gat`
--
ALTER TABLE `tbl_gat`
  ADD CONSTRAINT `tbl_gat_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_pre_req`
--
ALTER TABLE `tbl_pre_req`
  ADD CONSTRAINT `tbl_pre_req_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tbl_pre_req_ibfk_2` FOREIGN KEY (`Course_ID`) REFERENCES `tbl_courses` (`Course_ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_qualification`
--
ALTER TABLE `tbl_qualification`
  ADD CONSTRAINT `tbl_qualification_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_semester_progress_report`
--
ALTER TABLE `tbl_semester_progress_report`
  ADD CONSTRAINT `tbl_semester_progress_report_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_supervisor`
--
ALTER TABLE `tbl_supervisor`
  ADD CONSTRAINT `tbl_supervisor_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_synopsis`
--
ALTER TABLE `tbl_synopsis`
  ADD CONSTRAINT `tbl_synopsis_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_thesis`
--
ALTER TABLE `tbl_thesis`
  ADD CONSTRAINT `tbl_thesis_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_transcript_details`
--
ALTER TABLE `tbl_transcript_details`
  ADD CONSTRAINT `tbl_transcript_details_ibfk_1` FOREIGN KEY (`CID`) REFERENCES `tbl_candidate` (`CID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

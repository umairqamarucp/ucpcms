
<style>
form {
    border: none;
}

p{
color:black;
font-size:8px;
}

input[type=text], input[type=password], input[type=email], select,.form-control{
    width: 100%;
    padding: 8px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
    
}

button {
    background-color:white;
    color: white;
    padding: 6px 10px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
    font-size:8px;
    
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

.form-img{
    width: 160px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}
.panel {

  margin-bottom: 20px;
 
  border: 1px solid #983415;
  border-radius: 4px;
  -webkit-box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
  box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
}

</style>


<?php
include 'includes/connection.php';
if(isset($_POST['reg_id'])){
    $id = $_POST['reg_id'];
}


if (isset($_POST['code_id']))
{

    $code = $_POST['code_id'];
    if ($code == 1)
    {
        $result = mysqli_query($connection, "SELECT * FROM tbl_candidate where CID = '$id'") or die(mysqli_error($connection)); 
        $row = mysqli_fetch_array($result);
        echo " <div class='modal fade' id='myModal' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>Student Personal Information </Strong></h3>
      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>

      <div class='panel-body' style='background-color:#ab9f9b24;'>
         
         
         <form  id='my-form'  enctype='multipart/form-data'>
             
            
            
            <div class='form-group col-md-6'>
               <label for='control-label '>Name</label>
              <input type='text' class='form-control' name='name' id='name' value=\"{$row['Name']}\" pattern='([A-Za-z])+( [A-Za-z]+)' required />
            </div>  
            <div class='form-group col-md-6'>
               <label for='control-label '>Email</label>
         
              <input type='email' id='email' class='form-control' name='email' placeholder='' value=\"{$row['Email']}\" pattern='^([a-zA-Z0-9_\-\.]+)@([a-zA-Z\-\.]+)\.([a-zA-Z]{2,5})$' required/>
            </div> 
                  
             
             
             
            <div class='form-group col-md-6'>
               <label for='control-label '><span style='color:red'>*</span> Date of Birth</label>
                
                    <input type='date' class='form-control' name='dob' id='dob' value=\"{$row['DOB']}\" onblur='checkDate()'  required />
                </div>  
              <div class='form-group col-md-6'>
               <label for='control-label '>Profile Picture</label>
                        
                        <img src='uploads/{$row['Picture']}' class='form-img' alt='Profile Picture'>
                        <input type='file' name='profilepicture' id='profilepicture'>
               
                </div>
            
            <div class='form-group col-md-6'>
               <label for='control-label '><span style='color:red'>*</span> CNIC/Form-B</label>
               
                    <input type='text' id='cnic' class='form-control'  name='cnic' data-toggle='popover'  data-trigger='focus' value=\"{$row['CNIC_num']}\" pattern='^[0-9+]{5}-[0-9+]{7}-[0-9]{1}$' required />
                </div>

             <div class='col-md-12'>
             <div class='form-group col-md-6'>
                <label class='control-label '><span style='color:red'>*</span>CNIC/Form-B (Front)</label>
                <img src='uploads/{$row['CNIC_front']}' alt='No CNIC Uploaded' class='form-img'>
                
                <input type='file' class='form-control' name='cnicimg' style='padding:3px;'  id='picname' data-toggle='popover' data-trigger='focus' title='Important Message' data-content='Passport size picture is required. Otherwise your application may not be entertained' />
              <input type='hidden' class='form-control' name='ProfilePictureFileIsAlreadyUploaded' style='background-color:#983415;' />
                
                </div>
              
            
             
             <div class='form-group col-md-6'>
                <label class='control-label '><span style='color:red'>*</span>CNIC/Form-B (Back)</label>
                <img src='uploads/{$row['CNIC_back']}' alt='No CNIC Uploaded' class='form-img'>
                
                <input type='file' class='form-control' name='cnicimg_back' style='padding:3px;'  id='picname' data-toggle='popover' data-trigger='focus' title='Important Message' data-content='Passport size picture is required. Otherwise your application may not be entertained' />
              <input type='hidden' class='form-control' name='ProfilePictureFileIsAlreadyUploaded' style='background-color:#983415;' />
                
                </div>
              </div>
            
           <div class='form-group col-md-12'>
                  <label class='control-label '><span style='color:red'>*</span> Father's Name</label>
                
                    <input type='text' class='form-control' data-toggle='popover' data-trigger='focus' title='Important Message' data-content='The 'Father Name' fied is mandatory' name='fname' id='fname' value=\"{$row['Father_Name']}\" pattern='^[A-Za-z]{3,20}$' required  />
                </div>
              
                                
        
               <div class='form-group col-md-6'>
       <label class='control-label '>Gender</label>
      <select id='gender' class='form-control' name='gender'>
                                 <option value='Male' "; if ($row['Gender'] == 'Male') echo 'selected'; echo " >
                            Male
                        </option>
                        <option value='Female' "; if ($row['Gender'] == 'Female') echo 'selected'; echo ">                            Female
                        </option>
      </select>
    </div>                       
            
            
                
         <div class='form-group col-md-6'>
                  <label class='control-label ' for='mobile'><span style='color:red'>*</span> Mobile Number</label>
               
                    <input type='text' class='form-control' name='mobile' pattern='^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$'  id='mobile' required='required'  value=\"{$row['Mobile']}\"/>
                </div>
             
             
                                
               <div class='form-group col-md-6'>
                
                <label class='control-label '>Domicile</label>
               
                    <select name='domicile' id='domicile'>
                    
                    <option value='{$row['Domicile']}'>{$row['Domicile']}</option>
                    
                    <option value='Bagh' "; if ($row['Domicile'] == 'Male') echo 'selected'; echo " >
                                Bagh    
                        </option>
                                                <option value='Bhimber' "; if ($row['Domicile'] == 'Bhimber') echo 'selected '; echo " >
                                Bhimber    
                        </option>
                                                <option value='Hattian' "; if ($row['Domicile'] == 'Hattian') echo 'selected '; echo " >
                                Hattian    
                        </option>
                                                <option value='Haveli' "; if ($row['Domicile'] == 'Haveli') echo 'selected '; echo " >
                                Haveli    
                        </option>
                                                <option value='Kotli' "; if ($row['Domicile'] == 'Kotli') echo 'selected '; echo " >
                                Kotli    
                        </option>
                                                <option value='Mirpur' "; if ($row['Domicile'] == 'Mirpur') echo 'selected '; echo " >
                                Mirpur    
                        </option>
                                                <option value='Muzaffarabad' "; if ($row['Domicile'] == 'Muzaffarabad') echo 'selected'; echo " >
                                Muzaffarabad    
                        </option>
                                                <option value='Neelum' "; if ($row['Domicile'] == 'Neelum') echo 'selected'; echo " >
                                Neelum    
                        </option>
                                                <option value='Poonch' "; if ($row['Domicile'] == 'Poonch') echo 'selected'; echo " >
                                Poonch    
                        </option>
                                                <option value='Sudhnati' "; if ($row['Domicile'] == 'Sudhnati') echo 'selected'; echo " >
                                Sudhnati    
                        </option>
                                                <option value='Awaran' "; if ($row['Domicile'] == 'Awaran') echo 'selected'; echo " >
                                Awaran    
                        </option>
                                                <option value='Barkhan' "; if ($row['Domicile'] == 'Barkhan') echo 'selected'; echo " >
                                Barkhan    
                        </option>
                                                <option value='Bolan' "; if ($row['Domicile'] == 'Bolan') echo 'selected'; echo " >
                                Bolan    
                        </option>
                                                <option value='Chagai' "; if ($row['Domicile'] == 'Chagai') echo 'selected'; echo " >
                                Chagai    
                        </option>
                                                <option value='Dera Bugti' "; if ($row['Domicile'] == 'Dera Bugti') echo 'selected'; echo " >
                                Dera Bugti    
                        </option>
                                                <option value='Gwadar' "; if ($row['Domicile'] == 'Gwadar') echo 'selected'; echo " >
                                Gwadar    
                        </option>
                                                <option value='Harnai' "; if ($row['Domicile'] == 'Harnai') echo 'selected'; echo " >
                                Harnai    
                        </option>
                                                <option value='Jafarabad' "; if ($row['Domicile'] == 'Jafarabad') echo 'selected'; echo " >
                                Jafarabad    
                        </option>
                                                <option value='Jhal Magsi' "; if ($row['Domicile'] == 'Jhal Magsi') echo 'selected'; echo " >
                                Jhal Magsi    
                        </option>
                                                <option value='Kalat' "; if ($row['Domicile'] == 'Kalat') echo 'selected'; echo " >
                                Kalat    
                        </option>
                                                <option value='Kech Turbat' "; if ($row['Domicile'] == 'Kech Turbat') echo 'selected'; echo " >
                                Kech Turbat    
                        </option>
                                                <option value='Kharan' "; if ($row['Domicile'] == 'Kharan') echo 'selected'; echo " >
                                Kharan    
                        </option>
                                                <option value='Khuzdar' "; if ($row['Domicile'] == 'Khuzdar') echo 'selected'; echo " >
                                Khuzdar    
                        </option>
                                                <option value='Killa Abdullah' "; if ($row['Domicile'] == 'Killa Abdullah') echo 'selected'; echo " >
                                Killa Abdullah    
                        </option>
                                                <option value='Killa Saifullah' "; if ($row['Domicile'] == 'Killa Saifullah') echo 'selected'; echo " >
                                Killa Saifullah    
                        </option>
                                                <option value='Kohlu' "; if ($row['Domicile'] == 'Kohlu') echo 'selected'; echo " >
                                Kohlu    
                        </option>
                                                <option value='Lasbela' "; if ($row['Domicile'] == 'Lasbela') echo 'selected'; echo " >
                                Lasbela    
                        </option>
                                                <option value='Loralai' "; if ($row['Domicile'] == 'Loralai') echo 'selected'; echo " >
                                Loralai    
                        </option>
                                                <option value='Mastung' "; if ($row['Domicile'] == 'Mastung') echo 'selected'; echo " >
                                Mastung    
                        </option>
                                                <option value='Musakhel' "; if ($row['Domicile'] == 'Musakhel') echo 'selected'; echo " >
                                Musakhel    
                        </option>
                                                <option value='Nasirabad' "; if ($row['Domicile'] == 'Nasirabad') echo 'selected'; echo " >
                                Nasirabad    
                        </option>
                                                <option value='Nushki' "; if ($row['Domicile'] == 'Nushki') echo 'selected'; echo " >
                                Nushki    
                        </option>
                                                <option value='Panjgur' "; if ($row['Domicile'] == 'Panjgur') echo 'selected'; echo " >
                                Panjgur    
                        </option>
                                                <option value='Pishin' "; if ($row['Domicile'] == 'Pishin') echo 'selected'; echo " >
                                Pishin    
                        </option>
                                                <option value='Quetta' "; if ($row['Domicile'] == 'Quetta') echo 'selected'; echo " >
                                Quetta    
                        </option>
                                                <option value='Sherani' "; if ($row['Domicile'] == 'Sherani') echo 'selected'; echo " >
                                Sherani    
                        </option>
                                                <option value='Sibi' "; if ($row['Domicile'] == 'Sibi') echo 'selected'; echo " >
                                Sibi    
                        </option>
                                                <option value='Washuk' "; if ($row['Domicile'] == 'Washuk') echo 'selected'; echo " >
                                Washuk    
                        </option>
                                                <option value='Zhob' "; if ($row['Domicile'] == 'Zhob') echo 'selected'; echo " >
                                Zhob    
                        </option>
                                                <option value='Ziarat' "; if ($row['Domicile'] == 'Ziarat') echo 'selected'; echo " >
                                Ziarat    
                        </option>
                                                <option value='Bajaur' "; if ($row['Domicile'] == 'Bajaur') echo 'selected'; echo " >
                                Bajaur    
                        </option>
                                                <option value='F R Bannu' "; if ($row['Domicile'] == 'F R Bannu') echo 'selected'; echo " >
                                F R Bannu    
                        </option>
                                                <option value='F R Dera Ismail Khan'"; if ($row['Domicile'] == 'F R Dera Ismail Khan') echo 'selected'; echo " >
                                F R Dera Ismail Khan    
                        </option>
                                                <option value='F R Kohat'"; if ($row['Domicile'] == 'F R Kohat') echo 'selected'; echo " >
                                F R Kohat    
                        </option>
                                                <option value='F R Lakki Marwat'"; if ($row['Domicile'] == 'F R Lakki Marwat') echo 'selected'; echo " >
                                F R Lakki Marwat    
                        </option>
                                                <option value='F R Peshawar'"; if ($row['Domicile'] == 'F R Peshawar') echo 'selected'; echo " >
                                F R Peshawar    
                        </option>
                                                <option value='F R Tank'"; if ($row['Domicile'] == 'F R Tank') echo 'selected'; echo " >
                                F R Tank    
                        </option>
                                                <option value='Khyber'"; if ($row['Domicile'] == 'Khyber') echo 'selected'; echo " >
                                Khyber    
                        </option>
                                                <option value='Kurram'"; if ($row['Domicile'] == 'Kurram') echo 'selected'; echo " >
                                Kurram    
                        </option>
                                                <option value='Kurram Agency'"; if ($row['Domicile'] == 'Mohmand') echo 'selected'; echo " >
                                Kurram Agency    
                        </option>
                                                <option value='Mohmand'"; if ($row['Domicile'] == 'Ziarat') echo 'selected'; echo " >
                                Mohmand    
                        </option>
                                                <option value='North Waziristan'"; if ($row['Domicile'] == 'North Waziristan') echo 'selected'; echo " >
                                North Waziristan    
                        </option>
                                                <option value='Orakzai'"; if ($row['Domicile'] == 'Orakzai') echo 'selected'; echo " >
                                Orakzai    
                        </option>
                                                <option value='South Waziristan'"; if ($row['Domicile'] == 'South Waziristan') echo 'selected'; echo " >
                                South Waziristan    
                        </option>
                                                <option value='Astore'"; if ($row['Domicile'] == 'Astore') echo 'selected'; echo " >
                                Astore    
                        </option>
                                                <option value='Diamer'"; if ($row['Domicile'] == 'Diamer') echo 'selected'; echo " >
                                Diamer    
                        </option>
                                                <option value='Ghanche'"; if ($row['Domicile'] == 'Ghanche') echo 'selected'; echo " >
                                Ghanche    
                        </option>
                                                <option value='Ghizer'"; if ($row['Domicile'] == 'Ghizer') echo 'selected'; echo " >
                                Ghizer    
                        </option>
                                                <option value='Gilgit'"; if ($row['Domicile'] == 'Gilgit') echo 'selected'; echo " >
                                Gilgit    
                        </option>
                                                <option value='Hunza-Nagar'"; if ($row['Domicile'] == 'Hunza-Nagar') echo 'selected'; echo " >
                                Hunza-Nagar    
                        </option>
                                                <option value='Skardu'"; if ($row['Domicile'] == 'Skardu') echo 'selected'; echo " >
                                Skardu    
                        </option>
                                                <option value='Islamabad'"; if ($row['Domicile'] == 'Islamabad') echo 'selected'; echo " >
                                Islamabad    
                        </option>
                                                <option value='Abbottabad'"; if ($row['Domicile'] == 'Abbottabad') echo 'selected'; echo " >
                                Abbottabad    
                        </option>
                                                <option value='Bannu'"; if ($row['Domicile'] == 'Bannu') echo 'selected'; echo " >
                                Bannu    
                        </option>
                                                <option value='Battagram'"; if ($row['Domicile'] == 'Battagram') echo 'selected'; echo " >
                                Battagram    
                        </option>
                                                <option value='Buner'"; if ($row['Domicile'] == 'Buner') echo 'selected'; echo " >
                                Buner    
                        </option>
                                                <option value='Charsadda'"; if ($row['Domicile'] == 'Charsadda') echo 'selected'; echo " >
                                Charsadda    
                        </option>
                                                <option value='Chitral'"; if ($row['Domicile'] == 'Chitral') echo 'selected'; echo " >
                                Chitral    
                        </option>
                                                <option value='Dera Ismail Khan'"; if ($row['Domicile'] == 'Dera Ismail Khan') echo 'selected'; echo " >
                                Dera Ismail Khan    
                        </option>
                                                <option value='Haripur'"; if ($row['Domicile'] == 'Haripur') echo 'selected'; echo " >
                                Hangu    
                        </option>
                                                <option value='Haripur'"; if ($row['Domicile'] == 'Haripur') echo 'selected'; echo " >
                                Haripur    
                        </option>
                                                <option value='Kala Dhaka'"; if ($row['Domicile'] == 'Kala Dhaka') echo 'selected'; echo " >
                                Kala Dhaka    
                        </option>
                                                <option value='Karak'"; if ($row['Domicile'] == 'Karak') echo 'selected'; echo " >
                                Karak    
                        </option>
                                                <option value='Kohat'"; if ($row['Domicile'] == 'Kohat') echo 'selected'; echo " >
                                Kohat    
                        </option>
                                                <option value='Kohistan'"; if ($row['Domicile'] == 'Kohistan') echo 'selected'; echo " >
                                Kohistan    
                        </option>
                                                <option value='Lakki Marwat'"; if ($row['Domicile'] == 'Lakki Marwat') echo 'selected'; echo " >
                                Lakki Marwat    
                        </option>
                                                <option value='Lower Dir'"; if ($row['Domicile'] == 'Lower Dir') echo 'selected'; echo " >
                                Lower Dir    
                        </option>
                                                <option value='Malakand'"; if ($row['Domicile'] == 'Malakand') echo 'selected'; echo " >
                                Malakand    
                        </option>
                                                <option value='Mansehra'"; if ($row['Domicile'] == 'Mansehra') echo 'selected'; echo " >
                                Mansehra    
                        </option>
                                                <option value='Mardan'"; if ($row['Domicile'] == 'Mardan') echo 'selected'; echo " >
                                Mardan    
                        </option>
                                                <option value='Nowshera'"; if ($row['Domicile'] == 'Nowshera') echo 'selected'; echo " >
                                Nowshera    
                        </option>
                                                <option value='Peshawar'"; if ($row['Domicile'] == 'Peshawar') echo 'selected'; echo " >
                                Peshawar    
                        </option>
                                                <option value='Shangla'"; if ($row['Domicile'] == 'Shangla') echo 'selected'; echo " >
                                Shangla    
                        </option>
                                                <option value='Swabi'"; if ($row['Domicile'] == 'Swabi') echo 'selected'; echo " >
                                Swabi    
                        </option>
                                                <option value='Swat'"; if ($row['Domicile'] == 'Swat') echo 'selected'; echo " >
                                Swat    
                        </option>
                                                <option value='Tank'"; if ($row['Domicile'] == 'Tank') echo 'selected'; echo " >
                                Tank    
                        </option>
                                                <option value='Upper Dir'"; if ($row['Domicile'] == 'Upper Dir') echo 'selected'; echo " >
                                Upper Dir    
                        </option>
                                                <option value='Attock'"; if ($row['Domicile'] == 'Attock') echo 'selected'; echo " >
                                Attock    
                        </option>
                                                <option value='Bahawalnagar'"; if ($row['Domicile'] == 'Bahawalnagar') echo 'selected'; echo " >
                                Bahawalnagar    
                        </option>
                                                <option value='Bahawalpur'"; if ($row['Domicile'] == 'Bahawalpur') echo 'selected'; echo " >
                                Bahawalpur    
                        </option>
                                                <option value='Bhakkar'"; if ($row['Domicile'] == 'Bhakkar') echo 'selected'; echo " >
                                Bhakkar    
                        </option>
                                                <option value='Chakwal'"; if ($row['Domicile'] == 'Chakwal') echo 'selected'; echo " >
                                Chakwal    
                        </option>
                                                <option value='Chiniot'"; if ($row['Domicile'] == 'Chiniot') echo 'selected'; echo " >
                                Chiniot    
                        </option>
                                                <option value='Dera Ghazi Khan'"; if ($row['Domicile'] == 'Dera Ghazi Khan') echo 'selected'; echo " >
                                Dera Ghazi Khan    
                        </option>
                                                <option value='Faisalabad'"; if ($row['Domicile'] == 'Faisalabad') echo 'selected'; echo " >
                                Faisalabad    
                        </option>
                                                <option value='Gujranwala'"; if ($row['Domicile'] == 'Gujranwala') echo 'selected'; echo " >
                                Gujranwala    
                        </option>
                                                <option value='Gujrat'"; if ($row['Domicile'] == 'Gujrat') echo 'selected'; echo " >
                                Gujrat    
                        </option>
                                                <option value='Hafizabad'"; if ($row['Domicile'] == 'Hafizabad') echo 'selected'; echo " >
                                Hafizabad    
                        </option>
                                                <option value='Jhang'"; if ($row['Domicile'] == 'Jhang') echo 'selected'; echo " >
                                Jhang    
                        </option>
                                                <option value='Jhelum'"; if ($row['Domicile'] == 'Jhelum') echo 'selected'; echo " >
                                Jhelum    
                        </option>
                                                <option value='Kasur'"; if ($row['Domicile'] == 'Kasur') echo 'selected'; echo " >
                                Kasur    
                        </option>
                                                <option value='Khanewal'"; if ($row['Domicile'] == 'Khanewal') echo 'selected'; echo " >
                                Khanewal    
                        </option>
                                                <option value='Khushab'"; if ($row['Domicile'] == 'Khushab') echo 'selected'; echo " >
                                Khushab    
                        </option>
                                                <option value='Lahore'"; if ($row['Domicile'] == 'Lahore') echo 'selected'; echo " >
                                Lahore    
                        </option>
                                                <option value='Layyah'"; if ($row['Domicile'] == 'Layyah') echo 'selected'; echo " >
                                Layyah    
                        </option>
                                                <option value='Lodhran'"; if ($row['Domicile'] == 'Ziarat') echo 'selected'; echo " >
                                Lodhran    
                        </option>
                                                <option value='Mandi Bahauddin'"; if ($row['Domicile'] == 'Mandi Bahauddin') echo 'selected'; echo " >
                                Mandi Bahauddin    
                        </option>
                                                <option value='Mianwali'"; if ($row['Domicile'] == 'Mianwali') echo 'selected'; echo " >
                                Mianwali    
                        </option>
                                                <option value='Multan'"; if ($row['Domicile'] == 'Multan') echo 'selected'; echo " >
                                Multan    
                        </option>
                                                <option value='Muzaffargarh'"; if ($row['Domicile'] == 'Muzaffargarh') echo 'selected'; echo " >
                                Muzaffargarh    
                        </option>
                                                <option value='Nankana Sahib'"; if ($row['Domicile'] == 'Nankana Sahib') echo 'selected'; echo " >
                                Nankana Sahib    
                        </option>
                                                <option value='Narowal'"; if ($row['Domicile'] == 'Narowal') echo 'selected'; echo " >
                                Narowal    
                        </option>
                                                <option value='Okara'"; if ($row['Domicile'] == 'Okara') echo 'selected'; echo " >
                                Okara    
                        </option>
                                                <option value='Pakpattan'"; if ($row['Domicile'] == 'Pakpattan') echo 'selected'; echo " >
                                Pakpattan    
                        </option>
                                                <option value='Rahim Yar Khan'"; if ($row['Domicile'] == 'Rahim Yar Khan') echo 'selected'; echo " >
                                Rahim Yar Khan    
                        </option>
                                                <option value='Rajanpur'"; if ($row['Domicile'] == 'Rajanpur') echo 'selected'; echo " >
                                Rajanpur    
                        </option>
                                                <option value='Rawalpindi'"; if ($row['Domicile'] == 'Rawalpindi') echo 'selected'; echo " >
                                Rawalpindi    
                        </option>
                                                <option value='Sahiwal'"; if ($row['Domicile'] == 'Sahiwal') echo 'selected'; echo " >
                                Sahiwal    
                        </option>
                                                <option value='Sargodha'"; if ($row['Domicile'] == 'Sargodha') echo 'selected'; echo " >
                                Sargodha    
                        </option>
                                                <option value='Sheikhupura'"; if ($row['Domicile'] == 'Sheikhupura') echo 'selected'; echo " >
                                Sheikhupura    
                        </option>
                                                <option value='Sialkot'"; if ($row['Domicile'] == 'Sialkot') echo 'selected'; echo " >
                                Sialkot    
                        </option>
                                                <option value='Toba Tek Singh'"; if ($row['Domicile'] == 'Toba Tek Singh') echo 'selected'; echo " >
                                Toba Tek Singh    
                        </option>
                                                <option value='Vehari' "; if ($row['Domicile'] == 'Vehari') echo 'selected'; echo " >
                                Vehari    
                        </option>
                                                <option value='Badin'"; if ($row['Domicile'] == 'Badin') echo 'selected'; echo " >
                                Badin    
                        </option>
                                                <option value='Dadu'"; if ($row['Domicile'] == 'Dadu') echo 'selected'; echo " >
                                Dadu    
                        </option>
                                                <option value='Ghotki'"; if ($row['Domicile'] == 'Ghotki') echo 'selected'; echo " >
                                Ghotki    
                        </option>
                                                <option value='Hyderabad'"; if ($row['Domicile'] == 'Hyderabad') echo 'selected'; echo " >
                                Hyderabad    
                        </option>
                                                <option value='Jacobabad'"; if ($row['Domicile'] == 'Jacobabad') echo 'selected'; echo " >
                                Jacobabad    
                        </option>
                                                <option value='Jamshoro'"; if ($row['Domicile'] == 'Jamshoro') echo 'selected'; echo " >
                                Jamshoro    
                        </option>
                                                <option value='Karachi'"; if ($row['Domicile'] == 'Karachi') echo 'selected'; echo " >
                                Karachi    
                        </option>
                                                <option value='Kashmore'"; if ($row['Domicile'] == 'Kashmore') echo 'selected'; echo " >
                                Kashmore    
                        </option>
                                                <option value='Khairpur'"; if ($row['Domicile'] == 'Khairpur') echo 'selected'; echo " >
                                Khairpur    
                        </option>
                                                <option value='Larkana'"; if ($row['Domicile'] == 'Larkana') echo 'selected'; echo " >
                                Larkana    
                        </option>
                                                <option value='Matiari'"; if ($row['Domicile'] == 'Matiari') echo 'selected'; echo " >
                                Matiari    
                        </option>
                                                <option value='Mirpurkhas'"; if ($row['Domicile'] == 'Mirpurkhas') echo 'selected'; echo " >
                                Mirpurkhas    
                        </option>
                                                <option value='Naushahro Firoze'"; if ($row['Domicile'] == 'Naushahro Firoze') echo 'selected'; echo " >
                                Naushahro Firoze    
                        </option>
                                                <option value='Nawabshah'"; if ($row['Domicile'] == 'Nawabshah') echo 'selected'; echo " >
                                Nawabshah    
                        </option>
                                                <option value='Qambar Shahdadkot'"; if ($row['Domicile'] == 'Qambar Shahdadkot') echo 'selected'; echo " >
                                Qambar Shahdadkot    
                        </option>
                                                <option value='Sanghar'"; if ($row['Domicile'] == 'Sanghar') echo 'selected'; echo " >
                                Sanghar    
                        </option>
                                                <option value='Shikarpur'"; if ($row['Domicile'] == 'Shikarpur') echo 'selected'; echo " >
                                Shikarpur    
                        </option>
                                                <option value='Sukkur'"; if ($row['Domicile'] == 'Sukkur') echo 'selected'; echo " >
                                Sukkur    
                        </option>
                                                <option value='Tando Allahyar'"; if ($row['Domicile'] == 'Tando Allahyar') echo 'selected'; echo " >
                                Tando Allahyar    
                        </option>
                                                <option value='Tando Muhammad Khan'"; if ($row['Domicile'] == 'Tando Muhammad Khan') echo 'selected'; echo " >
                                Tando Muhammad Khan    
                        </option>
                                                <option value='Tharparkar'"; if ($row['Domicile'] == 'Tharparkar') echo 'selected'; echo " >
                                Tharparkar    
                        </option>
                                                <option value='Thatta'"; if ($row['Domicile'] == 'Thatta') echo 'selected'; echo " >
                                Thatta    
                        </option>
                                                <option value='Umerkot'"; if ($row['Domicile'] == 'Umerkot') echo 'selected'; echo " >
                                Umerkot    
                        </option>
                        <option value='0'>Other</option>
                                            </select>
                </div>             
                
            
             <div class='form-group col-md-6'>
                
                <label class='control-label '>Religion</label>
               
                    <select name='religion' id='religion'>

                    <option value='{$row['Religion']}'>{$row['Religion']}</option>
                                                <option value='Islam'"; if($row['Religion'] == 'Islam') echo 'selected '; echo " >
                                Islam    
                        </option>
                                                <option value='Christianity'"; if($row['Religion'] == 'Christianity') echo 'selected'; echo " >
                                Christianity    
                        </option>
                                                <option value='Hinduism'"; if ($row['Religion'] == 'Hinduism') echo 'selected'; echo " >
                                Hinduism    
                        </option>
                                                <option value='Sikhism'"; if ($row['Religion'] == 'Sikhism') echo 'selected'; echo " >
                                Sikhism    
                        </option>
                                                <option value='Bahai Faith'"; if ($row['Religion'] == 'Bahai Faith') echo 'selected'; echo " >
                                Bahai Faith    
                        </option>
                                                <option value='Buddhism'"; if ($row['Religion'] == 'Buddhism') echo 'selected'; echo " >
                                Buddhism    
                        </option>
                                                <option value='Judaism'"; if ($row['Religion'] == 'Judaism') echo 'selected'; echo " >
                                Judaism    
                        </option>
                        <option value='0'>Other</option>
                                            </select>
                </div>

               

                <div class='form-group col-md-6 edit-domicile' style='display:none;'>
                <label class='control-label ' for='mobile'><span style='color:red'>*</span> Domicile</label>
             
                  <input type='text' class='form-control' name='custom-domicile' id='edit-domicile' value=\"{$row['Domicile']}\"/>
              </div>

              <div class='form-group col-md-6 edit-religion' style='display:none;'>
              <label class='control-label ' for='religion'><span style='color:red'>*</span> Religion</label>
           
                <input type='text' class='form-control' name='custom-religion' id='edit-religion' value=\"{$row['Religion']}\"/>
            </div>

            
             <div class='form-group col-md-6'>
               
                 <input type='submit' style='background-color:green;color:white' class='btn' name='submit' value='Save and Continue' id='text' >                       
                </div>
                <input type='hidden' name='reg_id' value='{$id}'>
                <input type='hidden' name='submit' value='{$id}'>
          </form>                      <div class='col-md-12' id='form-msg'></div>
          
          <div class='col-md-12' id='form-msg'></div>
      </div>
      </div>
    </div></div></div>
        </div></div></div>
        <script>
                $('#domicile').change(function(){
                        var id = $(this).val();
                        if(id == 0){
                        $('.edit-domicile').css('display','block');
                        } 
                        else{
                        $('.edit-domicile').css('display','none');            
                        }
                });
                
        </script>
        <script>
        $('#religion').change(function(){
                var id = $(this).val();
                if(id == 0){
                $('.edit-religion').css('display','block');
                } 
                else{
                $('.edit-religion').css('display','none');            
                }
        });
        </script>
        ";
    }
    else if ($code == 2)
    {
                $result = mysqli_query($connection, "SELECT * FROM tbl_address where CID = '$id'") or die(mysqli_error($connection)); 
                $row = mysqli_fetch_array($result);
        echo "
        <div class='modal fade' id='myModal1' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
          <h3 style='text-align:center;color:white;'><Strong>Contact Details </Strong></h3>
          <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
          </div>
      <div class='panel-body' style='background-color:#ab9f9b24;'>
        <form  id='my-form' method = 'post'  enctype='multipart/form-data' >
                    
        <!-- Main content -->
        <section class='content'>
            
            
        <div class='row'>
            <div class='col-xs-12'>
              <!-- Widget: user widget -->
            <div class='box box-widget widget-user'>
             
                <div class='box-footer'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='box-body table-responsive'>
                                
            <fieldset>
                <legend></legend>       
                <div class='form-group'>
                        <div class='row'>
                          <label class='control-label col-sm-2'><span style='color:red'>*</span>Address</label>
                          <div class='col-sm-5'>
                              <textarea name='Permanent_Address' id='Permanent_Address' class='form-control' required=''>".$row['Perma_Address']."</textarea>
                          </div>
                        </div>

                       <div class='row'>
                          <label class='control-label col-sm-2'><span style='color:red'>*</span>City</label>
                          <div class='col-sm-5'>
                              <select name='Permanent_City' id='Permanent_City' required>
                             
                              <option value='{$row['Perma_City']}'>{$row['Perma_City']}</option>  
                              <option value='Abbottabad'"; if ($row['Perma_City'] == 'Abbottabad') echo 'selected'; echo " >
                              Abbottabad    
                      </option>
                                                      <option value='Bannu'"; if ($row['Perma_City'] == 'Bannu') echo 'selected'; echo " >
                              Bannu    
                      </option>
                                                       <option value='Battagram'"; if ($row['Perma_City'] == 'Battagram') echo 'selected'; echo " >
                              Battagram    
                      </option>
                                                        <option value='Buner'"; if ($row['Perma_City'] == 'Buner') echo 'selected'; echo " >
                              Buner    
                      </option>
                                                        <option value='Charsadda'"; if ($row['Perma_City'] == 'Charsadda') echo 'selected'; echo " >
                              Charsadda    
                      </option>
                                                        <option value='Chitral'"; if ($row['Perma_City'] == 'Chitral') echo 'selected'; echo " >
                              Chitral    
                      </option>
                                                        <option value='Dera Ismail Khan'"; if ($row['Perma_City'] == 'Dera Ismail Khan') echo 'selected'; echo " >
                              Dera Ismail Khan    
                      </option>
                                                        <option value='Hangu'"; if ($row['Perma_City'] == 'Hangu') echo 'selected'; echo " >
                              Hangu    
                      </option>
                                                        <option value='Haripur'"; if ($row['Perma_City'] == 'Haripur') echo 'selected'; echo " >
                              Haripur    
                      </option>
                                                        <option value='Kala Dhaka'"; if ($row['Perma_City'] == 'Kala Dhaka') echo 'selected'; echo " >
                              Kala Dhaka    
                      </option>
                                                        <option value='Karak'"; if ($row['Perma_City'] == 'Karak') echo 'selected'; echo " >
                              Karak    
                      </option>
                                                        <option value='Kohat'"; if ($row['Perma_City'] == 'Kohat') echo 'selected'; echo " >
                              Kohat    
                      </option>
                                                        <option value='Kohistan'"; if ($row['Perma_City'] == 'Kohistan') echo 'selected'; echo " >
                              Kohistan    
                      </option>
                                                        <option value='Lakki Marwat'"; if ($row['Perma_City'] == 'Lakki Marwat') echo 'selected'; echo " >
                              Lakki Marwat    
                      </option>
                                                        <option value='Lower Dir'"; if ($row['Perma_City'] == 'Lower Dir') echo 'selected'; echo " >
                              Lower Dir    
                      </option>
                                                        <option value='Mansehra'"; if ($row['Perma_City'] == 'Mansehra') echo 'selected'; echo " >
                              Mansehra    
                      </option>
                                                        <option value='Malakand'"; if ($row['Perma_City'] == 'Malakand') echo 'selected'; echo " >
                              Malakand    
                      </option>
                                                        <option value='Mardan'"; if ($row['Perma_City'] == 'Mardan') echo 'selected'; echo " >
                              Mardan    
                      </option>
                                                        <option value='Nowshera'"; if ($row['Perma_City'] == 'Nowshera') echo 'selected'; echo " >
                              Nowshera    
                      </option>
                                                        <option value='Peshawar'"; if ($row['Perma_City'] == 'Peshawar') echo 'selected'; echo " >
                              Peshawar    
                      </option>
                                                        <option value='Shangla'"; if ($row['Perma_City'] == 'Shangla') echo 'selected'; echo " >
                              Shangla    
                      </option>
                                                        <option value='Swabi'"; if ($row['Perma_City'] == 'Swabi') echo 'selected'; echo " >
                              Swabi    
                      </option>
                                                        <option value='Swat'"; if ($row['Perma_City'] == 'Swat') echo 'selected'; echo " >
                              Swat    
                      </option>
                                                        <option value='Tank'"; if ($row['Perma_City'] == 'Tank') echo 'selected'; echo " >
                              Tank    
                      </option>
                                                        <option value='Upper Dir'"; if ($row['Perma_City'] == 'Upper Dir') echo 'selected'; echo " >
                              Upper Dir    
                      </option>
                                                        <option value='Bajaur'"; if ($row['Perma_City'] == 'Bajaur') echo 'selected'; echo " >
                              Bajaur    
                      </option>
                                                        <option value='Khyber'"; if ($row['Perma_City'] == 'Khyber') echo 'selected'; echo " >
                              Khyber    
                      </option>
                                                        <option value='Kurram'"; if ($row['Perma_City'] == 'Kurram') echo 'selected'; echo " >
                              Kurram    
                      </option>
                                                        <option value='Mohmand'"; if ($row['Perma_City'] == 'Mohmand') echo 'selected'; echo " >
                              Mohmand    
                      </option>
                                                        <option value='North Waziristan'"; if ($row['Perma_City'] == 'North Waziristan') echo 'selected'; echo " >
                              North Waziristan    
                      </option>
                                                        <option value='Orakzai'"; if ($row['Perma_City'] == 'Orakzai') echo 'selected'; echo " >
                              Orakzai    
                      </option>
                                                        <option value='South Waziristan'"; if ($row['Perma_City'] == 'South Waziristan') echo 'selected'; echo " >
                              South Waziristan    
                      </option>
                                                        <option value='F R Bannu'"; if ($row['Perma_City'] == 'F R Bannu') echo 'selected'; echo " >
                              F R Bannu    
                      </option>
                                                        <option value='F R Dera Ismail Khan'"; if ($row['Perma_City'] == 'F R Dera Ismail Khan') echo 'selected'; echo " >
                              F R Dera Ismail Khan    
                      </option>
                                                        <option value='F R Kohat'"; if ($row['Perma_City'] == 'F R Kohat') echo 'selected'; echo " >
                              F R Kohat    
                      </option>
                                                       <option value='F R Lakki Marwat '"; if ($row['Perma_City'] == 'F R Lakki Marwat ') echo 'selected'; echo " >
                              F R Lakki Marwat    
                      </option>
                                                        <option value='F R Peshawar'"; if ($row['Perma_City'] == 'F R Peshawar') echo 'selected'; echo " >
                              F R Peshawar    
                      </option>
                                                        <option value='F R Tank'"; if ($row['Perma_City'] == 'F R Tank') echo 'selected'; echo " >
                              F R Tank    
                      </option>
                                                       <option value='Islamabad'"; if ($row['Perma_City'] == 'Islamabad') echo 'selected'; echo " >
                              Islamabad    
                      </option>
                                                        <option value='Attock'"; if ($row['Perma_City'] == 'Attock') echo 'selected'; echo " >
                              Attock    
                      </option>
                                                        <option value='Bahawalnagar'"; if ($row['Perma_City'] == 'Bahawalnagar') echo 'selected'; echo " >
                              Bahawalnagar    
                      </option>
                                                        <option value='Bahawalpur'"; if ($row['Perma_City'] == 'Bahawalpur') echo 'selected'; echo " >
                              Bahawalpur    
                      </option>
                                                        <option value='Bhakkar'"; if ($row['Perma_City'] == 'Bhakkar') echo 'selected'; echo " >
                              Bhakkar    
                      </option>
                                                       <option value='Chakwal'"; if ($row['Perma_City'] == 'Chakwal') echo 'selected'; echo " >
                              Chakwal    
                      </option>
                                                        <option value='Chiniot'"; if ($row['Perma_City'] == 'Chiniot') echo 'selected'; echo " >
                              Chiniot    
                      </option>
                                                        <option value='Dera Ghazi Khan'"; if ($row['Perma_City'] == 'Dera Ghazi Khan') echo 'selected'; echo " >
                              Dera Ghazi Khan    
                      </option>
                                                        <option value='Faisalabad'"; if ($row['Perma_City'] == 'Faisalabad') echo 'selected'; echo " >
                              Faisalabad    
                      </option>
                                                        <option value='Gujranwala'"; if ($row['Perma_City'] == 'Gujranwala') echo 'selected'; echo " >
                              Gujranwala    
                      </option>
                                                       <option value='Gujrat'"; if ($row['Perma_City'] == 'Gujrat') echo 'selected'; echo " >
                              Gujrat    
                      </option>
                                                        <option value='Hafizabad'"; if ($row['Perma_City'] == 'Hafizabad') echo 'selected'; echo " >
                              Hafizabad    
                      </option>
                                                       <option value='Jhang'"; if ($row['Perma_City'] == 'Jhang') echo 'selected'; echo " >
                              Jhang    
                      </option>
                                                        <option value='Jhelum'"; if ($row['Perma_City'] == 'Jhelum') echo 'selected'; echo " >
                              Jhelum    
                      </option>
                                                        <option value='Kasur'"; if ($row['Perma_City'] == 'Kasur') echo 'selected'; echo " >
                              Kasur    
                      </option>
                                                        <option value='Khanewal'"; if ($row['Perma_City'] == 'Khanewal') echo 'selected'; echo " >
                              Khanewal    
                      </option>
                                                        <option value='Khushab'"; if ($row['Perma_City'] == 'Khushab') echo 'selected'; echo " >
                              Khushab    
                      </option>
                                                        <option value='Lahore'"; if ($row['Perma_City'] == 'Lahore') echo 'selected'; echo " >
                              Lahore    
                      </option>
                                                        <option value='Layyah'"; if ($row['Perma_City'] == 'Layyah') echo 'selected'; echo " >
                              Layyah    
                      </option>
                                                        <option value='Lodhran'"; if ($row['Perma_City'] == 'Lodhran') echo 'selected'; echo " >
                              Lodhran    
                      </option>
                                                        <option value='Mandi Bahauddin'"; if ($row['Perma_City'] == 'Mandi Bahauddin') echo 'selected'; echo " >
                              Mandi Bahauddin    
                      </option>
                                                        <option value='Mianwali'"; if ($row['Perma_City'] == 'Mianwali') echo 'selected'; echo " >
                              Mianwali    
                      </option>
                                                        <option value='Multan'"; if ($row['Perma_City'] == 'Multan') echo 'selected'; echo " >
                              Multan    
                      </option>
                                                       <option value='Muzaffargarh'"; if ($row['Perma_City'] == 'Muzaffargarh') echo 'selected'; echo " >
                              Muzaffargarh    
                      </option>
                                                        <option value='Narowal'"; if ($row['Perma_City'] == 'Narowal') echo 'selected'; echo " >
                              Narowal    
                      </option>
                                                        <option value='Nankana Sahib'"; if ($row['Perma_City'] == 'Nankana Sahib') echo 'selected'; echo " >
                              Nankana Sahib    
                      </option>
                                                        <option value='Okara'"; if ($row['Perma_City'] == 'Okara') echo 'selected'; echo " >
                              Okara    
                      </option>
                                                        <option value='Pakpattan'"; if ($row['Perma_City'] == 'Pakpattan') echo 'selected'; echo " >
                              Pakpattan    
                      </option>
                                                        <option value='Rahim Yar Khan'"; if ($row['Perma_City'] == 'Rahim Yar Khan') echo 'selected'; echo " >
                              Rahim Yar Khan    
                      </option>
                                                        <option value='Rajanpur'"; if ($row['Perma_City'] == 'Rajanpur') echo 'selected'; echo " >
                              Rajanpur    
                      </option>
                                                        <option value='Rawalpindi'"; if ($row['Perma_City'] == 'Rawalpindi') echo 'selected'; echo " >
                              Rawalpindi    
                      </option>
                                                        <option value='Sahiwal'"; if ($row['Perma_City'] == 'Sahiwal') echo 'selected'; echo " >
                              Sahiwal    
                      </option>
                                                        <option value='Sargodha'"; if ($row['Perma_City'] == 'Sargodha') echo 'selected'; echo " >
                              Sargodha    
                      </option>
                                                        <option value='Sheikhupura'"; if ($row['Perma_City'] == 'Sheikhupura') echo 'selected'; echo " >
                              Sheikhupura    
                      </option>
                                                        <option value='Sialkot'"; if ($row['Perma_City'] == 'Sialkot') echo 'selected'; echo " >
                              Sialkot    
                      </option>
                                <option value='Toba Tek Singh'"; if ($row['Perma_City'] == 'Toba Tek Singh') echo 'selected'; echo " >
                              Toba Tek Singh    
                      </option>
                                                        <option value='Vehari'"; if ($row['Perma_City'] == 'Vehari') echo 'selected'; echo " >
                              Vehari    
                      </option>
                                                        <option value='Badin'"; if ($row['Perma_City'] == 'Badin') echo 'selected'; echo " >
                              Badin    
                      </option>
                                                        <option value='Dadu'"; if ($row['Perma_City'] == 'Dadu') echo 'selected'; echo " >
                              Dadu    
                      </option>
                                                        <option value='Ghotki'"; if ($row['Perma_City'] == 'Ghotki') echo 'selected'; echo " >
                              Ghotki    
                      </option>
                                                        <option value='Hyderabad'"; if ($row['Perma_City'] == 'Hyderabad') echo 'selected'; echo " >
                              Hyderabad    
                      </option>
                                                       <option value='Jacobabad'"; if ($row['Perma_City'] == 'Jacobabad') echo 'selected'; echo " >
                              Jacobabad    
                      </option>
                                                        <option value='Jamshoro'"; if ($row['Perma_City'] == 'Jamshoro') echo 'selected'; echo " >
                              Jamshoro    
                      </option>
                                                        <option value='Karachi'"; if ($row['Perma_City'] == 'Karachi') echo 'selected'; echo " >
                              Karachi    
                      </option>
                                                        <option value='Kashmore'"; if ($row['Perma_City'] == 'Kashmore') echo 'selected'; echo " >
                              Kashmore    
                      </option>
                                                        <option value='Khairpur'"; if ($row['Perma_City'] == 'Khairpur') echo 'selected'; echo " >
                              Khairpur    
                      </option>
                                                        <option value='Larkana'"; if ($row['Perma_City'] == 'Larkana') echo 'selected'; echo " >
                              Larkana    
                      </option>
                                                        <option value='Matiari'"; if ($row['Perma_City'] == 'Matiari') echo 'selected'; echo " >
                              Matiari    
                      </option>
                                                        <option value='Mirpurkhas'"; if ($row['Perma_City'] == 'Mirpurkhas') echo 'selected'; echo " >
                              Mirpurkhas    
                      </option>
                                                        <option value='Naushahro Firoze'"; if ($row['Perma_City'] == 'Naushahro Firoze') echo 'selected'; echo " >
                              Naushahro Firoze    
                      </option>
                                                        <option value='Nawabshah'"; if ($row['Perma_City'] == 'Nawabshah') echo 'selected'; echo " >
                              Nawabshah    
                      </option>
                                                        <option value='Qambar Shahdadkot'"; if ($row['Perma_City'] == 'Qambar Shahdadkot') echo 'selected'; echo " >
                              Qambar Shahdadkot    
                      </option>
                                                        <option value='Sanghar'"; if ($row['Perma_City'] == 'Sanghar') echo 'selected'; echo " >
                              Sanghar    
                      </option>
                                                        <option value='Shikarpur'"; if ($row['Perma_City'] == 'Shikarpur') echo 'selected'; echo " >
                              Shikarpur    
                      </option>
                                                        <option value='Sukkur'"; if ($row['Perma_City'] == 'Sukkur') echo 'selected'; echo " >
                              Sukkur    
                      </option>
                                                        <option value='Tando Allahyar'"; if ($row['Perma_City'] == 'Tando Allahyar') echo 'selected'; echo " >
                              Tando Allahyar    
                      </option>
                                                        <option value='Tando Muhammad Khan'"; if ($row['Perma_City'] == 'Tando Muhammad Khan') echo 'selected'; echo " >
                              Tando Muhammad Khan    
                      </option>
                                                        <option value='Tharparkar'"; if ($row['Perma_City'] == 'Tharparkar') echo 'selected'; echo " >
                              Tharparkar    
                      </option>
                                                        <option value='Thatta'"; if ($row['Perma_City'] == 'Thatta') echo 'selected'; echo " >
                              Thatta    
                      </option>
                                                        <option value='Umerkot'"; if ($row['Perma_City'] == 'Umerkot') echo 'selected'; echo " >
                              Umerkot    
                      </option>
                                                        <option value='Muzaffarabad'"; if ($row['Perma_City'] == 'Muzaffarabad') echo 'selected'; echo " >
                              Muzaffarabad    
                      </option>
                                                        <option value='Hattian'"; if ($row['Perma_City'] == 'Hattian') echo 'selected'; echo " >
                              Hattian    
                      </option>
                                                        <option value='Neelum'"; if ($row['Perma_City'] == 'Neelum') echo 'selected'; echo " >
                              Neelum    
                      </option>
                                                        <option value='Mirpur'"; if ($row['Perma_City'] == 'Mirpur') echo 'selected'; echo " >
                              Mirpur    
                      </option>
                                                       <option value='Bhimber'"; if ($row['Perma_City'] == 'Bhimber') echo 'selected'; echo " >
                              Bhimber    
                      </option>
                                                       <option value='Kotli'"; if ($row['Perma_City'] == 'Kotli') echo 'selected'; echo " >
                              Kotli    
                      </option>
                                                       <option value='Poonch'"; if ($row['Perma_City'] == 'Poonch') echo 'selected'; echo " >
                              Poonch    
                      </option>
                                                        <option value='Bagh'"; if ($row['Perma_City'] == 'Bagh') echo 'selected'; echo " >
                              Bagh    
                      </option>
                                                        <option value='Haveli'"; if ($row['Perma_City'] == 'Haveli') echo 'selected'; echo " >
                              Haveli    
                      </option>
                                                        <option value='Sudhnati'"; if ($row['Perma_City'] == 'Sudhnati') echo 'selected'; echo " >
                              Sudhnati    
                      </option>
                                                       <option value='Ghanche'"; if ($row['Perma_City'] == 'Ghanche') echo 'selected'; echo " >
                              Ghanche    
                      </option>
                                                        <option value='Skardu'"; if ($row['Perma_City'] == 'Skardu') echo 'selected'; echo " >
                              Skardu    
                      </option>
                                                        <option value='Astore'"; if ($row['Perma_City'] == 'Astore') echo 'selected'; echo " >
                              Astore    
                      </option>
                                                        <option value='Diamer'"; if ($row['Perma_City'] == 'Diamer') echo 'selected'; echo " >
                              Diamer    
                      </option>
                                                        <option value='Ghizer'"; if ($row['Perma_City'] == 'Ghizer') echo 'selected'; echo " >
                              Ghizer    
                      </option>
                                                        <option value='Gilgit'"; if ($row['Perma_City'] == 'Gilgit') echo 'selected'; echo " >
                              Gilgit    
                      </option>
                                                        <option value='Hunza-Nagar'"; if ($row['Perma_City'] == 'Hunza-Nagar') echo 'selected'; echo " >
                              Hunza-Nagar    
                      </option>
                                                       <option value='Awaran'"; if ($row['Perma_City'] == 'Awaran') echo 'selected'; echo " >
                              Awaran    
                      </option>
                                                        <option value='Barkhan'"; if ($row['Perma_City'] == 'Barkhan') echo 'selected'; echo " >
                              Barkhan    
                      </option>
                                                        <option value='Bolan'"; if ($row['Perma_City'] == 'Bolan') echo 'selected'; echo " >
                              Bolan    
                      </option>
                                                        <option value='Chagai'"; if ($row['Perma_City'] == 'Chagai') echo 'selected'; echo " >
                              Chagai    
                      </option>
                                                        <option value='Dera Bugti'"; if ($row['Perma_City'] == 'Dera Bugti') echo 'selected'; echo " >
                              Dera Bugti    
                      </option>
                                                        <option value='Gwadar'"; if ($row['Perma_City'] == 'Gwadar') echo 'selected'; echo " >
                              Gwadar    
                      </option>
                                                        <option value='Harnai'"; if ($row['Perma_City'] == 'Harnai') echo 'selected'; echo " >
                              Harnai    
                      </option>
                                                        <option value='Jafarabad'"; if ($row['Perma_City'] == 'Jafarabad') echo 'selected'; echo " >
                              Jafarabad    
                      </option>
                                                        <option value='Jhal Magsi'"; if ($row['Perma_City'] == 'Jhal Magsi') echo 'selected'; echo " >
                              Jhal Magsi    
                      </option>
                                                        <option value='Kalat'"; if ($row['Perma_City'] == 'Kalat') echo 'selected'; echo " >
                              Kalat    
                      </option>
                                                        <option value='Kech Turbat'"; if ($row['Perma_City'] == 'Kech Turbat') echo 'selected'; echo " >
                              Kech Turbat    
                      </option>
                                                        <option value='Kharan'"; if ($row['Perma_City'] == 'Kharan') echo 'selected'; echo " >
                              Kharan    
                      </option>
                                                        <option value='Kohlu'"; if ($row['Perma_City'] == 'Kohlu') echo 'selected'; echo " >
                              Kohlu    
                      </option>
                                                        <option value='Khuzdar'"; if ($row['Perma_City'] == 'Khuzdar') echo 'selected'; echo " >
                              Khuzdar    
                      </option>
                                                        <option value='Killa Abdullah'"; if ($row['Perma_City'] == 'Killa Abdullah') echo 'selected'; echo " >
                              Killa Abdullah    
                      </option>
                                                        <option value='Killa Saifullah'"; if ($row['Perma_City'] == 'Killa Saifullah') echo 'selected'; echo " >
                              Killa Saifullah    
                      </option>
                                                        <option value='Lasbela'"; if ($row['Perma_City'] == 'Lasbela') echo 'selected'; echo " >
                              Lasbela    
                      </option>
                                                        <option value='Loralai'"; if ($row['Perma_City'] == 'Loralai') echo 'selected'; echo " >
                              Loralai    
                      </option>
                                                        <option value='Mastung'"; if ($row['Perma_City'] == 'Mastung') echo 'selected'; echo " >
                              Mastung    
                      </option>
                                                        <option value='Musakhel'"; if ($row['Perma_City'] == 'Musakhel') echo 'selected'; echo " >
                              Musakhel    
                      </option>
                                                        <option value='Nasirabad'"; if ($row['Perma_City'] == 'Nasirabad') echo 'selected'; echo " >
                              Nasirabad    
                      </option>
                                                        <option value='Nushki'"; if ($row['Perma_City'] == 'Nushki') echo 'selected'; echo " >
                              Nushki    
                      </option>
                                                        <option value='Panjgur'"; if ($row['Perma_City'] == 'Panjgur') echo 'selected'; echo " >
                              Panjgur    
                      </option>
                                                        <option value='Pishin'"; if ($row['Perma_City'] == 'Pishin') echo 'selected'; echo " >
                              Pishin    
                      </option>
                                                        <option value='Quetta'"; if ($row['Perma_City'] == 'Quetta') echo 'selected'; echo " >
                              Quetta    
                      </option>
                                                        <option value='Sherani'"; if ($row['Perma_City'] == 'Sherani') echo 'selected'; echo " >
                              Sherani    
                      </option>
                                                        <option value='Sibi'"; if ($row['Perma_City'] == 'Sibi') echo 'selected'; echo " >
                              Sibi    
                      </option>
                                                        <option value='Washuk'"; if ($row['Perma_City'] == 'Washuk') echo 'selected'; echo " >
                              Washuk    
                      </option>
                                                        <option value='Zhob'"; if ($row['Perma_City'] == 'Zhob') echo 'selected'; echo " >
                              Zhob    
                      </option>
                                                        <option value='Ziarat'"; if ($row['Perma_City'] == 'Ziarat') echo 'selected'; echo " >
                              Ziarat    
                      </option>
                                                        <option value='FR Kurram'"; if ($row['Perma_City'] == 'FR Kurram') echo 'selected'; echo " >
                              FR Kurram    
                      </option>
                      <option value='0'>other</option>

                                                                </select>
                          </div>
                        </div>


                        <div class='row custom-city' style='display:none;'>
                          <label class='control-label col-sm-2'>Other City</label>
                          <div class='col-sm-5'>
                              <input name='custom-city' id='custom-city' class='form-control' value=\"{$row['Perma_City']}\">
                          </div>
                        </div>


                        <div class='row'>
                          <label class='control-label col-sm-2'><span style='color:red'>*</span>Phone No (If Any)</label>
                          <div class='col-sm-5'>
                              <input name='Permanent_Phone' pattern='^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$' id='Permanent_Phone' class='form-control' required='required' value=\"{$row['Perma_Phone']}\">
                          </div>
                        </div>

              </div>
                
            </fieldset>
            
            <fieldset>
                <legend>Current Address</legend>
                <input type='checkbox' id='sameAsPernanetForCurrent' name='sameAsPernanetForCurrent' /> Same as permanent address
                <div class='form-group'>
                        <div class='row'>
                          <label class='control-label col-sm-2'><span style='color:red'>*</span>Address</label>
                          <div class='col-sm-5'>
                              <textarea name='Current_Address' id='Current_Address' class='form-control'>".$row['Cur_Address']."</textarea>
                          </div>
                        </div>

                       <div class='row'>
                          <label class='control-label col-sm-2'><span style='color:red'>*</span>City</label>
                          <div class='col-sm-5'>
                              <select name='Current_District' id='Current_District'>
                              <option value='{$row['Cur_City']}'>{$row['Cur_City']}</option> 
                              <option value='Abbottabad'"; if ($row['Cur_City'] == 'Abbottabad') echo 'selected'; echo " >
                              Abbottabad    
                      </option>
                                                      <option value='Bannu'"; if ($row['Cur_City'] == 'Bannu') echo 'selected'; echo " >
                              Bannu    
                      </option>
                                                       <option value='Battagram'"; if ($row['Cur_City'] == 'Battagram') echo 'selected'; echo " >
                              Battagram    
                      </option>
                                                        <option value='Buner'"; if ($row['Cur_City'] == 'Buner') echo 'selected'; echo " >
                              Buner    
                      </option>
                                                        <option value='Charsadda'"; if ($row['Cur_City'] == 'Charsadda') echo 'selected'; echo " >
                              Charsadda    
                      </option>
                                                        <option value='Chitral'"; if ($row['Cur_City'] == 'Chitral') echo 'selected'; echo " >
                              Chitral    
                      </option>
                                                        <option value='Dera Ismail Khan'"; if ($row['Cur_City'] == 'Dera Ismail Khan') echo 'selected'; echo " >
                              Dera Ismail Khan    
                      </option>
                                                        <option value='Hangu'"; if ($row['Cur_City'] == 'Hangu') echo 'selected'; echo " >
                              Hangu    
                      </option>
                                                        <option value='Haripur'"; if ($row['Cur_City'] == 'Haripur') echo 'selected'; echo " >
                              Haripur    
                      </option>
                                                        <option value='Kala Dhaka'"; if ($row['Cur_City'] == 'Kala Dhaka') echo 'selected'; echo " >
                              Kala Dhaka    
                      </option>
                                                        <option value='Karak'"; if ($row['Cur_City'] == 'Karak') echo 'selected'; echo " >
                              Karak    
                      </option>
                                                        <option value='Kohat'"; if ($row['Cur_City'] == 'Kohat') echo 'selected'; echo " >
                              Kohat    
                      </option>
                                                        <option value='Kohistan'"; if ($row['Cur_City'] == 'Kohistan') echo 'selected'; echo " >
                              Kohistan    
                      </option>
                                                        <option value='Lakki Marwat'"; if ($row['Cur_City'] == 'Lakki Marwat') echo 'selected'; echo " >
                              Lakki Marwat    
                      </option>
                                                        <option value='Lower Dir'"; if ($row['Cur_City'] == 'Lower Dir') echo 'selected'; echo " >
                              Lower Dir    
                      </option>
                                                        <option value='Mansehra'"; if ($row['Cur_City'] == 'Mansehra') echo 'selected'; echo " >
                              Mansehra    
                      </option>
                                                        <option value='Malakand'"; if ($row['Cur_City'] == 'Malakand') echo 'selected'; echo " >
                              Malakand    
                      </option>
                                                        <option value='Mardan'"; if ($row['Cur_City'] == 'Mardan') echo 'selected'; echo " >
                              Mardan    
                      </option>
                                                        <option value='Nowshera'"; if ($row['Cur_City'] == 'Nowshera') echo 'selected'; echo " >
                              Nowshera    
                      </option>
                                                        <option value='Peshawar'"; if ($row['Cur_City'] == 'Peshawar') echo 'selected'; echo " >
                              Peshawar    
                      </option>
                                                        <option value='Shangla'"; if ($row['Cur_City'] == 'Shangla') echo 'selected'; echo " >
                              Shangla    
                      </option>
                                                        <option value='Swabi'"; if ($row['Cur_City'] == 'Swabi') echo 'selected'; echo " >
                              Swabi    
                      </option>
                                                        <option value='Swat'"; if ($row['Cur_City'] == 'Swat') echo 'selected'; echo " >
                              Swat    
                      </option>
                                                        <option value='Tank'"; if ($row['Cur_City'] == 'Tank') echo 'selected'; echo " >
                              Tank    
                      </option>
                                                        <option value='Upper Dir'"; if ($row['Cur_City'] == 'Upper Dir') echo 'selected'; echo " >
                              Upper Dir    
                      </option>
                                                        <option value='Bajaur'"; if ($row['Cur_City'] == 'Bajaur') echo 'selected'; echo " >
                              Bajaur    
                      </option>
                                                        <option value='Khyber'"; if ($row['Cur_City'] == 'Khyber') echo 'selected'; echo " >
                              Khyber    
                      </option>
                                                        <option value='Kurram'"; if ($row['Cur_City'] == 'Kurram') echo 'selected'; echo " >
                              Kurram    
                      </option>
                                                        <option value='Mohmand'"; if ($row['Cur_City'] == 'Mohmand') echo 'selected'; echo " >
                              Mohmand    
                      </option>
                                                        <option value='North Waziristan'"; if ($row['Cur_City'] == 'North Waziristan') echo 'selected'; echo " >
                              North Waziristan    
                      </option>
                                                        <option value='Orakzai'"; if ($row['Cur_City'] == 'Orakzai') echo 'selected'; echo " >
                              Orakzai    
                      </option>
                                                        <option value='South Waziristan'"; if ($row['Cur_City'] == 'South Waziristan') echo 'selected'; echo " >
                              South Waziristan    
                      </option>
                                                        <option value='F R Bannu'"; if ($row['Cur_City'] == 'F R Bannu') echo 'selected'; echo " >
                              F R Bannu    
                      </option>
                                                        <option value='F R Dera Ismail Khan'"; if ($row['Cur_City'] == 'F R Dera Ismail Khan') echo 'selected'; echo " >
                              F R Dera Ismail Khan    
                      </option>
                                                        <option value='F R Kohat'"; if ($row['Cur_City'] == 'F R Kohat') echo 'selected'; echo " >
                              F R Kohat    
                      </option>
                                                       <option value='F R Lakki Marwat '"; if ($row['Cur_City'] == 'F R Lakki Marwat ') echo 'selected'; echo " >
                              F R Lakki Marwat    
                      </option>
                                                        <option value='F R Peshawar'"; if ($row['Cur_City'] == 'F R Peshawar') echo 'selected'; echo " >
                              F R Peshawar    
                      </option>
                                                        <option value='F R Tank'"; if ($row['Cur_City'] == 'F R Tank') echo 'selected'; echo " >
                              F R Tank    
                      </option>
                                                       <option value='Islamabad'"; if ($row['Cur_City'] == 'Islamabad') echo 'selected'; echo " >
                              Islamabad    
                      </option>
                                                        <option value='Attock'"; if ($row['Cur_City'] == 'Attock') echo 'selected'; echo " >
                              Attock    
                      </option>
                                                        <option value='Bahawalnagar'"; if ($row['Cur_City'] == 'Bahawalnagar') echo 'selected'; echo " >
                              Bahawalnagar    
                      </option>
                                                        <option value='Bahawalpur'"; if ($row['Cur_City'] == 'Bahawalpur') echo 'selected'; echo " >
                              Bahawalpur    
                      </option>
                                                        <option value='Bhakkar'"; if ($row['Cur_City'] == 'Bhakkar') echo 'selected'; echo " >
                              Bhakkar    
                      </option>
                                                       <option value='Chakwal'"; if ($row['Cur_City'] == 'Chakwal') echo 'selected'; echo " >
                              Chakwal    
                      </option>
                                                        <option value='Chiniot'"; if ($row['Cur_City'] == 'Chiniot') echo 'selected'; echo " >
                              Chiniot    
                      </option>
                                                        <option value='Dera Ghazi Khan'"; if ($row['Cur_City'] == 'Dera Ghazi Khan') echo 'selected'; echo " >
                              Dera Ghazi Khan    
                      </option>
                                                        <option value='Faisalabad'"; if ($row['Cur_City'] == 'Faisalabad') echo 'selected'; echo " >
                              Faisalabad    
                      </option>
                                                        <option value='Gujranwala'"; if ($row['Cur_City'] == 'Gujranwala') echo 'selected'; echo " >
                              Gujranwala    
                      </option>
                                                       <option value='Gujrat'"; if ($row['Cur_City'] == 'Gujrat') echo 'selected'; echo " >
                              Gujrat    
                      </option>
                                                        <option value='Hafizabad'"; if ($row['Cur_City'] == 'Hafizabad') echo 'selected'; echo " >
                              Hafizabad    
                      </option>
                                                       <option value='Jhang'"; if ($row['Cur_City'] == 'Jhang') echo 'selected'; echo " >
                              Jhang    
                      </option>
                                                        <option value='Jhelum'"; if ($row['Cur_City'] == 'Jhelum') echo 'selected'; echo " >
                              Jhelum    
                      </option>
                                                        <option value='Kasur'"; if ($row['Cur_City'] == 'Kasur') echo 'selected'; echo " >
                              Kasur    
                      </option>
                                                        <option value='Khanewal'"; if ($row['Cur_City'] == 'Khanewal') echo 'selected'; echo " >
                              Khanewal    
                      </option>
                                                        <option value='Khushab'"; if ($row['Cur_City'] == 'Khushab') echo 'selected'; echo " >
                              Khushab    
                      </option>
                                                        <option value='Lahore'"; if ($row['Cur_City'] == 'Lahore') echo 'selected'; echo " >
                              Lahore    
                      </option>
                                                        <option value='Layyah'"; if ($row['Cur_City'] == 'Layyah') echo 'selected'; echo " >
                              Layyah    
                      </option>
                                                        <option value='Lodhran'"; if ($row['Cur_City'] == 'Lodhran') echo 'selected'; echo " >
                              Lodhran    
                      </option>
                                                        <option value='Mandi Bahauddin'"; if ($row['Cur_City'] == 'Mandi Bahauddin') echo 'selected'; echo " >
                              Mandi Bahauddin    
                      </option>
                                                        <option value='Mianwali'"; if ($row['Cur_City'] == 'Mianwali') echo 'selected'; echo " >
                              Mianwali    
                      </option>
                                                        <option value='Multan'"; if ($row['Cur_City'] == 'Multan') echo 'selected'; echo " >
                              Multan    
                      </option>
                                                       <option value='Muzaffargarh'"; if ($row['Cur_City'] == 'Muzaffargarh') echo 'selected'; echo " >
                              Muzaffargarh    
                      </option>
                                                        <option value='Narowal'"; if ($row['Cur_City'] == 'Narowal') echo 'selected'; echo " >
                              Narowal    
                      </option>
                                                        <option value='Nankana Sahib'"; if ($row['Cur_City'] == 'Nankana Sahib') echo 'selected'; echo " >
                              Nankana Sahib    
                      </option>
                                                        <option value='Okara'"; if ($row['Cur_City'] == 'Okara') echo 'selected'; echo " >
                              Okara    
                      </option>
                                                        <option value='Pakpattan'"; if ($row['Cur_City'] == 'Pakpattan') echo 'selected'; echo " >
                              Pakpattan    
                      </option>
                                                        <option value='Rahim Yar Khan'"; if ($row['Cur_City'] == 'Rahim Yar Khan') echo 'selected'; echo " >
                              Rahim Yar Khan    
                      </option>
                                                        <option value='Rajanpur'"; if ($row['Cur_City'] == 'Rajanpur') echo 'selected'; echo " >
                              Rajanpur    
                      </option>
                                                        <option value='Rawalpindi'"; if ($row['Cur_City'] == 'Rawalpindi') echo 'selected'; echo " >
                              Rawalpindi    
                      </option>
                                                        <option value='Sahiwal'"; if ($row['Cur_City'] == 'Sahiwal') echo 'selected'; echo " >
                              Sahiwal    
                      </option>
                                                        <option value='Sargodha'"; if ($row['Cur_City'] == 'Sargodha') echo 'selected'; echo " >
                              Sargodha    
                      </option>
                                                        <option value='Sheikhupura'"; if ($row['Cur_City'] == 'Sheikhupura') echo 'selected'; echo " >
                              Sheikhupura    
                      </option>
                                                        <option value='Sialkot'"; if ($row['Cur_City'] == 'Sialkot') echo 'selected'; echo " >
                              Sialkot    
                      </option>
                                                        <option value='Toba Tek Singh'"; if ($row['Cur_City'] == 'Toba Tek Singh') echo 'selected'; echo " >
                              Toba Tek Singh    
                      </option>
                                                        <option value='Vehari'"; if ($row['Cur_City'] == 'Vehari') echo 'selected'; echo " >
                              Vehari    
                      </option>
                                                        <option value='Badin'"; if ($row['Cur_City'] == 'Badin') echo 'selected'; echo " >
                              Badin    
                      </option>
                                                        <option value='Dadu'"; if ($row['Cur_City'] == 'Dadu') echo 'selected'; echo " >
                              Dadu    
                      </option>
                                                        <option value='Ghotki'"; if ($row['Cur_City'] == 'Ghotki') echo 'selected'; echo " >
                              Ghotki    
                      </option>
                                                        <option value='Hyderabad'"; if ($row['Cur_City'] == 'Hyderabad') echo 'selected'; echo " >
                              Hyderabad    
                      </option>
                                                       <option value='Jacobabad'"; if ($row['Cur_City'] == 'Jacobabad') echo 'selected'; echo " >
                              Jacobabad    
                      </option>
                                                        <option value='Jamshoro'"; if ($row['Cur_City'] == 'Jamshoro') echo 'selected'; echo " >
                              Jamshoro    
                      </option>
                                                        <option value='Karachi'"; if ($row['Cur_City'] == 'Karachi') echo 'selected'; echo " >
                              Karachi    
                      </option>
                                                        <option value='Kashmore'"; if ($row['Cur_City'] == 'Kashmore') echo 'selected'; echo " >
                              Kashmore    
                      </option>
                                                        <option value='Khairpur'"; if ($row['Cur_City'] == 'Khairpur') echo 'selected'; echo " >
                              Khairpur    
                      </option>
                                                        <option value='Larkana'"; if ($row['Cur_City'] == 'Larkana') echo 'selected'; echo " >
                              Larkana    
                      </option>
                                                        <option value='Matiari'"; if ($row['Cur_City'] == 'Matiari') echo 'selected'; echo " >
                              Matiari    
                      </option>
                                                        <option value='Mirpurkhas'"; if ($row['Cur_City'] == 'Mirpurkhas') echo 'selected'; echo " >
                              Mirpurkhas    
                      </option>
                                                        <option value='Naushahro Firoze'"; if ($row['Cur_City'] == 'Naushahro Firoze') echo 'selected'; echo " >
                              Naushahro Firoze    
                      </option>
                                                        <option value='Nawabshah'"; if ($row['Cur_City'] == 'Nawabshah') echo 'selected'; echo " >
                              Nawabshah    
                      </option>
                                                        <option value='Qambar Shahdadkot'"; if ($row['Cur_City'] == 'Qambar Shahdadkot') echo 'selected'; echo " >
                              Qambar Shahdadkot    
                      </option>
                                                        <option value='Sanghar'"; if ($row['Cur_City'] == 'Sanghar') echo 'selected'; echo " >
                              Sanghar    
                      </option>
                                                        <option value='Shikarpur'"; if ($row['Cur_City'] == 'Shikarpur') echo 'selected'; echo " >
                              Shikarpur    
                      </option>
                                                        <option value='Sukkur'"; if ($row['Cur_City'] == 'Sukkur') echo 'selected'; echo " >
                              Sukkur    
                      </option>
                                                        <option value='Tando Allahyar'"; if ($row['Cur_City'] == 'Tando Allahyar') echo 'selected'; echo " >
                              Tando Allahyar    
                      </option>
                                                        <option value='Tando Muhammad Khan'"; if ($row['Cur_City'] == 'Tando Muhammad Khan') echo 'selected'; echo " >
                              Tando Muhammad Khan    
                      </option>
                                                        <option value='Tharparkar'"; if ($row['Cur_City'] == 'Tharparkar') echo 'selected'; echo " >
                              Tharparkar    
                      </option>
                                                        <option value='Thatta'"; if ($row['Cur_City'] == 'Thatta') echo 'selected'; echo " >
                              Thatta    
                      </option>
                                                        <option value='Umerkot'"; if ($row['Cur_City'] == 'Umerkot') echo 'selected'; echo " >
                              Umerkot    
                      </option>
                                                        <option value='Muzaffarabad'"; if ($row['Cur_City'] == 'Muzaffarabad') echo 'selected'; echo " >
                              Muzaffarabad    
                      </option>
                                                        <option value='Hattian'"; if ($row['Cur_City'] == 'Hattian') echo 'selected'; echo " >
                              Hattian    
                      </option>
                                                        <option value='Neelum'"; if ($row['Cur_City'] == 'Neelum') echo 'selected'; echo " >
                              Neelum    
                      </option>
                                                        <option value='Mirpur'"; if ($row['Cur_City'] == 'Mirpur') echo 'selected'; echo " >
                              Mirpur    
                      </option>
                                                       <option value='Bhimber'"; if ($row['Cur_City'] == 'Bhimber') echo 'selected'; echo " >
                              Bhimber    
                      </option>
                                                       <option value='Kotli'"; if ($row['Cur_City'] == 'Kotli') echo 'selected'; echo " >
                              Kotli    
                      </option>
                                                       <option value='Poonch'"; if ($row['Cur_City'] == 'Poonch') echo 'selected'; echo " >
                              Poonch    
                      </option>
                                                        <option value='Bagh'"; if ($row['Cur_City'] == 'Bagh') echo 'selected'; echo " >
                              Bagh    
                      </option>
                                                        <option value='Haveli'"; if ($row['Cur_City'] == 'Haveli') echo 'selected'; echo " >
                              Haveli    
                      </option>
                                                        <option value='Sudhnati'"; if ($row['Cur_City'] == 'Sudhnati') echo 'selected'; echo " >
                              Sudhnati    
                      </option>
                                                       <option value='Ghanche'"; if ($row['Cur_City'] == 'Ghanche') echo 'selected'; echo " >
                              Ghanche    
                      </option>
                                                        <option value='Skardu'"; if ($row['Cur_City'] == 'Skardu') echo 'selected'; echo " >
                              Skardu    
                      </option>
                                                        <option value='Astore'"; if ($row['Cur_City'] == 'Astore') echo 'selected'; echo " >
                              Astore    
                      </option>
                                                        <option value='Diamer'"; if ($row['Cur_City'] == 'Diamer') echo 'selected'; echo " >
                              Diamer    
                      </option>
                                                        <option value='Ghizer'"; if ($row['Cur_City'] == 'Ghizer') echo 'selected'; echo " >
                              Ghizer    
                      </option>
                                                        <option value='Gilgit'"; if ($row['Cur_City'] == 'Gilgit') echo 'selected'; echo " >
                              Gilgit    
                      </option>
                                                        <option value='Hunza-Nagar'"; if ($row['Cur_City'] == 'Hunza-Nagar') echo 'selected'; echo " >
                              Hunza-Nagar    
                      </option>
                                                       <option value='Awaran'"; if ($row['Cur_City'] == 'Awaran') echo 'selected'; echo " >
                              Awaran    
                      </option>
                                                        <option value='Barkhan'"; if ($row['Cur_City'] == 'Barkhan') echo 'selected'; echo " >
                              Barkhan    
                      </option>
                                                        <option value='Bolan'"; if ($row['Cur_City'] == 'Bolan') echo 'selected'; echo " >
                              Bolan    
                      </option>
                                                        <option value='Chagai'"; if ($row['Cur_City'] == 'Chagai') echo 'selected'; echo " >
                              Chagai    
                      </option>
                                                        <option value='Dera Bugti'"; if ($row['Cur_City'] == 'Dera Bugti') echo 'selected'; echo " >
                              Dera Bugti    
                      </option>
                                                        <option value='Gwadar'"; if ($row['Cur_City'] == 'Gwadar') echo 'selected'; echo " >
                              Gwadar    
                      </option>
                                                        <option value='Harnai'"; if ($row['Cur_City'] == 'Harnai') echo 'selected'; echo " >
                              Harnai    
                      </option>
                                                        <option value='Jafarabad'"; if ($row['Cur_City'] == 'Jafarabad') echo 'selected'; echo " >
                              Jafarabad    
                      </option>
                                                        <option value='Jhal Magsi'"; if ($row['Cur_City'] == 'Jhal Magsi') echo 'selected'; echo " >
                              Jhal Magsi    
                      </option>
                                                        <option value='Kalat'"; if ($row['Cur_City'] == 'Kalat') echo 'selected'; echo " >
                              Kalat    
                      </option>
                                                        <option value='Kech Turbat'"; if ($row['Cur_City'] == 'Kech Turbat') echo 'selected'; echo " >
                              Kech Turbat    
                      </option>
                                                        <option value='Kharan'"; if ($row['Cur_City'] == 'Kharan') echo 'selected'; echo " >
                              Kharan    
                      </option>
                                                        <option value='Kohlu'"; if ($row['Cur_City'] == 'Kohlu') echo 'selected'; echo " >
                              Kohlu    
                      </option>
                                                        <option value='Khuzdar'"; if ($row['Cur_City'] == 'Khuzdar') echo 'selected'; echo " >
                              Khuzdar    
                      </option>
                                                        <option value='Killa Abdullah'"; if ($row['Cur_City'] == 'Killa Abdullah') echo 'selected'; echo " >
                              Killa Abdullah    
                      </option>
                                                        <option value='Killa Saifullah'"; if ($row['Cur_City'] == 'Killa Saifullah') echo 'selected'; echo " >
                              Killa Saifullah    
                      </option>
                                                        <option value='Lasbela'"; if ($row['Cur_City'] == 'Lasbela') echo 'selected'; echo " >
                              Lasbela    
                      </option>
                                                        <option value='Loralai'"; if ($row['Cur_City'] == 'Loralai') echo 'selected'; echo " >
                              Loralai    
                      </option>
                                                        <option value='Mastung'"; if ($row['Cur_City'] == 'Mastung') echo 'selected'; echo " >
                              Mastung    
                      </option>
                                                        <option value='Musakhel'"; if ($row['Cur_City'] == 'Musakhel') echo 'selected'; echo " >
                              Musakhel    
                      </option>
                                                        <option value='Nasirabad'"; if ($row['Cur_City'] == 'Nasirabad') echo 'selected'; echo " >
                              Nasirabad    
                      </option>
                                                        <option value='Nushki'"; if ($row['Cur_City'] == 'Nushki') echo 'selected'; echo " >
                              Nushki    
                      </option>
                                                        <option value='Panjgur'"; if ($row['Cur_City'] == 'Panjgur') echo 'selected'; echo " >
                              Panjgur    
                      </option>
                                                        <option value='Pishin'"; if ($row['Cur_City'] == 'Pishin') echo 'selected'; echo " >
                              Pishin    
                      </option>
                                                        <option value='Quetta'"; if ($row['Cur_City'] == 'Quetta') echo 'selected'; echo " >
                              Quetta    
                      </option>
                                                        <option value='Sherani'"; if ($row['Cur_City'] == 'Sherani') echo 'selected'; echo " >
                              Sherani    
                      </option>
                                                        <option value='Sibi'"; if ($row['Cur_City'] == 'Sibi') echo 'selected'; echo " >
                              Sibi    
                      </option>
                                                        <option value='Washuk'"; if ($row['Cur_City'] == 'Washuk') echo 'selected'; echo " >
                              Washuk    
                      </option>
                                                        <option value='Zhob'"; if ($row['Cur_City'] == 'Zhob') echo 'selected'; echo " >
                              Zhob    
                      </option>
                                                        <option value='Ziarat'"; if ($row['Cur_City'] == 'Ziarat') echo 'selected'; echo " >
                              Ziarat    
                      </option>
                                                        <option value='FR Kurram'"; if ($row['Cur_City'] == 'FR Kurram') echo 'selected'; echo " >
                              FR Kurram    
                      </option>
                      <option value='0'>other</option>

                                </select>
                          </div>
                        </div>

                        <div class='row custom-curr-city' style='display:none;'>
                          <label class='control-label col-sm-2'>Other City </label>
                          <div class='col-sm-5'>
                              <input name='custom-curr-city' id='custom-curr-city' class='form-control' value=\"{$row['Cur_City']}\">
                          </div>
                        </div>

                        <div class='row'>
                          <label class='control-label col-sm-2'><span style='color:red'>*</span>Phone No (If any)</label>
                          <div class='col-sm-5'>
                              <input name='Current_Phone' id='Current_Phone' pattern='^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$' value=\"{$row['Cur_Phone']}\" class='form-control' />
                          </div>
                        </div>
              </div>
                
            </fieldset>    
                                
            <fieldset>
                                    <legend>
                                        Guardian's Address
                                    </legend>
                                        <div class='form-group'>
                                            
                                            <input type='radio' name='sameAs' id='sameAsPernanetForGuadian' value = 'sameAsPernanetForGuadian' /> Same as permanent address
                                            <input type='radio' name='sameAs' id='sameAsCurrentForGuadian' value = 'sameAsCurrentForGuadian'/> Same as current address
                        <div class='row'>
                          <label class='control-label col-sm-2'><span style='color:red'>*</span>Address</label>
                          <div class='col-sm-5'>
                              <textarea name='Guadian_Address' id='Guadian_Address' class='form-control'>".$row['Guard_Address']."</textarea>
                          </div>
                        </div>

                       <div class='row'>
                          <label class='control-label col-sm-2'><span style='color:red'>*</span>City</label>
                          <div class='col-sm-5'>
                              <select name='Guadian_District' id='Guadian_District'>
                              <option value='{$row['Guard_City']}'>{$row['Guard_City']}</option> 
                              <option value='Abbottabad'"; if ($row['Guard_City'] == 'Abbottabad') echo 'selected'; echo " >
                                          Abbottabad    
                                  </option>
                                                                  <option value='Bannu'"; if ($row['Guard_City'] == 'Bannu') echo 'selected'; echo " >
                                          Bannu    
                                  </option>
                                                                   <option value='Battagram'"; if ($row['Guard_City'] == 'Battagram') echo 'selected'; echo " >
                                          Battagram    
                                  </option>
                                                                    <option value='Buner'"; if ($row['Guard_City'] == 'Buner') echo 'selected'; echo " >
                                          Buner    
                                  </option>
                                                                    <option value='Charsadda'"; if ($row['Guard_City'] == 'Charsadda') echo 'selected'; echo " >
                                          Charsadda    
                                  </option>
                                                                    <option value='Chitral'"; if ($row['Guard_City'] == 'Chitral') echo 'selected'; echo " >
                                          Chitral    
                                  </option>
                                                                    <option value='Dera Ismail Khan'"; if ($row['Guard_City'] == 'Dera Ismail Khan') echo 'selected'; echo " >
                                          Dera Ismail Khan    
                                  </option>
                                                                    <option value='Hangu'"; if ($row['Guard_City'] == 'Hangu') echo 'selected'; echo " >
                                          Hangu    
                                  </option>
                                                                    <option value='Haripur'"; if ($row['Guard_City'] == 'Haripur') echo 'selected'; echo " >
                                          Haripur    
                                  </option>
                                                                    <option value='Kala Dhaka'"; if ($row['Guard_City'] == 'Kala Dhaka') echo 'selected'; echo " >
                                          Kala Dhaka    
                                  </option>
                                                                    <option value='Karak'"; if ($row['Guard_City'] == 'Karak') echo 'selected'; echo " >
                                          Karak    
                                  </option>
                                                                    <option value='Kohat'"; if ($row['Guard_City'] == 'Kohat') echo 'selected'; echo " >
                                          Kohat    
                                  </option>
                                                                    <option value='Kohistan'"; if ($row['Guard_City'] == 'Kohistan') echo 'selected'; echo " >
                                          Kohistan    
                                  </option>
                                                                    <option value='Lakki Marwat'"; if ($row['Guard_City'] == 'Lakki Marwat') echo 'selected'; echo " >
                                          Lakki Marwat    
                                  </option>
                                                                    <option value='Lower Dir'"; if ($row['Guard_City'] == 'Lower Dir') echo 'selected'; echo " >
                                          Lower Dir    
                                  </option>
                                                                    <option value='Mansehra'"; if ($row['Guard_City'] == 'Mansehra') echo 'selected'; echo " >
                                          Mansehra    
                                  </option>
                                                                    <option value='Malakand'"; if ($row['Guard_City'] == 'Malakand') echo 'selected'; echo " >
                                          Malakand    
                                  </option>
                                                                    <option value='Mardan'"; if ($row['Guard_City'] == 'Mardan') echo 'selected'; echo " >
                                          Mardan    
                                  </option>
                                                                    <option value='Nowshera'"; if ($row['Guard_City'] == 'Nowshera') echo 'selected'; echo " >
                                          Nowshera    
                                  </option>
                                                                    <option value='Peshawar'"; if ($row['Guard_City'] == 'Peshawar') echo 'selected'; echo " >
                                          Peshawar    
                                  </option>
                                                                    <option value='Shangla'"; if ($row['Guard_City'] == 'Shangla') echo 'selected'; echo " >
                                          Shangla    
                                  </option>
                                                                    <option value='Swabi'"; if ($row['Guard_City'] == 'Swabi') echo 'selected'; echo " >
                                          Swabi    
                                  </option>
                                                                    <option value='Swat'"; if ($row['Guard_City'] == 'Swat') echo 'selected'; echo " >
                                          Swat    
                                  </option>
                                                                    <option value='Tank'"; if ($row['Guard_City'] == 'Tank') echo 'selected'; echo " >
                                          Tank    
                                  </option>
                                                                    <option value='Upper Dir'"; if ($row['Guard_City'] == 'Upper Dir') echo 'selected'; echo " >
                                          Upper Dir    
                                  </option>
                                                                    <option value='Bajaur'"; if ($row['Guard_City'] == 'Bajaur') echo 'selected'; echo " >
                                          Bajaur    
                                  </option>
                                                                    <option value='Khyber'"; if ($row['Guard_City'] == 'Khyber') echo 'selected'; echo " >
                                          Khyber    
                                  </option>
                                                                    <option value='Kurram'"; if ($row['Guard_City'] == 'Kurram') echo 'selected'; echo " >
                                          Kurram    
                                  </option>
                                                                    <option value='Mohmand'"; if ($row['Guard_City'] == 'Mohmand') echo 'selected'; echo " >
                                          Mohmand    
                                  </option>
                                                                    <option value='North Waziristan'"; if ($row['Guard_City'] == 'North Waziristan') echo 'selected'; echo " >
                                          North Waziristan    
                                  </option>
                                                                    <option value='Orakzai'"; if ($row['Guard_City'] == 'Orakzai') echo 'selected'; echo " >
                                          Orakzai    
                                  </option>
                                                                    <option value='South Waziristan'"; if ($row['Guard_City'] == 'South Waziristan') echo 'selected'; echo " >
                                          South Waziristan    
                                  </option>
                                                                    <option value='F R Bannu'"; if ($row['Guard_City'] == 'F R Bannu') echo 'selected'; echo " >
                                          F R Bannu    
                                  </option>
                                                                    <option value='F R Dera Ismail Khan'"; if ($row['Guard_City'] == 'F R Dera Ismail Khan') echo 'selected'; echo " >
                                          F R Dera Ismail Khan    
                                  </option>
                                                                    <option value='F R Kohat'"; if ($row['Guard_City'] == 'F R Kohat') echo 'selected'; echo " >
                                          F R Kohat    
                                  </option>
                                                                   <option value='F R Lakki Marwat '"; if ($row['Guard_City'] == 'F R Lakki Marwat ') echo 'selected'; echo " >
                                          F R Lakki Marwat    
                                  </option>
                                                                    <option value='F R Peshawar'"; if ($row['Guard_City'] == 'F R Peshawar') echo 'selected'; echo " >
                                          F R Peshawar    
                                  </option>
                                                                    <option value='F R Tank'"; if ($row['Guard_City'] == 'F R Tank') echo 'selected'; echo " >
                                          F R Tank    
                                  </option>
                                                                   <option value='Islamabad'"; if ($row['Guard_City'] == 'Islamabad') echo 'selected'; echo " >
                                          Islamabad    
                                  </option>
                                                                    <option value='Attock'"; if ($row['Guard_City'] == 'Attock') echo 'selected'; echo " >
                                          Attock    
                                  </option>
                                                                    <option value='Bahawalnagar'"; if ($row['Guard_City'] == 'Bahawalnagar') echo 'selected'; echo " >
                                          Bahawalnagar    
                                  </option>
                                                                    <option value='Bahawalpur'"; if ($row['Guard_City'] == 'Bahawalpur') echo 'selected'; echo " >
                                          Bahawalpur    
                                  </option>
                                                                    <option value='Bhakkar'"; if ($row['Guard_City'] == 'Bhakkar') echo 'selected'; echo " >
                                          Bhakkar    
                                  </option>
                                                                   <option value='Chakwal'"; if ($row['Guard_City'] == 'Chakwal') echo 'selected'; echo " >
                                          Chakwal    
                                  </option>
                                                                    <option value='Chiniot'"; if ($row['Guard_City'] == 'Chiniot') echo 'selected'; echo " >
                                          Chiniot    
                                  </option>
                                                                    <option value='Dera Ghazi Khan'"; if ($row['Guard_City'] == 'Dera Ghazi Khan') echo 'selected'; echo " >
                                          Dera Ghazi Khan    
                                  </option>
                                                                    <option value='Faisalabad'"; if ($row['Guard_City'] == 'Faisalabad') echo 'selected'; echo " >
                                          Faisalabad    
                                  </option>
                                                                    <option value='Gujranwala'"; if ($row['Guard_City'] == 'Gujranwala') echo 'selected'; echo " >
                                          Gujranwala    
                                  </option>
                                                                   <option value='Gujrat'"; if ($row['Guard_City'] == 'Gujrat') echo 'selected'; echo " >
                                          Gujrat    
                                  </option>
                                                                    <option value='Hafizabad'"; if ($row['Guard_City'] == 'Hafizabad') echo 'selected'; echo " >
                                          Hafizabad    
                                  </option>
                                                                   <option value='Jhang'"; if ($row['Guard_City'] == 'Jhang') echo 'selected'; echo " >
                                          Jhang    
                                  </option>
                                                                    <option value='Jhelum'"; if ($row['Guard_City'] == 'Jhelum') echo 'selected'; echo " >
                                          Jhelum    
                                  </option>
                                                                    <option value='Kasur'"; if ($row['Guard_City'] == 'Kasur') echo 'selected'; echo " >
                                          Kasur    
                                  </option>
                                                                    <option value='Khanewal'"; if ($row['Guard_City'] == 'Khanewal') echo 'selected'; echo " >
                                          Khanewal    
                                  </option>
                                                                    <option value='Khushab'"; if ($row['Guard_City'] == 'Khushab') echo 'selected'; echo " >
                                          Khushab    
                                  </option>
                                                                    <option value='Lahore'"; if ($row['Guard_City'] == 'Lahore') echo 'selected'; echo " >
                                          Lahore    
                                  </option>
                                                                    <option value='Layyah'"; if ($row['Guard_City'] == 'Layyah') echo 'selected'; echo " >
                                          Layyah    
                                  </option>
                                                                    <option value='Lodhran'"; if ($row['Guard_City'] == 'Lodhran') echo 'selected'; echo " >
                                          Lodhran    
                                  </option>
                                                                    <option value='Mandi Bahauddin'"; if ($row['Guard_City'] == 'Mandi Bahauddin') echo 'selected'; echo " >
                                          Mandi Bahauddin    
                                  </option>
                                                                    <option value='Mianwali'"; if ($row['Guard_City'] == 'Mianwali') echo 'selected'; echo " >
                                          Mianwali    
                                  </option>
                                                                    <option value='Multan'"; if ($row['Guard_City'] == 'Multan') echo 'selected'; echo " >
                                          Multan    
                                  </option>
                                                                   <option value='Muzaffargarh'"; if ($row['Guard_City'] == 'Muzaffargarh') echo 'selected'; echo " >
                                          Muzaffargarh    
                                  </option>
                                                                    <option value='Narowal'"; if ($row['Guard_City'] == 'Narowal') echo 'selected'; echo " >
                                          Narowal    
                                  </option>
                                                                    <option value='Nankana Sahib'"; if ($row['Guard_City'] == 'Nankana Sahib') echo 'selected'; echo " >
                                          Nankana Sahib    
                                  </option>
                                                                    <option value='Okara'"; if ($row['Guard_City'] == 'Okara') echo 'selected'; echo " >
                                          Okara    
                                  </option>
                                                                    <option value='Pakpattan'"; if ($row['Guard_City'] == 'Pakpattan') echo 'selected'; echo " >
                                          Pakpattan    
                                  </option>
                                                                    <option value='Rahim Yar Khan'"; if ($row['Guard_City'] == 'Rahim Yar Khan') echo 'selected'; echo " >
                                          Rahim Yar Khan    
                                  </option>
                                                                    <option value='Rajanpur'"; if ($row['Guard_City'] == 'Rajanpur') echo 'selected'; echo " >
                                          Rajanpur    
                                  </option>
                                                                    <option value='Rawalpindi'"; if ($row['Guard_City'] == 'Rawalpindi') echo 'selected'; echo " >
                                          Rawalpindi    
                                  </option>
                                                                    <option value='Sahiwal'"; if ($row['Guard_City'] == 'Sahiwal') echo 'selected'; echo " >
                                          Sahiwal    
                                  </option>
                                                                    <option value='Sargodha'"; if ($row['Guard_City'] == 'Sargodha') echo 'selected'; echo " >
                                          Sargodha    
                                  </option>
                                                                    <option value='Sheikhupura'"; if ($row['Guard_City'] == 'Sheikhupura') echo 'selected'; echo " >
                                          Sheikhupura    
                                  </option>
                                                                    <option value='Sialkot'"; if ($row['Guard_City'] == 'Sialkot') echo 'selected'; echo " >
                                          Sialkot    
                                  </option>
                                                                    <option value='Toba Tek Singh'"; if ($row['Guard_City'] == 'Toba Tek Singh') echo 'selected'; echo " >
                                          Toba Tek Singh    
                                  </option>
                                                                    <option value='Vehari'"; if ($row['Guard_City'] == 'Vehari') echo 'selected'; echo " >
                                          Vehari    
                                  </option>
                                                                    <option value='Badin'"; if ($row['Guard_City'] == 'Badin') echo 'selected'; echo " >
                                          Badin    
                                  </option>
                                                                    <option value='Dadu'"; if ($row['Guard_City'] == 'Dadu') echo 'selected'; echo " >
                                          Dadu    
                                  </option>
                                                                    <option value='Ghotki'"; if ($row['Guard_City'] == 'Ghotki') echo 'selected'; echo " >
                                          Ghotki    
                                  </option>
                                                                    <option value='Hyderabad'"; if ($row['Guard_City'] == 'Hyderabad') echo 'selected'; echo " >
                                          Hyderabad    
                                  </option>
                                                                   <option value='Jacobabad'"; if ($row['Guard_City'] == 'Jacobabad') echo 'selected'; echo " >
                                          Jacobabad    
                                  </option>
                                                                    <option value='Jamshoro'"; if ($row['Guard_City'] == 'Jamshoro') echo 'selected'; echo " >
                                          Jamshoro    
                                  </option>
                                                                    <option value='Karachi'"; if ($row['Guard_City'] == 'Karachi') echo 'selected'; echo " >
                                          Karachi    
                                  </option>
                                                                    <option value='Kashmore'"; if ($row['Guard_City'] == 'Kashmore') echo 'selected'; echo " >
                                          Kashmore    
                                  </option>
                                                                    <option value='Khairpur'"; if ($row['Guard_City'] == 'Khairpur') echo 'selected'; echo " >
                                          Khairpur    
                                  </option>
                                                                    <option value='Larkana'"; if ($row['Guard_City'] == 'Larkana') echo 'selected'; echo " >
                                          Larkana    
                                  </option>
                                                                    <option value='Matiari'"; if ($row['Guard_City'] == 'Matiari') echo 'selected'; echo " >
                                          Matiari    
                                  </option>
                                                                    <option value='Mirpurkhas'"; if ($row['Guard_City'] == 'Mirpurkhas') echo 'selected'; echo " >
                                          Mirpurkhas    
                                  </option>
                                                                    <option value='Naushahro Firoze'"; if ($row['Guard_City'] == 'Naushahro Firoze') echo 'selected'; echo " >
                                          Naushahro Firoze    
                                  </option>
                                                                    <option value='Nawabshah'"; if ($row['Guard_City'] == 'Nawabshah') echo 'selected'; echo " >
                                          Nawabshah    
                                  </option>
                                                                    <option value='Qambar Shahdadkot'"; if ($row['Guard_City'] == 'Qambar Shahdadkot') echo 'selected'; echo " >
                                          Qambar Shahdadkot    
                                  </option>
                                                                    <option value='Sanghar'"; if ($row['Guard_City'] == 'Sanghar') echo 'selected'; echo " >
                                          Sanghar    
                                  </option>
                                                                    <option value='Shikarpur'"; if ($row['Guard_City'] == 'Shikarpur') echo 'selected'; echo " >
                                          Shikarpur    
                                  </option>
                                                                    <option value='Sukkur'"; if ($row['Guard_City'] == 'Sukkur') echo 'selected'; echo " >
                                          Sukkur    
                                  </option>
                                                                    <option value='Tando Allahyar'"; if ($row['Guard_City'] == 'Tando Allahyar') echo 'selected'; echo " >
                                          Tando Allahyar    
                                  </option>
                                                                    <option value='Tando Muhammad Khan'"; if ($row['Guard_City'] == 'Tando Muhammad Khan') echo 'selected'; echo " >
                                          Tando Muhammad Khan    
                                  </option>
                                                                    <option value='Tharparkar'"; if ($row['Guard_City'] == 'Tharparkar') echo 'selected'; echo " >
                                          Tharparkar    
                                  </option>
                                                                    <option value='Thatta'"; if ($row['Guard_City'] == 'Thatta') echo 'selected'; echo " >
                                          Thatta    
                                  </option>
                                                                    <option value='Umerkot'"; if ($row['Guard_City'] == 'Umerkot') echo 'selected'; echo " >
                                          Umerkot    
                                  </option>
                                                                    <option value='Muzaffarabad'"; if ($row['Guard_City'] == 'Muzaffarabad') echo 'selected'; echo " >
                                          Muzaffarabad    
                                  </option>
                                                                    <option value='Hattian'"; if ($row['Guard_City'] == 'Hattian') echo 'selected'; echo " >
                                          Hattian    
                                  </option>
                                                                    <option value='Neelum'"; if ($row['Guard_City'] == 'Neelum') echo 'selected'; echo " >
                                          Neelum    
                                  </option>
                                                                    <option value='Mirpur'"; if ($row['Guard_City'] == 'Mirpur') echo 'selected'; echo " >
                                          Mirpur    
                                  </option>
                                                                   <option value='Bhimber'"; if ($row['Guard_City'] == 'Bhimber') echo 'selected'; echo " >
                                          Bhimber    
                                  </option>
                                                                   <option value='Kotli'"; if ($row['Guard_City'] == 'Kotli') echo 'selected'; echo " >
                                          Kotli    
                                  </option>
                                                                   <option value='Poonch'"; if ($row['Guard_City'] == 'Poonch') echo 'selected'; echo " >
                                          Poonch    
                                  </option>
                                                                    <option value='Bagh'"; if ($row['Guard_City'] == 'Bagh') echo 'selected'; echo " >
                                          Bagh    
                                  </option>
                                                                    <option value='Haveli'"; if ($row['Guard_City'] == 'Haveli') echo 'selected'; echo " >
                                          Haveli    
                                  </option>
                                                                    <option value='Sudhnati'"; if ($row['Guard_City'] == 'Sudhnati') echo 'selected'; echo " >
                                          Sudhnati    
                                  </option>
                                                                   <option value='Ghanche'"; if ($row['Guard_City'] == 'Ghanche') echo 'selected'; echo " >
                                          Ghanche    
                                  </option>
                                                                    <option value='Skardu'"; if ($row['Guard_City'] == 'Skardu') echo 'selected'; echo " >
                                          Skardu    
                                  </option>
                                                                    <option value='Astore'"; if ($row['Guard_City'] == 'Astore') echo 'selected'; echo " >
                                          Astore    
                                  </option>
                                                                    <option value='Diamer'"; if ($row['Guard_City'] == 'Diamer') echo 'selected'; echo " >
                                          Diamer    
                                  </option>
                                                                    <option value='Ghizer'"; if ($row['Guard_City'] == 'Ghizer') echo 'selected'; echo " >
                                          Ghizer    
                                  </option>
                                                                    <option value='Gilgit'"; if ($row['Guard_City'] == 'Gilgit') echo 'selected'; echo " >
                                          Gilgit    
                                  </option>
                                                                    <option value='Hunza-Nagar'"; if ($row['Guard_City'] == 'Hunza-Nagar') echo 'selected'; echo " >
                                          Hunza-Nagar    
                                  </option>
                                                                   <option value='Awaran'"; if ($row['Guard_City'] == 'Awaran') echo 'selected'; echo " >
                                          Awaran    
                                  </option>
                                                                    <option value='Barkhan'"; if ($row['Guard_City'] == 'Barkhan') echo 'selected'; echo " >
                                          Barkhan    
                                  </option>
                                                                    <option value='Bolan'"; if ($row['Guard_City'] == 'Bolan') echo 'selected'; echo " >
                                          Bolan    
                                  </option>
                                                                    <option value='Chagai'"; if ($row['Guard_City'] == 'Chagai') echo 'selected'; echo " >
                                          Chagai    
                                  </option>
                                                                    <option value='Dera Bugti'"; if ($row['Guard_City'] == 'Dera Bugti') echo 'selected'; echo " >
                                          Dera Bugti    
                                  </option>
                                                                    <option value='Gwadar'"; if ($row['Guard_City'] == 'Gwadar') echo 'selected'; echo " >
                                          Gwadar    
                                  </option>
                                                                    <option value='Harnai'"; if ($row['Guard_City'] == 'Harnai') echo 'selected'; echo " >
                                          Harnai    
                                  </option>
                                                                    <option value='Jafarabad'"; if ($row['Guard_City'] == 'Jafarabad') echo 'selected'; echo " >
                                          Jafarabad    
                                  </option>
                                                                    <option value='Jhal Magsi'"; if ($row['Guard_City'] == 'Jhal Magsi') echo 'selected'; echo " >
                                          Jhal Magsi    
                                  </option>
                                                                    <option value='Kalat'"; if ($row['Guard_City'] == 'Kalat') echo 'selected'; echo " >
                                          Kalat    
                                  </option>
                                                                    <option value='Kech Turbat'"; if ($row['Guard_City'] == 'Kech Turbat') echo 'selected'; echo " >
                                          Kech Turbat    
                                  </option>
                                                                    <option value='Kharan'"; if ($row['Guard_City'] == 'Kharan') echo 'selected'; echo " >
                                          Kharan    
                                  </option>
                                                                    <option value='Kohlu'"; if ($row['Guard_City'] == 'Kohlu') echo 'selected'; echo " >
                                          Kohlu    
                                  </option>
                                                                    <option value='Khuzdar'"; if ($row['Guard_City'] == 'Khuzdar') echo 'selected'; echo " >
                                          Khuzdar    
                                  </option>
                                                                    <option value='Killa Abdullah'"; if ($row['Guard_City'] == 'Killa Abdullah') echo 'selected'; echo " >
                                          Killa Abdullah    
                                  </option>
                                                                    <option value='Killa Saifullah'"; if ($row['Guard_City'] == 'Killa Saifullah') echo 'selected'; echo " >
                                          Killa Saifullah    
                                  </option>
                                                                    <option value='Lasbela'"; if ($row['Guard_City'] == 'Lasbela') echo 'selected'; echo " >
                                          Lasbela    
                                  </option>
                                                                    <option value='Loralai'"; if ($row['Guard_City'] == 'Loralai') echo 'selected'; echo " >
                                          Loralai    
                                  </option>
                                                                    <option value='Mastung'"; if ($row['Guard_City'] == 'Mastung') echo 'selected'; echo " >
                                          Mastung    
                                  </option>
                                                                    <option value='Musakhel'"; if ($row['Guard_City'] == 'Musakhel') echo 'selected'; echo " >
                                          Musakhel    
                                  </option>
                                                                    <option value='Nasirabad'"; if ($row['Guard_City'] == 'Nasirabad') echo 'selected'; echo " >
                                          Nasirabad    
                                  </option>
                                                                    <option value='Nushki'"; if ($row['Guard_City'] == 'Nushki') echo 'selected'; echo " >
                                          Nushki    
                                  </option>
                                                                    <option value='Panjgur'"; if ($row['Guard_City'] == 'Panjgur') echo 'selected'; echo " >
                                          Panjgur    
                                  </option>
                                                                    <option value='Pishin'"; if ($row['Guard_City'] == 'Pishin') echo 'selected'; echo " >
                                          Pishin    
                                  </option>
                                                                    <option value='Quetta'"; if ($row['Guard_City'] == 'Quetta') echo 'selected'; echo " >
                                          Quetta    
                                  </option>
                                                                    <option value='Sherani'"; if ($row['Guard_City'] == 'Sherani') echo 'selected'; echo " >
                                          Sherani    
                                  </option>
                                                                    <option value='Sibi'"; if ($row['Guard_City'] == 'Sibi') echo 'selected'; echo " >
                                          Sibi    
                                  </option>
                                                                    <option value='Washuk'"; if ($row['Guard_City'] == 'Washuk') echo 'selected'; echo " >
                                          Washuk    
                                  </option>
                                                                    <option value='Zhob'"; if ($row['Guard_City'] == 'Zhob') echo 'selected'; echo " >
                                          Zhob    
                                  </option>
                                                                    <option value='Ziarat'"; if ($row['Guard_City'] == 'Ziarat') echo 'selected'; echo " >
                                          Ziarat    
                                  </option>
                                                                    <option value='FR Kurram'"; if ($row['Guard_City'] == 'FR Kurram') echo 'selected'; echo " >
                                          FR Kurram    
                                  </option>
                                  <option value='0'>other</option>

                              </select>
                          </div>
                        </div>
                        <div class='row custom-guard-city' style='display:none;'>
                          <label class='control-label col-sm-2'>Other City</label>
                          <div class='col-sm-5'>
                              <input name='custom-guard-city' id='custom-guard-city' class='form-control' value=\"{$row['Guard_City']}\">
                          </div>
                        </div>
                        <div class='row'>
                          <label class='control-label col-sm-2'><span style='color:red'>*</span>Phone No (If any)</label>
                          <div class='col-sm-5'>
                              <input name='Guadian_Phone' id='Guadian_Phone' pattern='^((\+92)|(0092))-{0,1}\d{3}-{0,1}\d{7}$|^\d{11}$|^\d{4}-\d{7}$'  value=\"{$row['Guard_Phone']}\" class='form-control' />
                          </div>
                        </div>
              </div>

         </fieldset>
               
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'></label>
                <div class='col-sm-5'>
                 
                <input type='submit' style='background-color:green;color:white' class='btn' name='submit_address' value='Save and Continue' id='submit_address' >
                </div>

                <input type='hidden' name='reg_id' value='{$id}'>
                <input type='hidden' name='submit_address' value='0'>
              </div>
            </div>              
        </div>
    </div>
</div>
</div><!-- /.widget-user -->
</div>
        </div>
        </section><!-- /.content -->
        </form>
          
        <div class='col-md-12' id='form-msg'></div>

      </div></div></div>
      </div></div></div>
      <script>
                $('#Permanent_City').change(function(){
                        var id = $(this).val();
                        if(id == 0){
                        $('.custom-city').css('display','block');
                        } 
                        else{
                        $('.custom-city').css('display','none');            
                        }
                });
                $('#Current_District').change(function(){
                        var id = $(this).val();
                        if(id == 0){
                        $('.custom-curr-city').css('display','block');
                        } 
                        else{
                        $('.custom-curr-city').css('display','none');            
                        }
                });
                $('#Guadian_District').change(function(){
                        var id = $(this).val();
                        if(id == 0){
                        $('.custom-guard-city').css('display','block');
                        } 
                        else{
                        $('.custom-guard-city').css('display','none');            
                        }
                });
                
        </script>
        ";
    }
    else if ($code == 4)
    {
     
                $result = mysqli_query($connection, "SELECT * FROM tbl_defence_notification WHERE CID = '$id'") or die(mysqli_error($connection));
                $row = mysqli_fetch_array($result);
                    echo " <div class='modal fade' id='myModal15' role='dialog'>
                <div class='modal-dialog'>
                
                  <!-- Modal content-->
                  <div class='container'>
                    
                     <div class='col-lg-8 col-md-8  col-sm-8 '>
              
              <div class='panel panel-default'>
                  <div class='panel-heading' style='background-color:#983415;border:none' >
                  <h3 style='text-align:center;color:white;'><Strong>Public Defence Notification </Strong></h3>
                  <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
                  </div>
            
                  <div class='panel-body' style='background-color:#ab9f9b24;'>
                  <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
                <h3>  <small></small>
                      </h3><hr><div class='form-group'>
                          <div class='row'>
                            <label class='control-label col-sm-2'> <span style='color:red'>*</span> Notification Date</label>
                            <div class='col-sm-5'>
                                <input type='date' class='form-control' name='dob' value=\"{$row['Date']}\" id='dob' required  />
                            </div>  
                          </div>
                        </div>
                   
                    <br>            
                    <div class='form-group'>
                          <div class='row'>
                            <label class='control-label col-sm-2'>Upload BASR Approval File</label>
                            
                                    <div class='col-sm-5'>
                                    <img src='uploads/{$row['BASR_File']}' class='form-img' alt='No Image'>
                                        <input type='file' class='form-control' name='basrfile'  id='addform' value=''/>                           
                                    </div>
                                     
                           
                          </div>
                    <br>      
                    <div class='form-group'>
                          <div class='row'>
                            <label class='control-label col-sm-2'>Upload Commitee File</label>
                            
                                    <div class='col-sm-5'>
                                    <img src='uploads/{$row['Commitee_File']}' class='form-img' alt='No Image'>
                                        <input type='file' class='form-control' name='addmissionForm'  id='addform'/>                           
                                    </div>
                                     
                           
                          </div>
                        </div>
                       
                            <input type='submit' style='background-color:green;color:white' class='btn' name='submit_notification' value='Add Notofication' id='submit_thesis' >
            
                        <input type='hidden' name='reg_id' value='{$id}'>
                        <input type='hidden' name='submit_notification' value='0'>
                            </form>
                        </div></div></div></div></div></div>
            ";
    }
    else if ($code == 3)
    {

        if(!isset($_POST['degree']))
                $result = mysqli_query($connection, "SELECT * FROM tbl_qualification where CID = '$id'") or die(mysqli_error($connection)); 
        else
                $result = mysqli_query($connection, "SELECT * FROM tbl_qualification where CID = '$id' AND Degree = '{$_POST['degree']}'") or die(mysqli_error($connection)); 
                
        $row = mysqli_fetch_array($result);

        echo " 
        <div class='modal fade' id='myModal2' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>Student Educational Details</Strong></h3>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>
      <div class='panel-body' style='background-color:#ab9f9b24;'>
         
        
             <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
              <input type='hidden' name='QId' value='' />
                   
        <!-- Main content -->
        <section class='content'>
            <h2 style='color:red'></h2>
        <div class='row'>
            <div class='col-xs-12'>
              <!-- Widget: user widget -->
            <div class='box box-widget widget-user'>
             
                <div class='box-footer'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='box-body table-responsive'>
                                  <div class='form-group'>
                                      
                                      
             <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree</label>
                <div class='col-sm-5'>
                  
                    <select name='Degree' id='Degree'>
                                                <option value='SSC/Equivalent'"; if($row['Degree'] == 'SSC/Equivalent') echo 'selected '; echo " >
                                SSC/Equivalent    
                        </option>
                                                <option value='FA / FSc. /Equivalent'"; if($row['Degree'] == 'FA / FSc. /Equivalent') echo 'selected '; echo " >
                                FA / FSc. /Equivalent    
                        </option>
                                                <option value='BA / BSc. /Equivalent'"; if($row['Degree'] == 'BA / BSc. /Equivalent') echo 'selected '; echo " >
                                BA / BSc. /Equivalent    
                        </option>
                                                <option value='MA / MSc. /Equivalent'"; if($row['Degree'] == 'MA / MSc. /Equivalent') echo 'selected '; echo " >
                                MA / MSc. /Equivalent    
                        </option>
                                                <option value='MS / M.Phil/Equivalent'"; if($row['Degree'] == 'MS / M.Phil/Equivalent') echo 'selected '; echo " >
                                MS / M.Phil/Equivalent    
                        </option>
                                                <option value='PhD'"; if($row['Degree'] == 'PhD') echo 'selected '; echo " >
                                PhD    
                        </option>
                                               <option value='Post-Doc'"; if($row['Degree'] == 'Post-Doc') echo 'selected '; echo " >
                                Post-Doc    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div> 
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Year of Passing</label>
                <div class='col-sm-5'>
                                        <select name='YearOfPassing' id='YearOfPassing'>
                                                <option value='2017' "; if($row['Passing_Year'] == '2017') echo 'selected '; echo " >
                                2017    
                        </option>
                                                <option value='2016' "; if($row['Passing_Year'] == '2016') echo 'selected '; echo " >
                                2016    
                        </option>
                                                <option value='2015' "; if($row['Passing_Year'] == '2015') echo 'selected '; echo " >
                                2015    
                        </option>
                                                <option value='2014' "; if($row['Passing_Year'] == '2014') echo 'selected '; echo " >
                                2014    
                        </option>
                                                <option value='2013' "; if($row['Passing_Year'] == '2013') echo 'selected '; echo " >
                                2013    
                        </option>
                                                <option value='2012' "; if($row['Passing_Year'] == '2012') echo 'selected '; echo " >
                                2012    
                        </option>
                                                <option value='2011' "; if($row['Passing_Year'] == '2011') echo 'selected '; echo " >
                                2011    
                        </option>
                                                <option value='2010' "; if($row['Passing_Year'] == '2010') echo 'selected '; echo " >
                                2010    
                        </option>
                                                <option value='2009' "; if($row['Passing_Year'] == '2009') echo 'selected '; echo " >
                                2009    
                        </option>
                                                <option value='2008' "; if($row['Passing_Year'] == '2008') echo 'selected '; echo " >
                                2008    
                        </option>
                                                <option value='2007' "; if($row['Passing_Year'] == '2007') echo 'selected '; echo " >
                                2007    
                        </option>
                                                <option value='2006' "; if($row['Passing_Year'] == '2006') echo 'selected '; echo " >
                                2006    
                        </option>
                                                <option value='2005' "; if($row['Passing_Year'] == '2005') echo 'selected '; echo " >
                                2005    
                        </option>
                                                <option value='2004' "; if($row['Passing_Year'] == '2004') echo 'selected '; echo " >
                                2004    
                        </option>
                                                <option value='2003' "; if($row['Passing_Year'] == '2003') echo 'selected '; echo " >
                                2003    
                        </option>
                                                <option value='2002' "; if($row['Passing_Year'] == '2002') echo 'selected '; echo " >
                                2002    
                        </option>
                                                <option value='2001' "; if($row['Passing_Year'] == '2001') echo 'selected '; echo " >
                                2001    
                        </option>
                                                <option value='2000' "; if($row['Passing_Year'] == '2000') echo 'selected '; echo " >
                                2000    
                        </option>
                                                <option value='1999' "; if($row['Passing_Year'] == '1999') echo 'selected '; echo " >
                                1999    
                        </option>
                                                <option value='1998' "; if($row['Passing_Year'] == '1998') echo 'selected '; echo " >
                                1998    
                        </option>
                                                <option value='1997' "; if($row['Passing_Year'] == '1997') echo 'selected '; echo " >
                                1997    
                        </option>
                                                <option value='1996' "; if($row['Passing_Year'] == '1996') echo 'selected '; echo " >
                                1996    
                        </option>
                                                <option value='1995' "; if($row['Passing_Year'] == '1995') echo 'selected '; echo " >
                                1995    
                        </option>
                                                <option value='1994' "; if($row['Passing_Year'] == '1994') echo 'selected '; echo " >
                                1994    
                        </option>
                                                <option value='1993' "; if($row['Passing_Year'] == '1993') echo 'selected '; echo " >
                                1993    
                        </option>
                                                <option value='1992' "; if($row['Passing_Year'] == '1992') echo 'selected '; echo " >
                                1992    
                        </option>
                                                <option value='1991' "; if($row['Passing_Year'] == '1991') echo 'selected '; echo " >
                                1991    
                        </option>
                                                <option value='1990' "; if($row['Passing_Year'] == '1990') echo 'selected '; echo " >
                                1990    
                        </option>
                                                <option value='1989' "; if($row['Passing_Year'] == '1989') echo 'selected '; echo " >
                                1989    
                        </option>
                                                <option value='1988' "; if($row['Passing_Year'] == '1988') echo 'selected '; echo " >
                                1988    
                        </option>
                                                <option value='1987' "; if($row['Passing_Year'] == '1987') echo 'selected '; echo " >
                                1987    
                        </option>
                                                <option value='1986' "; if($row['Passing_Year'] == '1985') echo 'selected '; echo " >
                                1986    
                        </option>
                                                <option value='1985' "; if($row['Passing_Year'] == '7') echo 'selected '; echo " >
                                1985    
                        </option>
                                                <option value='1984' "; if($row['Passing_Year'] == '1984') echo 'selected '; echo " >
                                1984    
                        </option>
                                                <option value='1983' "; if($row['Passing_Year'] == '1983') echo 'selected '; echo " >
                                1983    
                        </option>
                                                <option value='1982' "; if($row['Passing_Year'] == '1982') echo 'selected '; echo " >
                                1982    
                        </option>
                                                <option value='1981' "; if($row['Passing_Year'] == '1981') echo 'selected '; echo " >
                                1981    
                        </option>
                                                <option value='1980' "; if($row['Passing_Year'] == '1980') echo 'selected '; echo " >
                                1980    
                        </option>
                                                <option value='1979' "; if($row['Passing_Year'] == '1979') echo 'selected '; echo " >
                                1979    
                        </option>
                                                <option value='1978' "; if($row['Passing_Year'] == '1978') echo 'selected '; echo " >
                                1978    
                        </option>
                                                <option value='1977' "; if($row['Passing_Year'] == '1977') echo 'selected '; echo " >
                                1977    
                        </option>
                                                <option value='1976' "; if($row['Passing_Year'] == '1976') echo 'selected '; echo " >
                                1976    
                        </option>
                                                <option value='1975' "; if($row['Passing_Year'] == '1975') echo 'selected '; echo " >
                                1975    
                        </option>
                                                <option value='1974' "; if($row['Passing_Year'] == '1974') echo 'selected '; echo " >
                                1974    
                        </option>
                                                <option value='1973' "; if($row['Passing_Year'] == '1973') echo 'selected '; echo " >
                                1973    
                        </option>
                                                <option value='1972' "; if($row['Passing_Year'] == '1972') echo 'selected '; echo " >
                                1972    
                        </option>
                                                <option value='1971' "; if($row['Passing_Year'] == '1971') echo 'selected '; echo " >
                                1971    
                        </option>
                                                <option value='1970' "; if($row['Passing_Year'] == '1970') echo 'selected '; echo " >
                                1970    
                        </option>
                                                <option value='1969' "; if($row['Passing_Year'] == '1969') echo 'selected '; echo " >
                                1969    
                        </option>
                                                <option value='1968' "; if($row['Passing_Year'] == '1968') echo 'selected '; echo " >
                                1968    
                        </option>
                                                <option value='1967' "; if($row['Passing_Year'] == '1967') echo 'selected '; echo " >
                                1967    
                        </option>
                                                <option value='1966' "; if($row['Passing_Year'] == '1966') echo 'selected '; echo " >
                                1966    
                        </option>
                                                <option value='1965' "; if($row['Passing_Year'] == '1965') echo 'selected '; echo " >
                                1965    
                        </option>
                                                <option value='1964' "; if($row['Passing_Year'] == '1964') echo 'selected '; echo " >
                                1964    
                        </option>
                                                <option value='1963' "; if($row['Passing_Year'] == '1963') echo 'selected '; echo " >
                                1963    
                        </option>
                                                <option value='1962' "; if($row['Passing_Year'] == '1962') echo 'selected '; echo " >
                                1962    
                        </option>
                                                <option value='1961' "; if($row['Passing_Year'] == '1961') echo 'selected '; echo " >
                                1961    
                        </option>
                                                <option value='1960' "; if($row['Passing_Year'] == '1960') echo 'selected '; echo " >
                                1960    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
                                      
           <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Board/University</label>
                <div class='col-sm-5'>
                  
                    <select name='Board' id='Board'>

                    <option value='{$row['Board']}'>{$row['Board']}</option> 
                    <option value='Board of Intermediate and Secondary Education, Kohat' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Kohat') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Kohat    
            </option>
            <option value='Inter Board Committee of Chairman (IBCC) Islamabad' "; if($row['Board'] == 'Inter Board Committee of Chairman (IBCC) Islamabad') echo 'selected '; echo " >
                    Inter Board Committee of Chairman (IBCC) Islamabad    
            </option>
                                    <option value='Federal Board of Intermediate and Secondary Education, Islamabad' "; if($row['Board'] == 'Federal Board of Intermediate and Secondary Education, Islamabad') echo 'selected '; echo " >
                    Federal Board of Intermediate and Secondary Education, Islamabad    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Bahawalpur' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Bahawalpur') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Bahawalpur    
            </option>

                                    <option value='Board of Intermediate and Secondary Education, DG Khan' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, DG Khan') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, DG Khan    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Faisalabad' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Faisalabad') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Faisalabad    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Gujranwala' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Gujranwala') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Gujranwala    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Lahore' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Lahore') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Lahore    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Multan' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Multan') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Multan    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Rawalpindi' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Rawalpindi') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Rawalpindi    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Sahiwal' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Sahiwal') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Sahiwal    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Sargodha' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Sargodha') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Sargodha    
            </option>
                                    <option value='Aga Khan Educational Board, Karachi' "; if($row['Board'] == 'Aga Khan Educational Board, Karachi') echo 'selected '; echo " >
                    Aga Khan Educational Board, Karachi    
            </option>
                                    <option value='Board of Intermediate Education, Karachi' "; if($row['Board'] == 'Board of Intermediate Education, Karachi') echo 'selected '; echo " >
                    Board of Intermediate Education, Karachi    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Hyderabad' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Hyderabad') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Hyderabad    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Larkana' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Larkana') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Larkana    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Sukkur' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Sukkur') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Sukkur    
            </option>
                                    <option value='Board of Secondary Education, Karachi' "; if($row['Board'] == 'Board of Secondary Education, Karachi') echo 'selected '; echo " >
                    Board of Secondary Education, Karachi    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Abbottabad' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Abbottabad') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Abbottabad    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Bannu' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Bannu') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Bannu    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Dera Ismail Khan' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Dera Ismail Khan') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Dera Ismail Khan    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Malakand' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Malakand') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Malakand    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Mardan' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Mardan') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Mardan    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Peshawar' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Peshawar') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Peshawar    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Swat' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Swat') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Swat    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Quetta' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Quetta') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Quetta    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Turbat' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Turbat') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Turbat    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Zhob' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Zhob') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Zhob    
            </option>
                                    <option value='Board of Intermediate and Secondary Education, Mirpur' "; if($row['Board'] == 'Board of Intermediate and Secondary Education, Mirpur') echo 'selected '; echo " >
                    Board of Intermediate and Secondary Education, Mirpur    
            </option>
                                    <option value='KPK Board of Technical Education, Peshawar' "; if($row['Board'] == 'KPK Board of Technical Education, Peshawar') echo 'selected '; echo " >
                    KPK Board of Technical Education, Peshawar    
            </option>
                                    <option value='Punjab Board of Technical Education, Lahore' "; if($row['Board'] == 'Punjab Board of Technical Education, Lahore') echo 'selected '; echo " >
                    Punjab Board of Technical Education, Lahore    
            </option>
                                    <option value='Sindh Board of Technical Education, Karachi' "; if($row['Board'] == 'Sindh Board of Technical Education, Karachi') echo 'selected '; echo " >
                    Sindh Board of Technical Education, Karachi    
            </option>
            <option value='Pakistan Military Academy, Malakand' "; if($row['Board'] == 'Pakistan Military Academy, Malakand') echo 'selected '; echo " >
                    Pakistan Military Academy, Malakand    
                </option>
                <option value='0'>Other</option>
                
                <option value='{$row['Board']}'>{$row['Board']}</option>
                <option value='Kohat University of Science & Technology, Kohat' "; if($row['Board'] == 'Kohat University of Science & Technology, Kohat') echo 'selected '; echo " >
                    Kohat University of Science & Technology, Kohat    
            </option>
                                    <option value='University of Peshawar, Peshawar'"; if($row['Board'] == 'University of Peshawar, Peshawar') echo 'selected '; echo " >
                    University of Peshawar, Peshawar    
            </option>
                                    
                                    <option value='Frontier Women University, Peshawar' "; if($row['Board'] == 'Frontier Women University, Peshawar') echo 'selected '; echo " >
                    Frontier Women University, Peshawar    
            </option>
                                    <option value='Gomal University, D.I.Khan' "; if($row['Board'] == 'Gomal University, D.I.Khan') echo 'selected '; echo " >
                    Gomal University, D.I.Khan    
            </option>
                                    <option value='Hazara University, Dodhial, Mansahra' "; if($row['Board'] == 'Hazara University, Dodhial, Mansahra') echo 'selected '; echo " >
                    Hazara University, Dodhial, Mansahra    
            </option>
                                    <option value='Institute of Management Sciences (IMSciences), Peshawar' "; if($row['Board'] == 'Institute of Management Sciences (IMSciences), Peshawar') echo 'selected '; echo " >
                    Institute of Management Sciences (IMSciences), Peshawar    
            </option>
                                    <option value='Islamia College University, Peshawar, Peshawar' "; if($row['Board'] == 'Islamia College University, Peshawar, Peshawar') echo 'selected '; echo " >
                    Islamia College University, Peshawar, Peshawar    
            </option>
                                    <option value='Khyber Medical University, Peshawar' "; if($row['Board'] == 'Khyber Medical University, Peshawar') echo 'selected '; echo " >
                    Khyber Medical University, Peshawar    
            </option>
                                    <option value='NWFP Agriculture University, Peshawar' "; if($row['Board'] == 'NWFP Agriculture University, Peshawar') echo 'selected '; echo " >
                    NWFP Agriculture University, Peshawar    
            </option>
                                    <option value='NWFP University of Engineering & Technology, Peshawar' "; if($row['Board'] == 'NWFP University of Engineering & Technology, Peshawar') echo 'selected '; echo " >
                    NWFP University of Engineering & Technology, Peshawar    
            </option>
                                    
                                    <option value='University of Science & Technology Bannu, Bannu' "; if($row['Board'] == 'University of Science & Technology Bannu, Bannu') echo 'selected '; echo " >
                    University of Science & Technology Bannu, Bannu    
            </option>
                                    <option value='Abasyn University, Peshawar' "; if($row['Board'] == 'Abasyn University, Peshawar') echo 'selected '; echo " >
                    Abasyn University, Peshawar    
            </option>
                                    <option value='CECOS University of Information Technology and Emerging Sciences, Peshawar' "; if($row['Board'] == 'CECOS University of Information Technology and Emerging Sciences, Peshawar') echo 'selected '; echo " >
                    CECOS University of Information Technology and Emerging Sciences, Peshawar    
            </option>
                                    <option value='City University of Science & Information Technology, Peshawar' "; if($row['Board'] == 'City University of Science & Information Technology, Peshawar') echo 'selected '; echo " >
                    City University of Science & Information Technology, Peshawar    
            </option>
                                    <option value='Gandhara University, Peshawar' "; if($row['Board'] == 'Gandhara University, Peshawar') echo 'selected '; echo " >
                    Gandhara University, Peshawar    
            </option>
                                    <option value='Ghulam Ishaq Khan Institute of Engineering Sciences & Technology, Topi' "; if($row['Board'] == 'Ghulam Ishaq Khan Institute of Engineering Sciences & Technology, Topi') echo 'selected '; echo " >
                    Ghulam Ishaq Khan Institute of Engineering Sciences & Technology, Topi    
            </option>
                                    <option value='Northern University, Nowshera' "; if($row['Board'] == 'Northern University, Nowshera') echo 'selected '; echo " >
                    Northern University, Nowshera    
            </option>
                                    <option value='Preston University, Kohat' "; if($row['Board'] == 'Preston University, Kohat') echo 'selected '; echo " >
                    Preston University, Kohat    
            </option>
                                    <option value='Qurtaba University of Science & Information Technology, D.I.Khan' "; if($row['Board'] == 'Qurtaba University of Science & Information Technology, D.I.Khan') echo 'selected '; echo " >
                    Qurtaba University of Science & Information Technology, D.I.Khan    
            </option>
                                    <option value='Sarhad University of Science & Information Technology, Peshawar' "; if($row['Board'] == 'Sarhad University of Science & Information Technology, Peshawar') echo 'selected '; echo " >
                    Sarhad University of Science & Information Technology, Peshawar    
            </option>
                                    <option value='Balochistan University of Engineering and Technology, Khuzdar' "; if($row['Board'] == 'Balochistan University of Engineering and Technology, Khuzdar') echo 'selected '; echo " >
                    Balochistan University of Engineering and Technology, Khuzdar    
            </option>
                                    <option value='Balochistan University of Information Technology and Management Sciences, Quetta' "; if($row['Board'] == 'Balochistan University of Information Technology and Management Sciences, Quetta') echo 'selected '; echo " >
                    Balochistan University of Information Technology and Management Sciences, Quetta    
            </option>
                                    <option value='Bolan University of Medical & Health Sciences, Quetta, Quetta' "; if($row['Board'] == 'Bolan University of Medical & Health Sciences, Quetta, Quetta') echo 'selected '; echo " >
                    Bolan University of Medical & Health Sciences, Quetta, Quetta    
            </option>
                                    <option value='Lasbela University of Agriculture, Water & Marine Science, Lasbela' "; if($row['Board'] == 'Lasbela University of Agriculture, Water & Marine Science, Lasbela') echo 'selected '; echo " >
                    Lasbela University of Agriculture, Water & Marine Science, Lasbela    
            </option>
                                    <option value='Sardar Bahadur Khan Women University, Quetta' "; if($row['Board'] == 'Sardar Bahadur Khan Women University, Quetta') echo 'selected '; echo " >
                    Sardar Bahadur Khan Women University, Quetta    
            </option>
                                    <option value='University of Balochistan, Quetta' "; if($row['Board'] == 'University of Balochistan, Quetta') echo 'selected '; echo " >
                    University of Balochistan, Quetta    
            </option>
                                    <option value='Al-Hamd Islamic University, Quetta' "; if($row['Board'] == 'Al-Hamd Islamic University, Quetta') echo 'selected '; echo " >
                    Al-Hamd Islamic University, Quetta    
            </option>
                                    <option value='Iqra University, Quetta' "; if($row['Board'] == 'Iqra University, Quetta') echo 'selected '; echo " >
                    Iqra University, Quetta    
            </option>
                                    <option value='COMSATS Institute of Information Technology' "; if($row['Board'] == 'COMSATS Institute of Information Technology') echo 'selected '; echo " >
                    COMSATS Institute of Information Technology    
            </option>
                                    <option value='University of Karachi' "; if($row['Board'] == 'University of Karachi') echo 'selected '; echo " >
                    University of Karachi    
            </option>
                                    <option value='Quaid-I-Azam University' "; if($row['Board'] == 'Quaid-I-Azam University') echo 'selected '; echo " >
                    Quaid-I-Azam University    
            </option>
                                    <option value='0'>
                    Other    
            </option>
              </select>
                    
                </div>
              </div>
            </div>

          <div class='row edit-Board' style='display:none;'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Other Board/University</label>
                <div class='col-sm-5'>
                    <input type='text' name='custom-Board' id='edit-Board' class='form-control' value=\"{$row['Board']}\"/>
                </div>
            </div>
          
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Major (i.e. Subjects)</label>
                <div class='col-sm-5'>
                  
                    <select name='Subjects' id='Subjects'>
                        <option value='Science' "; if($row['Major'] == 'Science') echo 'selected '; echo " >
                                Science    
                        </option>
                         <option value='Arts' "; if($row['Major'] == 'Arts') echo 'selected '; echo " >
                                Arts    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>            
            
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value=\"{$row['Max_Marks']}\"/>
                </div>
            </div>
                                      
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' required='required'  name='ObtainedMarks' value=\"{$row['Obt_Marks']}\" onblur='checkMarks()' />
                </div>
            </div>
            </div>
                                
           <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Detailed Marks Certificate</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['DMC']}' class='form-img' alt='DMC File'>
                            <input type='file' class='form-control'  name='DMCPicture'  id='DMCPicture' />
                            
                            
                        </div>
                     
                
              </div>
            </div>
                                
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree/Original Certificate</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['Original_Certificate']}' class='form-img' alt='Original Certificate File'>
                            <input type='file' class='form-control' name='DegreePicture'  id='DegreePicture' value = ".$row['']."/>
                            <input type='hidden' class='form-control' name='DegreePictureFileIsAlreadyUploaded'/>
                            
                        </div>
                         
               

              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction</label>
                <div class='col-sm-5'>                  
                    <select name='Distinction' id='Distinction'>
                        <option value='No-Position'  "; if($row['Distinction'] == 'No-Position') echo 'selected '; echo " >No Position</option>
                        <option value='First-Position'  "; if($row['Distinction'] == 'First-Position') echo 'selected '; echo " >First Position</option>
                        <option value='Second-Position' "; if($row['Distinction'] == 'Second-Position') echo 'selected '; echo " >Second Position</option>
                        <option value='Third-Position' "; if($row['Distinction'] == 'Third-Position') echo 'selected '; echo " >Third Position</option>
                    </select>                    
                </div>
              </div>
            </div> 
            <div class='form-group' id='DistinctionCertGrp' style='display:  none'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction Certificate</label>
                          <div class='col-sm-5'>
                                  <input type='file' class='form-control'  name='DistinctionCert'  id='DistinctionCert'/>
                                <input type='hidden' class='form-control' name='DistinctionCertFileIsAlreadyUploaded' value='' />                            
                          </div>
                                        
              </div>
            </div>              
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'></label>
                <div class='col-sm-5'>
                                        <input type='submit' class='btn btn-success' name='submit_education' id='submit_education' />
                                    </div>
              </div>
            </div>              
        </div>
    </div>
</div>
</div><!-- /.widget-user -->
</div>
        </div>
        </section><!-- /.content -->

        <input type='hidden' name='reg_id' value='{$id}'>
        <input type='hidden' name='submit_education' value='0'>
        </form>                      <div class='col-md-12' id='form-msg'></div>
      
        <div class='row'>
                <div class='col-sm-12'>
                        <table class='table table-responsive'>
                                <tr>
                                        <th>Degree</th>
                                        <th>Passing Year</th>
                                        <th>Board</th>
                                        <th>Majore</th>
                                        <th>Max Marks</th>
                                        <th>Obtained Marks</th>
                                        <th>Distinction</th>
                                        <!-- <th>DMC</th>
                                        <th>Degree</th> -->
                                </tr>";
                                $res = mysqli_query($connection,
                                "SELECT * FROM tbl_qualification WHERE CID = '{$id}' ");
                                while($r = mysqli_fetch_array($res))
                                        echo 
                                        "<tr qual-detail='{$r['Degree']}' style='cursor:pointer'>
                                                <td class='dg'>{$r['Degree']}</td>
                                                <td class='py'>{$r['Passing_Year']}</td>
                                                <td class=>{$r['Board']}</td>
                                                <td>{$r['Major']}</td>
                                                <td>{$r['Max_Marks']}</td>
                                                <td>{$r['Obt_Marks']}</td>
                                                <td>{$r['Distinction']}</td>
                                                <!-- <td><img src='uploads/{$r['DMC']}' class='form-img' alt='No Image'></td>
                                                <td><img src='uploads/{$r['Original_Certificate']}' class='form-img' alt='No Image'></td> -->
                                        </tr>";

                        echo "</table>
                </div>
        </div>
      
      </div></div></div></div></div></div></div>
      
      <script>
                $('tr[qual-detail]').click(function() {
                        var detail = $(this).attr('qual-detail');
                        $('.modal-backdrop').remove();
                        $.post('api.php',
                        {
                                code_id: 3,
                                reg_id: sessionStorage.regID,
                                degree: detail
                        },
                        function(data, status){
                            $('#my-div').html(data);
                            $('#myModal2').modal('show');
                        });
                });
      </script>
      <script>
        $('#Board').change(function(){
                var id = $(this).val();
                if(id == 0){
                $('.edit-Board').css('display','block');
                } 
                else{
                $('.edit-Board').css('display','none');            
                }
        });
        
      
      </script>
      
      ";
    }
    else if ($code == 5)
    {
        $query_form = "SELECT * FROM tbl_admission_form where CID='$id'";
        $result_form = mysqli_query($connection,$query_form);

        $row_form = mysqli_fetch_array($result_form);

        $res = mysqli_query($connection, "SELECT * FROM tbl_courses");
        
        $courses = array();
        while($r = mysqli_fetch_array($res))
                $courses[$r['Course_ID']] =  $r['Course_Name'];

        $query_pre = "SELECT p.Course_ID Course_ID, c.Course_Name Course_Name FROM tbl_pre_req p JOIN tbl_courses c ON p.Course_ID = c.Course_ID  where CID='$id'";
        $result_pre = mysqli_query($connection,$query_pre);
        $pre_courses = array();
        while($r = mysqli_fetch_array($result_pre))
                $pre_courses[$r['Course_ID']] = $r['Course_Name'];
        // $row_pre = mysqli_fetch_array($result_pre);

        $query_audit = "SELECT p.Course_ID Course_ID, c.Course_Name Course_Name FROM tbl_audit_courses p JOIN tbl_courses c ON p.Course_ID = c.Course_ID where CID='{$id}'";
        $result_audit = mysqli_query($connection,$query_audit);
        $audit_courses = array();
        while($r = mysqli_fetch_array($result_audit))
                $audit_courses[$r['Course_ID']] = $r['Course_Name'];

        // $row_audit = mysqli_fetch_array($result_audit);
        echo "
        <div class='modal fade' id='myModal3' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>Upload Addmission Form </Strong></h3>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>
      <div class='panel-body' style='background-color:#ab9f9b24;'>
         
        
    <form id='my-form' enctype='multipart/form-data'>   
<div class='form-group'>
                <div class='row'>
                        <label class='control-label col-sm-2'>Upload Addmission Form</label>
                
                        <div class='col-sm-5'>
                                <img src='uploads/{$row_form['Upload_Form']}' class='form-img' alt='Original Certificate File'>
                            <input type='file' class='form-control' name='addmissionForm' id='addform'/>                           
                        </div>
                        <div class='col-sm-3'>
                                <input type='hidden' name='reg_id' value='{$id}'>
                                <input type='hidden' name='submit_addmissionForm' value='0'>
                                
                                <input type='submit' class='btn btn-success form-control' name='submit_addmissionForm' value ='Upload' id='addform'/>                           
                        </div>
                </div>
            </div>
            </form>
<hr>
<h3> Pre-Req Courses List<small></small>
          </h3><hr>
          
          <div class='col-sm-12'>
                <table class='table'>
                        <tr>
                                <th>Course ID</th>
                                <th>Course Name</th>
                                <th>Action</th>
                        </tr>";

                        foreach($pre_courses as $key => $val)
                                echo 
                                "<tr>
                                        <td>{$key}</td>
                                        <td>{$val}</td>
                                        <td><button class='pre-del btn btn-danger btn-sm' value='$key' onclick='DelPreCourse(this)'>Delete</button>
                                        </td>
                                </tr>";

                echo "</table>
          </div>
          
          <div class='form-group'>
              <div class='row'>
<label class='control-label col-sm-2'><span style='color:red'>*</span> Pre-Req Course</label>
<div class='col-sm-5'>
<select name='pre-course-list' id='pre-course-list' required class='form-control' > ";
foreach($courses as $key => $val)
        if(!array_key_exists($key, $pre_courses))
            echo "<option value='{$key}'>{$val}</option>";    
echo "</select>
</div>
<div class='col-sm-3'>
<input type='button' class='btn btn-success form-control' id='add-course' value='Add Course' onclick='AddCourse()'>
</div>
              </div>
              </div>
<hr>
<h3> Audit Courses List<small></small>
          </h3><hr>
          
          <div class='col-sm-12'>
                <table class='table'>
                        <tr>
                                <th>Course ID</th>
                                <th>Course Name</th>
                        </tr>";

                        foreach($audit_courses as $key => $val)
                                echo 
                                "<tr>
                                        <td>{$key}</td>
                                        <td>{$val}</td>
                                        <td><button class='audit-del btn btn-danger btn-sm' value='$key' onclick='DelAuditCourse(this)'>Delete</button>
                                        </td>
                                </tr>";

                echo "</table>
          <div class='form-group'>
              <div class='row'>
                 <label class='control-label col-sm-2'><span style='color:red'>*</span>Audit Course</label>
<div class='col-sm-5'>
<select name='audit-course-list' id='audit-course-list' required class='form-control' > ";
foreach($courses as $key => $val)
        if(!array_key_exists($key, $audit_courses))
            echo "<option value='{$key}'>{$val}</option>";    
echo "</select>
</div>
<div class='col-sm-3'>
<input type='button' class='btn btn-success form-control' id='audit-course' value='Add Course' onclick='AuditCourse()'>
</div>
              </div>
              </div>

<script>
        function AddCourse (){
                c = $('#pre-course-list').val();
                data = {
                        add_course: 1,
                        course: c,
                        reg_id: sessionStorage.regID
                }
                $('.modal-backdrop').hide();
                $.ajax({
                        url: './pi.php',
                        type: 'POST',
                        data: data,
                        dataType: 'text'
                }).done(function(data, status){
                        if($.trim(data) != 'FAILED'){
                                $.post('api.php',
                                {
                                        code_id: 5,
                                        reg_id: sessionStorage.regID
                                },
                                function(data, status){
                                        $('#my-div').html(data);
                                        $('#myModal3').modal('show');
                                }).fail(function(xhr, status, error){
                                        $.alert({
                                                title: 'FAILED!',
                                                content: 'Failed to Add Course!',
                                                type: 'red',
                                                theme: 'Material',
                                        });
                                });
                                $.alert({
                                        title: 'SUCCESS!',
                                        content: 'Course Added!',
                                        type: 'green',
                                        theme: 'Material',
                                });  
                        }
                        else
                                $.alert({
                                        title: 'FAILED!',
                                        content: 'Failed to Add Course!',
                                        type: 'red',
                                        theme: 'Material',
                                });        
                        
                        // $('.modal').modal('hide');
                        // $.alert()
                }).fail(function(status){
                        $.alert({
                                title: 'FAILED!',
                                content: 'Failed to Add Course!',
                                type: 'red',
                                theme: 'Material',
                        });
                });
        }

        function AuditCourse (){
                c = $('#audit-course-list').val();
                data = {
                        audit_course: 1,
                        course: c,
                        reg_id: sessionStorage.regID
                }
                $('.modal-backdrop').hide();
                $.ajax({
                        url: './pi.php',
                        type: 'POST',
                        data: data,
                        dataType: 'text'
                }).done(function(data, status){
                        if($.trim(data) != 'FAILED'){
                                $.post('api.php',
                                {
                                        code_id: 5,
                                        reg_id: sessionStorage.regID
                                },
                                function(data, status){
                                        $('#my-div').html(data);
                                        $('#myModal3').modal('show');
                                }).fail(function(xhr, status, error){
                                        $.alert({
                                                title: 'FAILED!',
                                                content: 'Failed to Add Course!',
                                                type: 'red',
                                                theme: 'Material',
                                        });
                                });
                                $.alert({
                                        title: 'SUCCESS!',
                                        content: 'Course Added!',
                                        type: 'green',
                                        theme: 'Material',
                                });  
                        }
                        else
                                $.alert({
                                        title: 'FAILED!',
                                        content: 'Failed to Add Course!',
                                        type: 'red',
                                        theme: 'Material',
                                });        
                        
                        // $('.modal').modal('hide');
                        // $.alert()
                }).fail(function(status){
                        $.alert({
                                title: 'FAILED!',
                                content: 'Failed to Add Course!',
                                type: 'red',
                                theme: 'Material',
                        });
                });
        }

        function DelPreCourse (e){
                c = $(e).val();
                data = {
                        del_pre_course: 1,
                        course: c,
                        reg_id: sessionStorage.regID
                }
                $('.modal-backdrop').hide();
                $.ajax({
                        url: './pi.php',
                        type: 'POST',
                        data: data,
                        dataType: 'text'
                }).done(function(data, status){
                        if($.trim(data) != 'FAILED'){
                                $.post('api.php',
                                {
                                        code_id: 5,
                                        reg_id: sessionStorage.regID
                                },
                                function(data, status){
                                        $('#my-div').html(data);
                                        $('#myModal3').modal('show');
                                }).fail(function(xhr, status, error){
                                        $.alert({
                                                title: 'FAILED!',
                                                content: 'Failed to Delete Course!',
                                                type: 'red',
                                                theme: 'Material',
                                        });
                                });
                                $.alert({
                                        title: 'SUCCESS!',
                                        content: 'Course Deleted!',
                                        type: 'green',
                                        theme: 'Material',
                                });  
                        }
                        else
                                $.alert({
                                        title: 'FAILED!',
                                        content: 'Failed to Delete Course!',
                                        type: 'red',
                                        theme: 'Material',
                                });        
                        
                        // $('.modal').modal('hide');
                        // $.alert()
                }).fail(function(status){
                        $.alert({
                                title: 'FAILED!',
                                content: 'Failed to Delete Course!',
                                type: 'red',
                                theme: 'Material',
                        });
                });
        }

        function DelAuditCourse (e){
                c = $(e).val();
                data = {
                        del_audit_course: 1,
                        course: c,
                        reg_id: sessionStorage.regID
                }
                $('.modal-backdrop').hide();
                $.ajax({
                        url: './pi.php',
                        type: 'POST',
                        data: data,
                        dataType: 'text'
                }).done(function(data, status){
                        if($.trim(data) != 'FAILED'){
                                $.post('api.php',
                                {
                                        code_id: 5,
                                        reg_id: sessionStorage.regID
                                },
                                function(data, status){
                                        $('#my-div').html(data);
                                        $('#myModal3').modal('show');
                                }).fail(function(xhr, status, error){
                                        $.alert({
                                                title: 'FAILED!',
                                                content: 'Failed to Delete Course!',
                                                type: 'red',
                                                theme: 'Material',
                                        });
                                });
                                $.alert({
                                        title: 'SUCCESS!',
                                        content: 'Course Deleted!',
                                        type: 'green',
                                        theme: 'Material',
                                });  
                        }
                        else
                                $.alert({
                                        title: 'FAILED!',
                                        content: 'Failed to Delete Course!',
                                        type: 'red',
                                        theme: 'Material',
                                });        
                        
                        // $('.modal').modal('hide');
                        // $.alert()
                }).fail(function(status){
                        $.alert({
                                title: 'FAILED!',
                                content: 'Failed to Delete Course!',
                                type: 'red',
                                theme: 'Material',
                        });
                });
        }
</script>
";  
    }
    else if($code == 6){
        $result = mysqli_query($connection, "SELECT * FROM tbl_gat WHERE CID = '$id'") or die(mysqli_error($connection)); 

        $row = mysqli_fetch_array($result);
    echo "<div class='modal fade' id='myModal4' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>GAT General/Subject Details</Strong></h3>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>
      <div class='panel-body' style='background-color:#ab9f9b24;'>
         <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
        
    
          <hr><div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> GAT Test Date</label>
                <div class='col-sm-5'>
                    <input type='date' class='form-control' name='dob' value=\"{$row['Test_Date']}\" id='dob' required  />
                </div>  
              </div>
            </div>
<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value=\"{$row['Max_Marks']}\" />
                </div>
            </div>
                         <br>             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' value=\"{$row['Obt_Marks']}\" name='ObtainedMarks' onblur ='checkMarks()' required />
                </div>
            </div>
<br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload GAT Result</label>
                
                        <div class='col-sm-5'>
                            <img class='form-img' src='uploads/{$row['Result_Card']}' alt='GAT Result'>
                            <input type='file' class='form-control' name='addmissionForm'  id='addmissionForm'/>                           
                        </div>

              </div>
            </div>
            <input type='submit' style='background-color:green;color:white' class='btn' name='submit_gate' value='Add GAT Result' id='submit_gate' >
            <input type='hidden' name='reg_id' value='{$id}' />
            <input type='hidden' name='submit_gate' value='0'>
            </form>                      <div class='col-md-12' id='form-msg'></div></div></div></div></div></div></div> 
";
}
else if ($code == 7){
        $result = mysqli_query($connection, "SELECT * FROM tbl_degree_completion WHERE CID = '$id'") or die(mysqli_error($connection));
        $row = mysqli_fetch_array($result);
        $hec_t = !empty($row['Transcript_File_Back']) ? 'checked' : '';
        $hec_d = !empty($row['Final_Degree_File_Back']) ? 'checked' : '';
        echo " <div class='modal fade' id='myModal13' role='dialog'>
        <div class='modal-dialog'>
        
        <!-- Modal content-->
        <div class='container'>
        
        <div class='col-lg-8 col-md-8 col-sm-8 '>
        
        <div class='panel panel-default'>
        <div class='panel-heading' style='background-color:#983415;border:none' >
        <h3 style='text-align:center;color:white;'><Strong>Degree Completion </Strong></h3>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top: -50px;color: white;\">×</button>
        </div>
        
        <div class='panel-body' style='background-color:#ab9f9b24;'>
        <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
        <h3> <small></small>
        </h3><hr><div class='form-group'>
        <div class='row'>
        <label class='control-label col-sm-2'> <span style='color:red'>*</span> Degree Completion Date</label>
        <div class='col-sm-5'>
        <input type='date' class='form-control' name='dob' value=\"{$row['Date']}\" id='dob' required />
        </div> 
        </div>
        </div>
        
        <br> 
        <div class='form-group'>
        <div class='row'>
        <label class='control-label col-sm-2' for='hec_t'>HEC Attested Transcript?
        </label>
        <div class='col-sm-5'><input type='checkbox' name='hec_t' id='hec_t' {$hec_t}></div>
        
        </div>
        </div>
        <br> 
        <div class='form-group'>
        <div class='row'>
        <label class='control-label col-sm-2'>Upload Transcript File</label>
        <div class='col-sm-5'>
        <img src='uploads/{$row['Transcript_File']}' class='form-img' alt='No Image'>
        <input type='file' class='form-control' name='trnsfile' id='addform' value=''/> 
        </div>
        </div>
        </div>
        <br> 
        <div class='form-group' id='t_back' style='display:"; echo empty($row['Transcript_File_Back']) ? "none" : "block"; echo "'>
        <div class='row'>
        <label class='control-label col-sm-2'>Upload Transcript File Back</label>
        <div class='col-sm-5'>
        <img src='uploads/{$row['Transcript_File_Back']}' class='form-img' alt='No Image'>
        <input type='file' class='form-control' name='trnsfile_back' id='trnsfile_back' value=''/> 
        </div>
        </div>
        </div>
        <br> 
        <div class='form-group'>
        <div class='row'>
        <label class='control-label col-sm-2' for='hec_d'>HEC Attested Final Degree?
        </label>
        <div class='col-sm-5'><input type='checkbox' name='hec_d' id='hec_d' {$hec_d}></div>
        </div>
        </div>
        <br> 
        <div class='form-group'>
        <div class='row'>
        <label class='control-label col-sm-2'>Upload Final Degree File</label>
        
        <div class='col-sm-5'>
        <img src='uploads/{$row['Final_Degree_File']}' class='form-img' alt='No Image'>
        <input type='file' class='form-control' name='addmissionForm' id='addform'/> 
        </div>
        </div>
        </div> 
        <div class='form-group' id='d_back' style='display:"; echo empty($row['Final_Degree_File_Back']) ? "none" : "block"; echo "'>
        <div class='row'>
        <label class='control-label col-sm-2'>Upload Final Degree File</label>
        
        <div class='col-sm-5'>
        <img src='uploads/{$row['Final_Degree_File_Back']}' class='form-img' alt='No Image'>
        <input type='file' class='form-control' name='addmissionForm_back' id='addform_back'/> 
        </div>
        </div>
        </div>
        
        
        <input type='submit' style='background-color:green;color:white' class='btn' name='submit_degree' value='Add Dgree Detail' id='submit_degree' >
        
        <input type='hidden' name='reg_id' value='{$id}'>
        <input type='hidden' name='submit_degree' value='0'>
        </form>
        </div></div></div></div></div></div>
       
        <script>
        $('#hec_t').click(function(){
        if(this.checked){
        $('#t_back').show();
        }else{
        $('#t_back').hide();
        }
        });
        $('#hec_d').click(function(){
        if(this.checked){
        $('#d_back').show();
        }else{
        $('#d_back').hide();
        }
        });
        </script>
        ";
       }
       
       
else if ($code == 8){
echo "<h3> Audit Courses List<small></small>
          </h3><hr><div class='form-group'>
              <div class='row'>
                 <label class='control-label col-sm-2'><span style='color:red'>*</span>Audit Course</label>
<div class='col-sm-5'>
<input type='text' class='form-control' data-toggle='popover' data-trigger='focus' title='Important Message' data-content='' value='Computer Architecture' name='auditCourse' id='auditCourse' required /> 
</div>
<div class='col-sm-5'>
<a class='btn bg-blue' href='#'>Add Course</a>
</div>
              </div>
              </div>
";
}
else if($code == 9){
    if(!isset($_POST['semester']))
         $result = mysqli_query($connection, "SELECT * FROM tbl_transcript_details WHERE CID = '$id'") or die(mysqli_error($connection)); 
    else
         $result = mysqli_query($connection, "SELECT * FROM tbl_transcript_details WHERE CID = '$id' AND Semester_Num = '{$_POST['semester']}'") or die(mysqli_error($connection)); 
    $row = mysqli_fetch_array($result);

    echo "<div class='modal fade' id='myModal5' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>Transcript Details</Strong></h3>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>
      <div class='panel-body' style='background-color:#ab9f9b24;'>
      <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
         
    <hr>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>SEMESTER</label>
                <div class='col-sm-4'>                  
                    <select name='semester' id='semester' class='form-control'>
            <option value='FALL' "; if(substr($row['Semester_Num'], 0,-5) == 'FALL') echo 'selected '; echo " >FALL</option>
            <option value='SPRING' "; if(substr($row['Semester_Num'], 0,-5) == 'SPRING') echo 'selected '; echo " >SPRING</option>
            <option value='SUMMER' "; if(substr($row['Semester_Num'], 0,-5) == 'SUMMER') echo 'selected '; echo " >SUMMER</option>
                    </select>                    
                </div>
        <div class='col-sm-5'>
                   <select name='YearOfPassing' id='YearOfPassing' class='form-control'>
                        <option value='2017' "; if(substr($row['Semester_Num'], -4) == '2017') echo 'selected '; echo " >
                                2017</option>
                        <option value='2016' "; if(substr($row['Semester_Num'], -4) == '2016') echo 'selected '; echo " >
                                2016    
                        </option>
                        <option value='2015' "; if(substr($row['Semester_Num'], -4) == '2015') echo 'selected '; echo " >
                                2015    
                        </option>
                        <option value='2014' "; if(substr($row['Semester_Num'], -4) == '2014') echo 'selected '; echo " >
                                2014    
                        </option>
                        <option value='2013' "; if(substr($row['Semester_Num'], -4) == '2013') echo 'selected '; echo " >
                                2013    
                        </option>
                        <option value='2012' "; if(substr($row['Semester_Num'], -4) == '2012') echo 'selected '; echo " >
                                2012    
                        </option>
                        <option value='2011' "; if(substr($row['Semester_Num'], -4) == '2011') echo 'selected '; echo " >
                                2011    
                        </option>
                        <option value='2010' "; if(substr($row['Semester_Num'], -4) == '2010') echo 'selected '; echo " >
                                2010    
                        </option>
                        <option value='2009' "; if(substr($row['Semester_Num'], -4) == '2009') echo 'selected '; echo " >
                                2009    
                        </option>
                        <option value='2008' "; if(substr($row['Semester_Num'], -4) == '2008') echo 'selected '; echo " >
                                2008    
                        </option>
                        <option value='2007' "; if(substr($row['Semester_Num'], -4) == '2007') echo 'selected '; echo " >
                                2007    
                        </option>
                                                <option value='2006' "; if(substr($row['Semester_Num'], -4) == '2006') echo 'selected '; echo " >
                                2006    
                        </option>
                                                <option value='2005' "; if(substr($row['Semester_Num'], -4) == '2005') echo 'selected '; echo " >
                                2005    
                        </option>
                                                <option value='2004' "; if(substr($row['Semester_Num'], -4) == '2004') echo 'selected '; echo " >
                                2004    
                        </option>
                                                <option value='2003' "; if(substr($row['Semester_Num'], -4) == '2003') echo 'selected '; echo " >
                                2003    
                        </option>
                                                <option value='2002' "; if(substr($row['Semester_Num'], -4) == '2002') echo 'selected '; echo " >
                                2002    
                        </option>
                                                <option value='2001' "; if(substr($row['Semester_Num'], -4) == '2001') echo 'selected '; echo " >
                                2001    
                        </option>
                                                <option value='2000' "; if(substr($row['Semester_Num'], -4) == '2000') echo 'selected '; echo " >
                                2000    
                        </option>
                                                <option value='1999' "; if(substr($row['Semester_Num'], -4) == '1999') echo 'selected '; echo " >
                                1999    
                        </option>
                                                <option value='1998' "; if(substr($row['Semester_Num'], -4) == '1998') echo 'selected '; echo " >
                                1998    
                        </option>
                                                <option value='1997' "; if(substr($row['Semester_Num'], -4) == '1997') echo 'selected '; echo " >
                                1997    
                        </option>
                                                <option value='1996' "; if(substr($row['Semester_Num'], -4) == '1996') echo 'selected '; echo " >
                                1996    
                        </option>
                                                <option value='1995' "; if(substr($row['Semester_Num'], -4) == '1995') echo 'selected '; echo " >
                                1995    
                        </option>
                                                <option value='1994' "; if(substr($row['Semester_Num'], -4) == '1994') echo 'selected '; echo " >
                                1994    
                        </option>
                                                <option value='1993' "; if(substr($row['Semester_Num'], -4) == '1993') echo 'selected '; echo " >
                                1993    
                        </option>
                                                <option value='1992' "; if(substr($row['Semester_Num'], -4) == '1992') echo 'selected '; echo " >
                                1992    
                        </option>
                                                <option value='1991' "; if(substr($row['Semester_Num'], -4) == '1991') echo 'selected '; echo " >
                                1991    
                        </option>
                                                <option value='1990' "; if(substr($row['Semester_Num'], -4) == '1990') echo 'selected '; echo " >
                                1990    
                        </option>
        </select>
        </div>
              </div>
            </div>  

        <div class='row'>
                <div class='col-sm-12'>
                        <table class='table table-bordered no-hover'>
                                <thead>
                                        <tr>
                                                <th>Course</th>
                                                <th></th>
                                        </tr>   
                                </thead>
                                <tbody id='tbl_courses'>";
                                        $cno = 0;
                                        $rs = mysqli_query($connection, "SELECT * FROM tbl_transcript_course WHERE CID = '$id' AND Semester_Num = '{$row['Semester_Num']}'");
                                        while($r = mysqli_fetch_array($rs)){
                                                echo "
                                                <tr course='{$r['Course']}'>
                                                        <td>{$r['Course']}</td>
                                                        <td><button class='btn btn-primary delete-btn' value='{$r['Course']}'   >Delete</button></td>
                                        
                                                </tr>";
                                                $cno++;
                                        }
                                echo "
                                </tbody>
                        </table>
                </div>
        </div>";
        if($cno < 3){
                echo "
                <div class='form-group' id='new-course'>
                        <div class='row'>
                                        <label class='control-label col-sm-2'> <span style='color:red'></span> New Course</label>
                                        <div class='col-sm-5'>
                                                <input type='text' class='form-control' name='course' value='' id='course' "; if($cno == 0) echo "required"; echo " />
                                        </div>
                                        <div class='col-sm-2'>
                                                <input type='button' class='form-control btn btn-primary' name='course-add' value='Add' id='course-add' required />
                                        </div>  
                        </div>
                </div>";
        }
        echo "<div class='form-group'>
              <div class='row'>
                        <label class='control-label col-sm-2'> <span style='color:red'>*</span> Result Declaration Date</label>
                        <div class='col-sm-5'>
                                <input type='date' class='form-control' name='dob' value='{$row['Result_Date']}' id='dob' required value=\"{$row['Result_Date']}\"/>
                        </div>  
              </div>
        </div>
<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max CGPA</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' value='4.0' readonly/>
                </div>
            </div>
                         <br>             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained CGPA</label>
                <div class='col-sm-5'>
                    <input type='number' id='Obtainedcgpa' step='any' class='form-control' value=\"{$row['Obt_CGPA']}\" name='Obtainedcgpa' onblur='checkCGPA()' required/>
                </div>
            </div>
<br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload Result Card</label>
                
                        <div class='col-sm-5'>
                        <img class='form-img' src='uploads/{$row['Result_Card']}' alt='Transcript File'>
                            <input type='file' class='form-control' name='addmissionForm' id='addform'/>                           
                        </div>
                         
               
              </div>
            </div><input type='submit' style='background-color:green;color:white' class='btn' name='submit_transcript' value='Add/Update Result' id='submit_transcript' >
            <input type='hidden' name='reg_id' value='{$id}'>
            <input type='hidden' name='submit_transcript' value='0'>
            </form>                      
            <div class='col-md-12' id='form-msg'></div>
            
            <div class='row'>
            <div class='col-sm-12'>
                    <table class='table table-responsive'>
                            <tr>
                                    <th>Semester</th>
                                    <th>Courses</th>
                                    <th>Result Date</th>
                                    <th>Max CGPA</th>
                                    <th>Obtained CGPA</th>
                                    <th>Result Card</th>
                            </tr>";
                            $res = mysqli_query($connection,
                            "SELECT * FROM tbl_transcript_details WHERE CID = '{$id}' ");
                            $res2 = mysqli_query($connection,
                            "SELECT * FROM tbl_transcript_course WHERE CID = '{$id}' ");
                            $carr = array();
                            while($r2 = mysqli_fetch_array($res2))
                                $carr[] = $r2;
                            while($r = mysqli_fetch_array($res)){
                                    echo 
                                    "<tr semester-detail='{$r['Semester_Num']}' style='cursor:pointer'>
                                            <td>{$r['Semester_Num']}</td>
                                            <td><ol>";
                                                    foreach($carr as $r2)
                                                        if($r2['Semester_Num'] == $r['Semester_Num'])
                                                                echo '<li>' . $r2['Course'] . "</li>";
                                            echo "
                                            </ol></td>
                                            <td>{$r['Result_Date']}</td>
                                            <td>{$r['Max_CGPA']}</td>
                                            <td>{$r['Obt_CGPA']}</td>
                                            <td><img class='form-img' src='uploads/{$r['Result_Card']}' alt='No Result Card'></td>
                                    </tr>";
                            }

                    echo "</table>
            </div>
    </div>

            </div></div></div></div></div></div>
            
                <script>
                        var cno = "; echo $cno; echo ";
                        $('tr[semester-detail]').click(function() {
                                var detail = $(this).attr('semester-detail');
                                $('.modal-backdrop').remove();
                                $.post('api.php',
                                {
                                        code_id: 9,
                                        reg_id: sessionStorage.regID,
                                        semester: detail
                                },
                                function(data, status){
                                        $('#my-div').html(data);
                                        $('#myModal5').modal('show');
                                });
                        });

                        $('#course-add').click(function(){
                                var semester = $('#semester').val();
                                var year = $('#YearOfPassing').val();
                                var semesterno = semester + ' ' + year;
                                var course = $('#course').val();
                                if($.trim(course) == '')
                                        return;
                                // alert(sessionStorage.regID);

                                $.post('pi.php',
                                {
                                        transcript_course: 1,
                                        reg_id: sessionStorage.regID,
                                        sem_no: semesterno,
                                        course: course,
                                },
                                function(data, status){
                                        if(data == 'SUCCESS'){
                                                $('#tbl_courses').append(\"<tr course=\" + course + \"><td>\" + course + \"</td><td><button class='btn btn-primary delete-btn' value='\" + course + \"' onclick='delClick(event)'>Delete</button></td></tr>\");
                                                cno++;
                                                if(cno == 3) {
                                                        $('#new-course').hide();
                                                        $('#course').val('');
                                                }
                                        }
                                }).fail(function(xhr, status, error){
                                        
                                });
                        });
                        function delClick(e){
                                e.preventDefault();
                                var semester = $('#semester').val();
                                var year = $('#YearOfPassing').val();
                                var semesterno = semester + ' ' + year;
                                var course = e.target.value;
                                
                                alert(course);

                                $.post('pi.php',
                                {
                                        transcript_course_delete: 1,
                                        reg_id: sessionStorage.regID,
                                        sem_no: semesterno,
                                        course: course,
                                },
                                function(data, status){
                                        console.log(data)
                                        if(data == 'SUCCESS'){
                                                $('tr[course=' + course + ']').remove();
                                                cno--;
                                                if(cno == 0) 
                                                        $('#course').attr('required', '');
                                        }
                                }).fail(function(xhr, status, error){
                                        
                                });
                        }
                        $('.delete-btn').click(delClick)
                </script>
";
}
else if($code == 10){
    echo "<h3> Second Semester Result <small></small>
          </h3><hr><div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>SEMESTER</label>
                <div class='col-sm-2'>                  
                    <select name='semester' id='sem'>
            <option value='FALL'>FALL</option>
                        <option value='SPRING'>SPRING</option>
            <option value='SUMMER' >SUMMER</option>
                    </select>                    
                </div>
        <div class='col-sm-5'>
                   <select name='YearOfPassing'>
                                                <option value='2017' >
                                2017    
                        </option>
                                                <option value='2016' >
                                2016    
                        </option>
                                                <option value='2015' >
                                2015    
                        </option>
                                                <option value='2014' >
                                2014    
                        </option>
                                                <option value='2013' >
                                2013    
                        </option>
                                                <option value='2012' >
                                2012    
                        </option>
                                                <option value='2011' >
                                2011    
                        </option>
                                                <option value='2010' >
                                2010    
                        </option>
                                                <option value='2009' >
                                2009    
                        </option>
                                                <option value='2008' >
                                2008    
                        </option>
                                                <option value='2007' >
                                2007    
                        </option>
                                                <option value='2006' >
                                2006    
                        </option>
                                                <option value='2005' >
                                2005    
                        </option>
                                                <option value='2004' >
                                2004    
                        </option>
                                                <option value='2003' >
                                2003    
                        </option>
                                                <option value='2002' >
                                2002    
                        </option>
                                                <option value='2001' >
                                2001    
                        </option>
                                                <option value='2000' >
                                2000    
                        </option>
                                                <option value='1999' >
                                1999    
                        </option>
                                                <option value='1998' >
                                1998    
                        </option>
                                                <option value='1997' >
                                1997    
                        </option>
                                                <option value='1996' >
                                1996    
                        </option>
                                                <option value='1995' >
                                1995    
                        </option>
                                                <option value='1994' >
                                1994    
                        </option>
                                                <option value='1993' >
                                1993    
                        </option>
                                                <option value='1992' >
                                1992    
                        </option>
                                                <option value='1991' >
                                1991    
                        </option>
                                                <option value='1990' >
                                1990    
                        </option>
        </select>
        </div>
              </div>
            </div> <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Result Declaration Date</label>
                <div class='col-sm-5'>
                    <input type='date' class='form-control' name='dob' value='1984-08-10' id='dob' required  />
                </div>  
              </div>
            </div>
<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max CGPA</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value='4.0'  />
                </div>
            </div>
                         <br>             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained GPA</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' required='required' value='' name='ObtainedMarks'/>
                </div>
            </div>
<br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload Result Card</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control' name='addmissionForm'  id='addform'/>                           
                        </div>
                         
               
              </div>
            </div><a class='btn bg-blue' href='#'>Add Result</a>
";
}
else if($code == 11){
    echo "<h3> Third Semester Result <small></small>
          </h3><hr><div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>SEMESTER</label>
                <div class='col-sm-2'>                  
                    <select name='semester' id='sem'>
            <option value='FALL'>FALL</option>
                        <option value='SPRING'>SPRING</option>
            <option value='SUMMER' >SUMMER</option>
                    </select>                    
                </div>
        <div class='col-sm-5'>
                   <select name='YearOfPassing'>
                                                <option value='2017' >
                                2017    
                        </option>
                                                <option value='2016' >
                                2016    
                        </option>
                                                <option value='2015' >
                                2015    
                        </option>
                                                <option value='2014' >
                                2014    
                        </option>
                                                <option value='2013' >
                                2013    
                        </option>
                                                <option value='2012' >
                                2012    
                        </option>
                                                <option value='2011' >
                                2011    
                        </option>
                                                <option value='2010' >
                                2010    
                        </option>
                                                <option value='2009' >
                                2009    
                        </option>
                                                <option value='2008' >
                                2008    
                        </option>
                                                <option value='2007' >
                                2007    
                        </option>
                                                <option value='2006' >
                                2006    
                        </option>
                                                <option value='2005' >
                                2005    
                        </option>
                                                <option value='2004' >
                                2004    
                        </option>
                                                <option value='2003' >
                                2003    
                        </option>
                                                <option value='2002' >
                                2002    
                        </option>
                                                <option value='2001' >
                                2001    
                        </option>
                                                <option value='2000' >
                                2000    
                        </option>
                                                <option value='1999' >
                                1999    
                        </option>
                                                <option value='1998' >
                                1998    
                        </option>
                                                <option value='1997' >
                                1997    
                        </option>
                                                <option value='1996' >
                                1996    
                        </option>
                                                <option value='1995' >
                                1995    
                        </option>
                                                <option value='1994' >
                                1994    
                        </option>
                                                <option value='1993' >
                                1993    
                        </option>
                                                <option value='1992' >
                                1992    
                        </option>
                                                <option value='1991' >
                                1991    
                        </option>
                                                <option value='1990' >
                                1990    
                        </option>
        </select>
        </div>
              </div>
            </div> <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Result Declaration Date</label>
                <div class='col-sm-5'>
                    <input type='date' class='form-control' name='dob' value='1984-08-10' id='dob' required  />
                </div>  
              </div>
            </div>
<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max CGPA</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value='4.0'  />
                </div>
            </div>
                         <br>             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained GPA</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' required='required' value='' name='ObtainedMarks'/>
                </div>
            </div>
<br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload Result Card</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control' name='addmissionForm'  id='addform'/>                           
                        </div>
                         
               
              </div>
            </div><a class='btn bg-blue' href='#'>Add Result</a>
";
}
else if($code == 12){
    echo "<h3> Forth Semester Result <small></small>
          </h3><hr><div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>SEMESTER</label>
                <div class='col-sm-2'>                  
                    <select name='semester' id='sem'>
            <option value='FALL'>FALL</option>
                        <option value='SPRING'>SPRING</option>
            <option value='SUMMER' >SUMMER</option>
                    </select>                    
                </div>
        <div class='col-sm-5'>
                   <select name='YearOfPassing'>
                                                <option value='2017' >
                                2017    
                        </option>
                                                <option value='2016' >
                                2016    
                        </option>
                                                <option value='2015' >
                                2015    
                        </option>
                                                <option value='2014' >
                                2014    
                        </option>
                                                <option value='2013' >
                                2013    
                        </option>
                                                <option value='2012' >
                                2012    
                        </option>
                                                <option value='2011' >
                                2011    
                        </option>
                                                <option value='2010' >
                                2010    
                        </option>
                                                <option value='2009' >
                                2009    
                        </option>
                                                <option value='2008' >
                                2008    
                        </option>
                                                <option value='2007' >
                                2007    
                        </option>
                                                <option value='2006' >
                                2006    
                        </option>
                                                <option value='2005' >
                                2005    
                        </option>
                                                <option value='2004' >
                                2004    
                        </option>
                                                <option value='2003' >
                                2003    
                        </option>
                                                <option value='2002' >
                                2002    
                        </option>
                                                <option value='2001' >
                                2001    
                        </option>
                                                <option value='2000' >
                                2000    
                        </option>
                                                <option value='1999' >
                                1999    
                        </option>
                                                <option value='1998' >
                                1998    
                        </option>
                                                <option value='1997' >
                                1997    
                        </option>
                                                <option value='1996' >
                                1996    
                        </option>
                                                <option value='1995' >
                                1995    
                        </option>
                                                <option value='1994' >
                                1994    
                        </option>
                                                <option value='1993' >
                                1993    
                        </option>
                                                <option value='1992' >
                                1992    
                        </option>
                                                <option value='1991' >
                                1991    
                        </option>
                                                <option value='1990' >
                                1990    
                        </option>
        </select>
        </div>
              </div>
            </div> 

<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Result Declaration Date</label>
                <div class='col-sm-5'>
                    <input type='date' class='form-control' name='dob' value='1984-08-10' id='dob' required  />
                </div>  
              </div>
            </div>
<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max CGPA</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value='4.0'  />
                </div>
            </div>
                         <br>             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained GPA</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' required='required' value='' name='ObtainedMarks'/>
                </div>
            </div>
<br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload Result Card</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control' name='addmissionForm'  id='addform'/>                           
                        </div>
                         
               
              </div>
            </div><a class='btn bg-blue' href='#'>Add Result</a>
";
}
else if($code == 13){
        $result = mysqli_query($connection, "SELECT * FROM tbl_comprehensive WHERE CID = '$id'") or die(mysqli_error($connection)); 
        $row = mysqli_fetch_array($result);
        $status[0] = 'Fail';
        $status[1] = 'Pass';
        $paper = 1;
        for($i = 1; $i <= 6; ++$i)
        {
                if($row['Title' . $i])
                {
                        $paper = $i;
                        break;
                }
        } 
        if(isset($_POST['paper']))
                $paper = $_POST['paper'];
       
        echo "<div class='modal fade' id='myModal6' role='dialog'>
        <div class='modal-dialog'>
        
        <!-- Modal content-->
        <div class='container'>
                       
                        <div class='col-lg-8 col-md-8 col-sm-8 '>
        
        <div class='panel panel-default'>
        <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Comprehensive Examination Result </Strong></h3>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top: -50px;color: white;\">×</button>
        </div>
        <div class='panel-body' style='background-color:#ab9f9b24;'>
                        <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
               
        <hr><div class='form-group'>
        <div class='row'>
        <label class='control-label col-sm-2'> <span style='color:red'>*</span> Date Exam Conducted</label>
        <div class='col-sm-5'>
        <input type='date' class='form-control' name='dob' value=\"{$row['Exam_Date']}\" id='dob' required />
        </div> 
        </div>
        </div>
     
        <div class='form-group'>
        <div class='row'>
        <label class='control-label col-sm-2'>Upload Comprehensive Result</label>
        
        <div class='col-sm-5'>
        <img src='uploads/{$row['Comprehensive_Result']}' class='form-img' alt='Result File'>
        <input type='file' class='form-control' name='addmissionForm' id='addform'/> 
        </div>
        
        
        </div>
        </div>
   <hr>
        <div class='form-group'>
        <div class='row'>
        <label class='control-label col-sm-2'>Attempt</label>
        <div class='col-sm-5'>
        <input type='number' class='form-control' name='abcdef' value=\"{$row['attempt']}\" id='abcdef' disabled />
        </div> 
        </div>
        </div>
        <br>
        
        <div class='form-group'>
                <div class='row'>
                        <label class='control-label col-sm-2'>Title</label>
                        
                        <div class='col-sm-5'>
                                <select name='title' id='title'>
                                        <option value='1' "; if($paper == 1) echo "selected"; echo ">Paper 1</option>
                                        <option value='2' "; if($paper == 2) echo "selected"; echo ">Paper 2</option>
                                        <option value='3' "; if($paper == 3) echo "selected"; echo ">Paper 3</option>
                                        <option value='4' "; if($paper == 4) echo "selected"; echo ">Paper 4</option>
                                        <option value='5' "; if($paper == 5) echo "selected"; echo ">Paper 5</option>
                                        <option value='6' "; if($paper == 6) echo "selected"; echo ">Paper 6</option>
                                </select>
                        </div>
                </div>        
        </div>
        <div class='form-group'>
                <div class='row'>
                        <label class='control-label col-sm-2'>Question Paper</label>
                        
                        <div class='col-sm-5'>
                                <img class='form-img' src='uploads/{$row['Paper' . $paper]}' alt='Paper1 File'>
                                <input type='file' class='form-control' name='paper' id='addform'/>
                        </div>
                </div> 

        </div>

        <div class='form-group'>
                <div class='row'>
                        <label class='control-label col-sm-2'>Graded Paper</label>
                        
                        <div class='col-sm-5'>
                                <img class='form-img' src='uploads/{$row['Solution' . $paper]}' alt='Solution1 File'>
                                <input type='file' class='form-control' name='solution' id='addform'/> 
                        </div>
                </div>        
        </div>

        <div class='form-group'>
                <div class='row'>
                        <label class='control-label col-sm-2'>Result</label>
                        
                        <div class='col-sm-5'>
                                <select name='result'>
                                        <option value='1'"; if($row['Result' . $paper] == 1) echo "selected"; echo">Pass</option>
                                        <option value='0'"; if($row['Result' . $paper] == 0) echo "selected"; echo">Fail</option>
                                </select>
                        </div>
                </div>        
        </div>"
        ;
       
        if($row['attempt'] == 1 && (($row['Title1'] && !$row['Result1']) ||($row['Title2'] && !$row['Result2']) ||($row['Title3'] && !$row['Result3']) ||($row['Title4'] && !$row['Result4']) ||($row['Title5'] && !$row['Result5']) ||($row['Title6'] && !$row['Result6'])))
        echo "<div class='form-group row '>
        <label class='col-sm-5'for='att_chk'>Are you sure to add second attempt?</label>
        <input class='col-sm-2' type='checkbox' name='add2attempt' id='att_chk'>
        <input class='col-sm-3 btn btn-primary ' type='submit' name='addatt' value='Add Second Attempt'>
        </div>";
       
        echo "<input type='submit' style='background-color:green;color:white' class='btn' name='submit_Comprehensive' value='Add Result' id='submit_Comprehensive' >
       
        <input type='hidden' name='reg_id' value='{$id}'>
        <input type='hidden' name='submit_Comprehensive' value='0'>
       
        </form>
        
        <div class='row'>
                <div class='col-sm-12'>
                        <table class='table table-bordered'>
                                <thead>
                                        <tr>
                                                <th>Title</th>
                                                <th>Paper</th>
                                                <th>Solution</th>
                                                <th>Result</th>
                                                <th></th>
                                        </tr>
                                </thead>
                                <tbody>";
                                        for($i = 1; $i <= 6; ++$i)
                                        {
                                                if($row['Title' . $i])
                                                {
                                                echo "
                                                        <tr paper='$i'>
                                                                <td>Paper $i</td>
                                                                <td>
                                                                        <img class='form-img' src='uploads/{$row['Paper' . $i]}' alt='Paper File'>
                                                                </td>
                                                                <td>
                                                                        <img class='form-img' src='uploads/{$row['Solution' . $i]}' alt='Solution File'>
                                                                </td>
                                                                <td>
                                                                        ";echo $status[$row['Result' . $i]]; echo"
                                                                </td>
                                                        </tr>
                                                        ";  
                                                }
                                                
                                        }
                                echo "       
                                </tbody>
                        </table>
                </div>
        </div>
        
        
        <div class='col-md-12' id='form-msg'></div></div></div></div></div></div></div>

        <script>
                $('tr[paper]').click(function() {
                        var paper = $(this).attr('paper');
                        $('.modal-backdrop').remove();
                        $.post('api.php',
                        {
                                code_id: 13,
                                reg_id: sessionStorage.regID,
                                paper: paper
                        },
                        function(data, status){
                                $('#my-div').html(data);
                                $('#myModal6').modal('show');
                        });
                });
        </script>
       ";
       }
else if($code == 14){
    $result = mysqli_query($connection, "SELECT * FROM tbl_synopsis WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $row = mysqli_fetch_array($result);

    echo "
    <div class='modal fade' id='myModal7' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>Approval of Synopsis</Strong></h3>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>
      <div class='panel-body' style='background-color:#ab9f9b24;'>
          <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
          <hr><div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Approval Date</label>
                <div class='col-sm-5'>
                    <input type='date' class='form-control' name='dob'  value=\"{$row['Approval_Date']}\" id='dob' required  />
                </div>  
              </div>
            </div>
    <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Synopsis Title</label>
                <div class='col-sm-5'>
                    <input type='text' id='ThesisTitle' step='any' class='form-control' name='ThesisTitle' required='required' value=\"{$row['Title']}\" />
                </div>
            </div>
                         <br>             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Abstract</label>
                <div class='col-sm-5'>
                    <textarea id='abstract' step='any' class='form-control' name='abstract' required='required' >{$row['Abstract']}</textarea>
                </div>
            </div>
    <br>
    <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Supervisor</label>
                <div class='col-sm-5'>
                    <input type='text' id='supervisor' step='any' class='form-control' name='supervisor' pattern='^[A-Za-z]{3,20}$' required='required' value=\"{$row['Supervisor']}\" />
                </div>
            </div>
    <br>
    <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload Synopsis File</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['Synopsis_File']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='synopsisfile'  id='synopsisfile' />                           
                        </div>
                         
               
              </div>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload GPC Approval File</label>
                
                        <div class='col-sm-5'>
                            <img src='uploads/{$row['GPC_Approval_file']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='gpcfile'  id='gpcfile'/>                           
                        </div>
                         
               
              </div>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload DPC Approval File</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['DPC_Approcal_File']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='dpcfile'  id='dpcfile'/>                           
                        </div>
                         
               
              </div>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload BASR Approval File</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['BASR_Approval_File']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='basrfile'  id='basrfile'/>                           
                        </div>
                         
               
              </div>
            <input type='submit' style='background-color:green;color:white' class='btn' name='submit_Synopsis' value='Add Synopsis Details' id='submit_Synopsis' >

            <input type='hidden' name='reg_id' value='{$id}'>
            <input type='hidden' name='submit_Synopsis' value='0'>

            </form>                      <div class='col-md-12' id='form-msg'></div></div></div></div></div></div></div>
";
}
else if($code == 15){

    $result = mysqli_query($connection, "SELECT * FROM tbl_supervisor WHERE CID = '$id'") or die(mysqli_error($connection));
    $row = mysqli_fetch_array($result);

    echo " <div class='modal fade' id='myModal8' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>Approval of Supervisor </Strong></h3>
        <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>
      <div class='panel-body' style='background-color:#ab9f9b24;'>
         <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
    <hr><div class='form-group'>
    <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Supervisor Name</label>
                <div class='col-sm-5'>
                    <input type='text' id='supervisorname' step='any' class='form-control' name='supervisorname' required='required' pattern='^[A-Za-z]{3,20}$' value=\"{$row['Name']}\"  />
                </div>
            </div>
    <br>
    <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Affiliations</label>
                <div class='col-sm-5'>
                    <input type='text' id='Affiliations' step='any' class='form-control' name='Affiliations' required='required' value=\"{$row['Affiliations']}\"  />
                </div>
            </div>
    <br>
    <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload Approval File</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['Approval_file']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='app_file'  id='app_file' />                           
                        </div>
                         
               
              </div>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload GPC Approval File</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['GPC_Approval_File']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='gpc_file'  id='gpc_file' />                           
                        </div>
                         
               
              </div>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload BOS Approval File</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['BOS_Approval_File']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='bos_file'  id='bos_file'/>                           
                        </div>
                         
               
              </div>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload BASR Approval File</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['BASR_Approval_File']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='basr_file'  id='basr_file' />                           
                        </div>
                         
               
              </div>
            </div><input type='submit' style='background-color:green;color:white' class='btn' name='submit_supervisor' value='Add Supervisor Details' id='submit_supervisor' >

            <input type='hidden' name='reg_id' value='{$id}'>
            <input type='hidden' name='submit_supervisor' value='0'>

            </form>                      <div class='col-md-12' id='form-msg'></div></div></div></div></div></div></div>
";
}
else if ($code == 17)
    {
        echo " <h3> HSSC Qualification Details            <small></small>
          </h3>          <form id='my-form' action='/apply/add-degree' method='post' enctype='multipart/form-data' >
              <input type='hidden' name='QId' value='' />
                   
        <!-- Main content -->
        <section class='content'>
            <h2 style='color:red'></h2>
        <div class='row'>
            <div class='col-xs-12'>
              <!-- Widget: user widget -->
            <div class='box box-widget widget-user'>
             
                <div class='box-footer'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='box-body table-responsive'>
                                  <div class='form-group'>
                                      
                                      
             <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree</label>
                <div class='col-sm-5'>
                  
                    <select name='Degree'>
                                                <option value='1' >
                                SSC/Equivalent    
                        </option>
                           <option value='2' selected>
                                FA / FSc. /Equivalent    
                        </option>
                                                <option value='3' >
                                BA / BSc. /Equivalent    
                        </option>
                                                <option value='4' >
                                MA / MSc. /Equivalent    
                        </option>
                                                <option value='5' >
                                MS / M.Phil/Equivalent    
                        </option>
                                                <option value='6' >
                                PhD    
                        </option>
                                                <option value='7' >
                                Post-Doc    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div> 
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Year of Passing</label>
                <div class='col-sm-5'>
                                        <select name='YearOfPassing'>
                                                <option value='2017' >
                                2017    
                        </option>
                                                <option value='2016' >
                                2016    
                        </option>
                                                <option value='2015' >
                                2015    
                        </option>
                                                <option value='2014' >
                                2014    
                        </option>
                                                <option value='2013' >
                                2013    
                        </option>
                                                <option value='2012' >
                                2012    
                        </option>
                                                <option value='2011' >
                                2011    
                        </option>
                                                <option value='2010' >
                                2010    
                        </option>
                                                <option value='2009' >
                                2009    
                        </option>
                                                <option value='2008' >
                                2008    
                        </option>
                                                <option value='2007' >
                                2007    
                        </option>
                                                <option value='2006' >
                                2006    
                        </option>
                                                <option value='2005' >
                                2005    
                        </option>
                                                <option value='2004' >
                                2004    
                        </option>
                                                <option value='2003' >
                                2003    
                        </option>
                                                <option value='2002' >
                                2002    
                        </option>
                                                <option value='2001' >
                                2001    
                        </option>
                                                <option value='2000' >
                                2000    
                        </option>
                                                <option value='1999' >
                                1999    
                        </option>
                                                <option value='1998' >
                                1998    
                        </option>
                                                <option value='1997' >
                                1997    
                        </option>
                                                <option value='1996' >
                                1996    
                        </option>
                                                <option value='1995' >
                                1995    
                        </option>
                                                <option value='1994' >
                                1994    
                        </option>
                                                <option value='1993' >
                                1993    
                        </option>
                                                <option value='1992' >
                                1992    
                        </option>
                                                <option value='1991' >
                                1991    
                        </option>
                                                <option value='1990' >
                                1990    
                        </option>
                                                <option value='1989' >
                                1989    
                        </option>
                                                <option value='1988' >
                                1988    
                        </option>
                                                <option value='1987' >
                                1987    
                        </option>
                                                <option value='1986' >
                                1986    
                        </option>
                                                <option value='1985' >
                                1985    
                        </option>
                                                <option value='1984' >
                                1984    
                        </option>
                                                <option value='1983' >
                                1983    
                        </option>
                                                <option value='1982' >
                                1982    
                        </option>
                                                <option value='1981' >
                                1981    
                        </option>
                                                <option value='1980' >
                                1980    
                        </option>
                                                <option value='1979' >
                                1979    
                        </option>
                                                <option value='1978' >
                                1978    
                        </option>
                                                <option value='1977' >
                                1977    
                        </option>
                                                <option value='1976' >
                                1976    
                        </option>
                                                <option value='1975' >
                                1975    
                        </option>
                                                <option value='1974' >
                                1974    
                        </option>
                                                <option value='1973' >
                                1973    
                        </option>
                                                <option value='1972' >
                                1972    
                        </option>
                                                <option value='1971' >
                                1971    
                        </option>
                                                <option value='1970' >
                                1970    
                        </option>
                                                <option value='1969' >
                                1969    
                        </option>
                                                <option value='1968' >
                                1968    
                        </option>
                                                <option value='1967' >
                                1967    
                        </option>
                                                <option value='1966' >
                                1966    
                        </option>
                                                <option value='1965' >
                                1965    
                        </option>
                                                <option value='1964' >
                                1964    
                        </option>
                                                <option value='1963' >
                                1963    
                        </option>
                                                <option value='1962' >
                                1962    
                        </option>
                                                <option value='1961' >
                                1961    
                        </option>
                                                <option value='1960' >
                                1960    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
                                      
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Board/University</label>
                <div class='col-sm-5'>
                  
                    <select name='Board'>
                                                <option value='1' >
                                Board of Intermediate and Secondary Education, Kohat    
                        </option>
                                                <option value='2' >
                                Kohat University of Science & Technology, Kohat    
                        </option>
                                                <option value='3' >
                                University of Peshawar, Peshawar    
                        </option>
                                                <option value='4' >
                                Inter Board Committee of Chairman (IBCC) Islamabad    
                        </option>
                                                <option value='5' >
                                Federal Board of Intermediate and Secondary Education, Islamabad    
                        </option>
                                                <option value='6' >
                                Board of Intermediate and Secondary Education, Bahawalpur    
                        </option>
                                                <option value='7' >
                                Board of Intermediate and Secondary Education, DG Khan    
                        </option>
                                                <option value='8' >
                                Board of Intermediate and Secondary Education, Faisalabad    
                        </option>
                                                <option value='9' >
                                Board of Intermediate and Secondary Education, Gujranwala    
                        </option>
                                                <option value='10' >
                                Board of Intermediate and Secondary Education, Lahore    
                        </option>
                                                <option value='11' >
                                Board of Intermediate and Secondary Education, Multan    
                        </option>
                                                <option value='12' >
                                Board of Intermediate and Secondary Education, Rawalpindi    
                        </option>
                                                <option value='13' >
                                Board of Intermediate and Secondary Education, Sahiwal    
                        </option>
                                                <option value='14' >
                                Board of Intermediate and Secondary Education, Sargodha    
                        </option>
                                                <option value='15' >
                                Aga Khan Educational Board, Karachi    
                        </option>
                                                <option value='16' >
                                Board of Intermediate Education, Karachi    
                        </option>
                                                <option value='17' >
                                Board of Intermediate and Secondary Education, Hyderabad    
                        </option>
                                                <option value='18' >
                                Board of Intermediate and Secondary Education, Larkana    
                        </option>
                                                <option value='19' >
                                Board of Intermediate and Secondary Education, Sukkur    
                        </option>
                                                <option value='20' >
                                Board of Secondary Education, Karachi    
                        </option>
                                                <option value='21' >
                                Board of Intermediate and Secondary Education, Abbottabad    
                        </option>
                                                <option value='22' >
                                Board of Intermediate and Secondary Education, Bannu    
                        </option>
                                                <option value='23' >
                                Board of Intermediate and Secondary Education, Dera Ismail Khan    
                        </option>
                                                <option value='24' >
                                Board of Intermediate and Secondary Education, Malakand    
                        </option>
                                                <option value='25' >
                                Board of Intermediate and Secondary Education, Mardan    
                        </option>
                                                <option value='26' >
                                Board of Intermediate and Secondary Education, Peshawar    
                        </option>
                                                <option value='27' >
                                Board of Intermediate and Secondary Education, Swat    
                        </option>
                                                <option value='28' >
                                Board of Intermediate and Secondary Education, Quetta    
                        </option>
                                                <option value='29' >
                                Board of Intermediate and Secondary Education, Turbat    
                        </option>
                                                <option value='30' >
                                Board of Intermediate and Secondary Education, Zhob    
                        </option>
                                                <option value='31' >
                                Board of Intermediate and Secondary Education, Mirpur    
                        </option>
                                                <option value='32' >
                                KPK Board of Technical Education, Peshawar    
                        </option>
                                                <option value='33' >
                                Punjab Board of Technical Education, Lahore    
                        </option>
                                                <option value='34' >
                                Sindh Board of Technical Education, Karachi    
                        </option>
                                                <option value='35' >
                                Frontier Women University, Peshawar    
                        </option>
                                                <option value='36' >
                                Gomal University, D.I.Khan    
                        </option>
                                                <option value='37' >
                                Hazara University, Dodhial, Mansahra    
                        </option>
                                                <option value='38' >
                                Institute of Management Sciences (IMSciences), Peshawar    
                        </option>
                                                <option value='39' >
                                Islamia College University, Peshawar, Peshawar    
                        </option>
                                                <option value='40' >
                                Khyber Medical University, Peshawar    
                        </option>
                                                <option value='41' >
                                NWFP Agriculture University, Peshawar    
                        </option>
                                                <option value='42' >
                                NWFP University of Engineering & Technology, Peshawar    
                        </option>
                                                <option value='43' >
                                Pakistan Military Academy, Malakand    
                        </option>
                                                <option value='44' >
                                University of Science & Technology Bannu, Bannu    
                        </option>
                                                <option value='45' >
                                Abasyn University, Peshawar    
                        </option>
                                                <option value='46' >
                                CECOS University of Information Technology and Emerging Sciences, Peshawar    
                        </option>
                                                <option value='47' >
                                City University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='48' >
                                Gandhara University, Peshawar    
                        </option>
                                                <option value='49' >
                                Ghulam Ishaq Khan Institute of Engineering Sciences & Technology, Topi    
                        </option>
                                                <option value='50' >
                                Northern University, Nowshera    
                        </option>
                                                <option value='51' >
                                Preston University, Kohat    
                        </option>
                                                <option value='52' >
                                Qurtaba University of Science & Information Technology, D.I.Khan    
                        </option>
                                                <option value='53' >
                                Sarhad University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='54' >
                                Balochistan University of Engineering and Technology, Khuzdar    
                        </option>
                                                <option value='55' >
                                Balochistan University of Information Technology and Management Sciences, Quetta    
                        </option>
                                                <option value='56' >
                                Bolan University of Medical & Health Sciences, Quetta, Quetta    
                        </option>
                                                <option value='57' >
                                Lasbela University of Agriculture, Water & Marine Science, Lasbela    
                        </option>
                                                <option value='58' >
                                Sardar Bahadur Khan Women University, Quetta    
                        </option>
                                                <option value='59' >
                                University of Balochistan, Quetta    
                        </option>
                                                <option value='60' >
                                Al-Hamd Islamic University, Quetta    
                        </option>
                                                <option value='61' >
                                Iqra University, Quetta    
                        </option>
                                                <option value='62' >
                                COMSATS Institute of Information Technology    
                        </option>
                                                <option value='63' >
                                Other    
                        </option>
                                                <option value='64' >
                                University of Karachi    
                        </option>
                                                <option value='65' >
                                Quaid-I-Azam University    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Major (i.e. Subjects)</label>
                <div class='col-sm-5'>
                  
                    <select name='Subjects'>
                                                <option value='1' >
                                General Science    
                        </option>
                                                <option value='2' >
                                Arts    
                        </option>
                                                <option value='3' >
                                Pre-Engineering    
                        </option>
                         <option value='4' >
                                Pre-Medical
            </option>    
                              <option value='5' >
                 Humanity
            </option>
                         </select>
                    
                </div>
              </div>
            </div>            
             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value=''placeholder='1100' />
                </div>
            </div>
                                      
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' required='required' value='' name='ObtainedMarks' placeholder='867'/>
                </div>
            </div>
            </div>
                                
           <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Details Marks Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control'  name='DMCPicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DMCPictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                     
                
              </div>
            </div>
                                
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree/Original Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control' name='DegreePicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DegreePictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                         
               
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction</label>
                <div class='col-sm-5'>                  
                    <select name='Distinction' id='Distinction'>
                        <option value='No-Position'  >No Position</option>
                        <option value='First-Position'  >First Position</option>
                        <option value='Second-Position' >Second Position</option>
                        <option value='Third-Position' >Third Position</option>
                    </select>                    
                </div>
              </div>
            </div> 
            <div class='form-group' id='DistinctionCertGrp' style='display:  none'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction Certificate</label>
                          <div class='col-sm-5'>
                                  <input type='file' class='form-control'  name='DistinctionCert'  id='DistinctionCert'/>
                                <input type='hidden' class='form-control' name='DistinctionCertFileIsAlreadyUploaded' value='' />                            
                          </div>
                                        
              </div>
            </div>              
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'></label>
                <div class='col-sm-5'>
                                        <input type='submit' class='btn bg-blue' name='submit' id='text' />
                                    </div>
              </div>
            </div>              
        </div>
    </div>
</div>
</div><!-- /.widget-user -->
</div>
        </div>
        </section><!-- /.content -->
      </form>                      <div class='col-md-12' id='form-msg'></div>";
    }
else if ($code == 18)
    {
        echo " <h3> BA./BSc. Qualification Details            <small></small>
          </h3>          <form id='my-form' action='/apply/add-degree' method='post' enctype='multipart/form-data' >
              <input type='hidden' name='QId' value='' />
                   
        <!-- Main content -->
        <section class='content'>
            <h2 style='color:red'></h2>
        <div class='row'>
            <div class='col-xs-12'>
              <!-- Widget: user widget -->
            <div class='box box-widget widget-user'>
             
                <div class='box-footer'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='box-body table-responsive'>
                                  <div class='form-group'>
                                      
                                      
             <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree</label>
                <div class='col-sm-5'>
                  
                    <select name='Degree'>
                                                <option value='1' >
                                SSC/Equivalent    
                        </option>
                           <option value='2'>
                                FA / FSc. /Equivalent    
                        </option>
                          <option value='3' selected>
                                BA / BSc. /Equivalent    
                        </option>
                                                <option value='4' >
                                MA / MSc. /Equivalent    
                        </option>
                                                <option value='5' >
                                MS / M.Phil/Equivalent    
                        </option>
                                                <option value='6' >
                                PhD    
                        </option>
                                                <option value='7' >
                                Post-Doc    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div> 
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Year of Passing</label>
                <div class='col-sm-5'>
                                        <select name='YearOfPassing'>
                                                <option value='2017' >
                                2017    
                        </option>
                                                <option value='2016' >
                                2016    
                        </option>
                                                <option value='2015' >
                                2015    
                        </option>
                                                <option value='2014' >
                                2014    
                        </option>
                                                <option value='2013' >
                                2013    
                        </option>
                                                <option value='2012' >
                                2012    
                        </option>
                                                <option value='2011' >
                                2011    
                        </option>
                                                <option value='2010' >
                                2010    
                        </option>
                                                <option value='2009' >
                                2009    
                        </option>
                                                <option value='2008' >
                                2008    
                        </option>
                                                <option value='2007' >
                                2007    
                        </option>
                                                <option value='2006' >
                                2006    
                        </option>
                                                <option value='2005' >
                                2005    
                        </option>
                                                <option value='2004' >
                                2004    
                        </option>
                                                <option value='2003' >
                                2003    
                        </option>
                                                <option value='2002' >
                                2002    
                        </option>
                                                <option value='2001' >
                                2001    
                        </option>
                                                <option value='2000' >
                                2000    
                        </option>
                                                <option value='1999' >
                                1999    
                        </option>
                                                <option value='1998' >
                                1998    
                        </option>
                                                <option value='1997' >
                                1997    
                        </option>
                                                <option value='1996' >
                                1996    
                        </option>
                                                <option value='1995' >
                                1995    
                        </option>
                                                <option value='1994' >
                                1994    
                        </option>
                                                <option value='1993' >
                                1993    
                        </option>
                                                <option value='1992' >
                                1992    
                        </option>
                                                <option value='1991' >
                                1991    
                        </option>
                                                <option value='1990' >
                                1990    
                        </option>
                                                <option value='1989' >
                                1989    
                        </option>
                                                <option value='1988' >
                                1988    
                        </option>
                                                <option value='1987' >
                                1987    
                        </option>
                                                <option value='1986' >
                                1986    
                        </option>
                                                <option value='1985' >
                                1985    
                        </option>
                                                <option value='1984' >
                                1984    
                        </option>
                                                <option value='1983' >
                                1983    
                        </option>
                                                <option value='1982' >
                                1982    
                        </option>
                                                <option value='1981' >
                                1981    
                        </option>
                                                <option value='1980' >
                                1980    
                        </option>
                                                <option value='1979' >
                                1979    
                        </option>
                                                <option value='1978' >
                                1978    
                        </option>
                                                <option value='1977' >
                                1977    
                        </option>
                                                <option value='1976' >
                                1976    
                        </option>
                                                <option value='1975' >
                                1975    
                        </option>
                                                <option value='1974' >
                                1974    
                        </option>
                                                <option value='1973' >
                                1973    
                        </option>
                                                <option value='1972' >
                                1972    
                        </option>
                                                <option value='1971' >
                                1971    
                        </option>
                                                <option value='1970' >
                                1970    
                        </option>
                                                <option value='1969' >
                                1969    
                        </option>
                                                <option value='1968' >
                                1968    
                        </option>
                                                <option value='1967' >
                                1967    
                        </option>
                                                <option value='1966' >
                                1966    
                        </option>
                                                <option value='1965' >
                                1965    
                        </option>
                                                <option value='1964' >
                                1964    
                        </option>
                                                <option value='1963' >
                                1963    
                        </option>
                                                <option value='1962' >
                                1962    
                        </option>
                                                <option value='1961' >
                                1961    
                        </option>
                                                <option value='1960' >
                                1960    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
                                      
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Board/University</label>
                <div class='col-sm-5'>
                  
                    <select name='Board'>
                                                <option value='1' >
                                Board of Intermediate and Secondary Education, Kohat    
                        </option>
                                                <option value='2' >
                                Kohat University of Science & Technology, Kohat    
                        </option>
                                                <option value='3' >
                                University of Peshawar, Peshawar    
                        </option>
                                                <option value='4' >
                                Inter Board Committee of Chairman (IBCC) Islamabad    
                        </option>
                                                <option value='5' >
                                Federal Board of Intermediate and Secondary Education, Islamabad    
                        </option>
                                                <option value='6' >
                                Board of Intermediate and Secondary Education, Bahawalpur    
                        </option>
                                                <option value='7' >
                                Board of Intermediate and Secondary Education, DG Khan    
                        </option>
                                                <option value='8' >
                                Board of Intermediate and Secondary Education, Faisalabad    
                        </option>
                                                <option value='9' >
                                Board of Intermediate and Secondary Education, Gujranwala    
                        </option>
                                                <option value='10' >
                                Board of Intermediate and Secondary Education, Lahore    
                        </option>
                                                <option value='11' >
                                Board of Intermediate and Secondary Education, Multan    
                        </option>
                                                <option value='12' >
                                Board of Intermediate and Secondary Education, Rawalpindi    
                        </option>
                                                <option value='13' >
                                Board of Intermediate and Secondary Education, Sahiwal    
                        </option>
                                                <option value='14' >
                                Board of Intermediate and Secondary Education, Sargodha    
                        </option>
                                                <option value='15' >
                                Aga Khan Educational Board, Karachi    
                        </option>
                                                <option value='16' >
                                Board of Intermediate Education, Karachi    
                        </option>
                                                <option value='17' >
                                Board of Intermediate and Secondary Education, Hyderabad    
                        </option>
                                                <option value='18' >
                                Board of Intermediate and Secondary Education, Larkana    
                        </option>
                                                <option value='19' >
                                Board of Intermediate and Secondary Education, Sukkur    
                        </option>
                                                <option value='20' >
                                Board of Secondary Education, Karachi    
                        </option>
                                                <option value='21' >
                                Board of Intermediate and Secondary Education, Abbottabad    
                        </option>
                                                <option value='22' >
                                Board of Intermediate and Secondary Education, Bannu    
                        </option>
                                                <option value='23' >
                                Board of Intermediate and Secondary Education, Dera Ismail Khan    
                        </option>
                                                <option value='24' >
                                Board of Intermediate and Secondary Education, Malakand    
                        </option>
                                                <option value='25' >
                                Board of Intermediate and Secondary Education, Mardan    
                        </option>
                                                <option value='26' >
                                Board of Intermediate and Secondary Education, Peshawar    
                        </option>
                                                <option value='27' >
                                Board of Intermediate and Secondary Education, Swat    
                        </option>
                                                <option value='28' >
                                Board of Intermediate and Secondary Education, Quetta    
                        </option>
                                                <option value='29' >
                                Board of Intermediate and Secondary Education, Turbat    
                        </option>
                                                <option value='30' >
                                Board of Intermediate and Secondary Education, Zhob    
                        </option>
                                                <option value='31' >
                                Board of Intermediate and Secondary Education, Mirpur    
                        </option>
                                                <option value='32' >
                                KPK Board of Technical Education, Peshawar    
                        </option>
                                                <option value='33' >
                                Punjab Board of Technical Education, Lahore    
                        </option>
                                                <option value='34' >
                                Sindh Board of Technical Education, Karachi    
                        </option>
                                                <option value='35' >
                                Frontier Women University, Peshawar    
                        </option>
                                                <option value='36' >
                                Gomal University, D.I.Khan    
                        </option>
                                                <option value='37' >
                                Hazara University, Dodhial, Mansahra    
                        </option>
                                                <option value='38' >
                                Institute of Management Sciences (IMSciences), Peshawar    
                        </option>
                                                <option value='39' >
                                Islamia College University, Peshawar, Peshawar    
                        </option>
                                                <option value='40' >
                                Khyber Medical University, Peshawar    
                        </option>
                                                <option value='41' >
                                NWFP Agriculture University, Peshawar    
                        </option>
                                                <option value='42' >
                                NWFP University of Engineering & Technology, Peshawar    
                        </option>
                                                <option value='43' >
                                Pakistan Military Academy, Malakand    
                        </option>
                                                <option value='44' >
                                University of Science & Technology Bannu, Bannu    
                        </option>
                                                <option value='45' >
                                Abasyn University, Peshawar    
                        </option>
                                                <option value='46' >
                                CECOS University of Information Technology and Emerging Sciences, Peshawar    
                        </option>
                                                <option value='47' >
                                City University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='48' >
                                Gandhara University, Peshawar    
                        </option>
                                                <option value='49' >
                                Ghulam Ishaq Khan Institute of Engineering Sciences & Technology, Topi    
                        </option>
                                                <option value='50' >
                                Northern University, Nowshera    
                        </option>
                                                <option value='51' >
                                Preston University, Kohat    
                        </option>
                                                <option value='52' >
                                Qurtaba University of Science & Information Technology, D.I.Khan    
                        </option>
                                                <option value='53' >
                                Sarhad University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='54' >
                                Balochistan University of Engineering and Technology, Khuzdar    
                        </option>
                                                <option value='55' >
                                Balochistan University of Information Technology and Management Sciences, Quetta    
                        </option>
                                                <option value='56' >
                                Bolan University of Medical & Health Sciences, Quetta, Quetta    
                        </option>
                                                <option value='57' >
                                Lasbela University of Agriculture, Water & Marine Science, Lasbela    
                        </option>
                                                <option value='58' >
                                Sardar Bahadur Khan Women University, Quetta    
                        </option>
                                                <option value='59' >
                                University of Balochistan, Quetta    
                        </option>
                                                <option value='60' >
                                Al-Hamd Islamic University, Quetta    
                        </option>
                                                <option value='61' >
                                Iqra University, Quetta    
                        </option>
                                                <option value='62' >
                                COMSATS Institute of Information Technology    
                        </option>
                                                <option value='63' >
                                Other    
                        </option>
                                                <option value='64' >
                                University of Karachi    
                        </option>
                                                <option value='65' >
                                Quaid-I-Azam University    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Major (i.e. Subjects)</label>
                <div class='col-sm-5'>
                  
                    <select name='Subjects'>
                                                <option value='1' >
                                Science    
                        </option>
                                                <option value='2' >
                                Arts    
                        </option>
                                                <option value='3' >
                                Pre-Engineering    
                        </option>
                                                <option value='4' >
                                Pre-Medical    
                        </option>
                                                <option value='9' >
                                Business Administration    
                        </option>
                                                <option value='10' >
                                Education    
                        </option>
                                                <option value='11' >
                                Botany    
                        </option>
                                                <option value='12' >
                                Biotechnology    
                        </option>
                                                <option value='13' >
                                Computer Science    
                        </option>
                                                <option value='14' >
                                Chemistry    
                        </option>
                                                <option value='15' >
                                Economics    
                        </option>
                                                <option value='16' >
                                English    
                        </option>
                                                <option value='17' >
                                Journalism    
                        </option>
                                                <option value='18' >
                                Microbiology    
                        </option>
                                                <option value='19' >
                                Mathematices    
                        </option>
                                                <option value='20' >
                                Physics    
                        </option>
                                                <option value='21' >
                                Sociology    
                        </option>
                                                <option value='22' >
                                Social Work    
                        </option>
                                                <option value='23' >
                                Social Work & Sociology    
                        </option>
                                                <option value='24' >
                                Zoology    
                        </option>
                                                <option value='25' >
                                Islamic Studies    
                        </option>
                                                <option value='26' >
                                Management Sciences    
                        </option>
                                                <option value='27' >
                                Biotecnology    
                        </option>
                                                <option value='28' >
                                Microbilogy    
                        </option>
                                                <option value='29' >
                                Pharmacy D    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>            
             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value=''placeholder='1100' />
                </div>
            </div>
                                      
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' required='required' value='' name='ObtainedMarks' placeholder='867'/>
                </div>
            </div>
            </div>
                                
           <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Details Marks Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control'  name='DMCPicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DMCPictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                     
                
              </div>
            </div>
                                
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree/Original Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control' name='DegreePicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DegreePictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                         
               
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction</label>
                <div class='col-sm-5'>                  
                    <select name='Distinction' id='Distinction'>
                        <option value='No-Position'  >No Position</option>
                        <option value='First-Position'  >First Position</option>
                        <option value='Second-Position' >Second Position</option>
                        <option value='Third-Position' >Third Position</option>
                    </select>                    
                </div>
              </div>
            </div> 
            <div class='form-group' id='DistinctionCertGrp' style='display:  none'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction Certificate</label>
                          <div class='col-sm-5'>
                                  <input type='file' class='form-control'  name='DistinctionCert'  id='DistinctionCert'/>
                                <input type='hidden' class='form-control' name='DistinctionCertFileIsAlreadyUploaded' value='' />                            
                          </div>
                                        
              </div>
            </div>              
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'></label>
                <div class='col-sm-5'>
                                        <input type='submit' class='btn bg-blue' name='submit' id='text' />
                                    </div>
              </div>
            </div>              
        </div>
    </div>
</div>
</div><!-- /.widget-user -->
</div>
        </div>
        </section><!-- /.content -->
      </form>                      <div class='col-md-12' id='form-msg'></div>";
    }
else if ($code == 19)
    {
        echo " <h3> MA./MSc. Qualification Details            <small></small>
          </h3>          <form id='my-form' action='/apply/add-degree' method='post' enctype='multipart/form-data' >
              <input type='hidden' name='QId' value='' />
                   
        <!-- Main content -->
        <section class='content'>
            <h2 style='color:red'></h2>
        <div class='row'>
            <div class='col-xs-12'>
              <!-- Widget: user widget -->
            <div class='box box-widget widget-user'>
             
                <div class='box-footer'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='box-body table-responsive'>
                                  <div class='form-group'>
                                      
                                      
             <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree</label>
                <div class='col-sm-5'>
                  
                    <select name='Degree'>
                                                <option value='1' >
                                SSC/Equivalent    
                        </option>
                           <option value='2'>
                                FA / FSc. /Equivalent    
                        </option>
                          <option value='3'>
                                BA / BSc. /Equivalent    
                        </option>
                        <option value='4' selected>
                                MA / MSc. /Equivalent    
                        </option>
                                                <option value='5' >
                                MS / M.Phil/Equivalent    
                        </option>
                                                <option value='6' >
                                PhD    
                        </option>
                                                <option value='7' >
                                Post-Doc    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div> 
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Year of Passing</label>
                <div class='col-sm-5'>
                                        <select name='YearOfPassing'>
                                                <option value='2017' >
                                2017    
                        </option>
                                                <option value='2016' >
                                2016    
                        </option>
                                                <option value='2015' >
                                2015    
                        </option>
                                                <option value='2014' >
                                2014    
                        </option>
                                                <option value='2013' >
                                2013    
                        </option>
                                                <option value='2012' >
                                2012    
                        </option>
                                                <option value='2011' >
                                2011    
                        </option>
                                                <option value='2010' >
                                2010    
                        </option>
                                                <option value='2009' >
                                2009    
                        </option>
                                                <option value='2008' >
                                2008    
                        </option>
                                                <option value='2007' >
                                2007    
                        </option>
                                                <option value='2006' >
                                2006    
                        </option>
                                                <option value='2005' >
                                2005    
                        </option>
                                                <option value='2004' >
                                2004    
                        </option>
                                                <option value='2003' >
                                2003    
                        </option>
                                                <option value='2002' >
                                2002    
                        </option>
                                                <option value='2001' >
                                2001    
                        </option>
                                                <option value='2000' >
                                2000    
                        </option>
                                                <option value='1999' >
                                1999    
                        </option>
                                                <option value='1998' >
                                1998    
                        </option>
                                                <option value='1997' >
                                1997    
                        </option>
                                                <option value='1996' >
                                1996    
                        </option>
                                                <option value='1995' >
                                1995    
                        </option>
                                                <option value='1994' >
                                1994    
                        </option>
                                                <option value='1993' >
                                1993    
                        </option>
                                                <option value='1992' >
                                1992    
                        </option>
                                                <option value='1991' >
                                1991    
                        </option>
                                                <option value='1990' >
                                1990    
                        </option>
                                                <option value='1989' >
                                1989    
                        </option>
                                                <option value='1988' >
                                1988    
                        </option>
                                                <option value='1987' >
                                1987    
                        </option>
                                                <option value='1986' >
                                1986    
                        </option>
                                                <option value='1985' >
                                1985    
                        </option>
                                                <option value='1984' >
                                1984    
                        </option>
                                                <option value='1983' >
                                1983    
                        </option>
                                                <option value='1982' >
                                1982    
                        </option>
                                                <option value='1981' >
                                1981    
                        </option>
                                                <option value='1980' >
                                1980    
                        </option>
                                                <option value='1979' >
                                1979    
                        </option>
                                                <option value='1978' >
                                1978    
                        </option>
                                                <option value='1977' >
                                1977    
                        </option>
                                                <option value='1976' >
                                1976    
                        </option>
                                                <option value='1975' >
                                1975    
                        </option>
                                                <option value='1974' >
                                1974    
                        </option>
                                                <option value='1973' >
                                1973    
                        </option>
                                                <option value='1972' >
                                1972    
                        </option>
                                                <option value='1971' >
                                1971    
                        </option>
                                                <option value='1970' >
                                1970    
                        </option>
                                                <option value='1969' >
                                1969    
                        </option>
                                                <option value='1968' >
                                1968    
                        </option>
                                                <option value='1967' >
                                1967    
                        </option>
                                                <option value='1966' >
                                1966    
                        </option>
                                                <option value='1965' >
                                1965    
                        </option>
                                                <option value='1964' >
                                1964    
                        </option>
                                                <option value='1963' >
                                1963    
                        </option>
                                                <option value='1962' >
                                1962    
                        </option>
                                                <option value='1961' >
                                1961    
                        </option>
                                                <option value='1960' >
                                1960    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
                                      
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Board/University</label>
                <div class='col-sm-5'>
                  
                    <select name='Board'>
                                                <option value='1' >
                                Board of Intermediate and Secondary Education, Kohat    
                        </option>
                                                <option value='2' >
                                Kohat University of Science & Technology, Kohat    
                        </option>
                                                <option value='3' >
                                University of Peshawar, Peshawar    
                        </option>
                                                <option value='4' >
                                Inter Board Committee of Chairman (IBCC) Islamabad    
                        </option>
                                                <option value='5' >
                                Federal Board of Intermediate and Secondary Education, Islamabad    
                        </option>
                                                <option value='6' >
                                Board of Intermediate and Secondary Education, Bahawalpur    
                        </option>
                                                <option value='7' >
                                Board of Intermediate and Secondary Education, DG Khan    
                        </option>
                                                <option value='8' >
                                Board of Intermediate and Secondary Education, Faisalabad    
                        </option>
                                                <option value='9' >
                                Board of Intermediate and Secondary Education, Gujranwala    
                        </option>
                                                <option value='10' >
                                Board of Intermediate and Secondary Education, Lahore    
                        </option>
                                                <option value='11' >
                                Board of Intermediate and Secondary Education, Multan    
                        </option>
                                                <option value='12' >
                                Board of Intermediate and Secondary Education, Rawalpindi    
                        </option>
                                                <option value='13' >
                                Board of Intermediate and Secondary Education, Sahiwal    
                        </option>
                                                <option value='14' >
                                Board of Intermediate and Secondary Education, Sargodha    
                        </option>
                                                <option value='15' >
                                Aga Khan Educational Board, Karachi    
                        </option>
                                                <option value='16' >
                                Board of Intermediate Education, Karachi    
                        </option>
                                                <option value='17' >
                                Board of Intermediate and Secondary Education, Hyderabad    
                        </option>
                                                <option value='18' >
                                Board of Intermediate and Secondary Education, Larkana    
                        </option>
                                                <option value='19' >
                                Board of Intermediate and Secondary Education, Sukkur    
                        </option>
                                                <option value='20' >
                                Board of Secondary Education, Karachi    
                        </option>
                                                <option value='21' >
                                Board of Intermediate and Secondary Education, Abbottabad    
                        </option>
                                                <option value='22' >
                                Board of Intermediate and Secondary Education, Bannu    
                        </option>
                                                <option value='23' >
                                Board of Intermediate and Secondary Education, Dera Ismail Khan    
                        </option>
                                                <option value='24' >
                                Board of Intermediate and Secondary Education, Malakand    
                        </option>
                                                <option value='25' >
                                Board of Intermediate and Secondary Education, Mardan    
                        </option>
                                                <option value='26' >
                                Board of Intermediate and Secondary Education, Peshawar    
                        </option>
                                                <option value='27' >
                                Board of Intermediate and Secondary Education, Swat    
                        </option>
                                                <option value='28' >
                                Board of Intermediate and Secondary Education, Quetta    
                        </option>
                                                <option value='29' >
                                Board of Intermediate and Secondary Education, Turbat    
                        </option>
                                                <option value='30' >
                                Board of Intermediate and Secondary Education, Zhob    
                        </option>
                                                <option value='31' >
                                Board of Intermediate and Secondary Education, Mirpur    
                        </option>
                                                <option value='32' >
                                KPK Board of Technical Education, Peshawar    
                        </option>
                                                <option value='33' >
                                Punjab Board of Technical Education, Lahore    
                        </option>
                                                <option value='34' >
                                Sindh Board of Technical Education, Karachi    
                        </option>
                                                <option value='35' >
                                Frontier Women University, Peshawar    
                        </option>
                                                <option value='36' >
                                Gomal University, D.I.Khan    
                        </option>
                                                <option value='37' >
                                Hazara University, Dodhial, Mansahra    
                        </option>
                                                <option value='38' >
                                Institute of Management Sciences (IMSciences), Peshawar    
                        </option>
                                                <option value='39' >
                                Islamia College University, Peshawar, Peshawar    
                        </option>
                                                <option value='40' >
                                Khyber Medical University, Peshawar    
                        </option>
                                                <option value='41' >
                                NWFP Agriculture University, Peshawar    
                        </option>
                                                <option value='42' >
                                NWFP University of Engineering & Technology, Peshawar    
                        </option>
                                                <option value='43' >
                                Pakistan Military Academy, Malakand    
                        </option>
                                                <option value='44' >
                                University of Science & Technology Bannu, Bannu    
                        </option>
                                                <option value='45' >
                                Abasyn University, Peshawar    
                        </option>
                                                <option value='46' >
                                CECOS University of Information Technology and Emerging Sciences, Peshawar    
                        </option>
                                                <option value='47' >
                                City University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='48' >
                                Gandhara University, Peshawar    
                        </option>
                                                <option value='49' >
                                Ghulam Ishaq Khan Institute of Engineering Sciences & Technology, Topi    
                        </option>
                                                <option value='50' >
                                Northern University, Nowshera    
                        </option>
                                                <option value='51' >
                                Preston University, Kohat    
                        </option>
                                                <option value='52' >
                                Qurtaba University of Science & Information Technology, D.I.Khan    
                        </option>
                                                <option value='53' >
                                Sarhad University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='54' >
                                Balochistan University of Engineering and Technology, Khuzdar    
                        </option>
                                                <option value='55' >
                                Balochistan University of Information Technology and Management Sciences, Quetta    
                        </option>
                                                <option value='56' >
                                Bolan University of Medical & Health Sciences, Quetta, Quetta    
                        </option>
                                                <option value='57' >
                                Lasbela University of Agriculture, Water & Marine Science, Lasbela    
                        </option>
                                                <option value='58' >
                                Sardar Bahadur Khan Women University, Quetta    
                        </option>
                                                <option value='59' >
                                University of Balochistan, Quetta    
                        </option>
                                                <option value='60' >
                                Al-Hamd Islamic University, Quetta    
                        </option>
                                                <option value='61' >
                                Iqra University, Quetta    
                        </option>
                                                <option value='62' >
                                COMSATS Institute of Information Technology    
                        </option>
                                                <option value='63' >
                                Other    
                        </option>
                                                <option value='64' >
                                University of Karachi    
                        </option>
                                                <option value='65' >
                                Quaid-I-Azam University    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Major (i.e. Subjects)</label>
                <div class='col-sm-5'>
                  
                    <select name='Subjects'>
                                                <option value='1' >
                                Science    
                        </option>
                                                <option value='2' >
                                Arts    
                        </option>
                                                <option value='3' >
                                Pre-Engineering    
                        </option>
                                                <option value='4' >
                                Pre-Medical    
                        </option>
                                                <option value='9' >
                                Business Administration    
                        </option>
                                                <option value='10' >
                                Education    
                        </option>
                                                <option value='11' >
                                Botany    
                        </option>
                                                <option value='12' >
                                Biotechnology    
                        </option>
                                                <option value='13' >
                                Computer Science    
                        </option>
                                                <option value='14' >
                                Chemistry    
                        </option>
                                                <option value='15' >
                                Economics    
                        </option>
                                                <option value='16' >
                                English    
                        </option>
                                                <option value='17' >
                                Journalism    
                        </option>
                                                <option value='18' >
                                Microbiology    
                        </option>
                                                <option value='19' >
                                Mathematices    
                        </option>
                                                <option value='20' >
                                Physics    
                        </option>
                                                <option value='21' >
                                Sociology    
                        </option>
                                                <option value='22' >
                                Social Work    
                        </option>
                                                <option value='23' >
                                Social Work & Sociology    
                        </option>
                                                <option value='24' >
                                Zoology    
                        </option>
                                                <option value='25' >
                                Islamic Studies    
                        </option>
                                                <option value='26' >
                                Management Sciences    
                        </option>
                                                <option value='27' >
                                Biotecnology    
                        </option>
                                                <option value='28' >
                                Microbilogy    
                        </option>
                                                <option value='29' >
                                Pharmacy D    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>            
             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value=''placeholder='1100' />
                </div>
            </div>
                                      
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' required='required' value='' name='ObtainedMarks' placeholder='867'/>
                </div>
            </div>
            </div>
                                
           <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Details Marks Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control'  name='DMCPicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DMCPictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                     
                
              </div>
            </div>
                                
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree/Original Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control' name='DegreePicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DegreePictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                         
               
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction</label>
                <div class='col-sm-5'>                  
                    <select name='Distinction' id='Distinction'>
                        <option value='No-Position'  >No Position</option>
                        <option value='First-Position'  >First Position</option>
                        <option value='Second-Position' >Second Position</option>
                        <option value='Third-Position' >Third Position</option>
                    </select>                    
                </div>
              </div>
            </div> 
            <div class='form-group' id='DistinctionCertGrp' style='display:  none'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction Certificate</label>
                          <div class='col-sm-5'>
                                  <input type='file' class='form-control'  name='DistinctionCert'  id='DistinctionCert'/>
                                <input type='hidden' class='form-control' name='DistinctionCertFileIsAlreadyUploaded' value='' />                            
                          </div>
                                        
              </div>
            </div>              
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'></label>
                <div class='col-sm-5'>
                                        <input type='submit' class='btn bg-blue' name='submit' id='text' />
                                    </div>
              </div>
            </div>              
        </div>
    </div>
</div>
</div><!-- /.widget-user -->
</div>
        </div>
        </section><!-- /.content -->
      </form>                      <div class='col-md-12' id='form-msg'></div>";
    }
else if ($code == 20)
    {
        echo " <h3> MS./MPhil. Qualification Details            <small></small>
          </h3>          <form id='my-form' action='/apply/add-degree' method='post' enctype='multipart/form-data' >
              <input type='hidden' name='QId' value='' />
                   
        <!-- Main content -->
        <section class='content'>
            <h2 style='color:red'></h2>
        <div class='row'>
            <div class='col-xs-12'>
              <!-- Widget: user widget -->
            <div class='box box-widget widget-user'>
             
                <div class='box-footer'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='box-body table-responsive'>
                                  <div class='form-group'>
                                      
                                      
             <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree</label>
                <div class='col-sm-5'>
                  
                    <select name='Degree'>
                                                <option value='1' >
                                SSC/Equivalent    
                        </option>
                           <option value='2'>
                                FA / FSc. /Equivalent    
                        </option>
                          <option value='3'>
                                BA / BSc. /Equivalent    
                        </option>
                        <option value='4'>
                                MA / MSc. /Equivalent    
                        </option>
                        <option value='5' selected>
                                MS / M.Phil/Equivalent    
                        </option>
                                                <option value='6' >
                                PhD    
                        </option>
                                                <option value='7' >
                                Post-Doc    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div> 
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Year of Passing</label>
                <div class='col-sm-5'>
                                        <select name='YearOfPassing'>
                                                <option value='2017' >
                                2017    
                        </option>
                                                <option value='2016' >
                                2016    
                        </option>
                                                <option value='2015' >
                                2015    
                        </option>
                                                <option value='2014' >
                                2014    
                        </option>
                                                <option value='2013' >
                                2013    
                        </option>
                                                <option value='2012' >
                                2012    
                        </option>
                                                <option value='2011' >
                                2011    
                        </option>
                                                <option value='2010' >
                                2010    
                        </option>
                                                <option value='2009' >
                                2009    
                        </option>
                                                <option value='2008' >
                                2008    
                        </option>
                                                <option value='2007' >
                                2007    
                        </option>
                                                <option value='2006' >
                                2006    
                        </option>
                                                <option value='2005' >
                                2005    
                        </option>
                                                <option value='2004' >
                                2004    
                        </option>
                                                <option value='2003' >
                                2003    
                        </option>
                                                <option value='2002' >
                                2002    
                        </option>
                                                <option value='2001' >
                                2001    
                        </option>
                                                <option value='2000' >
                                2000    
                        </option>
                                                <option value='1999' >
                                1999    
                        </option>
                                                <option value='1998' >
                                1998    
                        </option>
                                                <option value='1997' >
                                1997    
                        </option>
                                                <option value='1996' >
                                1996    
                        </option>
                                                <option value='1995' >
                                1995    
                        </option>
                                                <option value='1994' >
                                1994    
                        </option>
                                                <option value='1993' >
                                1993    
                        </option>
                                                <option value='1992' >
                                1992    
                        </option>
                                                <option value='1991' >
                                1991    
                        </option>
                                                <option value='1990' >
                                1990    
                        </option>
                                                <option value='1989' >
                                1989    
                        </option>
                                                <option value='1988' >
                                1988    
                        </option>
                                                <option value='1987' >
                                1987    
                        </option>
                                                <option value='1986' >
                                1986    
                        </option>
                                                <option value='1985' >
                                1985    
                        </option>
                                                <option value='1984' >
                                1984    
                        </option>
                                                <option value='1983' >
                                1983    
                        </option>
                                                <option value='1982' >
                                1982    
                        </option>
                                                <option value='1981' >
                                1981    
                        </option>
                                                <option value='1980' >
                                1980    
                        </option>
                                                <option value='1979' >
                                1979    
                        </option>
                                                <option value='1978' >
                                1978    
                        </option>
                                                <option value='1977' >
                                1977    
                        </option>
                                                <option value='1976' >
                                1976    
                        </option>
                                                <option value='1975' >
                                1975    
                        </option>
                                                <option value='1974' >
                                1974    
                        </option>
                                                <option value='1973' >
                                1973    
                        </option>
                                                <option value='1972' >
                                1972    
                        </option>
                                                <option value='1971' >
                                1971    
                        </option>
                                                <option value='1970' >
                                1970    
                        </option>
                                                <option value='1969' >
                                1969    
                        </option>
                                                <option value='1968' >
                                1968    
                        </option>
                                                <option value='1967' >
                                1967    
                        </option>
                                                <option value='1966' >
                                1966    
                        </option>
                                                <option value='1965' >
                                1965    
                        </option>
                                                <option value='1964' >
                                1964    
                        </option>
                                                <option value='1963' >
                                1963    
                        </option>
                                                <option value='1962' >
                                1962    
                        </option>
                                                <option value='1961' >
                                1961    
                        </option>
                                                <option value='1960' >
                                1960    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
                                      
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Board/University</label>
                <div class='col-sm-5'>
                  
                    <select name='Board'>
                                                <option value='1' >
                                Board of Intermediate and Secondary Education, Kohat    
                        </option>
                                                <option value='2' >
                                Kohat University of Science & Technology, Kohat    
                        </option>
                                                <option value='3' >
                                University of Peshawar, Peshawar    
                        </option>
                                                <option value='4' >
                                Inter Board Committee of Chairman (IBCC) Islamabad    
                        </option>
                                                <option value='5' >
                                Federal Board of Intermediate and Secondary Education, Islamabad    
                        </option>
                                                <option value='6' >
                                Board of Intermediate and Secondary Education, Bahawalpur    
                        </option>
                                                <option value='7' >
                                Board of Intermediate and Secondary Education, DG Khan    
                        </option>
                                                <option value='8' >
                                Board of Intermediate and Secondary Education, Faisalabad    
                        </option>
                                                <option value='9' >
                                Board of Intermediate and Secondary Education, Gujranwala    
                        </option>
                                                <option value='10' >
                                Board of Intermediate and Secondary Education, Lahore    
                        </option>
                                                <option value='11' >
                                Board of Intermediate and Secondary Education, Multan    
                        </option>
                                                <option value='12' >
                                Board of Intermediate and Secondary Education, Rawalpindi    
                        </option>
                                                <option value='13' >
                                Board of Intermediate and Secondary Education, Sahiwal    
                        </option>
                                                <option value='14' >
                                Board of Intermediate and Secondary Education, Sargodha    
                        </option>
                                                <option value='15' >
                                Aga Khan Educational Board, Karachi    
                        </option>
                                                <option value='16' >
                                Board of Intermediate Education, Karachi    
                        </option>
                                                <option value='17' >
                                Board of Intermediate and Secondary Education, Hyderabad    
                        </option>
                                                <option value='18' >
                                Board of Intermediate and Secondary Education, Larkana    
                        </option>
                                                <option value='19' >
                                Board of Intermediate and Secondary Education, Sukkur    
                        </option>
                                                <option value='20' >
                                Board of Secondary Education, Karachi    
                        </option>
                                                <option value='21' >
                                Board of Intermediate and Secondary Education, Abbottabad    
                        </option>
                                                <option value='22' >
                                Board of Intermediate and Secondary Education, Bannu    
                        </option>
                                                <option value='23' >
                                Board of Intermediate and Secondary Education, Dera Ismail Khan    
                        </option>
                                                <option value='24' >
                                Board of Intermediate and Secondary Education, Malakand    
                        </option>
                                                <option value='25' >
                                Board of Intermediate and Secondary Education, Mardan    
                        </option>
                                                <option value='26' >
                                Board of Intermediate and Secondary Education, Peshawar    
                        </option>
                                                <option value='27' >
                                Board of Intermediate and Secondary Education, Swat    
                        </option>
                                                <option value='28' >
                                Board of Intermediate and Secondary Education, Quetta    
                        </option>
                                                <option value='29' >
                                Board of Intermediate and Secondary Education, Turbat    
                        </option>
                                                <option value='30' >
                                Board of Intermediate and Secondary Education, Zhob    
                        </option>
                                                <option value='31' >
                                Board of Intermediate and Secondary Education, Mirpur    
                        </option>
                                                <option value='32' >
                                KPK Board of Technical Education, Peshawar    
                        </option>
                                                <option value='33' >
                                Punjab Board of Technical Education, Lahore    
                        </option>
                                                <option value='34' >
                                Sindh Board of Technical Education, Karachi    
                        </option>
                                                <option value='35' >
                                Frontier Women University, Peshawar    
                        </option>
                                                <option value='36' >
                                Gomal University, D.I.Khan    
                        </option>
                                                <option value='37' >
                                Hazara University, Dodhial, Mansahra    
                        </option>
                                                <option value='38' >
                                Institute of Management Sciences (IMSciences), Peshawar    
                        </option>
                                                <option value='39' >
                                Islamia College University, Peshawar, Peshawar    
                        </option>
                                                <option value='40' >
                                Khyber Medical University, Peshawar    
                        </option>
                                                <option value='41' >
                                NWFP Agriculture University, Peshawar    
                        </option>
                                                <option value='42' >
                                NWFP University of Engineering & Technology, Peshawar    
                        </option>
                                                <option value='43' >
                                Pakistan Military Academy, Malakand    
                        </option>
                                                <option value='44' >
                                University of Science & Technology Bannu, Bannu    
                        </option>
                                                <option value='45' >
                                Abasyn University, Peshawar    
                        </option>
                                                <option value='46' >
                                CECOS University of Information Technology and Emerging Sciences, Peshawar    
                        </option>
                                                <option value='47' >
                                City University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='48' >
                                Gandhara University, Peshawar    
                        </option>
                                                <option value='49' >
                                Ghulam Ishaq Khan Institute of Engineering Sciences & Technology, Topi    
                        </option>
                                                <option value='50' >
                                Northern University, Nowshera    
                        </option>
                                                <option value='51' >
                                Preston University, Kohat    
                        </option>
                                                <option value='52' >
                                Qurtaba University of Science & Information Technology, D.I.Khan    
                        </option>
                                                <option value='53' >
                                Sarhad University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='54' >
                                Balochistan University of Engineering and Technology, Khuzdar    
                        </option>
                                                <option value='55' >
                                Balochistan University of Information Technology and Management Sciences, Quetta    
                        </option>
                                                <option value='56' >
                                Bolan University of Medical & Health Sciences, Quetta, Quetta    
                        </option>
                                                <option value='57' >
                                Lasbela University of Agriculture, Water & Marine Science, Lasbela    
                        </option>
                                                <option value='58' >
                                Sardar Bahadur Khan Women University, Quetta    
                        </option>
                                                <option value='59' >
                                University of Balochistan, Quetta    
                        </option>
                                                <option value='60' >
                                Al-Hamd Islamic University, Quetta    
                        </option>
                                                <option value='61' >
                                Iqra University, Quetta    
                        </option>
                                                <option value='62' >
                                COMSATS Institute of Information Technology    
                        </option>
                                                <option value='63' >
                                Other    
                        </option>
                                                <option value='64' >
                                University of Karachi    
                        </option>
                                                <option value='65' >
                                Quaid-I-Azam University    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Major (i.e. Subjects)</label>
                <div class='col-sm-5'>
                  
                    <select name='Subjects'>
                                                <option value='1' >
                                Science    
                        </option>
                                                <option value='2' >
                                Arts    
                        </option>
                                                <option value='3' >
                                Pre-Engineering    
                        </option>
                                                <option value='4' >
                                Pre-Medical    
                        </option>
                                                <option value='9' >
                                Business Administration    
                        </option>
                                                <option value='10' >
                                Education    
                        </option>
                                                <option value='11' >
                                Botany    
                        </option>
                                                <option value='12' >
                                Biotechnology    
                        </option>
                                                <option value='13' >
                                Computer Science    
                        </option>
                                                <option value='14' >
                                Chemistry    
                        </option>
                                                <option value='15' >
                                Economics    
                        </option>
                                                <option value='16' >
                                English    
                        </option>
                                                <option value='17' >
                                Journalism    
                        </option>
                                                <option value='18' >
                                Microbiology    
                        </option>
                                                <option value='19' >
                                Mathematices    
                        </option>
                                                <option value='20' >
                                Physics    
                        </option>
                                                <option value='21' >
                                Sociology    
                        </option>
                                                <option value='22' >
                                Social Work    
                        </option>
                                                <option value='23' >
                                Social Work & Sociology    
                        </option>
                                                <option value='24' >
                                Zoology    
                        </option>
                                                <option value='25' >
                                Islamic Studies    
                        </option>
                                                <option value='26' >
                                Management Sciences    
                        </option>
                                                <option value='27' >
                                Biotecnology    
                        </option>
                                                <option value='28' >
                                Microbilogy    
                        </option>
                                                <option value='29' >
                                Pharmacy D    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>            
             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value=''placeholder='1100' />
                </div>
            </div>
                                      
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' required='required' value='' name='ObtainedMarks' placeholder='867'/>
                </div>
            </div>
            </div>
                                
           <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Details Marks Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control'  name='DMCPicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DMCPictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                     
                
              </div>
            </div>
                                
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree/Original Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control' name='DegreePicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DegreePictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                         
               
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction</label>
                <div class='col-sm-5'>                  
                    <select name='Distinction' id='Distinction'>
                        <option value='No-Position'  >No Position</option>
                        <option value='First-Position'  >First Position</option>
                        <option value='Second-Position' >Second Position</option>
                        <option value='Third-Position' >Third Position</option>
                    </select>                    
                </div>
              </div>
            </div> 
            <div class='form-group' id='DistinctionCertGrp' style='display:  none'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction Certificate</label>
                          <div class='col-sm-5'>
                                  <input type='file' class='form-control'  name='DistinctionCert'  id='DistinctionCert'/>
                                <input type='hidden' class='form-control' name='DistinctionCertFileIsAlreadyUploaded' value='' />                            
                          </div>
                                        
              </div>
            </div>              
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'></label>
                <div class='col-sm-5'>
                                        <input type='submit' class='btn bg-blue' name='submit' id='text' />
                                    </div>
              </div>
            </div>              
        </div>
    </div>
</div>
</div><!-- /.widget-user -->
</div>
        </div>
        </section><!-- /.content -->
      </form>                      <div class='col-md-12' id='form-msg'></div>";
    }
else if ($code == 21)
    {
        echo " <h3> PhD. Qualification Details            <small></small>
          </h3>          <form id='my-form' action='/apply/add-degree' method='post' enctype='multipart/form-data' >
              <input type='hidden' name='QId' value='' />
                   
        <!-- Main content -->
        <section class='content'>
            <h2 style='color:red'></h2>
        <div class='row'>
            <div class='col-xs-12'>
              <!-- Widget: user widget -->
            <div class='box box-widget widget-user'>
             
                <div class='box-footer'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='box-body table-responsive'>
                                  <div class='form-group'>
                                      
                                      
             <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree</label>
                <div class='col-sm-5'>
                  
                    <select name='Degree'>
                                                <option value='1' >
                                SSC/Equivalent    
                        </option>
                           <option value='2'>
                                FA / FSc. /Equivalent    
                        </option>
                          <option value='3'>
                                BA / BSc. /Equivalent    
                        </option>
                        <option value='4'>
                                MA / MSc. /Equivalent    
                        </option>
                                                <option value='5' >
                                MS / M.Phil/Equivalent    
                        </option>
                          <option value='6' selected>
                                PhD    
                        </option>
                          <option value='7' >
                                Post-Doc    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div> 
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Year of Passing</label>
                <div class='col-sm-5'>
                                        <select name='YearOfPassing'>
                                                <option value='2017' >
                                2017    
                        </option>
                                                <option value='2016' >
                                2016    
                        </option>
                                                <option value='2015' >
                                2015    
                        </option>
                                                <option value='2014' >
                                2014    
                        </option>
                                                <option value='2013' >
                                2013    
                        </option>
                                                <option value='2012' >
                                2012    
                        </option>
                                                <option value='2011' >
                                2011    
                        </option>
                                                <option value='2010' >
                                2010    
                        </option>
                                                <option value='2009' >
                                2009    
                        </option>
                                                <option value='2008' >
                                2008    
                        </option>
                                                <option value='2007' >
                                2007    
                        </option>
                                                <option value='2006' >
                                2006    
                        </option>
                                                <option value='2005' >
                                2005    
                        </option>
                                                <option value='2004' >
                                2004    
                        </option>
                                                <option value='2003' >
                                2003    
                        </option>
                                                <option value='2002' >
                                2002    
                        </option>
                                                <option value='2001' >
                                2001    
                        </option>
                                                <option value='2000' >
                                2000    
                        </option>
                                                <option value='1999' >
                                1999    
                        </option>
                                                <option value='1998' >
                                1998    
                        </option>
                                                <option value='1997' >
                                1997    
                        </option>
                                                <option value='1996' >
                                1996    
                        </option>
                                                <option value='1995' >
                                1995    
                        </option>
                                                <option value='1994' >
                                1994    
                        </option>
                                                <option value='1993' >
                                1993    
                        </option>
                                                <option value='1992' >
                                1992    
                        </option>
                                                <option value='1991' >
                                1991    
                        </option>
                                                <option value='1990' >
                                1990    
                        </option>
                                                <option value='1989' >
                                1989    
                        </option>
                                                <option value='1988' >
                                1988    
                        </option>
                                                <option value='1987' >
                                1987    
                        </option>
                                                <option value='1986' >
                                1986    
                        </option>
                                                <option value='1985' >
                                1985    
                        </option>
                                                <option value='1984' >
                                1984    
                        </option>
                                                <option value='1983' >
                                1983    
                        </option>
                                                <option value='1982' >
                                1982    
                        </option>
                                                <option value='1981' >
                                1981    
                        </option>
                                                <option value='1980' >
                                1980    
                        </option>
                                                <option value='1979' >
                                1979    
                        </option>
                                                <option value='1978' >
                                1978    
                        </option>
                                                <option value='1977' >
                                1977    
                        </option>
                                                <option value='1976' >
                                1976    
                        </option>
                                                <option value='1975' >
                                1975    
                        </option>
                                                <option value='1974' >
                                1974    
                        </option>
                                                <option value='1973' >
                                1973    
                        </option>
                                                <option value='1972' >
                                1972    
                        </option>
                                                <option value='1971' >
                                1971    
                        </option>
                                                <option value='1970' >
                                1970    
                        </option>
                                                <option value='1969' >
                                1969    
                        </option>
                                                <option value='1968' >
                                1968    
                        </option>
                                                <option value='1967' >
                                1967    
                        </option>
                                                <option value='1966' >
                                1966    
                        </option>
                                                <option value='1965' >
                                1965    
                        </option>
                                                <option value='1964' >
                                1964    
                        </option>
                                                <option value='1963' >
                                1963    
                        </option>
                                                <option value='1962' >
                                1962    
                        </option>
                                                <option value='1961' >
                                1961    
                        </option>
                                                <option value='1960' >
                                1960    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
                                      
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Board/University</label>
                <div class='col-sm-5'>
                  
                    <select name='Board'>
                                                <option value='1' >
                                Board of Intermediate and Secondary Education, Kohat    
                        </option>
                                                <option value='2' >
                                Kohat University of Science & Technology, Kohat    
                        </option>
                                                <option value='3' >
                                University of Peshawar, Peshawar    
                        </option>
                                                <option value='4' >
                                Inter Board Committee of Chairman (IBCC) Islamabad    
                        </option>
                                                <option value='5' >
                                Federal Board of Intermediate and Secondary Education, Islamabad    
                        </option>
                                                <option value='6' >
                                Board of Intermediate and Secondary Education, Bahawalpur    
                        </option>
                                                <option value='7' >
                                Board of Intermediate and Secondary Education, DG Khan    
                        </option>
                                                <option value='8' >
                                Board of Intermediate and Secondary Education, Faisalabad    
                        </option>
                                                <option value='9' >
                                Board of Intermediate and Secondary Education, Gujranwala    
                        </option>
                                                <option value='10' >
                                Board of Intermediate and Secondary Education, Lahore    
                        </option>
                                                <option value='11' >
                                Board of Intermediate and Secondary Education, Multan    
                        </option>
                                                <option value='12' >
                                Board of Intermediate and Secondary Education, Rawalpindi    
                        </option>
                                                <option value='13' >
                                Board of Intermediate and Secondary Education, Sahiwal    
                        </option>
                                                <option value='14' >
                                Board of Intermediate and Secondary Education, Sargodha    
                        </option>
                                                <option value='15' >
                                Aga Khan Educational Board, Karachi    
                        </option>
                                                <option value='16' >
                                Board of Intermediate Education, Karachi    
                        </option>
                                                <option value='17' >
                                Board of Intermediate and Secondary Education, Hyderabad    
                        </option>
                                                <option value='18' >
                                Board of Intermediate and Secondary Education, Larkana    
                        </option>
                                                <option value='19' >
                                Board of Intermediate and Secondary Education, Sukkur    
                        </option>
                                                <option value='20' >
                                Board of Secondary Education, Karachi    
                        </option>
                                                <option value='21' >
                                Board of Intermediate and Secondary Education, Abbottabad    
                        </option>
                                                <option value='22' >
                                Board of Intermediate and Secondary Education, Bannu    
                        </option>
                                                <option value='23' >
                                Board of Intermediate and Secondary Education, Dera Ismail Khan    
                        </option>
                                                <option value='24' >
                                Board of Intermediate and Secondary Education, Malakand    
                        </option>
                                                <option value='25' >
                                Board of Intermediate and Secondary Education, Mardan    
                        </option>
                                                <option value='26' >
                                Board of Intermediate and Secondary Education, Peshawar    
                        </option>
                                                <option value='27' >
                                Board of Intermediate and Secondary Education, Swat    
                        </option>
                                                <option value='28' >
                                Board of Intermediate and Secondary Education, Quetta    
                        </option>
                                                <option value='29' >
                                Board of Intermediate and Secondary Education, Turbat    
                        </option>
                                                <option value='30' >
                                Board of Intermediate and Secondary Education, Zhob    
                        </option>
                                                <option value='31' >
                                Board of Intermediate and Secondary Education, Mirpur    
                        </option>
                                                <option value='32' >
                                KPK Board of Technical Education, Peshawar    
                        </option>
                                                <option value='33' >
                                Punjab Board of Technical Education, Lahore    
                        </option>
                                                <option value='34' >
                                Sindh Board of Technical Education, Karachi    
                        </option>
                                                <option value='35' >
                                Frontier Women University, Peshawar    
                        </option>
                                                <option value='36' >
                                Gomal University, D.I.Khan    
                        </option>
                                                <option value='37' >
                                Hazara University, Dodhial, Mansahra    
                        </option>
                                                <option value='38' >
                                Institute of Management Sciences (IMSciences), Peshawar    
                        </option>
                                                <option value='39' >
                                Islamia College University, Peshawar, Peshawar    
                        </option>
                                                <option value='40' >
                                Khyber Medical University, Peshawar    
                        </option>
                                                <option value='41' >
                                NWFP Agriculture University, Peshawar    
                        </option>
                                                <option value='42' >
                                NWFP University of Engineering & Technology, Peshawar    
                        </option>
                                                <option value='43' >
                                Pakistan Military Academy, Malakand    
                        </option>
                                                <option value='44' >
                                University of Science & Technology Bannu, Bannu    
                        </option>
                                                <option value='45' >
                                Abasyn University, Peshawar    
                        </option>
                                                <option value='46' >
                                CECOS University of Information Technology and Emerging Sciences, Peshawar    
                        </option>
                                                <option value='47' >
                                City University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='48' >
                                Gandhara University, Peshawar    
                        </option>
                                                <option value='49' >
                                Ghulam Ishaq Khan Institute of Engineering Sciences & Technology, Topi    
                        </option>
                                                <option value='50' >
                                Northern University, Nowshera    
                        </option>
                                                <option value='51' >
                                Preston University, Kohat    
                        </option>
                                                <option value='52' >
                                Qurtaba University of Science & Information Technology, D.I.Khan    
                        </option>
                                                <option value='53' >
                                Sarhad University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='54' >
                                Balochistan University of Engineering and Technology, Khuzdar    
                        </option>
                                                <option value='55' >
                                Balochistan University of Information Technology and Management Sciences, Quetta    
                        </option>
                                                <option value='56' >
                                Bolan University of Medical & Health Sciences, Quetta, Quetta    
                        </option>
                                                <option value='57' >
                                Lasbela University of Agriculture, Water & Marine Science, Lasbela    
                        </option>
                                                <option value='58' >
                                Sardar Bahadur Khan Women University, Quetta    
                        </option>
                                                <option value='59' >
                                University of Balochistan, Quetta    
                        </option>
                                                <option value='60' >
                                Al-Hamd Islamic University, Quetta    
                        </option>
                                                <option value='61' >
                                Iqra University, Quetta    
                        </option>
                                                <option value='62' >
                                COMSATS Institute of Information Technology    
                        </option>
                                                <option value='63' >
                                Other    
                        </option>
                                                <option value='64' >
                                University of Karachi    
                        </option>
                                                <option value='65' >
                                Quaid-I-Azam University    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Major (i.e. Subjects)</label>
                <div class='col-sm-5'>
                  
                    <select name='Subjects'>
                                                <option value='1' >
                                Science    
                        </option>
                                                <option value='2' >
                                Arts    
                        </option>
                                                <option value='3' >
                                Pre-Engineering    
                        </option>
                                                <option value='4' >
                                Pre-Medical    
                        </option>
                                                <option value='9' >
                                Business Administration    
                        </option>
                                                <option value='10' >
                                Education    
                        </option>
                                                <option value='11' >
                                Botany    
                        </option>
                                                <option value='12' >
                                Biotechnology    
                        </option>
                                                <option value='13' >
                                Computer Science    
                        </option>
                                                <option value='14' >
                                Chemistry    
                        </option>
                                                <option value='15' >
                                Economics    
                        </option>
                                                <option value='16' >
                                English    
                        </option>
                                                <option value='17' >
                                Journalism    
                        </option>
                                                <option value='18' >
                                Microbiology    
                        </option>
                                                <option value='19' >
                                Mathematices    
                        </option>
                                                <option value='20' >
                                Physics    
                        </option>
                                                <option value='21' >
                                Sociology    
                        </option>
                                                <option value='22' >
                                Social Work    
                        </option>
                                                <option value='23' >
                                Social Work & Sociology    
                        </option>
                                                <option value='24' >
                                Zoology    
                        </option>
                                                <option value='25' >
                                Islamic Studies    
                        </option>
                                                <option value='26' >
                                Management Sciences    
                        </option>
                                                <option value='27' >
                                Biotecnology    
                        </option>
                                                <option value='28' >
                                Microbilogy    
                        </option>
                                                <option value='29' >
                                Pharmacy D    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>            
             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value=''placeholder='1100' />
                </div>
            </div>
                                      
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' required='required' value='' name='ObtainedMarks' placeholder='867'/>
                </div>
            </div>
            </div>
                                
           <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Details Marks Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control'  name='DMCPicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DMCPictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                     
                
              </div>
            </div>
                                
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree/Original Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control' name='DegreePicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DegreePictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                         
               
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction</label>
                <div class='col-sm-5'>                  
                    <select name='Distinction' id='Distinction'>
                        <option value='No-Position'  >No Position</option>
                        <option value='First-Position'  >First Position</option>
                        <option value='Second-Position' >Second Position</option>
                        <option value='Third-Position' >Third Position</option>
                    </select>                    
                </div>
              </div>
            </div> 
            <div class='form-group' id='DistinctionCertGrp' style='display:  none'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction Certificate</label>
                          <div class='col-sm-5'>
                                  <input type='file' class='form-control'  name='DistinctionCert'  id='DistinctionCert'/>
                                <input type='hidden' class='form-control' name='DistinctionCertFileIsAlreadyUploaded' value='' />                            
                          </div>
                                        
              </div>
            </div>              
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'></label>
                <div class='col-sm-5'>
                                        <input type='submit' class='btn bg-blue' name='submit' id='text' />
                                    </div>
              </div>
            </div>              
        </div>
    </div>
</div>
</div><!-- /.widget-user -->
</div>
        </div>
        </section><!-- /.content -->
      </form>                      <div class='col-md-12' id='form-msg'></div>";
    }
else if ($code == 22)
    {
        echo " <h3> PostDoc Qualification Details            <small></small>
          </h3>          <form id='my-form' action='/apply/add-degree' method='post' enctype='multipart/form-data' >
              <input type='hidden' name='QId' value='' />
                   
        <!-- Main content -->
        <section class='content'>
            <h2 style='color:red'></h2>
        <div class='row'>
            <div class='col-xs-12'>
              <!-- Widget: user widget -->
            <div class='box box-widget widget-user'>
             
                <div class='box-footer'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='box-body table-responsive'>
                                  <div class='form-group'>
                                      
                                      
             <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree</label>
                <div class='col-sm-5'>
                  
                    <select name='Degree'>
                                                <option value='1' >
                                SSC/Equivalent    
                        </option>
                           <option value='2'>
                                FA / FSc. /Equivalent    
                        </option>
                          <option value='3'>
                                BA / BSc. /Equivalent    
                        </option>
                        <option value='4'>
                                MA / MSc. /Equivalent    
                        </option>
                                                <option value='5' >
                                MS / M.Phil/Equivalent    
                        </option>
                          <option value='6' >
                                PhD    
                        </option>
                          <option value='7' selected >
                                Post-Doc    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div> 
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Year of Passing</label>
                <div class='col-sm-5'>
                                        <select name='YearOfPassing'>
                                                <option value='2017' >
                                2017    
                        </option>
                                                <option value='2016' >
                                2016    
                        </option>
                                                <option value='2015' >
                                2015    
                        </option>
                                                <option value='2014' >
                                2014    
                        </option>
                                                <option value='2013' >
                                2013    
                        </option>
                                                <option value='2012' >
                                2012    
                        </option>
                                                <option value='2011' >
                                2011    
                        </option>
                                                <option value='2010' >
                                2010    
                        </option>
                                                <option value='2009' >
                                2009    
                        </option>
                                                <option value='2008' >
                                2008    
                        </option>
                                                <option value='2007' >
                                2007    
                        </option>
                                                <option value='2006' >
                                2006    
                        </option>
                                                <option value='2005' >
                                2005    
                        </option>
                                                <option value='2004' >
                                2004    
                        </option>
                                                <option value='2003' >
                                2003    
                        </option>
                                                <option value='2002' >
                                2002    
                        </option>
                                                <option value='2001' >
                                2001    
                        </option>
                                                <option value='2000' >
                                2000    
                        </option>
                                                <option value='1999' >
                                1999    
                        </option>
                                                <option value='1998' >
                                1998    
                        </option>
                                                <option value='1997' >
                                1997    
                        </option>
                                                <option value='1996' >
                                1996    
                        </option>
                                                <option value='1995' >
                                1995    
                        </option>
                                                <option value='1994' >
                                1994    
                        </option>
                                                <option value='1993' >
                                1993    
                        </option>
                                                <option value='1992' >
                                1992    
                        </option>
                                                <option value='1991' >
                                1991    
                        </option>
                                                <option value='1990' >
                                1990    
                        </option>
                                                <option value='1989' >
                                1989    
                        </option>
                                                <option value='1988' >
                                1988    
                        </option>
                                                <option value='1987' >
                                1987    
                        </option>
                                                <option value='1986' >
                                1986    
                        </option>
                                                <option value='1985' >
                                1985    
                        </option>
                                                <option value='1984' >
                                1984    
                        </option>
                                                <option value='1983' >
                                1983    
                        </option>
                                                <option value='1982' >
                                1982    
                        </option>
                                                <option value='1981' >
                                1981    
                        </option>
                                                <option value='1980' >
                                1980    
                        </option>
                                                <option value='1979' >
                                1979    
                        </option>
                                                <option value='1978' >
                                1978    
                        </option>
                                                <option value='1977' >
                                1977    
                        </option>
                                                <option value='1976' >
                                1976    
                        </option>
                                                <option value='1975' >
                                1975    
                        </option>
                                                <option value='1974' >
                                1974    
                        </option>
                                                <option value='1973' >
                                1973    
                        </option>
                                                <option value='1972' >
                                1972    
                        </option>
                                                <option value='1971' >
                                1971    
                        </option>
                                                <option value='1970' >
                                1970    
                        </option>
                                                <option value='1969' >
                                1969    
                        </option>
                                                <option value='1968' >
                                1968    
                        </option>
                                                <option value='1967' >
                                1967    
                        </option>
                                                <option value='1966' >
                                1966    
                        </option>
                                                <option value='1965' >
                                1965    
                        </option>
                                                <option value='1964' >
                                1964    
                        </option>
                                                <option value='1963' >
                                1963    
                        </option>
                                                <option value='1962' >
                                1962    
                        </option>
                                                <option value='1961' >
                                1961    
                        </option>
                                                <option value='1960' >
                                1960    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
                                      
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Board/University</label>
                <div class='col-sm-5'>
                  
                    <select name='Board'>
                                                <option value='1' >
                                Board of Intermediate and Secondary Education, Kohat    
                        </option>
                                                <option value='2' >
                                Kohat University of Science & Technology, Kohat    
                        </option>
                                                <option value='3' >
                                University of Peshawar, Peshawar    
                        </option>
                                                <option value='4' >
                                Inter Board Committee of Chairman (IBCC) Islamabad    
                        </option>
                                                <option value='5' >
                                Federal Board of Intermediate and Secondary Education, Islamabad    
                        </option>
                                                <option value='6' >
                                Board of Intermediate and Secondary Education, Bahawalpur    
                        </option>
                                                <option value='7' >
                                Board of Intermediate and Secondary Education, DG Khan    
                        </option>
                                                <option value='8' >
                                Board of Intermediate and Secondary Education, Faisalabad    
                        </option>
                                                <option value='9' >
                                Board of Intermediate and Secondary Education, Gujranwala    
                        </option>
                                                <option value='10' >
                                Board of Intermediate and Secondary Education, Lahore    
                        </option>
                                                <option value='11' >
                                Board of Intermediate and Secondary Education, Multan    
                        </option>
                                                <option value='12' >
                                Board of Intermediate and Secondary Education, Rawalpindi    
                        </option>
                                                <option value='13' >
                                Board of Intermediate and Secondary Education, Sahiwal    
                        </option>
                                                <option value='14' >
                                Board of Intermediate and Secondary Education, Sargodha    
                        </option>
                                                <option value='15' >
                                Aga Khan Educational Board, Karachi    
                        </option>
                                                <option value='16' >
                                Board of Intermediate Education, Karachi    
                        </option>
                                                <option value='17' >
                                Board of Intermediate and Secondary Education, Hyderabad    
                        </option>
                                                <option value='18' >
                                Board of Intermediate and Secondary Education, Larkana    
                        </option>
                                                <option value='19' >
                                Board of Intermediate and Secondary Education, Sukkur    
                        </option>
                                                <option value='20' >
                                Board of Secondary Education, Karachi    
                        </option>
                                                <option value='21' >
                                Board of Intermediate and Secondary Education, Abbottabad    
                        </option>
                                                <option value='22' >
                                Board of Intermediate and Secondary Education, Bannu    
                        </option>
                                                <option value='23' >
                                Board of Intermediate and Secondary Education, Dera Ismail Khan    
                        </option>
                                                <option value='24' >
                                Board of Intermediate and Secondary Education, Malakand    
                        </option>
                                                <option value='25' >
                                Board of Intermediate and Secondary Education, Mardan    
                        </option>
                                                <option value='26' >
                                Board of Intermediate and Secondary Education, Peshawar    
                        </option>
                                                <option value='27' >
                                Board of Intermediate and Secondary Education, Swat    
                        </option>
                                                <option value='28' >
                                Board of Intermediate and Secondary Education, Quetta    
                        </option>
                                                <option value='29' >
                                Board of Intermediate and Secondary Education, Turbat    
                        </option>
                                                <option value='30' >
                                Board of Intermediate and Secondary Education, Zhob    
                        </option>
                                                <option value='31' >
                                Board of Intermediate and Secondary Education, Mirpur    
                        </option>
                                                <option value='32' >
                                KPK Board of Technical Education, Peshawar    
                        </option>
                                                <option value='33' >
                                Punjab Board of Technical Education, Lahore    
                        </option>
                                                <option value='34' >
                                Sindh Board of Technical Education, Karachi    
                        </option>
                                                <option value='35' >
                                Frontier Women University, Peshawar    
                        </option>
                                                <option value='36' >
                                Gomal University, D.I.Khan    
                        </option>
                                                <option value='37' >
                                Hazara University, Dodhial, Mansahra    
                        </option>
                                                <option value='38' >
                                Institute of Management Sciences (IMSciences), Peshawar    
                        </option>
                                                <option value='39' >
                                Islamia College University, Peshawar, Peshawar    
                        </option>
                                                <option value='40' >
                                Khyber Medical University, Peshawar    
                        </option>
                                                <option value='41' >
                                NWFP Agriculture University, Peshawar    
                        </option>
                                                <option value='42' >
                                NWFP University of Engineering & Technology, Peshawar    
                        </option>
                                                <option value='43' >
                                Pakistan Military Academy, Malakand    
                        </option>
                                                <option value='44' >
                                University of Science & Technology Bannu, Bannu    
                        </option>
                                                <option value='45' >
                                Abasyn University, Peshawar    
                        </option>
                                                <option value='46' >
                                CECOS University of Information Technology and Emerging Sciences, Peshawar    
                        </option>
                                                <option value='47' >
                                City University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='48' >
                                Gandhara University, Peshawar    
                        </option>
                                                <option value='49' >
                                Ghulam Ishaq Khan Institute of Engineering Sciences & Technology, Topi    
                        </option>
                                                <option value='50' >
                                Northern University, Nowshera    
                        </option>
                                                <option value='51' >
                                Preston University, Kohat    
                        </option>
                                                <option value='52' >
                                Qurtaba University of Science & Information Technology, D.I.Khan    
                        </option>
                                                <option value='53' >
                                Sarhad University of Science & Information Technology, Peshawar    
                        </option>
                                                <option value='54' >
                                Balochistan University of Engineering and Technology, Khuzdar    
                        </option>
                                                <option value='55' >
                                Balochistan University of Information Technology and Management Sciences, Quetta    
                        </option>
                                                <option value='56' >
                                Bolan University of Medical & Health Sciences, Quetta, Quetta    
                        </option>
                                                <option value='57' >
                                Lasbela University of Agriculture, Water & Marine Science, Lasbela    
                        </option>
                                                <option value='58' >
                                Sardar Bahadur Khan Women University, Quetta    
                        </option>
                                                <option value='59' >
                                University of Balochistan, Quetta    
                        </option>
                                                <option value='60' >
                                Al-Hamd Islamic University, Quetta    
                        </option>
                                                <option value='61' >
                                Iqra University, Quetta    
                        </option>
                                                <option value='62' >
                                COMSATS Institute of Information Technology    
                        </option>
                                                <option value='63' >
                                Other    
                        </option>
                                                <option value='64' >
                                University of Karachi    
                        </option>
                                                <option value='65' >
                                Quaid-I-Azam University    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Major (i.e. Subjects)</label>
                <div class='col-sm-5'>
                  
                    <select name='Subjects'>
                                                <option value='1' >
                                Science    
                        </option>
                                                <option value='2' >
                                Arts    
                        </option>
                                                <option value='3' >
                                Pre-Engineering    
                        </option>
                                                <option value='4' >

                                Pre-Medical    
                        </option>
                                                <option value='9' >
                                Business Administration    
                        </option>
                                                <option value='10' >
                                Education    
                        </option>
                                                <option value='11' >
                                Botany    
                        </option>
                                                <option value='12' >
                                Biotechnology    
                        </option>
                                                <option value='13' >
                                Computer Science    
                        </option>
                                                <option value='14' >
                                Chemistry    
                        </option>
                                                <option value='15' >
                                Economics    
                        </option>
                                                <option value='16' >
                                English    
                        </option>
                                                <option value='17' >
                                Journalism    
                        </option>
                                                <option value='18' >
                                Microbiology    
                        </option>
                                                <option value='19' >
                                Mathematices    
                        </option>
                                                <option value='20' >
                                Physics    
                        </option>
                                                <option value='21' >
                                Sociology    
                        </option>
                                                <option value='22' >
                                Social Work    
                        </option>
                                                <option value='23' >
                                Social Work & Sociology    
                        </option>
                                                <option value='24' >
                                Zoology    
                        </option>
                                                <option value='25' >
                                Islamic Studies    
                        </option>
                                                <option value='26' >
                                Management Sciences    
                        </option>
                                                <option value='27' >
                                Biotecnology    
                        </option>
                                                <option value='28' >
                                Microbilogy    
                        </option>
                                                <option value='29' >
                                Pharmacy D    
                        </option>
                                            </select>
                    
                </div>
              </div>
            </div>            
             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' required='required' value=''placeholder='1100' />
                </div>
            </div>
                                      
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' required='required' value='' name='ObtainedMarks' placeholder='867'/>
                </div>
            </div>
            </div>
                                
           <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Details Marks Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control'  name='DMCPicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DMCPictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                     
                
              </div>
            </div>
                                
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree/Original Certificate</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control' name='DegreePicture'  id='DMC'/>
                            <input type='hidden' class='form-control' name='DegreePictureFileIsAlreadyUploaded' value='' />
                            
                        </div>
                         
               
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction</label>
                <div class='col-sm-5'>                  
                    <select name='Distinction' id='Distinction'>
                        <option value='No-Position'  >No Position</option>
                        <option value='First-Position'  >First Position</option>
                        <option value='Second-Position' >Second Position</option>
                        <option value='Third-Position' >Third Position</option>
                    </select>                    
                </div>
              </div>
            </div> 
            <div class='form-group' id='DistinctionCertGrp' style='display:  none'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction Certificate</label>
                          <div class='col-sm-5'>
                                  <input type='file' class='form-control'  name='DistinctionCert'  id='DistinctionCert'/>
                                <input type='hidden' class='form-control' name='DistinctionCertFileIsAlreadyUploaded' value='' />                            
                          </div>
                                        
              </div>
            </div>              
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'></label>
                <div class='col-sm-5'>
                                        <input type='submit' class='btn bg-blue' name='submit' id='text' />
                                    </div>
              </div>
            </div>              
        </div>
    </div>
</div>
</div><!-- /.widget-user -->
</div>
        </div>
        </section><!-- /.content -->
      </form>                      <div class='col-md-12' id='form-msg'></div>";
    }
else if ($code == 23)
    {
        echo " <h3> Semester Wise Progress Report<small></small>
          </h3><hr>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>SEMESTER</label>
                <div class='col-sm-2'>                  
                    <select name='semester' id='sem'>
            <option value='FALL'>FALL</option>
                        <option value='SPRING'>SPRING</option>
            <option value='SUMMER' >SUMMER</option>
                    </select>                    
                </div>
        <div class='col-sm-5'>
                   <select name='YearOfPassing'>
                                                <option value='2017' >
                                2017    
                        </option>
                                                <option value='2016' >
                                2016    
                        </option>
                                                <option value='2015' >
                                2015    
                        </option>
                                                <option value='2014' >
                                2014    
                        </option>
                                                <option value='2013' >
                                2013    
                        </option>
                                                <option value='2012' >
                                2012    
                        </option>
                                                <option value='2011' >
                                2011    
                        </option>
                                                <option value='2010' >
                                2010    
                        </option>
                                                <option value='2009' >
                                2009    
                        </option>
                                                <option value='2008' >
                                2008    
                        </option>
                                                <option value='2007' >
                                2007    
                        </option>
                                                <option value='2006' >
                                2006    
                        </option>
                                                <option value='2005' >
                                2005    
                        </option>
                                                <option value='2004' >
                                2004    
                        </option>
                                                <option value='2003' >
                                2003    
                        </option>
                                                <option value='2002' >
                                2002    
                        </option>
                                                <option value='2001' >
                                2001    
                        </option>
                                                <option value='2000' >
                                2000    
                        </option>
                                                <option value='1999' >
                                1999    
                        </option>
                                                <option value='1998' >
                                1998    
                        </option>
                                                <option value='1997' >
                                1997    
                        </option>
                                                <option value='1996' >
                                1996    
                        </option>
                                                <option value='1995' >
                                1995    
                        </option>
                                                <option value='1994' >
                                1994    
                        </option>
                                                <option value='1993' >
                                1993    
                        </option>
                                                <option value='1992' >
                                1992    
                        </option>
                                                <option value='1991' >
                                1991    
                        </option>
                                                <option value='1990' >
                                1990    
                        </option>
        </select>
        </div>
              </div>
<br>
<div class='form-group'>
     <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Semester Report Date</label>
                <div class='col-sm-5'>
                    <input type='date' class='form-control' name='dob' value='1984-08-10' id='dob' required  />
                </div>  
              </div>
            </div>
    <br>
    <div class='container'>
  <h2>Student Progress Report</h2>
  
<div class='progress'>
  <div class='progress-bar progress-bar-striped active' role='progressbar'
  aria-valuenow='40' aria-valuemin='0' aria-valuemax='100' style='width:40%'>
    40%
  </div>
</div>
</div>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload Progress Report</label>
                
                        <div class='col-sm-5'>
                            <input type='file' class='form-control' name='addmissionForm'  id='addform'/>                           
                        </div>
                         
               
              </div>
            </div><a class='btn bg-blue' href='#'>Add Progress Report   </a>";
    }
else if($code == 24){
        $result = mysqli_query($connection, "SELECT * FROM tbl_research_paper WHERE CID = '$id'") or die(mysqli_error($connection));
    $row = mysqli_fetch_array($result);
    echo " <div class='modal fade' id='myModal9' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>Publication Research Paper </Strong></h3>
      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>

      <div class='panel-body' style='background-color:#ab9f9b24;'>
      <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
      <div class='container'>
    <h3>  <small></small>
          </h3><hr>
        <div class='form-group'>                              
                <div class='row'>
                <label class='control-label col-sm-2'>Title</label>
                <div class='col-sm-5'>
                <input type='text' class='form-control' data-toggle='popover' data-trigger='focus' title='Important Message' data-content='' placeholder='Verification of Android Permission Extension Framework using SPF and JPF' name='title' id='title' value=\"{$row['Titl']}\"  />

                </div>
              </div>
            </div> 
        <div class='form-group'>                              
                <div class='row'>
                <label class='control-label col-sm-2'>Authors</label>
                <div class='col-sm-5'>
                <input type='text' class='form-control' data-toggle='popover' data-trigger='focus' title='Important Message' data-content='' placeholder='Saeed Iqbal Khattak, Dr. Muhammad Nauman, Dr. Mohammad Shaheen' name='author' id='author' value=\"{$row['Author']}\"  />
                </div>
              </div>
            </div> 
        <div class='form-group'>
                <div class='row'>
                <label class='control-label col-sm-2'>Publication Type</label>
                <div class='col-sm-5'>   
               
                    <select name='PublicationType' id='pub'>
            <option value='conf' "; if ($row['publication_Type'] == 'conf') echo 'selected'; echo " >Conference</option>
                        <option value='journal' "; if ($row['publication_Type'] == 'journal') echo 'selected'; echo " >Journal</option>
            <option value='BookChapter' "; if ($row['publication_Type'] == 'BookChapter') echo 'selected'; echo ">Book Chapter</option>
            <option value='workshopPaper' "; if ($row['publication_Type'] == 'workshopPaper') echo 'selected'; echo ">Workshop Paper</option>
                    </select>                    
                </div>
              </div>
            </div>
<div class='form-group'>                              
                <div class='row'>
                <label class='control-label col-sm-2'>Journal/Conference Name</label>
                <div class='col-sm-5'>
                <input type='text' class='form-control' data-toggle='popover' data-trigger='focus' title='Important Message' data-content='' placeholder='IEEE 3rd International Conference on System Engineering and Technology (ICSET), 2013' name='con_name' id='con_name' value=\"{$row['Conference_Name']}\" />
                </div>
              </div>
            </div>
    <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> HEC Recongnized</label>
                <div class='col-sm-5'>
                    <label class='checkbox-inline'><input type='checkbox' value=''></label>
                </div>  
              </div>
            </div>
    <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Impact Factor</label>
                <div class='col-sm-5'>
                    <input type='number' id='impactFactor' step='any' class='form-control' name='impactFactor' required='required' value=\"{$row['Impact_Factor']}\" />
                </div>
            </div><br>
    <div class='row'>
                <label class='control-label col-sm-2'>ISSN Number (If any)</label>
                <div class='col-sm-5'>
                    <input type='number' id='issnNumb' step='any' class='form-control' name='issnNum' required='required' value=\"{$row['ISSN_Number']}\" />
                </div>
            </div>
                         <br>             
              <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Publication Date</label>
                <div class='col-sm-5'>
                    <input type='date' class='form-control' name='dob'  value=\"{$row['Publication_Date']}\" id='pbdate' required  />
                </div>  
              </div>
            </div>

<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Attach Publication</label>
                
                <div class='col-sm-5'>
                <img class='form-img' src='uploads/publication/{$row['Attach_Publication']}' alt='Publication Research Paper'>
                <input type='file' class='form-control' name='attchfile'  id='attchfile'/>
            </div>
               
              </div>
            </div>
<br>
            </div>
            <input type='submit' style='background-color:green;color:white' class='btn' name='submit_publication' value='Add Publication' id='submit_publication' >

            <input type='hidden' name='reg_id' value='{$id}'>
            <input type='hidden' name='submit_publication' value='0'>
            </form></div></div></div></div></div></div>
";
}
else if($code == 25){
        $result = mysqli_query($connection, "SELECT * FROM tbl_evaluation WHERE CID = '$id'") or die(mysqli_error($connection));
    $row = mysqli_fetch_array($result);
    echo " <div class='modal fade' id='myModal11' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>Thesis Evaluation Reports </Strong></h3>
      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>

      <div class='panel-body' style='background-color:#ab9f9b24;'>
      <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
      <h3> <small></small>
          </h3><hr><div class='form-group'>
          <div class='form-group'>
          <div class='row'>
            <label class='control-label col-sm-2'>Evaluator</label>
            <div class='col-sm-5'>
              
                <select name='evaluator' id='evaluator'>
                        <option value='Foreign'"; if($row['Evaluator'] == 'Foreign') echo 'selected '; echo " >Foreign</option>
                        <option value='Local'"; if($row['Evaluator'] == 'Local') echo 'selected '; echo " >Local </option>
                         
                 </select>
                
            </div>
          </div>
        </div> 
        <br>
        <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Local/Foreign Evaluator Name</label>
                <div class='col-sm-5'>
                    <input type='text' id='evaluation' step='any' class='form-control' name='evaluation' required='required' value=\"{$row['Evaluator_name']}\"  />
                </div>
            </div>
    <br>
    <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Affiliations</label>
                <div class='col-sm-5'>
                    <input type='text' id='Affeliations' step='any' class='form-control' name='Affeliations' required='required' value=\"{$row['Affeliations']}\" />
                </div>
            </div>
    <br>
        <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> Upload Evaluation Report</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['Evaluation_Report']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='evaluationreport'  id='addform'/>                           
                        </div>
              </div>
    <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload Approval File</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['Approval_File']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='aprovalreport'  id='addform'/>                           
                        </div>
                         
               
              </div>

            </div>
            <input type='submit' style='background-color:green;color:white' class='btn' name='submit_evaluation' value='Add Evaluation Report' id='submit_evaluation' >

            <input type='hidden' name='reg_id' value='{$id}'>
            <input type='hidden' name='submit_evaluation' value='0'>

            </form>
            <div class='col-md-12' id='form-msg'></div>
            
            <div class='row'>
            <div class='col-sm-12'>
                    <table class='table table-responsive'>
                            <tr>
                                    <th>Evaluator</th>
                                    <th>Evaluator Name</th>
                                    <th>Affeliations</th>
                                    <th>Evaluation Report</th>
                                    <th>Approval File</th>
                            </tr>";
                            $res = mysqli_query($connection,
                            "SELECT * FROM tbl_evaluation WHERE CID = '{$id}' ");
                            while($r = mysqli_fetch_array($res))
                                    echo 
                                    "<tr Evaluator='{$r['Evaluator']}' style='cursor:pointer'>
                                            <td>{$r['Evaluator']}</td>
                                            <td>{$r['Evaluator_name']}</td>
                                            <td>{$r['Affeliations']}</td>
                                            <td><img class='form-img' src='uploads/{$r['Evaluation_Report']}' alt='No Evaluation Report'></td>
                                            <td><img class='form-img' src='uploads/{$r['Approval_File']}' alt='No Approval File'></td>
                                    </tr>";

                    echo "</table>
            </div>
    </div>

            </div></div></div></div></div></div>
";
}

else if($code == 26){
        $result = mysqli_query($connection, "SELECT * FROM tbl_thesis WHERE CID = '$id'") or die(mysqli_error($connection));
    $row = mysqli_fetch_array($result);
    echo " <div class='modal fade' id='myModal10' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>Thesis Submission  </Strong></h3>
      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>

      <div class='panel-body' style='background-color:#ab9f9b24;'>
      <form id='my-form' action='pi.php' method='post' enctype='multipart/form-data' >
    <h3>  <small></small>
          </h3><hr><div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Thesis Submission Date</label>
                <div class='col-sm-5'>
                    <input type='date' class='form-control' name='dob' value=\"{$row['Date']}\" id='dob' required  />
                </div>  
              </div>
            </div>
    <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Thesis Title</label>
                <div class='col-sm-5'>
                    <input type='text' id='ThesisTitle' step='any' class='form-control' name='ThesisTitle' required='required' value=\"{$row['title']}\"  />
                </div>
            </div>
        <br>            
    <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload GPC Meeting Minutes</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['gpcfile']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='gpcMeeting'  id='gpcMeeting' value=''/>                           
                        </div>
                         
               
              </div>
        <br>      
    <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Upload Thesis</label>
                
                        <div class='col-sm-5'>
                        <img src='uploads/{$row['thesisfile']}' class='form-img' alt='No Image'>
                            <input type='file' class='form-control' name='addmissionForm'  id='addmissionForm'/>                           
                        </div>
                         
               
              </div>
            </div>
           
                <input type='submit' style='background-color:green;color:white' class='btn' name='submit_thesis' value='Add Thesis Submition' id='submit_thesis' >

            <input type='hidden' name='reg_id' value='{$id}'>
            <input type='hidden' name='submit_thesis' value='0'>
                </form>
            </div></div></div></div></div></div>
";
}

else if($code == 27){
        echo "<div class='modal fade' id='myModal14' role='dialog'>
            <div class='modal-dialog'>
            
              <!-- Modal content-->
              <div class='container'>
                
                 <div class='col-lg-8 col-md-8  col-sm-8 '>
          
          <div class='panel panel-default'>
              <div class='panel-heading' style='background-color:#983415;border:none' >
              <h3 style='text-align:center;color:white;'><Strong>Students Complete Report </Strong></h3>
              <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
              </div>
        
              <div class='panel-body' style='background-color:#ab9f9b24;'>
              <table class=\"table\">
              <tbody>
                <tr>
                  <td><a href=\"#\">Personal Information</a></td>
                  <td><a href=\"#\">Complete Reports</a></td>
                  <td><a href=\"#\">Educational Details</a></td>
                  <td><a href=\"#\">Admission Form</a></td>
                </tr>
                <tr>
                <td><a href=\"#\">GAT General/Subject</a></td>
                <td><a href=\"#\">Transcript Details</a></td>
                <td><a href=\"#\">Comprehensive Examination</a></td>
                <td><a href=\"#\">Aproval OF Synopip</a></td>

                </tr>
                <tr>
                <td><a href=\"#\">Aproval Of Supervisor</a></td>
                <td><a href=\"#\">Publication Research Paper</a></td>
                <td><a href=\"#\">Thesis Submission</a></td>
                <td><a href=\"#\">Foreign Evaluation</a></td>

                </tr>
                <tr>
                <td><a href=\"#\">Public Defence Notification</a></td>
                <td><a href=\"#\">Degree Completion</a></td>
                <td></td>
                <td></td>
                </tr>
              </tbody>
            </table>
                
        </div></div></div></div></div></div>";
}
else if($code == 28){
echo "<div class='modal fade' id='myModal14' role='dialog'>
    <div class='modal-dialog'>
    
      <!-- Modal content-->
      <div class='container'>
        
         <div class='col-lg-8 col-md-8  col-sm-8 '>
  
  <div class='panel panel-default'>
      <div class='panel-heading' style='background-color:#983415;border:none' >
      <h3 style='text-align:center;color:white;'><Strong>Students Complete Report </Strong></h3>
      <button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"text-align:right;line-height: 0;margin-top:  -50px;color: white;\">×</button>
      </div>

      <div class='panel-body' style='background-color:#ab9f9b24;'>
  <h2></h2>
  <table class='table table-striped'>
    <thead>
<tr>
<th>Personal Information</th>
<th>Address Details</th>
<th>Education Details</th>
<th>Addmission Form</th>
<th>GAT General/Subject</th>
<th>Transcript Details</th>
<th>Comprehensive Examination</th>
<th>Approval of Synopsis</th><!---->
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
  <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
        <td><div class='btn-group' data-toggle='buttons'>
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
      </tr>
      <tr>
        <td>Saeed</td>
  <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
        <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
      </tr>
<tr>
        <td>Imran</td>
  <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
        <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
      </tr>
<tr>
        <td>Awais</td>
  <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
        <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
      </tr>
<tr>
        <td>Kamran</td>
  <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
        <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
      </tr>
<tr>
        <td>Zeeshan</td>
  <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
        <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
      </tr>
<tr>
        <td>Haroon</td>
  <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
        <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
      </tr>
<tr>
        <td>Irfan</td>
  <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
        <td><div class='btn-group' data-toggle='buttons'>
            
            <label class='btn btn-success active'>
                <input type='checkbox' autocomplete='off' checked>
                <span class='glyphicon glyphicon-ok'></span>
            </label></td>
      </tr>
    </tbody>
  </table>
</div></div></div></div></div></div>";
}
echo "
<script>
$( '#my-form' ).submit( function( e ) {
        console.log(this);
        var data = new FormData( this );
        console.log('DATA:  ' + data);
        $.ajax({
                url: './pi.php',
                type: 'POST',
                data: data,
                processData: false,
                contentType: false,
                dataType: 'text'
        })
        .done(function(data, status){
                console.log(data);
                if($.trim(data) == 'FAILED')
                        
                        $('#form-msg').html(`
                                <div class='alert alert-danger'>
                                        <strong>FAILED!</strong> Form Updation Failed
                                </div>
                        `);

                else {
                
                        $('.modal').modal('hide');
                        $.alert({
                                title: 'Success!',
                                content: 'Form Updated Successfully!',
                                type: 'green',
                                theme: 'Material',
                        });
                        // mid = $('.modal').attr('id');
                        // if(mid == 'myModal2'
                        /*$('#form-msg').html(`
                                <div class='alert alert-success'>
                                        <strong>SUCCESS!</strong> Form Updated Successfully
                                </div>
                        `);*/
                }
        })
        .fail(function(status){
                $('#form-msg').html(`
                        <div class='alert alert-danger' >
                                <strong>FAILED!</strong> Form Updation Failed
                        </div>
                `);
        });
        e.preventDefault();
} );
</script>
<script>

        function checkMarks(){
                var max = document.getElementById('MaxMarks').value;
                var obt = document.getElementById('ObtainedMarks').value;
                if(max > obt)
                {
                        alert('Enter correct marks ');
                        document.getElementById('ObtainedMarks').value='';
                }
        }
        function checkDate(){
                var birth = document.getElementById('dob').value;
                var y = new Date(birth);
                
                var year = y.getFullYear();
                var curr = new Date();
                var curr_year = curr.getFullYear();
                var age = curr_year - year;
                if(age < 18){
                        alert('Your age is less than 18 Enter Valid age');
                        document.getElementById('dob').value='';
                }
                
        }
        

        

</script>
<script>
function checkCGPA(){
        var obt = document.getElementById('Obtainedcgpa').value;
        if(obt > 4.0){
                alert('Enter Correct CGPA');
                document.getElementById('Obtainedcgpa').value='';
        }
        else if(obt < 0.0){
                alert('Enter Positive CGPA');
                document.getElementById('Obtainedcgpa').value='';
        }
}
</script>


";
}


?>
<?php
include 'includes/connection.php';
$array = $_POST['srch-txt'];
$arr = $array;
//echo "$arr";
    //$result1 = mysqli_query($connection, "SELECT * FROM tbl_candidate where CID = $arr OR Email = $arr OR Name = $arr OR Father_Name = $arr OR DOB = $arr OR CNIC_num = $arr OR Gender = $arr OR Mobile = $arr OR Domicile = $arr OR Religion = $arr") or die(mysqli_error($connection));
    
    $result1 = mysqli_query($connection, "SELECT * FROM tbl_candidate where CID like '%$arr%' OR Email like '%$arr%' OR Name like '%$arr%' OR Father_Name like '%$arr%' OR DOB like '%$arr%' OR CNIC_num like '%$arr%' OR Gender like '%$arr%' OR Mobile like '%$arr%' OR Domicile like '%$arr%' OR Religion like '%$arr%'") or die(mysqli_error($connection));

    $result2 = mysqli_query($connection, "SELECT * FROM tbl_address where CID like '%$arr%' OR Perma_Address like '%$arr%' OR Perma_City like '%$arr%' OR Perma_Phone like '%$arr%' OR Cur_Address like '%$arr%' OR Cur_City like '%$arr%' OR Cur_Phone like '%$arr%' OR Guard_Address like '%$arr%' OR Guard_City like '%$arr%' OR Guard_Phone like '%$arr%'") or die(mysqli_error($connection));

    $result3 = mysqli_query($connection, "SELECT * FROM tbl_qualification where CID like '%$arr%' OR Degree like '%$arr%' OR Passing_Year like '%$arr%' OR Board like '%$arr%' OR Major like '%$arr%' OR Max_Marks like '%$arr%' OR Obt_Marks like '%$arr%' OR DMC like '%$arr%' OR Original_Certificate like '%$arr%' OR Distinction like '%$arr%'") or die(mysqli_error($connection));

    $result4 = mysqli_query($connection, "SELECT * FROM tbl_admission_form where CID like '%$arr%' ") or die(mysqli_error($connection));

    $result5 = mysqli_query($connection, "SELECT * FROM tbl_gat where CID like '%$arr%' OR Test_Date like '%$arr%' OR Max_Marks like '%$arr%' OR Obt_Marks like '%$arr%' ") or die(mysqli_error($connection));

    $result6 = mysqli_query($connection, "SELECT * FROM tbl_transcript_details where CID like '%$arr%' OR Semester_Num like '%$arr%' OR Result_Date like '%$arr%' OR Max_CGPA like '%$arr%' OR Obt_CGPA like '%$arr%' ") or die(mysqli_error($connection));

    $result7 = mysqli_query($connection, "SELECT * FROM tbl_comprehensive where CID like '%$arr%' OR Exam_Date like '%$arr%' OR Max_Marks like '%$arr%' OR Obt_Marks like '%$arr%'") or die(mysqli_error($connection));

    $result8 = mysqli_query($connection, "SELECT * FROM tbl_synopsis where CID like '%$arr%' OR Approval_Date like '%$arr%' OR Title like '%$arr%' OR Abstract like '%$arr%' OR Supervisor like '%$arr%'") or die(mysqli_error($connection));
 
    $result9 = mysqli_query($connection, "SELECT * FROM tbl_supervisor where CID like '%$arr%' OR Name like '%$arr%' OR Affiliations like '%$arr%' ") or die(mysqli_error($connection));

    $result10 = mysqli_query($connection, "SELECT * FROM tbl_thesis where CID like '%$arr%' OR Date like '%$arr%' OR title like '%$arr%'") or die(mysqli_error($connection));

    $result11 = mysqli_query($connection, "SELECT * FROM tbl_evaluation where CID like '%$arr%' OR Evaluator_name like '%$arr%' OR Affeliations like '%$arr%'") or die(mysqli_error($connection));
  
    $result12 = mysqli_query($connection, "SELECT * FROM tbl_defence_notification where CID like '%$arr%' OR Date like '%$arr%' ") or die(mysqli_error($connection));
   
    $result13 = mysqli_query($connection, "SELECT * FROM tbl_degree_completion where CID like '%$arr%' OR Date like '%$arr%' ") or die(mysqli_error($connection));

?>
<!DOCTYPE html>
<html lang="en">

<head>
	  <title>WAD | QUIZ</title>
	  <!--<link rel="icon" href="images/favicon.ico"/>-->
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta http-equiv="cache-control" content="no-cache" />
	  <meta http-equiv="pragma" content="no-cache" />
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
      <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">	</script>
      
</head>
<body>
<?php
    
    echo "<div style='padding-top:  56px;'>
    <div class='panel panel-danger'>";

    if($result1){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Personal Information</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Email</th>
            <th>Name</th>
            <th>Father Name</th>
            <th>Profile Picture</th>
            <th>DOB</th>
            <th>CNIC NO.</th>
            <th>CNIC Front</th>
            <th>CNIC Back</th>
            <th>Gender</th>
            <th>Mobile</th>
            <th>Domicile</th>
            <th>Religion</th>
            </tr>
        </thead>
        <tbody>";
            while($row = mysqli_fetch_array($result1)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Email']}</td>
                <td>{$row['Name']}</td>
                <td>{$row['Father_Name']}</td>
                <td>{$row['Picture']}</td>
                <td>{$row['DOB']}</td>
                <td>{$row['CNIC_num']}</td>
                <td>{$row['CNIC_front']}</td>
                <td>{$row['CNIC_back']}</td>
                <td>{$row['Gender']}</td>
                <td>{$row['Mobile']}</td>
                <td>{$row['Domicile']}</td>
                <td>{$row['Religion']}</td>

            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result2){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Permanent Address</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Permanent Address</th>
            <th>Permanent City</th>
            <th>Permanent Phone</th>
            <th>Current Address</th>
            <th>Current City</th>
            <th>Current Phone</th>
            <th>Guardian Address</th>
            <th>Guardian City</th>
            <th>Guardian Phone</th>
            </tr>
        </thead>
        <tbody>";
            while($row = mysqli_fetch_array($result2)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Perma_Address']}</td>
                <td>{$row['Perma_City']}</td>
                <td>{$row['Perma_Phone']}</td>
                <td>{$row['Cur_Address']}</td>
                <td>{$row['Cur_City']}</td>
                <td>{$row['Cur_Phone']}</td>
                <td>{$row['Guard_Address']}</td>
                <td>{$row['Guard_City']}</td>
                <td>{$row['Guard_Phone']}</td>

            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result3){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Education Detail</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Degree</th>
            <th>Year of Passing</th>
            <th>Board</th>
            <th>Major</th>
            <th>Maximum Marks</th>
            <th>Obtained Marks</th>
            <th>DMC</th>
            <th>Original_Certificate</th>
            <th>Distinction</th>
            
            </tr>
        </thead><tbody>";
            while($row = mysqli_fetch_array($result3)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Degree']}</td>
                <td>{$row['Passing_Year']}</td>
                <td>{$row['Board']}</td>
                <td>{$row['Major']}</td>
                <td>{$row['Max_Marks']}</td>
                <td>{$row['Obt_Marks']}</td>
                <td>{$row['DMC']}</td>
                <td>{$row['Original_Certificate']}</td>
                <td>{$row['Distinction']}</td>

            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result4){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Admission Form</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Uploaded Form</th>
        </tr>
        </thead><tbody>";
            while($row = mysqli_fetch_array($result4)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Upload_Form']}</td>

            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result5){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>GAT General/Subject Details</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Test Date</th>
            <th>Maximum Marks</th>
            <th>Obtained Marks</th>
            <th>Result Card</th>
            
            </tr>
        </thead><tbody>";
            while($row = mysqli_fetch_array($result5)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Test_Date']}</td>
                <td>{$row['Max_Marks']}</td>
                <td>{$row['Obt_Marks']}</td>
                <td>{$row['Result_Card']}</td>

            </tr>
            
            ";
        }
        echo "</tbody></table>";
    }
    if($result6){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Transcript Details</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Smester Number</th>
            <th>Result Date</th>
            <th>Maximum CGPA</th>
            <th>Obtained CGPA</th>
            <th>Result Card</th>
            </tr>
        </thead><tbody>";
            while($row = mysqli_fetch_array($result6)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Semester_Num']}</td>
                <td>{$row['Result_Date']}</td>
                <td>{$row['Max_CGPA']}</td>
                <td>{$row['Obt_CGPA']}</td>
                <td>{$row['Result_Card']}</td>

            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result7){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Comprehensive Examination Result</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Exam Date</th>
            <th>Maximum Marks</th>
            <th>Obtained Marks</th>
            <th>Paper1</th>
            <th>Paper2</th>
            <th>Paper3</th>
            <th>Comprehensive Result</th>
            
            </tr>
        </thead><tbody>";
            while($row = mysqli_fetch_array($result7)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Exam_Date']}</td>
                <td>{$row['Max_Marks']}</td>
                <td>{$row['Obt_Marks']}</td>
                <td>{$row['Paper1']}</td>
                <td>{$row['Paper2']}</td>
                <td>{$row['Paper3']}</td>
                <td>{$row['Comprehensive_Result']}</td>
            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result8){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Approval of Synopsis</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Approval Date</th>
            <th>Title</th>
            <th>Abstract</th>
            <th>Supervisor</th>
            <th>Synopsis File</th>
            <th>GPC Approval File</th>
            <th>DPC Approval File</th>
            <th>BASR Approval File</th>
            </tr>
        </thead><tbody>";
            while($row = mysqli_fetch_array($result8)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Approval_Date']}</td>
                <td>{$row['Title']}</td>
                <td>{$row['Abstract']}</td>
                <td>{$row['Supervisor']}</td>
                <td>{$row['Synopsis_File']}</td>
                <td>{$row['GPC_Approval_file']}</td>
                <td>{$row['DPC_Approcal_File']}</td>
                <td>{$row['BASR_Approval_File']}</td>
                
            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result9){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Approval of Supervisor</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Name</th>
            <th>Affiliations</th>
            <th>Approval File</th>
            <th>GPC Approval File</th>
            <th>BOS Approval File</th>
            <th>BASR Approval File</th>
            </tr>
        </thead><tbody>";
            while($row = mysqli_fetch_array($result9)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Name']}</td>
                <td>{$row['Affiliations']}</td>
                <td>{$row['Approval_file']}</td>
                <td>{$row['GPC_Approval_File']}</td>
                <td>{$row['BOS_Approval_File']}</td>
                <td>{$row['BASR_Approval_File']}</td>
                
            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result10){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Thesis Submissions</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Date</th>
            <th>Title</th>
            <th>GPC File</th>
            <th>Thesis File</th>
            </tr>
        </thead><tbody>";
            while($row = mysqli_fetch_array($result10)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Date']}</td>
                <td>{$row['title']}</td>
                <td>{$row['gpcfile']}</td>
                <td>{$row['thesisfile']}</td>
            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result11){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Thesis Evaluation Reports</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Evaluator_name</th>
            <th>Affeliations</th>
            <th>Evaluation_Report</th>
            <th>Approval File</th>
            
            </tr>
        </thead><tbody>";
            while($row = mysqli_fetch_array($result11)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Evaluator_name']}</td>
                <td>{$row['Affeliations']}</td>
                <td>{$row['Evaluation_Report']}</td>
                <td>{$row['Approval_File']}</td>
            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result12){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Public Defence Notification</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Date</th>
            <th>BASR File</th>
            <th>Commitee File</th>
            
            </tr>
        </thead><tbody>";
            while($row = mysqli_fetch_array($result12)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Date']}</td>
                <td>{$row['BASR_File']}</td>
                <td>{$row['Commitee_File']}</td>

            </tr>
            ";
        }
        echo "</tbody></table>";
    }
    if($result13){
        echo "<div class='panel-heading' style='text-align:  center;'> <strong>Degree Completion</strong></div>
        <table class='table table-striped table-bordered' cellspacing='0' width='100%'>
        <thead>
        <tr>
            <th>CID</th>
            <th>Date</th>
            <th>Transcript File</th>
            <th>Final Degree File</th>
            </tr>
        </thead>
        <tbody>";
            while($row = mysqli_fetch_array($result13)){
            echo "
            <tr>
                <td>{$row['CID']}</td>
                <td>{$row['Date']}</td>
                <td>{$row['Transcript_File']}</td>
                <td>{$row['Final_Degree_File']}</td>

            </tr>
            ";
        }
        echo "</tbody>
        </table>";
    }
echo "</div></div>";

?>
</body>
<?php
include 'includes/connection.php';
if (isset($_POST['code_id']))
{
$code = $_POST['code_id'];
if($code == 1){
	
	echo " <h3> Add Research Paper <small></small>
          </h3><hr>
		<div class='form-group'>                              
             	<div class='row'>
             	<div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <label class='control-label'>Title</label>
                <input type='text' class='form-control' data-toggle='popover' data-trigger='focus' title='Important Message' data-content='' placeholder='Verification of Android Permission Extension Framework using SPF and JPF' name='name' id='title'/>
				</div>
				<div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <label class='control-label'>Authors</label>
                <input type='text' class='form-control' data-toggle='popover' data-trigger='focus' title='Important Message' data-content='' placeholder='Saeed Iqbal Khattak, Dr. Muhammad Nauman, Dr. Mohammad Shaheen' name='name' id='author'/>
               </div>
               <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
		<label class='control-label col-sm-4'>Publication	</label>
            <select name='PublicationType' id='pub' class='form-control' style='width:200px'>
			<option value='conf' selected >Conference</option>
            <option value='journal'>Journal</option>
			<option value='BookChapter'>Book Chapter</option>
			<option value='workshopPaper'>Workshop Paper</option>
            </select>     
            </div>
			<div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
				<input type ='radio' name ='pjtype' id='jtype'/>
				<label for='jtype' class='control-label'>Journal Name</label>
				<input type ='radio' name ='pjtype' id = 'ctype' />
				<label for='ctype' class='control-label'>Conference Name</label>
                <input type='text' class='form-control' data-toggle='popover' data-trigger='focus' title='Important Message' data-content='' placeholder='IEEE 3rd International Conference on System Engineering and Technology (ICSET), 2013' name='name' id='conf' />
				</div>
				
        </div>

                <!-- <input type='checkbox' value=''><label class='control-label'><span style='color:red'>*</span> HEC Recongnized</label><br><br><br>-->
			<hr>
        <div class='row'>        
                <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <label class='control-label'><span style='color:red'>*</span>Impact Factor</label>                
                <input type='number' id='impactFactor' step='any' class='form-control' name='impactFactor' required='required' value=''/>
                </div>
		
			<div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <label class='control-label'>ISSN Number (If any)</label>

                    <input type='number' id='issnNum' step='any' class='form-control' name='issnNum' required='required' value=''  />

			</div>
			<div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <label class='control-label'> <span style='color:red'>*</span> Publication Date</label>

                    <input type='date' class='form-control' name='dob' value='1984-08-10' id='pbdate' required  />
            </div>
            <div class='col-lg-3 col-md-3 col-sm-3 col-xs-3'>
                <label class='control-label'>Attach Publication</label>
               
                            <input type='file' class='form-control' name='publication'  id='pubFile'/>
                            <input type='hidden' class='form-control' name='DegreePictureFileIsAlreadyUploaded' value='' />
                            
			</div>
		</div>
		<hr>
		<button class='btn btn-primary btn-lg' onclick='addPub()'>Add Publication</button>
		<div id='msg'></div>
	</div></div>";
}
else if($code == 2){
	echo "<h3>Search Publication</h3>
			  <div class='form-group'>
			    
			    <div class='col-sm-4'>
			      <input type='email' class='form-control' id='search' placeholder='Search By Author Name, Publication Title, Publication Type'>
			    </div>
			  <div class='form-group'> 
			    <div class='col-sm-2 col-sm-2'>
				  <button name='' id =''  onclick='searchPub()' class='btn btn-success'>SEARCH</button>
				  
				</div>
				<div id='msg1'></div>
			  </div>";
	}

}
?>

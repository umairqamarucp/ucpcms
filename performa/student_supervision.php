<?php
// session_start();
include("header.php");
?>

    <div class="container">
        <div class="row">
            <h4 class="display-4"><i class="fa fa-user"></i>  Supervised Student</h4>
            <hr>
            
            <div class="col-md-6">
                <div class="form-group">
                    <label for="std_prog"><i class="fa fa-book"></i> Student Program:</label>
                    <select type="text" class="form-control std--program" id="std_prog">
                        <option></option>
                        <option>Masters</option>
                        <option>Doctor in Philosophy</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="spr_select"><i class="fa fa-graduation-cap"></i> Semester:</label>
                    <select class="spr_select form-control" id="spr_select" name="spr_select" style="width:100%">
                        <option></option>
                        <option>Fall</option>
                        <option>Spring</option>
                        <option>Summer</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="std_id"><i class="fa fa-id-card"></i> Student Id:</label>
                    <div class="form-group row">
                        <input type="text" class="form-control col-md-2 margin" required pattern="" placeholder="L1" required title="" id="std_id_p1">
                        <input type="text" class="form-control col-md-2 margin" required pattern="" placeholder="F/S" required title="" id="std_id_p2">
                        <input type="text" class="form-control col-md-2 margin" required pattern="" placeholder="18" required title="" id="std_id_p3">
                        <div class="col-md-2 margin" style="padding-left:0; padding-right:0">
                            <input type="text" class="form-control" required pattern="" placeholder="MSCS/PHDC" required title="" id="std_id_p4">
                            <div id='program_title' class='program_title' style='z-index: 9999; position: absolute;'></div>
                        </div>
                        <input type="text" class="form-control col-md-2 margin" required pattern="" placeholder="01" required title="" id="std_id_p5">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="std_name"><i class="fa fa-user"></i> Student Name:</label>
                    <input type="text" class="form-control" required pattern="[a-zA-Z]{3,}" id="std_name">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="thesis_title"><i class="fa fa-file"></i> Thesis Title:</label>
                    <input type="text" class="form-control" id="thesis_title" required patter="[a-zA-Z]{5,}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="spr_year">Year:</label>
                    <select class="spr_year form-control" name="spr_year" id="spr_year" style="width:100%">
                        <option></option>
                        <?php 
                                for($i = 2010 ; $i <= date('Y'); $i++){
                                    echo "<option value='$i'>$i</option>";
                                }
                            ?>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <button class="btn btn-success btn-block" id="supervision_button"><i class="fa fa-plus"></i> Add</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <form action="postprocess.php" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                    <table class="table table-stripes table-borderless" id='supervision_table'>
                        <thead>
                            <tr>
                                <th>Student Id</th>
                                <th>Student Name</th>
                                <th>Student Program</th>
                                <th>Thesis Title</th>
                                <th>Semester</th>
                                <th>Year</th>
                                <th>Edit/Delete</th>
                            </tr>
                        </thead>
                        <tbody id="supervision_tbody">
                            <?php

                    // var_dump($_SESSION);
                    include('connection.php');
                    if(isset($_SESSION["searched_faculty"]))
                    {
                        $query="SELECT * FROM supervised_students WHERE cnic = '".$_SESSION['searched_faculty']."'";
                        $result = mysqli_query($con, $query);
                        // var_dump($query);
                        if(mysqli_num_rows($result))
                        {
                            $output = '';
                                while($row = mysqli_fetch_array($result))
                                {
                                    $output.= '<tr>';
                                    $output.= '<td class="cls_id">'.$row['std_id'].'<input type="hidden" name="std_id[]" value='.$row['std_id'].'></td>';
                                    $output.= '<td class="cls_name">'.$row['std_name'].'<input type="hidden" name="std_name[]" value='.$row['std_name'].'></td>';
                                    $output.= '<td class="cls_prg">'.$row['std_program'].'<input type="hidden" name="std_program[]" value='.$row['std_program'].'></td>';
                                    $output.= '<td class="cls_thes">'.$row['std_thesis'].'<input type="hidden" name="std_thesis[]" value='.$row['std_thesis'].'></td>';
                                    $output.= '<td class="cls_sem">'.$row['std_semester'].'<input type="hidden" name="std_semester[]" value='.$row['std_semester'].'></td>';
                                    $output.= '<td class="cls_yr">'.$row['std_year'].'<input type="hidden" name="std_year[]" value='.$row['std_year'].'></td>';
                                    $output.= "<td><div class='buttons'><button type='button' class='btn btn-info btn-sm' id='edit_button'>Edit</button><button type='button' class='btn btn-danger btn-sm' id='delete_button'>Delete</button></div></td>";
                                    $output.= '</tr>';
                                }
                                echo $output;
                        }
                    } 
                    ?>
                        </tbody>
                    </table>
                </div>
                <!-- <div class="form-group">
                    <button class="btn btn-success btn-block" id="supervision_button_submit"><i class="fa fa-save"></i> Submit</button>
                </div> -->
        </div>
        </form>
    </div>

    <?php
include("footer.php");
?>
        <script>
            $(document).ready(function() {

                $('#nav_conference').removeClass("active");
                $('#nav_faculty').removeClass("active");
                $('#nav_home').removeClass("active");
                $('#nav_qual').removeClass("active");
                $('#nav_workload').removeClass("active");
                $('#nav_std_supr').addClass("active");
                $('#nav_docs').removeClass("active");

                $("#spr_select").select2({
                    placeholder: "Please Select Semester"
                });
                $("#spr_year").select2({
                    placeholder: "Please Select Year"
                });
                $(".std--program").select2({
                    placeholder: "Please Select Student Program"
                });

                $(document).on('keyup', '#std_id_p4', function(){
                    var keyword = $(this).val();
                    if(keyword.length)
                    {
                    // alert(keyword);
                        $.ajax({
                            type: "POST",
                            url: "postprocess.php",
                            data: {program_title: keyword},
                            success: function(msg){	
                                if(msg != 0)
                                $("#program_title").fadeIn("slow").html(msg);
                                else
                                {
                                $("#program_title").fadeIn("slow");	
                                $("#program_title").html('<div style="text-align:left;">No Matches Found</div>');
                                }
                            }
                            });
                    }
                });

                 $(document).on('focusin', '#std_id_p4', function(){
                    $.ajax({
                        type: "POST",
                        url: "postprocess.php",
                        data:{focus:"focus"},
                        success: function(msg){	
                            if(msg != 0)
                            {
                                $("#program_title").fadeIn("slow").html(msg);
                                console.log(msg);
                            }
                            else
                            {
                            $("#program_title").fadeIn("slow");	
                            $("#program_title").html('<div style="text-align:left;">No Matches Found</div>');
                            }
                        }
                        });
                    });

                $(document).on('click', '#program', function(){  
                    $('#std_id_p4').val($(this).text()); 
                    $('#program_title').fadeOut("slow"); 
                });	

                // $(document).click(function(){
                //     $("#program_title").hide('slow');
                // });

                $(document).on("click", "#supervision_button", function() {
                    var std_id_p1 = $("#std_id_p1").val();
                    var std_id_p2 = $("#std_id_p2").val();
                    var std_id_p3 = $("#std_id_p3").val();
                    var std_id_p4 = $("#std_id_p4").val();
                    var std_id_p5 = $("#std_id_p5").val();


                    if(std_id_1 =="" || std_id_2 =="" || std_id_3 =="" || std_id_4 =="" || std_id_5 =="")
                    {
                        alert("Please Fill the ID.");
                    }

                    else if(std_id_1 !="" && std_id_2 !="" && std_id_3 !="" && std_id_4 !="" && std_id_5 !="")
                    {
                        var std_id = std_id_p1 + std_id_p2 + std_id_p3 + std_id_p4 + std_id_p5; 
                    

                    var std_program = $("#std_prog").val();
                    var std_semester = $("#spr_select").val();
                    var std_name = $("#std_name").val();
                    var std_thesis = $("#thesis_title").val();
                    var std_year = $("#spr_year").val();
                    
                    // alert(std_program);
                    if(std_name!="" && std_id!="" && std_thesis!="" && std_year!="" && std_program!="" && std_semester!="" ){
                        $.ajax({
                            type :'POST',
                            url : 'supervised_postprocess.php',
                            data : {
                                std_id:std_id,
                                std_program:std_program,
                                std_semester:std_semester,
                                std_name:std_name,
                                std_thesis:std_thesis,
                                std_year:std_year
                            }
                        }).done(function(tbody){
                            console.log(tbody);
                            
                                $("#supervision_tbody").html(tbody);
                            
                        });
                    }

                    $("#std_id_p1").val("");
                    $("#std_id_p2").val("");
                    $("#std_id_p3").val("");
                    $("#std_id_p4").val("");
                    $("#std_id_p5").val("");

                    $("#std_name").val("");
                    $("#std_prog").val("");
                    $("#thesis_title").val("");
                    $("#spr_select").val("").trigger("change");
                    $("#spr_year").val("").trigger("change");
                    }
                    else{
                        alert("Please fill all the fields");
                    }
                });

                $(document).on("click", "#delete_button", function() {

                    var $row = $(this).closest('tr');
                    var st_id = $row.find(".cls_id").text();

                    $.ajax({
                        type: 'POST',
                        url: 'postprocess.php',
                        data: {
                            stdsupervision_id: st_id
                        }
                    }).done(function(msg) {
                        console.log(msg);
                    });

                    $(this).closest('tr').remove();
                });

                $(document).on("click", "#edit_button", function() {
                    $(this).closest('tr').remove();

                    var $row = $(this).closest('tr');
                    var st_id = $row.find(".cls_id").text();
                    var st_name = $row.find(".cls_name").text();
                    var st_prg = $row.find(".cls_prg").text();
                    var st_thes = $row.find(".cls_thes").text();
                    var st_sem = $row.find(".cls_sem").text();
                    var st_yr = $row.find(".cls_yr").text();

                    $("#std_id").val(st_id);
                    $("#std_name").val(st_name);
                    $("#std_prog").val(st_prg);
                    $("#thesis_title").val(st_thes);
                    $("#spr_select").val(st_sem).trigger("change");
                    $("#spr_year").val(st_yr).trigger("change");
                    
                    $.ajax({
                        type: 'POST',
                        url: 'postprocess.php',
                        data: {
                            stdsupervision_id: st_id
                        }
                    }).done(function(msg) {
                        console.log(msg);
                    });

                });
            });
        </script>
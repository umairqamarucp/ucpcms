<?php
// session_start();
include("header.php");
?>
    <div class="container">
    
        <div class="row">
        <h4 class="display-4"><i class="fa fa-graduation-cap"></i>  Faculty Qualification</h4>
            <hr>

            <div class="col-md-6">
                <div class="form-group">
                    <label for="education"><i class="fa fa-graduation-cap"></i>Degrees:</label>
                    <select class="education_select form-control" name="education" id="education" style="width:100%">
                        <option></option>
                        <option >Matriculation</option>
                        <option >Intermediate</option>
                        <option >Bachelors</option>
                        <option >Masters</option>
                        <option >Doctrate</option>
                        <option >Post Doctrate</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="year"><i class="fa fa-calender"></i>Year:</label>
                    <select class="year_select form-control" name="year" id="year" style="width:100%">
                    <option></option>
                        <?php 
                                for($i = 1950 ; $i <= date('Y'); $i++){
                                    echo "<option value='$i'>$i</option>";
                                }
                            ?>
                    </select>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                    <label for="board"><i class="fa fa-graduation-cap"></i> Board/University:</label>
                    <select class="board_select" name="board" id="board" style="width:100%">
                    </select>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                    <label for="total_marks"><i class="fa fa-file"></i>Total Marks:</label>
                    <input type="text" class="form-control" id="total_marks" pattern="[0-9]{2,4}" title="Only numeric" name="total_marks">
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                    <label for="obtained_marks"><i class="fa fa-file"></i> Obtained Marks:</label>
                    <input type="text" class="form-control" id="obtained_marks" pattern="[0-9]{2,4}" title="Only numeric" name="obtained_marks">
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                   
                    <h6> <i class="fa fa-file"></i> DMC/Transcript</h6>
                    <hr>
                    <label>
                        <input type="checkbox" class="filled-in" id="hec_attested_dmc" name="hec_attested_dmc" />
                        <span>HEC attested</span>
                    </label><br>
                    <label for="front_side">Front Side</label>
                    <input type="file" class="form-control" id="front_side" name="front_side">
                    <!-- <div id="back_side_div">
                    <label for="back_side">Back Side</label>
                    <input type="file" class="form-control" id="backside" name="backside">
                    </div> -->
                
                </div>
                </div>
                
            
                
                <div class="col-md-6">
                <div class="form-group">
                   
                    <h6><i class="fa fa-file"></i> Degree/Certificate</h6>
                    <hr>
                    <label>
                        <input type="checkbox" class="filled-in" id="hec_attested_degree" name="hec_attested_degree" />
                        <span>HEC attested</span>
                    </label><br>
                    <label for="front_side_degree">Front Side</label>
                    <input type="file" class="form-control" id="front_side_degree" name="front_side_degree">
                    <!-- <div id="back_side_div_degree">
                    <label for="back_side_degree">Back Side</label>
                    <input type="file" class="form-control" id="back_side_degree" name="back_side_degree">
                    </div> -->
                
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="distinction"><i class="fa fa-user"></i> Distinction</label>
                    <select class="distinction form-control" name="distinction" id="distinction">
                    <option></option>
                    <option>Distinction 1</option>
                    <option>Distinction 2</option>
                    <option>Distinction 3</option>
                    </select>
                </div>
                
            </div>
            
            <div class="col-md-12">
                <div class="form-group">
                    <button class="btn btn-success btn-block" id="add_qualification" type="button">Add</button>
                </div>
            </div>

        </div>

        <div class="row">
        <form action="postprocess.php" method="post" enctype="multipart/form-data">
            <div class="col-md-12">
                <table class="table table-stripes table-borderless">
                    <thead>
                        <tr>
                            <th>Degrees</th>
                            <th>Board/University</th>
                            <th>Year</th>
                            <th>Total Marks/CGPA</th>
                            <th>Obtained Marks/CGPA</th>
                            <th>Distinction</th>
                            <th>Edit/Delete</th>
                        </tr>
                    </thead>
                    <tbody id="education_tbody">
                    <?php
                    include('connection.php');
                    if(isset($_SESSION["searched_faculty"]))
                    {
                        $query="SELECT * FROM qualification WHERE cnic = '".$_SESSION['searched_faculty']."'";
                        $result = mysqli_query($con, $query);
                        // var_dump($query);
                        // var_dump($result);
                        if(mysqli_num_rows($result))
                        {
                            $output = '';
                                while($row = mysqli_fetch_array($result))
                                {
                                    $output.= '<tr>';
                                    $output.= '<td class="deg">'.$row['degree'].'<input type="hidden" name="degree[]" value='.$row['degree'].'></td>';
                                    $output.= '<td class="brd">'.$row['board'].'<input type="hidden" name="board[]" value='.$row['board'].'></td>';
                                    $output.= '<td class="yr">'.$row['year'].'<input type="hidden" name="year[]" value='.$row['year'].'></td>';
                                    $output.= '<td class="tmarks">'.$row['tot_marks'].'<input type="hidden" name="tot_marks[]" value='.$row['tot_marks'].'></td>';
                                    $output.= '<td class="omarks">'.$row['ob_marks'].'<input type="hidden" name="ob_marks[]" value='.$row['ob_marks'].'></td>';
                                    $output.= '<td class="distin">'.$row['distinction'].'<input type="hidden" name="distinction[]" value='.$row['distinction'].'></td>';
                                    $output.= "<td><div class='buttons'><button type='button' class='btn btn-info btn-sm' id='edit_button'>Edit</button><button type='button' class='btn btn-danger btn-sm' id='delete_button'>Delete</button></div></td>";
                                    $output.= '</tr>';
                                }
                                echo $output;
                        }
                    } 
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        
</form>
    </div>

    <?php 
include("footer.php");
?>
        <script>
        function check_duplicates(c, n) {
                    
                    $("table tr").each(function(i, v){
                        data[i] = Array();
                        $(this).find('.'+c).each(function(ii, vv){
                            data[i][ii] = $(this).text();
                            // debugger;
                            // alert(data[i][ii]+"/SentValue: "+n);
                            if(n == data[i][ii]){
                                alert("Record Exist Already");
                                $('.education_select').val(null).trigger('change');
                                $('.board_select').val(null).trigger('change');
                                $('.year_select').val(null).trigger('change');
                                $('.distinction').val(null).trigger('change');
                            }
                        }); 
                    });

                }
                function check_year(c, n) {
                    
                    $("table tr").each(function(i, v){
                        data[i] = Array();
                        $(this).find('.'+c).each(function(ii, vv){
                            data[i][ii] = parseInt($(this).text());
                            // debugger;
                            // alert(data[i][ii]+"/SentValue: "+n);
                            if(parseInt(n) <= data[i][ii]){
                                alert("Year of education either exists already or it is less than the most fresh year of education.");
                                // $('.education_select').val(null).trigger('change');
                                // $('.board_select').val(null).trigger('change');
                                $('.year_select').val(null).trigger('change');
                                // $('.distinction').val(null).trigger('change');
                            }
                        }); 
                    });

                }
                function check_marks() { 
                    var ob = parseInt($("#obtained_marks").val());
                    var tot = parseInt($("#total_marks").val());
                    if(ob > tot)
                    {
                        $("#obtained_marks").css({"color": "red", "border": "1px solid"});
                        alert("Obtained marks ("+ob+") are greater than total marks ("+tot+")");
                        console.log(ob+"/"+tot);
                    }
                    else{
                        $("#obtained_marks").css({"color": "black", "border": "0px solid"});
                    }
                }

                function delete_record(degree){
                    var form_data = new FormData();                  
                    form_data.append('degree', degree);
                    $.ajax({
                        type: 'POST',
                        url: 'postprocess.php',
                        data: form_data
                    }).done(function(msg){
                        console.log(msg);
                    });

                }
            $(document).ready(function() {
                
                data = Array();

                $('#nav_conference').removeClass("active");

                 $('#nav_faculty').removeClass("active");
                $('#nav_home').removeClass("active");
                $('#nav_qual').addClass("active");
                $('#nav_workload').removeClass("active");
                $('#nav_std_supr').removeClass("active");
                $('#nav_docs').removeClass("active");
                
                $(".education_select").select2({
                    placeholder: "Please Select Degree"
                });
                $(".year_select").select2({
                    placeholder: "Please Select Year"
                });
                $(".board_select").select2({
                    placeholder: "Please Select Board/University"
                });
                $(".distinction").select2({
                    placeholder: "Distinction"
                });

                $("#back_side_div").hide();
                $("#back_side_div_degree").hide();

                

                $(document).on("keyup", "#obtained_marks", function(){
                    check_marks();
                });

                // $(document).on("change", "#hec_attested_dmc", function(){
                //     var check = $("#hec_attested_dmc").prop('checked');
                //     if(check)
                //     {
                //         $("#back_side_div").show();
                //     }
                //     else{
                //         $("#back_side_div").hide();
                //     }
                // });

                // $(document).on("change", "#hec_attested_degree", function(){
                //     var check = $("#hec_attested_degree").prop('checked');
                //     if(check)
                //     {
                //         $("#back_side_div_degree").show();
                //     }
                //     else{
                //         $("#back_side_div_degree").hide();
                //     }
                // });

                $(document).on("change", "#education", function() {
                    var edu = $("#education").val();
                    var board = "<option></option>" +
                        "<option>Federal BISE Islamabad</option>" +
                        "<option>BISE Faisalabad</option>" +
                        "<option>BISE Bahawalpur</option>" +
                        "<option>BISE Gujranwala</option>" +
                        "<option>BISE Lahore</option>" +
                        "<option>BISE Multan</option>" +
                        "<option>BISE Rawalpindi</option>" +
                        "<option>BISE Sahiwal</option>" +
                        "<option>BISE Sargodha</option>" +
                        "<option>BISE D.G Khan</option>";

                    var university = "<option></option>" +
                        "<option>University of Punjab</option>" +
                        "<option>University of Sargodha</option>" +
                        "<option>University of Central Punjab</option>" +
                        "<option>Govt. College University</option>" +
                        "<option>University of Engineering & Technology</option>" +
                        "<option>Bahauddin Zakariya University</option>" +
                        "<option>University of Agriculture, Faisalabad</option>" +
                        "<option>Lahore University of Management Sciences</option>" +
                        "<option>University of Health Sciences</option>";

                    var table_edu = $("#education_tbody").find(".deg").text();

                    check_duplicates("deg", edu);
                    
                    if([edu].includes(null)){
                        alert("Please Select Degree First");
                    }
                    else{
                        if (edu == "Matriculation" || edu == "Intermediate") {
                            $("#board").html(board);
                        } 
                        else if (edu == "Bachelors" || edu == "Masters" || edu == "Doctrate" || edu == "Post Doctrate") {
                            $("#board").html(university);
                        }
                    }

                });
                $(document).on("change", "#year", function(){
                    var year = $("#year").val();
                    var table_yr = $("#education_tbody").find(".yr").text();

                    check_year("yr", year);
                    
                    // if(year == table_yr)
                    // {
                    //     alert("You already have a Qualification in year "+year+".");
                        
                    //     $('.education_select').val(null).trigger('change');
                    //     $('.board_select').val(null).trigger('change');
                    //     $('.year_select').val(null).trigger('change');
                    //     $('.distinction').val(null).trigger('change');
                    // }

                });
                

                $(document).on("click", "#add_qualification", function(){
                    var degree = $("#education").val();
                    var board = $("#board").val();
                    var year = $("#year").val();
                    var tot_marks = parseFloat($("#total_marks").val());
                    var ob_marks = parseFloat($("#obtained_marks").val());
                    var distinction = $("#distinction").val();

                    var hec_dmc = $("#hec_dmc").val();
                    var hec_degree = $("#hec_degree").val();
                    
                    if(ob_marks > tot_marks)
                    {
                        $("#obtained_marks").focus();
                        check_marks();
                    }
                    // alert($("#front_side").val());
                    else if(ob_marks <= tot_marks){
                        // alert("Marks are less");
                        if([degree, board, year, distinction, tot_marks, ob_marks].includes(undefined) || [degree, board, year, distinction,tot_marks, ob_marks].includes(null) || [tot_marks, ob_marks].includes("0") ||[degree, board, year, distinction, tot_marks, ob_marks].includes(NaN))
                        {
                             alert("Some values has not been set. Please input every field provided");
                        }  
                        else{

                            $.ajax({
                                type :'POST',
                                url : 'postprocess.php',
                                data : {
                                    degree:degree,
                                    board:board,
                                    year:year,
                                    tot_marks:tot_marks,
                                    ob_marks:ob_marks,
                                    distinction:distinction
                                }
                            }).done(function(tbody){
                                // console.log(tbody);
                                
                                $("#education_tbody").html(tbody);
                            
                            });

                            // var tbody = "<tr>"+
                            // "<td class='deg'>"+degree+"<input type='hidden' name='degree[]' value='"+degree+"'></td>"+
                            // "<td class='brd'>"+board+"<input type='hidden' name='board[]' value='"+board+"'></td>"+
                            // "<td class='yr'>"+year+"<input type='hidden' name='year[]' value='"+year+"'></td>"+
                            // "<td class='tmarks'>"+tot_marks+"<input type='hidden' name='tot_marks[]' value='"+tot_marks+"'></td>"+
                            // "<td class='omarks'>"+ob_marks+"<input type='hidden' name='ob_marks[]' value='"+ob_marks+"'></td>"+
                            // "<td class='distin'>"+distinction+"<input type='hidden' name='distinction[]' value='"+distinction+"'></td>"+
                            // "<td><div class='buttons'><button type='button' class='btn btn-info btn-sm' id='edit_button'>Edit</button><button type='button' class='btn btn-danger btn-sm' id='delete_button'>Delete</button></div></td>"+
                            // "</tr>";
                            // $("#education_tbody").append(tbody); 

                            $('.education_select').val(null).trigger('change');
                            $('.board_select').val(null).trigger('change');
                            $('.year_select').val(null).trigger('change');
                            $('.distinction').val(null).trigger('change');
                            $("#total_marks").val("");
                            $("#obtained_marks").val("");

                            // debugger;
                    }
                    }

                    
                });

                $(document).on("click", "#delete_button",function(){
                    var $row = $(this).closest('tr');
                    var degree = $row.find(".deg").text();
                    
                    // delete_record(degree);
                    $.ajax({
                        type: 'POST',
                        url: 'postprocess.php',
                        data: {quali_degree:degree}
                    }).done(function(msg){
                        console.log(msg);
                    });

                    $(this).closest('tr').remove();
                });

                $(document).on("click", "#edit_button", function(){
                        
                        var $row = $(this).closest('tr');
                        var degree = $row.find(".deg").text();
                        var board = $row.find(".brd").text();
                        var year = $row.find(".yr").text();
                        var tmarks = $row.find(".tmarks").text();
                        var omarks = $row.find(".omarks").text();
                        var distinct = $row.find(".distin").text();

                        // debugger;

                        $(this).closest('tr').remove();

                        $("#education").val(degree).trigger("change");
                        $("#board").val(board).trigger("change");
                        $("#year").val(year).trigger("change");
                        $("#total_marks").val(tmarks);
                        $("#obtained_marks").val(omarks);
                        $("#distinction").val(distinct).trigger("change");

                        $.ajax({
                        type: 'POST',
                        url: 'postprocess.php',
                        data: {quali_degree:degree}
                    }).done(function(msg){
                        console.log(msg);
                    });
 
                });

                // $(document).on('click', '#submitbutton', function(){
                //     var edu = $('input[name = ""]').val();
                //     var year = $('#year').val();
                //     var board = $('#board').val();
                //     var tot_marks = $('#total_marks').val();
                //     var ob_marks = $('#obtained_marks').val();
                //     var dist = $('#distinction').val();
                    
                //     var form_data = new FormData();                  
                //     form_data.append('education', edu);

                // });
                
                $(document).on('change', '#front_side', function(){
                    var front_side = $('#front_side').prop('files')[0];
                    var form_data = new FormData();                  
                    form_data.append('file', front_side);
                    $.ajax({
                        url: 'upload.php', 
                        dataType: 'text',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,                         
                        type: 'post',
                        success: function(php_script_response){
                            console.log(php_script_response);
                        }
                    });
                });

                // $(document).on('change', '#back_side_degree', function(){
                //     var front_side = $('#back_side_degree').prop('files')[0];
                //     var form_data = new FormData();                  
                //     form_data.append('file', front_side);
                //     $.ajax({
                //         url: 'upload.php',
                //         dataType: 'text',  // what to expect back from the PHP script, if anything
                //         cache: false,
                //         contentType: false,
                //         processData: false,
                //         data: form_data,                         
                //         type: 'post',
                //         success: function(php_script_response){
                //             console.log(php_script_response);
                //         }
                //     });
                // });
                
                // $(document).on('change', '#back_side', function(){
                //     var front_side = $('#back_side').prop('files')[0];
                //     var form_data = new FormData();                  
                //     form_data.append('file', front_side);
                //     $.ajax({
                //         url: 'upload.php',
                //         dataType: 'text',  // what to expect back from the PHP script, if anything
                //         cache: false,
                //         contentType: false,
                //         processData: false,
                //         data: form_data,                         
                //         type: 'post',
                //         success: function(php_script_response){
                //             console.log(php_script_response);
                //         }
                //     });
                // });

                $(document).on('change', '#front_side_degree', function(){
                    var front_side = $('#front_side_degree').prop('files')[0];
                    var form_data = new FormData();                  
                    form_data.append('file', front_side);
                    $.ajax({
                        url: 'upload.php',
                        dataType: 'text',  // what to expect back from the PHP script, if anything
                        cache: false,
                        contentType: false,
                        processData: false,
                        data: form_data,                         
                        type: 'post',
                        success: function(php_script_response){
                            console.log(php_script_response);
                        }
                    });
                });
            });
        </script>
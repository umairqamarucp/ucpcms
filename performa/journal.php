<?php
include("header.php");
?>

    <div class="container-fluid">
        <div class="row">
            <h5 class="display-5">Journal</h5>
            <hr>
            <form action="postprocess.php" method="POST">
            <table class="table table-stripes table-borderless">
                <thead>
                    <th></th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    <tr>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Title</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="journal_title" name="journal_title">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>First Author</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" id="journal_first_author" required pattern="[a-zA-Z\s]{3,}" name="journal_first_author">
                        </td>
                        <td class="col--3 align-bottom">
                            <label>
                                <input type="checkbox" class="filled-in" id="ucp_check" name="ucp_check" /><span>UCP</span></label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Position in Authorship</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="journal_author_pos" name="journal_author_pos">
                                <option></option>
                                <option>1st</option><option>2nd</option><option>3rd</option><option>4th</option>
                                <option>5th</option><option>6th</option><option>7th</option><option>8th</option>

                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Author Affiliation</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="journal_author_affiliation" name="journal_author_affiliation">
                                <option></option>
                                <option>UCP</option>
                                <option>Others</option>
                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Paper Status</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="journal_paper_status" name="journal_paper_status">
                                <option></option>
                                <option>Accepted</option>
                                <option>Published</option>

                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>HEC Category</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="journal_HEC_category" name="journal_HEC_category">
                                <option></option>
                                <option>W</option><option>X</option><option>Y</option><option>Z</option>
                                <option>Non-Recognized</option>
                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Journal</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required pattern="[a-zA-Z\s]{3,}" id="journal" name="journal">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Volume</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required pattern="[0-9\s]{1,}" id="journal_vol" name="journal_vol">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Issue</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="journal_issue" name="journal_issue">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Pages</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[0-9\s]{1,}" required id="journal_pages" name="journal_pages">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Year</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="journal_year" name="journal_year">
                            <option></option>
                        <?php 
                                for($i = 2008 ; $i <= date('Y'); $i++){
                                    echo "<option value='$i'>$i</option>";
                                }
                            ?>
                    </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Publisher</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required id="journal_publisher" name="journal_publisher">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>URL/DOI</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="journal_url" name="journal_url">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1"></td>
                        <td class="col--2">
                            <button class="btn btn-primary btn-sm" type="submit" id="journal_save">Save</button>
                            <button class="btn btn-primary btn-sm" type="button" id="journal_list">List</button>
                        </td>
                        <td class="col--3"></td>
                    </tr>

                </tbody>
            </table>
</form>
        </div>
        
        <div class="row">
                <table class="table table-stripes table-borderless">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Paper Status</th>
                            <th>HEC Category</th>
                            <th>Journal</th>
                            <th>Year </th>
                            <th>URL</th>
                        </tr>
                    </thead>
                    <tbody id="education_tbody">
                    <?php
                    include('connection.php');
                    if(isset($_SESSION["searched_faculty"]))
                    {
                        $query="SELECT * FROM journal"; // WHERE cnic = '".$_SESSION['searched_faculty']."'
                        $result = mysqli_query($con, $query);
                        // var_dump($query);
                        // var_dump($result);
                        if(mysqli_num_rows($result))
                        {
                            $output = '';
                                while($row = mysqli_fetch_array($result))
                                {
                                    $output.= '<tr>';
                                    $output.= '<td class="deg">'.$row['journal_title'].'<input type="hidden" name="degree[]" value='.$row['journal_title'].'></td>';
                                    $output.= '<td class="brd">'.$row['journal_first_author'].'<input type="hidden" name="board[]" value='.$row['journal_first_author'].'></td>';
                                    $output.= '<td class="yr">'.$row['journal_paper_status'].'<input type="hidden" name="year[]" value='.$row['journal_paper_status'].'></td>';
                                    $output.= '<td class="tmarks">'.$row['journal_HEC_category'].'<input type="hidden" name="tot_marks[]" value='.$row['journal_HEC_category'].'></td>';
                                    $output.= '<td class="omarks">'.$row['journal'].'<input type="hidden" name="ob_marks[]" value='.$row['journal'].'></td>';
                                    $output.= '<td class="distin">'.$row['journal_year'].'<input type="hidden" name="distinction[]" value='.$row['journal_year'].'></td>';
                                    $output.= '<td class="distin">'.$row['journal_url'].'<input type="hidden" name="distinction[]" value='.$row['journal_url'].'></td>';
                                    $output.= '</tr>';
                                }
                                echo $output;
                        }
                    } 
                    ?>
                    </tbody>
                </table>
        </div>
        
    </div>
    </div>

    <?php
include('footer.php');
?>
        <script>
            $(document).ready(function() {
                $('#nav_conference').addClass("active");
                $('#nav_faculty').removeClass("active");
                $('#nav_home').removeClass("active");
                $('#nav_qual').removeClass("active");
                $('#nav_workload').removeClass("active");
                $('#nav_std_supr').removeClass("active");
                $('#nav_docs').removeClass("active");

                $(".select--tag").select2({
                    placeholder: "--Select--"
                });
                $("#conf_mode").select2({
                    placeholder: "--Select--"
                });

            });
        </script>
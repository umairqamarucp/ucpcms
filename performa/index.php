<?php session_start()?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>UCP</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">

<style>
@import url(https://fonts.googleapis.com/css?family=Roboto:300);

.login-page {
  width: 360px;
  padding: 8% 0 0;
  margin: auto;
}
.form {
  position: relative;
  z-index: 1;
  background: #FFFFFF;
  max-width: 360px;
  margin: 0 auto 100px;
  padding: 45px;
  text-align: center;
  box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);
}
.form input {
  font-family: "Roboto", sans-serif;
  outline: 0;
  background: #f2f2f2;
  width: 100%;
  border: 0;
  margin: 0 0 15px;
  padding: 15px;
  box-sizing: border-box;
  font-size: 14px;
}
.form button {
  font-family: "Roboto", sans-serif;
  text-transform: uppercase;
  outline: 0;
  background: #4CAF50;
  width: 100%;
  border: 0;
  padding: 15px;
  color: #FFFFFF;
  font-size: 14px;
  -webkit-transition: all 0.3 ease;
  transition: all 0.3 ease;
  cursor: pointer;
}
.form button:hover,.form button:active,.form button:focus {
  background: #43A047;
}
.form .message {
  margin: 15px 0 0;
  color: #b3b3b3;
  font-size: 12px;
}
.form .message a {
  color: #4CAF50;
  text-decoration: none;
}
.form .register-form {
  display: none;
}
.container {
  position: relative;
  z-index: 1;
  max-width: 300px;
  margin: 0 auto;
}
.container:before, .container:after {
  content: "";
  display: block;
  clear: both;
}
.container .info {
  margin: 50px auto;
  text-align: center;
}
.container .info h1 {
  margin: 0 0 15px;
  padding: 0;
  font-size: 36px;
  font-weight: 300;
  color: #1a1a1a;
}
.container .info span {
  color: #4d4d4d;
  font-size: 12px;
}
.container .info span a {
  color: #000000;
  text-decoration: none;
}
.container .info span .fa {
  color: #EF3B3A;
}
body {
  background: #76b852; /* fallback for old browsers */
  background: -webkit-linear-gradient(right, #76b852, #8DC26F);
  background: -moz-linear-gradient(right, #76b852, #8DC26F);
  background: -o-linear-gradient(right, #76b852, #8DC26F);
  background: linear-gradient(to left, #2d2d2d, #8DC26F);
  font-family: "Roboto", sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;      
}
</style>
</head>
<body>

<div class="login-page">
  <div class="form">
  <form class="register-form" action="#" method="POST">
      <input type="text" name="create_name" id="create_name" placeholder="Name" required/>
      <input type="text" name="create_cnic" id="create_cnic" pattern="[0-9]{5}-[0-9]{7}-[0-9]{1}" placeholder="CNIC (XXXXX-XXXXXXX-X)" required/>
      <input type="password" name="create_password" id="create_password" placeholder="Password" required/>
      <select type="text" class="subject_selection form-control" style='margin-top:15px' required id="subject_selection_create" name="subject_selection_create">
          <option></option>
          <option>MCS</option>
          <option>ECO</option>
          <option>Social Science</option>
          <option>Data Science</option>
      </select>
      <button type="button" id='create' style='margin-top:15px'>Create</button>
      <p class="message">Already registered? <a href="#">Sign In</a></p>
    </form>
    <form class="login-form" action="#" method="POST">
      <input type="text" name="cnic" id="cnic" placeholder="XXXXX-XXXXXXX-X" required/>
      <input type="password" name="password" id="password" placeholder="password" required/>
      <select type="text" class="subject_selection form-control" style='margin-top:15px' required id="subject_selection_login" name="subject_selection_login">
          <option></option>
          <option>MCS</option>
          <option>ECO</option>
          <option>Social Science</option>
          <option>Data Science</option>
      </select>
      <button type="button" id='login' style='margin-top:15px'>login</button>
      <p class="message">Not registered? <a href="#">Create an account</a></p>
    </form>
    <p id="msg" style="color: Green">Enter your CNIC registered with us in the given format</p>
  </div>
</div>
</body>
</html>

<?php 
include('footer.php');
?>
<script>

$(document).ready(function(){
  $(".subject_selection").select2({
    placeholder: 'Select subject'
  });
  $('.message a').click(function(){
   $('form').animate({height: "toggle", opacity: "toggle"}, "slow");
});
      $('#login').on('click', function(){

        var cnic =  $('#cnic').val();
        var password =  $('#password').val();
        var degree = $('#subject_selection_login').val();
        console.log(degree);
        if(cnic != "" && password != ""){
        $.ajax({
            type: 'POST',
            url: 'postprocess.php',
            data: {login_cnic: cnic, login_password: password, login_degree: degree}
        }).done(function(res){
            // debugger;
            console.log(res);
            if(res == 1)
            {
                window.location.href = "everyone.php";
            }
            else{
              console.log();
                $('#msg').html("1. Entered CNIC is not registered with us.</br>2. Please check the CNIC or Format.</br>3. Credentials are wrong").css('color', 'Red');
            }

        });
        }
        else{
          $('#msg').html("Please Fill in all the fields").css('color', 'Red');
        }

      });

      $("#create").on('click', function(){
        var cnic =  $('#create_cnic').val();
        var password =  $('#create_password').val();
        var name =  $('#create_name').val();
        
        if(cnic != "" && password != "" && name != ""){
        $.ajax({
            type: 'POST',
            url: 'postprocess.php',
            data: {create_cnic: cnic, create_password: password, create_name: name}
        }).done(function(res){
            // debugger;
            if(res == 1)
            {
                window.location.href = "everyone.php";
            }
            else{
                $('#msg').html("1. Try again with correct formats.</br>OR </br>2. This CNIC already exists in our Database").css('color', 'Red');
            }

        });
        }
        else{
          $('#msg').html("Please Fill in all the fields").css('color', 'Red');
        }

      });
});
</script>
<?php
include("header.php");
?>

    <div class="container-fluid">
        <div class="row">
            <h5 class="display-5">Conference</h5>
            <hr>
            <form action="postprocess.php" method="POST">
            <table class="table table-stripes table-borderless">
                <thead>
                    <th></th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    <tr>
                        <td class="col--1 align-middle">
                            <p>Conference Type</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" id="conf_type" required name="conf_type">
                                <option></option>
                                <option>Local</option>
                                <option>International</option>
                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Title</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="conf_title" name="conf_title">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>First Author</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" id="conf_first_author" pattern="[a-zA-Z\s]{3,}" required name="conf_first_author">
                        </td>
                        <td class="col--3 align-bottom">
                            <label>
                                <input type="checkbox" class="filled-in" id="ucp_check" name="ucp_check" /><span>UCP</span></label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Position in Authorship</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="conf_author_pos" name="conf_author_pos">
                            <option></option>
                                <option>1st</option><option>2nd</option><option>3rd</option><option>4th</option>
                                <option>5th</option><option>6th</option><option>7th</option><option>8th</option>
                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Author Affiliation</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="conf_author_affiliation" name="conf_author_affiliation">
                            <option></option>
                                <option>UCP</option>
                                <option>Others</option>
                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Conference</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required id="conf_conference" name="conf_conference">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Conference Venue</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required id="conf_venue" name="conf_venue">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Mode of Participation</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="conf_mode" name="conf_mode">
                                <option></option>
                                <option>Key note Speaker</option>
                                <option>Oral Representation</option>

                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Volume</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[0-9\s]{1,}" required id="conf_vol" name="conf_vol">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Issue</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required id="conf_issue" name="conf_issue">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Pages</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[0-9\s]{1,}" required id="conf_pages" name="conf_pages">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Year</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="conf_year" name="conf_year">
                            <option></option>
                        <?php 
                                for($i = 2008 ; $i <= date('Y'); $i++){
                                    echo "<option value='$i'>$i</option>";
                                }
                            ?>
                    </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Publisher</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required id="conf_publisher" name="conf_publisher">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>URL/DOI</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="conf_url" name="conf_url">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1"></td>
                        <td class="col--2">
                            <button class="btn btn-primary btn-sm" type="submit" id="conf_save">Save</button>
                            <button class="btn btn-primary btn-sm" type="button" id="conf_list">List</button>
                        </td>
                        <td class="col--3"></td>
                    </tr>

                </tbody>
            </table>
</form>
        </div>
        <div class="row">
                <table class="table table-stripes table-borderless">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Conference</th>
                            <th>Year </th>
                            <th>URL</th>
                        </tr>
                    </thead>
                    <tbody id="education_tbody">
                    <?php
                    include('connection.php');
                    if(isset($_SESSION["searched_faculty"]))
                    {
                        $query="SELECT * FROM conference"; // WHERE cnic = '".$_SESSION['searched_faculty']."'
                        $result = mysqli_query($con, $query);
                        // var_dump($query);
                        // var_dump($result);
                        if(mysqli_num_rows($result))
                        {
                            $output = '';
                                while($row = mysqli_fetch_array($result))
                                {
                                    $output.= '<tr>';
                                    $output.= '<td class="deg">'.$row['title'].'<input type="hidden" name="degree[]" value='.$row['title'].'></td>';
                                    $output.= '<td class="brd">'.$row['first_author'].'<input type="hidden" name="board[]" value='.$row['first_author'].'></td>';
                                    $output.= '<td class="yr">'.$row['conference'].'<input type="hidden" name="year[]" value='.$row['conference'].'></td>';
                                    $output.= '<td class="distin">'.$row['year'].'<input type="hidden" name="distinction[]" value='.$row['year'].'></td>';
                                    $output.= '<td class="distin">'.$row['url'].'<input type="hidden" name="distinction[]" value='.$row['url'].'></td>';
                                    $output.= '</tr>';
                                }
                                echo $output;
                        }
                    } 
                    ?>
                    </tbody>
                </table>
        </div>
    </div>

    <?php
include('footer.php');
?>
        <script>
            $(document).ready(function() {
                $('#nav_conference').addClass("active");
                $('#nav_faculty').removeClass("active");
                $('#nav_home').removeClass("active");
                $('#nav_qual').removeClass("active");
                $('#nav_workload').removeClass("active");
                $('#nav_std_supr').removeClass("active");
                $('#nav_docs').removeClass("active");

                $(".select--tag").select2({
                    placeholder: "--Select--"
                });
                $("#conf_mode").select2({
                    placeholder: "--Select--"
                });

            });
        </script>
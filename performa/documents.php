<?php
   include("header.php");
   ?>
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1>Upload Documents</h1>
            <p>Upload all the documents provided in the checklist by HEC. Upload buttons are labeled for easier interaction.</p>
            <p class="text-danger">
                <li class="text-danger">First of all select the program.</li>
                <li class="text-danger">Then select the faculty member to proceed.</li>
            </p>
        </div>
    </div>
    <div class="container" style="margin-top:30px">
        <div class="row">
            <div class="col-md-10">
                <form action="upload_documents.php" method="post" enctype="multipart/form-data">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="program">Program:</label>
                            <select class="js-example-basic-single-program" name="program" id="program" style="width:100%">
                                <option></option>
                                <option value="Ph.D">Ph.D</option>
                                <option value="MS">MS/M.Phil</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="faculty_members">Faculty Members:</label>
                            <select class="js-example-basic-single-faculty-members faculty" name="faculty_members" id="faculty_members" style="width:100%">
                                <option value=""></option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12" id="ms_div">

                        <table class="table table-responsive">
                            <tbody>
                                <tr>
                                    <td>
                                        <label for="offer_letter_ms">Offer Letter:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="offer_letter_ms" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="joining_report_ms">Joining Report:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="joining_report_ms" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="appointment_notification_ms">Contract/Notification of Appointment:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="appointment_notification_ms" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="cv_ms">Detailed CV:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="cv_ms" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="ms_degree">Degrees of MS/M.Phil Or Equivalent:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="ms_degree" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="ms_transcript">Transcript of MS/M.Phil Or Equivalent:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="ms_transcript" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="ms_equivalence">Equivalence of MS/M.Phil from HEC:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="ms_equivalence" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="bs_degree">Degrees of Bachelor or Equivalent:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="bs_degree" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="bs_equivalence">Transcript pf Bachelor or Equivalent:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="bs_equivalence" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="semester_workload">Semester Work Load:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="semester_workload" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="course_detail">Details of courses being taught in Current Semester:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="course_detail" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="ms_students">List of MS/M.Phil students being currently Supervised:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="ms_students" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="undertaking_certificate">Certificate of undertaking that no student is supervised outside the University:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="undertaking_certificate" name="ms[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>

                            </tbody>
                        </table>

                        <button class="btn btn-success btn-lg" id="upload_documents" type="submit">Upload Documents</button>
                    </div>

                    <div class="col-md-12" id="phd_div">

                        <table class="table table-responsive">
                            <tbody>
                                <tr>
                                    <td>
                                        <label for="offer_letter_phd">Offer Letter:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="offer_letter_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="joining_report_phd">Joining Report:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="joining_report_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="appointment_notification_phd">Contract/Notification of Appointment:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="appointment_notification_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="cv_phd">Detailed CV:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="cv_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="phd_degree">Degrees of Ph.D Or Equivalent:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="phd_degree" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="phd_transcript">Transcript of Ph.D Or Equivalent:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="phd_transcript" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="phd_equivalence">Equivalence of Ph.D from HEC:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="phd_equivalence" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="ms_degree_phd">Degrees of MS/M.Phil Or Equivalent:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="ms_degree_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="ms_transcript_phd">Transcript of MS/M.Phil Or Equivalent:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="ms_transcript_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="ms_equivalence_phd">Equivalence of MS/M.Phil from HEC:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="ms_equivalence_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="semester_workload_phd">Semester Work Load:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="semester_workload_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="course_detail_phd">Details of courses being taught in Current Semester:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="course_detail_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="phd_students">List of Ph.D students being currently Supervised:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="phd_students" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="ms_students_phd">List of MS/M.Phil students being currently Supervised:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="ms_students_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="undertaking_certificate_phd">Certificate of undertaking that no student is supervised outside the University:</label>
                                    </td>
                                    <td>
                                        <input class="file-upload" id="undertaking_certificate_phd" name="phd[]" type="file" accept="image/jpeg,image/gif,image/png,application/pdf,application/doc,application/docx" />
                                    </td>
                                </tr>

                            </tbody>
                        </table>

                        <button class="btn btn-success btn-lg" id="upload_documents" type="submit">Upload Documents</button>
                    </div>
                </form>
            </div>
            <div class="col-md-2" id="phd_header_div">
                <table class="table">
                    <tbody>
                        <tr>
                            <td>
                                <button class="btn btn-success btn-sm phd--header" id="offer_phd">Offer Letter</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm phd--header disabled" id="joining_phd">Joining Report</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm phd--header disabled" id="appointment_phd">Appointment</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm phd--header disabled" id="cv_phd">Detailed CV</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-success btn-sm phd--header" id="phd_degree_phd">Ph.D Degree</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm phd--header disabled" id="phd_transcript_phd">Ph.D Transcript</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm phd--header disabled" id="phd_hec_phd">Ph.D HEC</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-success btn-sm phd--header" id="ms_degree_phd">MS Degree</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm phd--header disabled" id="ms_transcript_phd">MS Transcript</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm phd--header disabled" id="ms_hec_phd">MS HEC</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm phd--header disabled" id="workload_phd">Work Load</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm phd--header disabled" id="course_details_phd">Course Details</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-success btn-sm phd--header" id="phd_students_phd">Ph.D Students</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-success btn-sm phd--header" id="ms_students_phd">MS Students</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm phd--header disabled" id="undertaking_phd">Undertaking Certificate</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-2" id="ms_header_div">
                <table class="table ">
                    <tbody>
                        <tr>
                            <td>
                                <button class="btn btn-success btn-sm ms--header" id="offer_ms">Offer Letter</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm ms--header disabled" id="joining_ms">Joining Report</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm ms--header disabled" id="appointment_ms">Appointment</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm ms--header disabled" id="cv_ms">Detailed CV</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-success btn-sm ms--header" id="ms_degree_ms">MS Degree</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm ms--header disabled" id="ms_transcript_ms">MS Transcript</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm ms--header disabled" id="ms_hec_ms">MS HEC</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-success btn-sm ms--header" id="bs_degree_ms">BS Degree</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm ms--header disabled" id="bs_transcript_ms">BS Transcript</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm ms--header disabled" id="workload_ms">Semester WorkLoad</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm ms--header disabled" id="course_detail_ms">Course Detail</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-success btn-sm ms--header" id="ms_students_ms">MS Students</button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button class="btn btn-dark btn-sm ms--header disabled" id="undertaking_ms">Undertaking Certificate</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

        </div>

        <!-- <button class="btn btn-success btn-lg" id="upload_documents" >Upload Documents</button> -->
    </div>
    <?php
    include("modals.php");
   include("footer.php");
   ?>

        <script>
            $(document).ready(function() {
                function grey_color() {
                    $("#offer_letter_ms").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#offer_letter_phd").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#joining_report_ms").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#joining_report_phd").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#appointment_notification_ms").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#appointment_notification_phd").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#cv_ms").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#cv_phd").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#phd_degree").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#phd_transcript").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#phd_equivalence").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#ms_degree").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#ms_degree_phd").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#ms_transcript").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#ms_transcript_phd").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#ms_equivalence").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#ms_equivalence_phd").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#bs_degree").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#bs_equivalence").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#semester_workload").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#semester_workload_phd").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#course_detail").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#course_detail_phd").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#phd_students").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#ms_students").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#ms_students_phd").parent().parent().css({
                        "color": "Grey"
                    });

                    $("#undertaking_certificate").parent().parent().css({
                        "color": "Grey"
                    });
                    $("#undertaking_certificate_phd").parent().parent().css({
                        "color": "Grey"
                    });
                }

                $(document).on('change', '#faculty_members', function() {
                    var fm = $("#faculty_members").val();
                    //sending the selected faculty member and retreiving the info about the documents uploaded
                    $.ajax({
                        type: "POST",
                        url: "separatefile.php",
                        data: {
                            fm: fm
                        }
                    }).done(function(response) {
                        console.log(response);
                        grey_color();
                        if (response != 0) {
                            data = $.parseJSON(response);
                            $.each(data, function(i, value) {
                                console.log('index: ' + i + ',value: ' + value);
                                if (value == 0) {
                                    if (i == 'offer_letter') {
                                        $("#offer_letter_ms").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#offer_letter_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'joining_report') {
                                        $("#joining_report_ms").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#joining_report_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'appointment_notification') {
                                        $("#appointment_notification_ms").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#appointment_notification_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'cv') {
                                        $("#cv_ms").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#cv_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'phd_degree') {
                                        $("#phd_degree").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'phd_transcript') {
                                        $("#phd_transcript").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'phd_equivalence') {
                                        $("#phd_equivalence").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'ms_degree') {
                                        $("#ms_degree").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#ms_degree_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'ms_transcript') {
                                        $("#ms_transcript").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#ms_transcript_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'ms_equivalence') {
                                        $("#ms_equivalence").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#ms_equivalence_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'bs_degree') {
                                        $("#bs_degree").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'bs_equivalence') {
                                        $("#bs_equivalence").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'semester_workload') {
                                        $("#semester_workload").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#semester_workload_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'course_detail') {
                                        $("#course_detail").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#course_detail_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'phd_students') {
                                        $("#phd_students").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'ms_students') {
                                        $("#ms_students").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#ms_students_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    } else if (i == 'undertaking_certificate') {
                                        $("#undertaking_certificate").parent().parent().css({
                                            "color": "red"
                                        });
                                        $("#undertaking_certificate_phd").parent().parent().css({
                                            "color": "red"
                                        });
                                    }

                                }
                            });
                        } else {
                            $("#offer_letter_ms").parent().parent().css({
                                "color": "red"
                            });
                            $("#offer_letter_phd").parent().parent().css({
                                "color": "red"
                            });

                            $("#joining_report_ms").parent().parent().css({
                                "color": "red"
                            });
                            $("#joining_report_phd").parent().parent().css({
                                "color": "red"
                            });

                            $("#appointment_notification_ms").parent().parent().css({
                                "color": "red"
                            });
                            $("#appointment_notification_phd").parent().parent().css({
                                "color": "red"
                            });

                            $("#cv_ms").parent().parent().css({
                                "color": "red"
                            });
                            $("#cv_phd").parent().parent().css({
                                "color": "red"
                            });

                            $("#phd_degree").parent().parent().css({
                                "color": "red"
                            });

                            $("#phd_transcript").parent().parent().css({
                                "color": "red"
                            });

                            $("#phd_equivalence").parent().parent().css({
                                "color": "red"
                            });

                            $("#ms_degree").parent().parent().css({
                                "color": "red"
                            });
                            $("#ms_degree_phd").parent().parent().css({
                                "color": "red"
                            });

                            $("#ms_transcript").parent().parent().css({
                                "color": "red"
                            });
                            $("#ms_transcript_phd").parent().parent().css({
                                "color": "red"
                            });

                            $("#ms_equivalence").parent().parent().css({
                                "color": "red"
                            });
                            $("#ms_equivalence_phd").parent().parent().css({
                                "color": "red"
                            });

                            $("#bs_degree").parent().parent().css({
                                "color": "red"
                            });

                            $("#bs_equivalence").parent().parent().css({
                                "color": "red"
                            });

                            $("#semester_workload").parent().parent().css({
                                "color": "red"
                            });
                            $("#semester_workload_phd").parent().parent().css({
                                "color": "red"
                            });

                            $("#course_detail").parent().parent().css({
                                "color": "red"
                            });
                            $("#course_detail_phd").parent().parent().css({
                                "color": "red"
                            });

                            $("#phd_students").parent().parent().css({
                                "color": "red"
                            });

                            $("#ms_students").parent().parent().css({
                                "color": "red"
                            });
                            $("#ms_students_phd").parent().parent().css({
                                "color": "red"
                            });

                            $("#undertaking_certificate").parent().parent().css({
                                "color": "red"
                            });
                            $("#undertaking_certificate_phd").parent().parent().css({
                                "color": "red"
                            });
                        }

                    }).fail(function(response) {
                        console.log(response);
                    });
                });

                $(document).on('change', '#program', function() {
                    $('.faculty').select2();

                    var program = $('#program').val();
                    grey_color();
                    //filling out the dropdown on the basis of program selected
                    $.ajax({
                        type: 'POST',
                        url: 'separatefile.php',
                        data: {
                            document: program
                        }
                    }).done(function(response) {
                        $('.faculty').html(response);
                        // debugger;
                        // alert("success");
                    }).fail(function(response) {
                        alert(response);
                    });
                });

                $(document).on('click', '.phd--header', function(){
                    var id = this.id;
                    if(id=="offer_phd")
                    {
                        $("#offer_letter").modal();
                    }
                    else if(id=="ms_degree_phd")
                    {
                        $("#ms_modal").modal();
                    }
                    else if(id=="phd_degree_phd")
                    {
                        $("#phd_modal").modal();
                    }
                    else if(id == "ms_students_phd")
                    {
                        $("#ms_supervision_modal").modal();
                    }
                    else if(id == "phd_students_phd")
                    {
                        $("#phd_supervision_modal").modal();
                    }
                });

                $(document).on('click', '.ms--header', function(){
                    var id = this.id;
                    if(id=="offer_ms")
                    {
                        $("#offer_letter").modal();
                    }
                    else if(id=="bs_degree_ms")
                    {
                        $("#bs_modal").modal();
                    }
                    else if(id=="ms_degree_ms")
                    {
                        $("#ms_modal").modal();
                    }
                    else if(id == "ms_students_ms")
                    {
                        $("#ms_supervision_modal").modal();
                    }
                });
            });
        </script>
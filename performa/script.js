$(document).ready(function(){
    $('#update_password').modal();

    $('.js-example-basic-single-program').select2({
        placeholder: "Select a Program"
    });
    $('.faculty').select2({
        placeholder: "Select a Faculty Member to submit documents"
    });
    $(".dropdown-trigger").dropdown();
    $(document).find("#ms_div").hide();
    $(document).find("#phd_div").hide();
    $(document).find("#ms_header_div").hide();
    $(document).find("#phd_header_div").hide();

    $(document).on("change", "#program", function(){
        var select_val = $("#program").val();
        if(select_val == "Ph.D")
        {
            $(document).find("#phd_div").show(); 
            $(document).find("#ms_div").hide();
            $(document).find("#phd_header_div").show(); 
            $(document).find("#ms_header_div").hide();   
        }
        else if(select_val == "MS")
        {
            $(document).find("#ms_div").show();
            $(document).find("#phd_div").hide(); 
            $(document).find("#ms_header_div").show();
            $(document).find("#phd_header_div").hide();    
        }
    });

    $(document).on('click','#logout', function(){

        $.ajax({
            type: 'POST',
            url: 'postprocess.php',
            data: {logout: 'logout'}
        }).done(function(res){
            if(res == 1)
            {
                window.location.href = "index.php";
            }
        });
    });
    
    $(document).on('keyup', '#old_password', function(){
        $("#old_msg").html("");
    });

    $(document).on('keyup', '#confirm_password', function(){
        var pwd = $("#new_password").val();
        var c_pwd = $("#confirm_password").val();
        console.log(c_pwd);
        if(pwd == c_pwd)
        {
            $("#c_pwd_msg").html("Password Match").css('color', 'Green');
            $("#update_pwd_btn").prop('disabled', false);
        }
        else{
            $("#c_pwd_msg").html("Password does not Match").css('color', 'Red');
            $("#update_pwd_btn").prop('disabled', true);
        }
    });
    $(document).on('click', '#update_pwd_btn', function(){
        var pwd = $("#new_password").val();
        var old_pwd = $("#old_password").val();

        $.ajax({
            type: 'POST',
            url: 'postprocess.php',
            data: {
                updated_pwd: pwd,
                old_pwd: old_pwd
            }
        }).done(function(res){
            console.log(res);
            
            if(res==1)
            {
                $("#success_msg").html("Password updated successfully.").css('color', 'green');
                $("#old_msg").html("");
                $("#old_password").val("");
                $("#new_password").val("");
                $("#confirm_password").val("");
            }
            else
                $("#old_msg").html("Old password is incorrect").css('color', 'red');
        });
    });


    

  });
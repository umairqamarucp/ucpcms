<?php
include("header.php");
include("connection.php");
// session_start();

if(isset($_SESSION['searched_faculty'])){
$select_query = "SELECT * FROM faculty WHERE cnic = '".$_SESSION['searched_faculty']."'";
$result = mysqli_query($con, $select_query);
if(mysqli_num_rows($result)){while($row = mysqli_fetch_array($result))
    {
// var_dump(mysqli_num_rows($result));
?>

    <div class="container" style="margin-top:30px">
        <form action="postprocess.php" method="post" enctype="multipart/form-data">
            <div class="row">
                <h4 class="display-4"><i class="fa fa-edit"></i>  Personal Info</h4>
                <hr>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name_faculty"><i class="fa fa-user"></i> Name:</label>
                        <input type="text" class="form-control" name="name_faculty" id="name_faculty" required pattern="[a-zA-Z\s]{3,}" value="<?php echo $row['name'];?>">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="father_name"><i class="fa fa-user"></i> Father Name:</label>
                        <input type="text" class="form-control" id="father_name" name="father_name" required pattern="[a-zA-Z\s]{3,}" value="<?php echo $row['father'];?>">
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="cnic"><i class="fa fa-id-card"></i> CNIC:</label>
                        <input type="text" class="form-control" id="cnic" name="cnic" pattern="[0-9]{5}-[0-9]{7}-[0-9]{1}" title="33xxx-1234567-x" required value="<?php echo $row['cnic'];?>">
                    </div>
                </div>
                <div class="col-md-6">
                        <div class="form-group">
                            <label for="cnic_file"><i class="fa fa-file"></i> CNIC:</label>
                            <input type="file" class="form-control" id="cnic_file" name="cnic_file">
                        </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email"><i class="fa fa-at"></i> Email:</label>
                        <input type="email" class="form-control" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required value="<?php echo $row['email'];?>">
                    </div>
                </div>
                <div class="col-md-6">

                    <div class="form-group">
                        <label for="cell_phone"><i class="fa fa-phone"></i> Cell#:</label>
                        <input type="text" class="form-control" id="cell_phone" name="cell_phone" pattern="[0-9]{11,11}" title="03123456789" required value="<?php echo $row['cell'];?>">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="domicile"><i class="fa fa-file"></i> Domicile:</label>
                        <select type="text" class="form-control domicile" id="domicile" required name="domicile">
                            <option></option>
                            <?php

                    $string = file_get_contents("district.json");
                    $json_a = json_decode($string, true);

                    // Populate the Domicile and city
                    // ", if($row['domicile']==$val['city']) ? 'selected:"selected"' : '', "
                    $myvar = "";
                    foreach ($json_a as $key => $val) {
                         $myvar .= "<option"; 
                         
                         if($row['domicile']==$val['city'])
                            {
                                $myvar .= " selected='selected'";
                            } 
                         else{
                             $myvar .= " ";
                         }
                         $myvar .= ">"; 
                         $myvar .= $val['city'];
                         $myvar .= "</option>";
                    }
                        echo $myvar;
                    ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="religion" <i class="fa fa-user">
                            </i> Religion:</label>
                        <select type="text" class="form-control religion" id="religion" name="religion" value="<?php echo $row['religion']; ?>">
                            <option></option>
                            <option <?=$row[ 'religion']=='Islam' ? ' selected="selected"' : '';?>>Islam</option>
                            <option <?=$row[ 'religion']=='Christianity' ? ' selected="selected"' : '';?>>Christianity</option>
                            <option <?=$row[ 'religion']=='Hinduism' ? ' selected="selected"' : '';?>>Hinduism</option>
                            <option <?=$row[ 'religion']=='Jewish' ? ' selected="selected"' : '';?>>Jewish</option>
                            <option <?=$row[ 'religion']=='Other' ? ' selected="selected"' : '';?>>Other</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="designation"><i class="fa fa-briefcase"></i> Designation:</label>
                        <select class="form-control" id="designation" name="designation" required>
                            <option></option>
                            <option <?=$row[ 'designation']=='Professor' ? ' selected="selected"' : '';?>>Professor</option>
                            <option <?=$row[ 'designation']=='Instructor' ? ' selected="selected"' : '';?>>Instructor</option>
                            <option <?=$row[ 'designation']=='Lab Instructor' ? ' selected="selected"' : '';?>>Lab Instructor</option>
                        </select>
                    </div>
                </div>
                
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="job_type"><i class="fa fa-briefcase"></i> Job Type:</label>
                            <select class="js-example-basic-single form-control" name="job_type" id="job_type" required style="width:100%">
                                <option value="visiting" <?=$row[ 'job']=='Visiting' ? ' selected="selected"' : '';?>>Visiting</option>
                                <option value="permanent" <?=$row[ 'job']=='Permanent' ? ' selected="selected"' : '';?>>Permanent</option>
                                <option value="adjoint" <?=$row[ 'job']=='Adjoint' ? ' selected="selected"' : '';?>>Adjoint</option>
                            </select>
                            <input type="file" class="form-control" id="job_type_doc" style="margin-top:5px" name="job_type_doc">
                        </div>
                    </div>
                

            <div class="col-md-6">
                <div class="form-group">
                    <label for="cv"><i class="fa fa-file"></i> Detailed CV:</label>
                    <input type="file" class="form-control" id="cv" name="cv">
                </div>
            </div>
            

    </div>
    </div>

    <hr>

    <div class="container">
        <div class="row">

            <div class="col-md-6">
                <h5 class="display-5"><i class="fa fa-home"></i> Temporary Address</h5>
                <hr>
                <div class="form-group">
                    <label for="temp_house">House #:</label>
                    <input type="text" class="form-control" id="temp_house" name="temp_house" required pattern="[0-9]{1,3}" title="Only numeric values" value="<?php echo $row['temp_house'];?>">
                </div>
                <div class="form-group">
                    <label for="temp_street">Street #:</label>
                    <input type="text" class="form-control" id="temp_street" name="temp_street" required pattern="[0-9]{1,3}" title="Only numeric values" value="<?php echo $row['temp_street'];?>">
                </div>
                <div class="form-group">
                    <label for="temp_block">Phase/Block:</label>
                    <input type="text" class="form-control" id="temp_block" name="temp_block" required pattern="[a-zA-Z\s]{3,20}" title="Only alphabetical values" value="<?php echo $row['temp_block'];?>">
                </div>
                
                <div class="form-group">
                    <label for="temp_state">Province/State:</label>
                    <select type="text" class="form-control province--select" id="temp_state" name="temp_state" value="<?php echo $row['temp_state'];?>">
                        <option></option>
                        <option <?=$row[ 'temp_state']=='Punjab' ? ' selected="selected"' : '';?>>Punjab</option>
                        <option <?=$row[ 'temp_state']=='Sindh' ? ' selected="selected"' : '';?>>Sindh</option>
                        <option <?=$row[ 'temp_state']=='Khyber Pakhtunkhwa' ? ' selected="selected"' : '';?>>Khyber Pakhtunkhwa</option>
                        <option <?=$row[ 'temp_state']=='Balochistan' ? ' selected="selected"' : '';?>>Balochistan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="temp_city">City:</label>
                    <select type="text" class="form-control city" id="temp_city" name="temp_city" required value="<?php echo $row['temp_city'];?>">
                        <?php
                         $string = file_get_contents("pk.json");
                         $json_a = json_decode($string, true);

                         $myvar3 = "<option></option>";
                         foreach ($json_a as $key => $val) {
                             $myvar3 .= "<option"; 
                             
                             if($row['temp_city']==$val['city'])
                                 {
                                     $myvar3 .= " selected='selected'";
                                 } 
                             else{
                                 $myvar3 .= " ";
                             }
                             $myvar3 .= ">"; 
                             $myvar3 .= $val['city'];
                             $myvar3 .= "</option>";
                         }
                             echo $myvar3;
                        ?>
                    </select>
                </div>
            </div>

            <div class="col-md-6">
                <h5 class="display-5"><i class="fa fa-home"></i> Permanent Address</h5>
                <hr>
                <div class="form-group">
                    <label>
                        <input type="checkbox" class="filled-in" id="temp_address_check" name="temp_address_check" />
                        <span>Same as temporary address</span>
                    </label>
                </div>
                <div class="form-group">
                    <label for="perm_house">House #:</label>
                    <input type="text" class="form-control" id="perm_house" required pattern="[0-9]{1,3}" title="Only numeric values" name="perm_house" value="<?php echo $row['perm_house'];?>">
                </div>
                <div class="form-group">
                    <label for="perm_street">Street #:</label>
                    <input type="text" class="form-control" id="perm_street" required pattern="[0-9]{1,3}" title="Only numeric values" name="perm_street" value="<?php echo $row['perm_street'];?>">
                </div>
                <div class="form-group">
                    <label for="perm_block">Phase/Block:</label>
                    <input type="text" class="form-control" id="perm_block" required pattern="[a-zA-Z\s]{3,20}" title="Only alphabetical values" name="perm_block" value="<?php echo $row['perm_block'];?>">
                </div>
                <div class="form-group">
                    <label for="perm_state">Province/State:</label>
                    <select class="form-control province--select" id="perm_state" required name="perm_state">
                        <option></option>
                        <option <?=$row[ 'perm_state']=='Punjab' ? ' selected="selected"' : '';?>>Punjab</option>
                        <option <?=$row[ 'perm_state']=='Sindh' ? ' selected="selected"' : '';?>>Sindh</option>
                        <option <?=$row[ 'perm_state']=='Khyber Pakhtunkhwa' ? ' selected="selected"' : '';?>>Khyber Pakhtunkhwa</option>
                        <option <?=$row[ 'perm_state']=='Balochistan' ? ' selected="selected"' : '';?>>Balochistan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="perm_city">City:</label>
                    <select type="text" class="form-control city" id="perm_city" required name="perm_city" value="<?php echo $row['perm_city'];?>">
                        <option></option>
                        <?php

                                $string = file_get_contents("pk.json");
                                $json_a = json_decode($string, true);

                                $myvar3 = "<option></option>";
                                foreach ($json_a as $key => $val) {
                                    $myvar3 .= "<option"; 
                                    
                                    if($row['temp_city']==$val['city'])
                                        {
                                            $myvar3 .= " selected='selected'";
                                        } 
                                    else{
                                        $myvar3 .= " ";
                                    }
                                    $myvar3 .= ">"; 
                                    $myvar3 .= $val['city'];
                                    $myvar3 .= "</option>";
                                }
                                    echo $myvar3;

                                ?>
                    </select>
                </div>
                
            </div>
        </div>
        <div class="form-group btn--add--record col-md-12">
            <button class="btn btn-success btn-block" id="add_record" type="submit"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>

    </div>
    </form>
    </div>

    <?php
    }}}
    //else{
        ?>
        <!-- <div class="container" style="margin-top:30px">
            <form action="postprocess.php" method="post" enctype="multipart/form-data">
                <div class="row">
                    <h4 class="display-4"><i class="fa fa-edit"></i>  Personal Info</h4>
                    <hr>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name_faculty"><i class="fa fa-user"></i> Name:</label>
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required name="name_faculty" id="name_faculty">
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="father_name"><i class="fa fa-user"></i> Father Name:</label>
                            <input type="text" class="form-control" id="father_name" pattern="[a-zA-Z\s]{3,}" required name="father_name">
                        </div>
                        </div>

                        <div class="col-md-6">

                        <div class="form-group">
                            <label for="cnic"><i class="fa fa-id-card"></i> CNIC:</label>
                            <input type="text" class="form-control" id="cnic" pattern="[0-9]{5}-[0-9]{7}-[0-9]{1}" required name="cnic">
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="email"><i class="fa fa-at"></i> Email:</label>
                            <input type="text" class="form-control" id="email" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" name="email">
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="cell_phone"><i class="fa fa-phone"></i> Cell#:</label>
                            <input type="text" class="form-control" id="cell_phone" pattern="[0-9]{11,11}" required name="cell_phone">
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="domicile"><i class="fa fa-file"></i> Domicile:</label>
                            <select type="text" class="form-control domicile" id="domicile" required name="domicile">
                                <option></option>
                                <?php

                    $string = file_get_contents("district.json");
                    $json_a = json_decode($string, true);

                    foreach ($json_a as $key => $val) {
                        echo "<option>" .$val['city']. "</option>";
                    }

                    ?>
                            </select>
                        </div>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="religion" <i class="fa fa-user">
                                </i> Religion:</label>
                            <select type="text" class="form-control religion" id="religion" name="religion">
                                <option></option>
                                <option>Islam</option>
                                <option>Christianity</option>
                                <option>Hinduism</option>
                                <option>Jewish</option>
                                <option>Other</option>
                            </select>
                        </div>
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="designation"><i class="fa fa-briefcase"></i> Designation:</label>
                            <input type="text" class="form-control" id="designation" required name="designation">
                        </div>
                        </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="job_type"><i class="fa fa-briefcase"></i> Job Type:</label>
                                    <select class="js-example-basic-single form-control" required name="job_type" id="job_type" style="width:100%">
                                        <option value="visiting">Visiting</option>
                                        <option value="permanent">Permanent</option>
                                        <option value="adjoint">Adjoint</option>
                                    </select>
                                    <input type="file" class="form-control" id="job_type_doc" style="margin-top:5px" name="job_type_doc">
                                </div>
                            </div>
                           
                    </div>
                    
                        
                        
                       
                        <div class="col-md-6">
                        <div class="form-group">
                            <label for="cv"><i class="fa fa-file"></i> Detailed CV:</label>
                            <input type="file" class="form-control" id="cv" name="cv">
                        </div>

                    </div>
                    </div>

                    <hr>

                    <div class="container">
                        <div class="row">

                            <div class="col-md-6">
                                <h5 class="display-5"><i class="fa fa-home"></i> Temporary Address</h5>
                                <hr>
                                <div class="form-group">
                                    <label for="temp_house">House #:</label>
                                    <input type="text" class="form-control" id="temp_house" required pattern="[0-9]{1,3}" title="Only numeric values" name="temp_house">
                                </div>
                                <div class="form-group">
                                    <label for="temp_street">Street #:</label>
                                    <input type="text" class="form-control" id="temp_street" required pattern="[0-9]{1,3}" title="Only numeric values" name="temp_street">
                                </div>
                                <div class="form-group">
                                    <label for="temp_block">Phase/Block:</label>
                                    <input type="text" class="form-control" id="temp_block" required pattern="[a-zA-Z\s]{3,20}" title="Only alphabetical values" name="temp_block">
                                </div>
                                <div class="form-group">
                                    <label for="temp_city">City:</label>
                                    <select type="text" class="form-control city" id="temp_city" required name="temp_city">
                                        <option></option>
                                        <?php

                            $string = file_get_contents("pk.json");
                            $json_a = json_decode($string, true);

                            foreach ($json_a as $key => $val) {
                                echo "<option>" .$val['city']. "</option>";
                            }

                            ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="temp_state">Province/State:</label>
                                    <select type="text" class="form-control province--select" id="temp_state" name="temp_state">
                                        <option></option>
                                        <option>Punjab</option>
                                        <option>Sindh</option>
                                        <option>Khyber PakhtonKhaah</option>
                                        <option>Balochistan</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <h5 class="display-5"><i class="fa fa-home"></i> Permanent Address</h5>
                                <hr>
                                <div class="form-group">
                                    <label>
                                        <input type="checkbox" class="filled-in" id="temp_address_check" name="temp_address_check" />
                                        <span>Same as temporary address</span>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="perm_house">House #:</label>
                                    <input type="text" class="form-control" id="perm_house" required pattern="[0-9]{1,3}" title="Only numeric values" name="perm_house">
                                </div>
                                <div class="form-group">
                                    <label for="perm_street">Street #:</label>
                                    <input type="text" class="form-control" id="perm_street" required pattern="[0-9]{1,3}" title="Only numeric values" name="perm_street">
                                </div>
                                <div class="form-group">
                                    <label for="perm_block">Phase/Block:</label>
                                    <input type="text" class="form-control" id="perm_block" required pattern="[a-zA-Z\s]{3,20}" title="Only alphabetical values" name="perm_block">
                                </div>
                                <div class="form-group">
                                    <label for="perm_city">City:</label>
                                    <select type="text" class="form-control city" id="perm_city" required name="perm_city">
                                        <option></option>
                                        <?php

                                $string = file_get_contents("pk.json");
                                $json_a = json_decode($string, true);

                                foreach ($json_a as $key => $val) {
                                    echo "<option>" .$val['city']. "</option>";
                                }

                                ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="perm_state">Province/State:</label>
                                    <select class="form-control province--select" id="perm_state" required name="perm_state">
                                        <option></option>
                                        <option>Punjab</option>
                                        <option>Sindh</option>
                                        <option>Khyber PakhtonKhwaah</option>
                                        <option>Balochistan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group btn--add--record col-md-12">
                            <button class="btn btn-success btn-block" id="add_record" type="submit"><i class="fa fa-save"></i> Save</button>
                        </div>
                    </div>

                </div>
            </form>
        </div> -->
        <?php
    //}
include('footer.php');
?>

            <script>
                $(document).ready(function() {
                    $('#nav_conference').removeClass("active");
                    $('#nav_faculty').removeClass("active");
                    $('#nav_home').addClass("active");
                    $('#nav_qual').removeClass("active");
                    $('#nav_workload').removeClass("active");
                    $('#nav_std_supr').removeClass("active");
                    $('#nav_docs').removeClass("active");

                    $('.js-example-basic-single').select2();
                    $('.province--select').select2({
                        placeholder: "Select Province"
                    });
                    $('.domicile').select2({
                        placeholder: "Domicile"
                    });
                    $('.religion').select2({
                        placeholder: "Religion"
                    });
                    $('.city').select2({
                        placeholder: "Select City"
                    });

                    $('#designation').select2({
                        placeholder: "Select Designation"
                    });
                    var my_temp_state = $('#temp_state').val();
                    var my_perm_state = $('#perm_state').val();
                    if(my_temp_state!="")
                    {
                        $.ajax({
                                type: "POST",
                                url: "cities.php",
                                data: {state: my_temp_state}
                            }).done(function(res){
                                console.log(res);
                                $('#temp_city').html(res);
                            });
                    }
                    
                    if(my_perm_state!="")
                    {
                        // alert(my_perm_state);
                        $.ajax({
                                type: "POST",
                                url: "cities.php",
                                data: {perm_state: my_perm_state}
                            }).done(function(res){
                                console.log(res);
                                $('#perm_city').html(res);
                            });
                    }
                    

                    $(document).on("change", "#temp_state", function(){
                        var state = $("#temp_state").val();
                        // alert(state);

                        $.ajax({
                            type: "POST",
                            url: "cities.php",
                            data: {state: state}
                        }).done(function(res){
                            console.log(res);
                            $('#temp_city').html(res);
                        });
                    });

                    $(document).on("change", "#perm_state", function(){
                        var state = $("#perm_state").val();
                        // alert(state);

                        $.ajax({
                            type: "POST",
                            url: "cities.php",
                            data: {perm_state: state}
                        }).done(function(res){
                            console.log(res);
                            $('#perm_city').html(res);
                        });
                    });

                    $(document).on("change", "#temp_address_check", function() {
                        var check = $("#temp_address_check").prop('checked');
                        if (check) {
                            var house = $("#temp_house").val();
                            var street = $("#temp_street").val();
                            var block = $("#temp_block").val();
                            var city = $("#temp_city").val();
                            var state = $("#temp_state").val();

                            $("#perm_house").val(house);
                            $("#perm_street").val(street);
                            $("#perm_block").val(block);
                            $("#perm_city").val(city).trigger("change");
                            $("#perm_state").val(state).trigger("change");
                        } else {
                            $("#perm_house").val("");
                            $("#perm_street").val("");
                            $("#perm_block").val("");
                            $("#perm_city").val("").trigger("change");
                            $("#perm_state").val("").trigger("change");
                        }
                    });

                    $(document).on('change', '#job_type_doc', function() {
                        var front_side = $('#job_type_doc').prop('files')[0];
                        var form_data = new FormData();
                        form_data.append('file', front_side);
                        var name = $("#cnic").val();
                        form_data.append('name', name);
                        $.ajax({
                            url: 'upload_landing.php', // point to server-side PHP script 
                            dataType: 'text', // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function(php_script_response) {
                                console.log(php_script_response); // display response from the PHP script, if any
                            }
                        });
                    });
                    $(document).on('change', '#cv', function() {
                        var front_side = $('#cv').prop('files')[0];
                        var form_data = new FormData();
                        form_data.append('file', front_side);
                        var name = $("#cnic").val();
                        form_data.append('name', name);
                        $.ajax({
                            url: 'upload_landing.php', // point to server-side PHP script 
                            dataType: 'text', // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function(php_script_response) {
                                console.log(php_script_response); // display response from the PHP script, if any
                            }
                        });
                    });
                    $(document).on('change', '#cnic_file', function() {
                        var front_side = $('#cnic_file').prop('files')[0];
                        var form_data = new FormData();
                        form_data.append('file', front_side);
                        var name = $("#cnic").val();
                        form_data.append('name', name);
                        $.ajax({
                            url: 'upload_landing.php', // point to server-side PHP script 
                            dataType: 'text', // what to expect back from the PHP script, if anything
                            cache: false,
                            contentType: false,
                            processData: false,
                            data: form_data,
                            type: 'post',
                            success: function(php_script_response) {
                                console.log(php_script_response); // display response from the PHP script, if any
                            }
                        });
                    });

                });
            </script>
<?php 
session_start();
if(! isset($_SESSION['searched_faculty']))
{
    header('Location: index.php');
    exit();
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>UCP</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">

<style>
.dropdown-trigger:hover
{
    color: white;
}
@media (max-width:576px) { #collapsibleNavbar{
        background:black;
    }
}
</style>
</head>

<body>

    <nav class="navbar navbar-expand-sm bg-dark navbar-dark justify-content-end">
        <a class="navbar-brand" href="everyone.php">UCP</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="nav navbar-nav">
                <li>
                <a class="nav-link" href="everyone.php" id="nav_faculty">Faculty Members</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="landingpage.php" id="nav_home">New Faculty</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="qualification.php" id="nav_qual">Faculty Qualification</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="workload.php" id="nav_workload">Semester Workload</a>
                </li>

                <li class="nav-item">
                <a class="nav-link" href="student_supervision.php" id="nav_std_supr">Student Supervision</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="docs.php" id="nav_docs">Faculty Documents</a>
                </li>
                <!-- <li class="nav-item">
                <a class="nav-link" href="ucpcms/login.html" id="nav_docs">Faculty</a>
                </li> -->
                <!-- <li>
                <a class="nav-link" href="conference.php" id="nav_conference">Publication</a>
                </li> -->
                <div class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" style="padding-top:7px">
                    Publication
                </a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="journal.php">Journal</a>
                    <a class="dropdown-item" href="conference.php">Conference</a>
                    <a class="dropdown-item" href="bookchapter.php">Book Chapter</a>
                    <a class="dropdown-item" href="policyreport.php">Policy Report</a>
                    <a class="dropdown-item" href="patent.php">Patent</a>
                </div>
                </div>
                
                <!-- <li>
                <a class="dropdown-trigger" style="padding:0; padding-top:7px" href="#" data-target="dropdown1">Publication <i class="fa fa-caret-down"></i></a>
                </li>


                <ul id="dropdown1" class="dropdown-content">
                <li><a href="journal.php">Journal</a></li>
                <li><a href="conference.php">Conference</a></li>
                <li><a href="bookchapter.php">Book Chapter</a></li>
                <li><a href="policyreport.php">Policy Report</a></li>
                <li><a href="patent.php">Patent</a></li>

                </ul> -->
                
            </ul>
            <ul class="nav navbar-nav ml-auto">
                    <?php 
                    if(isset($_SESSION['name']))
                        {
                            echo "<div class='dropdown'><a class='dropdown-toggle' data-toggle='dropdown' style='padding-top:7px'>".$_SESSION['name']."</a><div class='dropdown-menu' style='margin-left: -85px;'><!--<a class='dropdown-item modal-trigger' href='#update_password'>Update Password</a>--><a class='dropdown-item' id='logout' href='#'>Logout</a></div></div>"; 
                        }
                    else 
                        echo " No User Selected";
                    ?>
                </ul>
        </div>
    </nav>

    <?php
    include('modals.php'); 
    ?>
<div id="update_password" class="modal">
    <div class="modal-content">
      <h4>Update Password</h4>
      <div class="container">
            <div class="form-group">
                <label for="joining_date">Old Password:</label>
                <input type="text" class="form-control" id="old_password" name="old_password" /><p id='old_msg'></p>
                <label for="probation">New Password:</label>
                <input type="text" class="form-control" id="new_password" name="new_password" />
                <label for="probation">Confirm Password:</label>
                <input type="text" class="form-control" id="confirm_password" name="confirm_password" />
                <p id=c_pwd_msg></p>
            </div>
        </div>
        <p id="success_msg"></p>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-success waves-effect waves-green" id="update_pwd_btn">Update</button>
    </div>
  </div>


<div class="modal fade" id="update_password11">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Update Password</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="container">
                    <div class="form-group">
                        <label for="joining_date">Old Password:</label>
                        <input type="text" class="form-control" id="old_password" name="old_password" /><p id='old_msg'></p>
                        <label for="probation">New Password:</label>
                        <input type="text" class="form-control" id="new_password" name="new_password" />
                        <label for="probation">Confirm Password:</label>
                        <input type="text" class="form-control" id="confirm_password" name="confirm_password" />
                        <p id=c_pwd_msg></p>
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="update_pwd_btn">Update</button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="offer_letter">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Offer Letter</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="container">
                    <div class="form-group">
                        <label for="joining_date">Joining Date:</label>
                        <input type="text" class="form-control" id="joining_date" name="joining_date" />
                        <label for="probation">Probation:</label>
                        <input type="text" class="form-control" id="probation" name="probation" />
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="offer_letter_save" data-dismiss="modal">Save</button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="bs_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Bachelors</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="container">
                    <div class="form-group">
                        <label for="certificate_name">Certificate Name:</label>
                        <input type="text" class="form-control" id="certificate_name" name="certificate_name" />
                        <label for="specialization">Specialization:</label>
                        <input type="text" class="form-control" id="specialization" name="specialization" />
                        <label for="subjects">Subjects:</label>
                        <input type="text" class="form-control" id="subjects" name="subjects" />
                        <label for="degree_date">Degree Date:</label>
                        <input type="text" class="form-control" id="degree_date" name="degree_date" />
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="bs_save" data-dismiss="modal">Save</button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="ms_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">MS/M.Phil</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="container">
                    <div class="form-group">
                    <label for="certificate_name">Certificate Name:</label>
                        <input type="text" class="form-control" id="certificate_name" name="certificate_name" />
                        <label for="specialization">Specialization:</label>
                        <input type="text" class="form-control" id="specialization" name="specialization" />
                        <label for="subjects">Subjects:</label>
                        <input type="text" class="form-control" id="subjects" name="subjects" />
                        <label for="degree_date">Degree Date:</label>
                        <input type="text" class="form-control" id="degree_date" name="degree_date" />
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="ms_save" data-dismiss="modal">Save</button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="phd_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Ph.D</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="container">
                    <div class="form-group">
                    <label for="certificate_name">Certificate Name:</label>
                        <input type="text" class="form-control" id="certificate_name" name="certificate_name" />
                        <label for="specialization">Specialization:</label>
                        <input type="text" class="form-control" id="specialization" name="specialization" />
                        <label for="subjects">Subjects:</label>
                        <input type="text" class="form-control" id="subjects" name="subjects" />
                        <label for="degree_date">Degree Date:</label>
                        <input type="text" class="form-control" id="degree_date" name="degree_date" />
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="phd_save" data-dismiss="modal">Save</button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="ms_supervision_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">MS Students</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="container">
                    <div class="form-group">
                    <label for="ms_student_name">Name:</label>
                        <input type="text" class="form-control" id="ms_student_name" name="ms_student_name" />
                        <label for="ms_student_rollno">Roll #:</label>
                        <input type="text" class="form-control" id="ms_student_rollno" name="ms_student_rollno" />
                        <label for="ms_thesis">Thesis Title:</label>
                        <input type="text" class="form-control" id="ms_thesis" name="ms_thesis" />
                        <label for="ms_study_area">Area of Study:</label>
                        <input type="text" class="form-control" id="ms_study_area" name="ms_study_area" />
                        <label for="ms_start_date">Start Date:</label>
                        <input type="text" class="form-control" id="ms_start_date" name="ms_start_date" />
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="ms_students_save" data-dismiss="modal">Save</button>
            </div>

        </div>
    </div>
</div>

<div class="modal fade" id="phd_supervision_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">Ph.D Students</h4>
                <button type="button" class="close" data-dismiss="modal">×</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <div class="container">
                    <div class="form-group">
                    <label for="phd_student_name">Name:</label>
                        <input type="text" class="form-control" id="phd_student_name" name="phd_student_name" />
                        <label for="phd_student_rollno">Roll #:</label>
                        <input type="text" class="form-control" id="phd_student_rollno" name="phd_student_rollno" />
                        <label for="phd_thesis">Thesis Title:</label>
                        <input type="text" class="form-control" id="phd_thesis" name="phd_thesis" />
                        <label for="phd_study_area">Area of Study:</label>
                        <input type="text" class="form-control" id="phd_study_area" name="phd_study_area" />
                        <label for="phd_start_date">Start Date:</label>
                        <input type="text" class="form-control" id="phd_start_date" name="phd_start_date" />
                    </div>
                </div>
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-success" id="phd_students_save" data-dismiss="modal">Save</button>
            </div>

        </div>
    </div>
</div>
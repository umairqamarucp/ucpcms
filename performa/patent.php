<?php
include("header.php");
?>

    <div class="container-fluid">
        <div class="row">
            <h5 class="display-5">Patent</h5>
            <hr>
            <form action="postprocess.php" method="POST">
            <table class="table table-stripes table-borderless">
                <thead>
                    <th></th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Title</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="patent_title" name="patent_title">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Inventor</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required pattern="[a-zA-Z\s]{3,}" id="patent_inventor" name="patent_inventor">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Additional Inventor</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required pattern="[a-zA-Z\s]{3,}" id="patent_inventor_add" name="patent_inventor_add">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Position in Inventor</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="patent_inventor_pos" name="patent_inventor_pos">
                            <option></option>
                                <option>1st</option><option>2nd</option><option>3rd</option><option>4th</option>
                                <option>5th</option><option>6th</option><option>7th</option><option>8th</option>

                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Patent Affiliation</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="patent_affiliation" name="patent_affiliation">
                            <option></option>
                                <option>UCP</option>
                                <option>Others</option>
                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Patent Office</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="patent_office" name="patent_office">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Patent Number</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[0-9\s]{1,}" required id="patent_number" name="patent_number">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Source</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="patent_source" name="patent_source">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Year</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="patent_year" name="patent_year">
                            <option></option>
                        <?php 
                                for($i = 2008 ; $i <= date('Y'); $i++){
                                    echo "<option value='$i'>$i</option>";
                                }
                            ?>
                    </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Publisher</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required id="patent_publisher" name="patent_publisher">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>URL/DOI</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="patent_url" name="patent_url">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1"></td>
                        <td class="col--2">
                            <button class="btn btn-primary btn-sm" type="submit" id="patent_save">Save</button>
                            <button class="btn btn-primary btn-sm" type="button" id="patent_list">List</button>
                        </td>
                        <td class="col--3"></td>
                    </tr>

                </tbody>
            </table>
</form>
        </div>
        <div class="row">
                <table class="table table-stripes table-borderless">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Inventor</th>
                            <th>Source</th>
                            <th>Year </th>
                            <th>URL</th>
                        </tr>
                    </thead>
                    <tbody id="education_tbody">
                    <?php
                    include('connection.php');
                    if(isset($_SESSION["searched_faculty"]))
                    {
                        $query="SELECT * FROM patent"; // WHERE cnic = '".$_SESSION['searched_faculty']."'
                        $result = mysqli_query($con, $query);
                        // var_dump($query);
                        // var_dump($result);
                        if(mysqli_num_rows($result))
                        {
                            $output = '';
                                while($row = mysqli_fetch_array($result))
                                {
                                    $output.= '<tr>';
                                    $output.= '<td class="deg">'.$row['patent_title'].'<input type="hidden" name="degree[]" value='.$row['patent_title'].'></td>';
                                    $output.= '<td class="brd">'.$row['patent_inventor'].'<input type="hidden" name="board[]" value='.$row['patent_inventor'].'></td>';
                                    $output.= '<td class="yr">'.$row['patent_source'].'<input type="hidden" name="year[]" value='.$row['patent_source'].'></td>';
                                    $output.= '<td class="distin">'.$row['patent_year'].'<input type="hidden" name="distinction[]" value='.$row['patent_year'].'></td>';
                                    $output.= '<td class="distin">'.$row['patent_url'].'<input type="hidden" name="distinction[]" value='.$row['patent_url'].'></td>';
                                    $output.= '</tr>';
                                }
                                echo $output;
                        }
                    } 
                    ?>
                    </tbody>
                </table>
        </div>
    </div>

    <?php
include('footer.php');
?>
        <script>
            $(document).ready(function() {
                $('#nav_conference').addClass("active");
                $('#nav_faculty').removeClass("active");
                $('#nav_home').removeClass("active");
                $('#nav_qual').removeClass("active");
                $('#nav_workload').removeClass("active");
                $('#nav_std_supr').removeClass("active");
                $('#nav_docs').removeClass("active");

                $(".select--tag").select2({
                    placeholder: "--Select--"
                });
                $("#conf_mode").select2({
                    placeholder: "--Select--"
                });

            });
        </script>
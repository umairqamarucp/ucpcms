<?php
// session_start();
include("header.php");
?>
    <div class="container">
        <div class="row">
            <h4 class="display-4"><i class="fa fa-list"></i>  Work Load</h4>
            <hr>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="semester_select"><i class="fa fa-user-graduate"></i> Semester:</label>
                    <select class="semester_select form-control" id="semester_select" required name="semester_select" style="width:100%">
                        <option></option>
                        <option>Fall</option>
                        <option>Spring</option>
                        <option>Summer</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="courses"><i class="fa fa-list"></i> Course List:</label>
                    <select class="courses form-control" id="courses" required name="courses" style="width:100%">
                        <option></option>
                        <option>OOP</option>
                        <option>DataBase</option>
                        <option>Programming</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>
                        <input type="checkbox" class="filled-in" id="lab_check" name="lab_check" />
                        <span>Lab</span>
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="year">Year:</label>
                    <select class="year form-control" name="year" id="year" required style="width:100%">
                        <option></option>
                        <?php 
                                for($i = 2010 ; $i <= date('Y'); $i++){
                                    echo "<option value='$i'>$i</option>";
                                }
                            ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="cred_hrs"><i class="fa fa-hourglass"></i> Credit Hours:</label>
                    <select class="cred_hrs form-control" id="cred_hrs" name="cred_hrs" required style="width:100%">
                        <option></option>
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                    </select>
                </div>
                <div class="form-group">
                    <button class="btn btn-success btn-block" id="workload_button"><i class="fa fa-plus"></i> Add</button>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <form action="postprocess.php" method="post" enctype="multipart/form-data">
                <div class="col-md-12">
                    <table class="table table-stripes table-borderless">
                        <thead>
                            <tr>
                                <th>Semester</th>
                                <th>Year</th>
                                <th>Course</th>
                                <th>Credit Hours</th>
                                <th>Edit/Delete</th>
                            </tr>
                        </thead>
                        <tbody id="workload_tbody">
                            <?php
                    include('connection.php');
                    if(isset($_SESSION["searched_faculty"]))
                    {
                        $query="SELECT * FROM workload WHERE cnic = '".$_SESSION['searched_faculty']."'";
                        $result = mysqli_query($con, $query);
                        // var_dump($query);
                        if(mysqli_num_rows($result))
                        {
                            $output = '';
                                while($row = mysqli_fetch_array($result))
                                {
                                    $output.= '<tr>';
                                    // $output.= '<td>'.$row['name'].'</td>';
                                    $output.= '<td class="sems">'.$row['semester'].'<input type="hidden" name="semester[]" value='.$row['semester'].'></td>';
                                    $output.= '<td class="yr">'.$row['year'].'<input type="hidden" name="year[]" value='.$row['year'].'></td>';
                                    $output.= '<td class="crs">'.$row['courses'].'<input type="hidden" name="courses[]" value='.$row['courses'].'></td>';
                                    $output.= '<td class="crd_hrs">'.$row['cred_hrs'].'<input type="hidden" name="cred_hrs[]" value='.$row['cred_hrs'].'></td>';
                                    $output.= "<td><div class='buttons'><button type='button' class='btn btn-info btn-sm' id='edit_button'>Edit</button><button type='button' class='btn btn-danger btn-sm' id='delete_button'>Delete</button></div></td>";
                                    $output.= '</tr>';
                                }
                                echo $output;
                        }
                    } 
                    ?>
                        </tbody>
                    </table>
                </div>
                <!-- <div class="form-group">
                    <button class="btn btn-success btn-block" id="workload_button_submit"><i class="fa fa-save"></i> Submit</button>
                </div> -->
            </form>
        </div>

    </div>

    <?php
include("footer.php");
?>

        <script>
            $(document).ready(function() {
                $('#nav_conference').removeClass("active");
                $('#nav_faculty').removeClass("active");
                $('#nav_home').removeClass("active");
                $('#nav_qual').removeClass("active");
                $('#nav_workload').addClass("active");
                $('#nav_std_supr').removeClass("active");
                $('#nav_docs').removeClass("active");

                $(".semester_select").select2({
                    placeholder: "Please Select Semester"
                });
                $(".year").select2({
                    placeholder: "Please Select Year"
                });
                $(".courses").select2({
                    placeholder: "Please Select Course"
                });
                $(".cred_hrs").select2({
                    placeholder: "Please Select Credit Hours"
                });

                function courseCount(count, labs, semester, year, courses, cred_hrs) {
                    if (count < 3) {
                        if (labs) {
                            var check = "On";
                            cred_hrs += 1;
                        } else {
                            var check = "Off";
                        }

                        var tbody = "<tr>" +
                            "<td class='sems'>" + semester + "<input type='hidden' name='semester[]' value='" + semester + "'></td>" +
                            "<td class='yr'>" + year + "<input type='hidden' name='year[]' value='" + year + "'></td>" +
                            "<td class='crs'>" + courses + "<input type='hidden' name='courses[]' value='" + courses + "'></td>" +
                            "<td class='crd_hrs'>" + cred_hrs + "<input type='hidden' name='cred_hrs[]' value='" + cred_hrs + "'></td>" +
                            "<td><div class='buttons'><button type='button' class='btn btn-info btn-sm' id='edit_button'>Edit</button><button type='button' class='btn btn-danger btn-sm' id='delete_button'>Delete</button></div></td>" +
                            "</tr>";
                        $("#workload_tbody").append(tbody);
                    } else {
                        alert("Only 3 courses allowed for a semester.");
                    }

                }

                $(document).on("click", "#workload_button", function() {
                    var semester = $("#semester_select").val();
                    var courses = $("#courses").val();
                    var year = $("#year").val();
                    var cred_hrs = parseInt($("#cred_hrs").val());
                    var labs = $("#lab_check").prop("checked");

                    var rowCount = $('#workload_tbody tr').length;
                    // var fallcount, summercount, springcount;
                    var fallcount = $("#workload_tbody td:contains('Fall')").length;
                    var springcount = $("#workload_tbody td:contains('Spring')").length;
                    var summercount = $("#workload_tbody td:contains('Summer')").length;

                    var yearcount = $("#workload_tbody td:contains('"+year+"')").length;

                    // alert("fallcount: "+fallcount);

                    if (semester == 'Fall') {
                        courseCount(fallcount, labs, semester, year, courses, cred_hrs);
                    } else if (semester == 'Spring') {
                        courseCount(springcount, labs, semester, year, courses, cred_hrs);
                    } else if (semester == 'Summer') {
                        courseCount(summercount, labs, semester, year, courses, cred_hrs);
                    }


                    if([semester, courses, year, cred_hrs].includes(undefined) || [semester, courses, year, cred_hrs].includes(null) ||[semester, courses, year, cred_hrs].includes(NaN))
                        {
                             alert("Some values has not been set. Please input every field provided");
                        }  
                        else{
                        $.ajax({
                            type :'POST',
                            url : 'postprocess.php',
                            data : {
                                semester:semester,
                                courses:courses,
                                year:year,
                                cred_hrs:cred_hrs
                            }
                        }).done(function(tbody){
                            // console.log(tbody);
                                $("#workload_tbody").html(tbody);
                            
                        });

                    $("#semester_select").val("").trigger("change");
                    $("#courses").val("").trigger("change");
                    $("#year").val("").trigger("change");
                    $("#cred_hrs").val("").trigger("change");
                        }

                    // alert ("Fall count: " +count);
                });

                $(document).on("click", "#delete_button", function() {

                var $row = $(this).closest('tr');
                var semester = $row.find(".yr").text();

                $.ajax({
                    type: 'POST',
                    url: 'postprocess.php',
                    data: {
                        workload_semester: semester
                    }
                }).done(function(msg) {
                    console.log(msg);
                });

                $(this).closest('tr').remove();
                });
                $(document).on("click", "#edit_button", function() {
                    $(this).closest('tr').remove();

                    var $row = $(this).closest('tr');
                    var semester = $row.find(".sems").text();
                    var year = $row.find(".yr").text();
                    var course = $row.find(".crs").text();
                    var cred_hrs = $row.find(".crd_hrs").text();
                    var labs = $row.find(".labs").text();

                    if (labs == "On") {
                        $("#lab_check").prop("checked", "true");
                    } else if (labs == "Off") {
                        $("#lab_check").prop("checked", "false");
                    }

                    $("#semester_select").val(semester).trigger("change");
                    $("#courses").val(course).trigger("change");
                    $("#year").val(year).trigger("change");
                    $("#cred_hrs").val(cred_hrs).trigger("change");

                    $.ajax({
                    type: 'POST',
                    url: 'postprocess.php',
                    data: {
                        workload_semester: year
                    }
                }).done(function(msg) {
                    console.log(msg);
                });
                });
            });
        </script>
<?php
include("header.php");
?>



<div class='container'>
    <div class='row'>
    <h4 class="display-4"><i class="fa fa-file"></i>  Documents</h4>
            <hr>
    <form action="upload.php" method="post" enctype="multipart/form-data">
        <div class='col-md-6'>
            <label for="offer_letter"><i class="fa fa-file"></i> Offer_letter</label>
            <input type="file" class="form-control" id="offer_letter" name="docs[]">
        </div>
        <div class='col-md-6'>
            <label for="joining_report"><i class="fa fa-file"></i> Joining Report</label>
            <input type="file" class="form-control" id="joining_report" name="docs[]">
        </div>
        <div class='col-md-6'>
            <label for="approval"><i class="fa fa-file"></i> Approval</label>
            <input type="file" class="form-control" id="approval" name="docs[]">
        </div>
        <!-- <div class='col-md-6'>
            <label for="cnic"><i class="fa fa-id-card"></i> CNIC</label>
            <input type="file" class="form-control" id="cnic" name="docs[]">
        </div>
        <div class='col-md-6'>
            <label for="workload"> <i class="fa fa-file"></i> Work Load</label>
            <input type="file" class="form-control" id="approval" name="docs[]">
        </div>
        <div class='col-md-6'>
            <label for="stdsuper"><i class="fa fa-file"></i> Student Supervision</label>
            <input type="file" class="form-control" id="stdsuper" name="docs[]">
        </div> -->
        <div class="col-md-6">
            <button class="btn btn-sm btn-primary" type="submit" style="margin-top:10px">Upload</button>
        </div>
        </form>
    </div>
</div>

<?php
include('footer.php');
?>

<script>
$(document).ready(function(){
    $('#nav_conference').removeClass("active");
    $('#nav_faculty').removeClass("active");
    $('#nav_home').removeClass("active");
    $('#nav_qual').removeClass("active");
    $('#nav_workload').removeClass("active");
    $('#nav_std_supr').removeClass("active");
    $('#nav_docs').addClass("active");

    // $(document).on('click', "#upload", function(){
    //     var doc1 = $('#offer_letter').prop('files')[0];
    //     var doc2 = $('#joining_report').prop('files')[0];
    //     var doc3 = $('#approval').prop('files')[0];
    //     var doc4 = $('#cnic').prop('files')[0];
    //     var doc5 = $('#approval').prop('files')[0];
    //     var doc6 = $('#stdsuper').prop('files')[0];

    //     var form_data = new FormData();                  
    //     form_data.append('file', doc1);
    //     form_data.append('file', doc2);
    //     form_data.append('file', doc3);
    //     form_data.append('file', doc4);
    //     form_data.append('file', doc5);
    //     form_data.append('file', doc6);
        
    //     $.ajax({
    //         url: 'upload.php', 
    //         dataType: 'text',  // what to expect back from the PHP script, if anything
    //         cache: false,
    //         contentType: false,
    //         processData: false,
    //         data: form_data,                         
    //         type: 'post',
    //         success: function(php_script_response){
    //             console.log(php_script_response);
    //         }
    //     });
    // });

    // $(document).on('change', '#offer_letter', function(){
    //                 var front_side = $('#offer_letter').prop('files')[0];
    //                 var form_data = new FormData();                  
    //                 form_data.append('file', front_side);
    //                 $.ajax({
    //                     url: 'upload.php', 
    //                     dataType: 'text',  // what to expect back from the PHP script, if anything
    //                     cache: false,
    //                     contentType: false,
    //                     processData: false,
    //                     data: form_data,                         
    //                     type: 'post',
    //                     success: function(php_script_response){
    //                         console.log(php_script_response);
    //                     }
    //                 });
    //             });

                // $(document).on('change', '#joining_report', function(){
                //     var front_side = $('#joining_report').prop('files')[0];
                //     var form_data = new FormData();                  
                //     form_data.append('file', front_side);
                //     $.ajax({
                //         url: 'upload.php',
                //         dataType: 'text',  // what to expect back from the PHP script, if anything
                //         cache: false,
                //         contentType: false,
                //         processData: false,
                //         data: form_data,                         
                //         type: 'post',
                //         success: function(php_script_response){
                //             console.log(php_script_response);
                //         }
                //     });
                // });
});
</script>
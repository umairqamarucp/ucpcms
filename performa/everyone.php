<?php
include('header.php');
?>
<!-- <div class="jumbotron jumbotron-fluid">
        <div class="container">
        <h5 class="display-5"><i class="fa fa-search"></i>  Search Faculty</h5>
            <p>Please follow these instructions</p>
            <p class="text-danger">
                <li class="text-danger">Please type in the name field and a table will be generated according to the typed string.</li>
                <li class="text-danger">To add particluars of a faculty member. Just click on that faculty member and navigate through the tabs provided in navigation bar and fill the respective fields.</li>
                <li class="text-danger">Provide CNIC and Name, then click Register to register the faculty member. You can add the respective fields in the tabs provided in the navigation bar.</li>
            </p>
        </div>
    </div> -->
    <div class="container">
    <h4 class="display-4"><i class="fa fa-search"></i>  Search Faculty</h4>
    <hr>
        <div class="row">
            
            <div class="col-md-4 form-group">
                <label for="name"><i class="fa fa-user"></i> Name</label>
                <input type="text" class="form-control" id="name">
            </div>
            <div class="col-md-4 form-group">
                <label for="cnic"><i class="fa fa-id-card"></i> CNIC</label>
                <input type="text" class="form-control" id="cnic">
            </div>
            
            <div class="col-md-4 form-group flex justify-content-center">
                <!-- <button type="button" class="btn btn-success btn-block col-md-6" id="search_button"><i class="fa fa-search"></i> Search</button> -->
                <button type="button" class="btn btn-success btn-block col-md-6" id="register_button" style="margin-top:35px"><i class="fa fa-pencil"></i> Register</button>
            </div>
        </div>
        <div class="row">
            <table class="table table-stripes table-borderless table-hover" id="faculty_table">
            <thead>
                <th>Name</th>
                <th>CNIC</th>
                <th>Email</th>
                <th>Designation</th>
            </thead>
            <tbody id="faculty_tbody">
            </tbody>
            </table>
        </div>
    </div>

<?php
include('footer.php');
?>
            <script>


                $(document).ready(function() {
                    $('#nav_conference').removeClass("active");
                    $('#nav_faculty').addClass("active");
                    $('#nav_home').removeClass("active");
                $('#nav_qual').removeClass("active");
                $('#nav_workload').removeClass("active");
                $('#nav_std_supr').removeClass("active");
                $('#nav_docs').removeClass("active");

                    $(document).on("click", "#search_button", function(){
                        var cnic = $("#cnic").val();
                        var name = $("#name").val();
                        var form_data = new FormData();                  
                        if(name!="")
                        {
                            form_data.append('search_name', name);
                        }
                        if(cnic!="")
                        {
                            form_data.append('search_cnic', cnic);
                        }
                        $.ajax({
                            type: "POST",
                            url: "postprocess.php",
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data:form_data
                        }).done(function(msg){
                            $("#faculty_tbody").html(msg);

                        });
                    });

                    $(document).on('click', "#register_button", function(){
                        var name = $("#name").val();
                        var cnic = $("#cnic").val();
                        
                        $.ajax({
                            type: "POST",
                            url: "postprocess.php",
                            data: {reg_name:name, reg_cnic:cnic}
                        }).done(function(msg){
                            alert(msg);
                            window.location.href = "everyone.php";
                        });

                    });

                    //Populating the table with live search

                    $(document).on('keyup', '#name', function(){
                        var name = $('#name').val();

                        $.ajax({
                            type:'POST',
                            url: 'postprocess.php',
                            data: {searchName: name}
                        }).done(function(msg){

                            $('#faculty_tbody').html(msg);

                        });
                    });

                    // Row click function

                    $(document).on("click", "#faculty_tbody tr", function(){
                        // alert($(this).find(".everyname").text());
                        
                        var name = $(this).find(".everyname").text();
                        var cnic = $(this).find(".everycnic").text();
                        var email = $(this).find(".everyemail").text();
                        var designation = $(this).find(".everydesig").text();
                        
                        // alert(cnic+"/"+name);
                        var form_data = new FormData();
                        form_data.append('everycnic', cnic);
                        form_data.append('name', name);

                        $.ajax({
                            type: "POST",
                            url: "postprocess.php",
                            dataType: 'text',
                            cache: false,
                            contentType: false,
                            processData: false,
                            data:form_data
                        }).done(function(msg){
                            alert(msg);
                            window.location.href = "everyone.php";

                        });
                });
                });
            </script>
<?php
include("header.php");
?>

    <div class="container-fluid">
        <div class="row">
            <h5 class="display-5">TECHNICAL / POLICY REPORT / OTHER</h5>
            <hr>
            <form action="postprocess.php" method="POST">
            <table class="table table-stripes table-borderless">
                <thead>
                    <th></th>
                    <th></th>
                    <th></th>
                </thead>
                <tbody>
                    <tr>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1">
                            
                        </td>
                        <td valign="=middle" style="display: flex; width: 60%;">
                            <div>
                                <p>
                                  <label>
                                    <input name="group1" type="radio" checked/>
                                    <span>Technical / Policy Report / Brief</span>
                                  </label>
                                </p>
                            </div>
                            <div>
                                 <p>
                                  <label>
                                    <input name="group1" type="radio"  />
                                    <span>Other</span>
                                  </label>
                                </p>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Title</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="pr_title" name="pr_title">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>First Author</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" id="pr_first_author" pattern="[a-zA-Z\s]{3,}" required name="pr_first_author">
                        </td>
                        <td class="col--3 align-bottom">
                            <label>
                                <input type="checkbox" class="filled-in" id="ucp_check" name="ucp_check" /><span>UCP</span></label>
                        </td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Position in Authorship</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="pr_author_pos" name="pr_author_pos">
                            <option></option>
                                <option>1st</option><option>2nd</option><option>3rd</option><option>4th</option>
                                <option>5th</option><option>6th</option><option>7th</option><option>8th</option>

                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Report Affiliation</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="pr_affiliation" name="pr_affiliation">
                            <option></option>
                                <option>UCP</option>
                                <option>Others</option>
                            </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Appeared In</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required id="pr_appearance" name="pr_appearance">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                      <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Source</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required id="pr_source" name="pr_source">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    </tr>
                      <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Report Number</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[0-9\s]{1,}" required id="pr_report" name="pr_report">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Volume</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[0-9\s]{1,}" required id="pr_vol" name="pr_vol">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Issue</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required id="pr_issue" name="pr_issue">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Pages</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[0-9\s]{1,}" required id="pr_pages" name="pr_pages">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Year</p>
                        </td>
                        <td class="col--2">
                            <select type="text" class="select--tag form-control" required id="pr_year" name="pr_year">
                            <option></option>
                        <?php 
                                for($i = 2008 ; $i <= date('Y'); $i++){
                                    echo "<option value='$i'>$i</option>";
                                }
                            ?>
                    </select>
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>Publisher</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" pattern="[a-zA-Z\s]{3,}" required id="pr_publisher" name="pr_publisher">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1 align-bottom" valign="middle">
                            <p>URL/DOI</p>
                        </td>
                        <td class="col--2">
                            <input type="text" class="form-control" required id="pr_url" name="pr_url">
                        </td>
                        <td class="col--3"></td>
                    </tr>
                    <tr>
                        <td class="col--1"></td>
                        <td class="col--2">
                            <button class="btn btn-primary btn-sm" type="submit" id="pr_save">Save</button>
                            <button class="btn btn-primary btn-sm" type="button" id="pr_list">List</button>
                        </td>
                        <td class="col--3"></td>
                    </tr>

                </tbody>
            </table>
</form>
        </div>
        <div class="row">
                <table class="table table-stripes table-borderless">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Source</th>
                            <th>Year </th>
                            <th>URL</th>
                        </tr>
                    </thead>
                    <tbody id="education_tbody">
                    <?php
                    include('connection.php');
                    if(isset($_SESSION["searched_faculty"]))
                    {
                        $query="SELECT * FROM policy_report"; // WHERE cnic = '".$_SESSION['searched_faculty']."'
                        $result = mysqli_query($con, $query);
                        // var_dump($query);
                        // var_dump($result);
                        if(mysqli_num_rows($result))
                        {
                            $output = '';
                                while($row = mysqli_fetch_array($result))
                                {
                                    $output.= '<tr>';
                                    $output.= '<td class="deg">'.$row['pr_title'].'<input type="hidden" name="degree[]" value='.$row['pr_title'].'></td>';
                                    $output.= '<td class="brd">'.$row['pr_first_author'].'<input type="hidden" name="board[]" value='.$row['pr_first_author'].'></td>';
                                    $output.= '<td class="yr">'.$row['pr_source'].'<input type="hidden" name="year[]" value='.$row['pr_source'].'></td>';
                                    $output.= '<td class="distin">'.$row['pr_year'].'<input type="hidden" name="distinction[]" value='.$row['pr_year'].'></td>';
                                    $output.= '<td class="distin">'.$row['pr_url'].'<input type="hidden" name="distinction[]" value='.$row['pr_url'].'></td>';
                                    $output.= '</tr>';
                                }
                                echo $output;
                        }
                    } 
                    ?>
                    </tbody>
                </table>
        </div>
    </div>

    <?php
include('footer.php');
?>
        <script>
            $(document).ready(function() {
                $('#nav_conference').addClass("active");
                $('#nav_faculty').removeClass("active");
                $('#nav_home').removeClass("active");
                $('#nav_qual').removeClass("active");
                $('#nav_workload').removeClass("active");
                $('#nav_std_supr').removeClass("active");
                $('#nav_docs').removeClass("active");

                $(".select--tag").select2({
                    placeholder: "--Select--"
                });
                $("#conf_mode").select2({
                    placeholder: "--Select--"
                });

            });
        </script>
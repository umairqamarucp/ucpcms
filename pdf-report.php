<?php
include 'includes/connection.php';
?>
<!DOCTYPE html>
<html lang="en">
     
<style>
form {
    border: none;
}

p{
color:black;
font-size:14px;

}

input[type=text], input[type=password], input[type=email], select,.form-control{
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
	
}

button {
    background-color:white;
    color: white;
    padding: 6px 10px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
	font-size:15px;
	
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}

</style>
<head>

	  <title>MS/PhD Content Management System</title>
	  <!--<link rel="icon" href="images/favicon.ico"/>-->
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta http-equiv="cache-control" content="no-cache" />
	  <meta http-equiv="pragma" content="no-cache" />
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	  <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

	<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	  <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
	  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">	</script>



    <!-- Bootstrap core CSS     -->
    <!-- <link href="assets/css/bootstrap.min.css" rel="stylesheet" /> -->

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <!--     Fonts and icons     -->
	<link rel="stylesheet"href="assets/css/bootstrap.min.css">
  <script src="assets/js/jquery-1.10.2.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<script>
function readURL(input) {
	alert("hiii");
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#picname").change(function(){
    readURL(this);
});
</script>
<body>



 
        
<div class="w3-container" style="background-color:#983415;color:#fff;">
  
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
  <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
  <h1 style="text-align:center" >MS/PhD. CMS</h1>
  </div>
   <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
  <h1 style="text-align:center" >MS/PhD. CMS</h1>
  </div>
  <div class="col-lg-2 col-lg-offset-2 col-md-2 col-md-offset-2 col-sm-2 col-sm-offset-2 ">
  
  <button style="margin-top:15px;width:100px;background-color:#000000;border:none" class="btn btn-primary btn btn-lg"><a href ="cms.html" style="color:white" id = 'home'> Login</a></button>
   </div>
</div>
</div>

<div class="w3-container-fluid">

  
  <div class="row btn-bar">
		<div class="col-md-12" style="margin-top: 32px;">
		    <div class="row">
						
				<form action='fpdf-report.php' method="POST">
                <div class="col-md-1 form-group"> </div>    
                <div class="col-md-2 form-group">
                        <label for="srch-id" class="control-label">Pdf Generate By Id</label>
                            <input type="text" name="srch-id" id="srch-id" placeholder="L1F17MSCS0011 / Saeed Iqbal" class="form-control">
                    </div>
                    <div class="col-md-1 form-group">
                            <input type="submit" value="pdf" class="form-control btn btn-success" style="width:  100%;margin-top: 32px;padding:  4px;"/>
                    </div>
                </form>
                <form action='all-record.php' method="POST">
				    <div class="col-md-2 form-group">
				    	<label for="srch-fall" class="control-label">Fall</label>
                        <select name='semester' id='semester'>
            <option value='FALL' "; if(substr($row['Semester_Num'], 0,-5) == 'FALL') echo 'selected '; echo " >FALL</option>
            <option value='SPRING' "; if(substr($row['Semester_Num'], 0,-5) == 'SPRING') echo 'selected '; echo " >SPRING</option>
            <option value='SUMMER' "; if(substr($row['Semester_Num'], 0,-5) == 'SUMMER') echo 'selected '; echo " >SUMMER</option>
                    </select> 
                    </div>
                    <div class="col-md-2 form-group">
                            <label for="srch-year" class="control-label">Year</label>
                            <select name='semester_no'>
                        <option value='2017' "; if(substr($row['Semester_Num'], -4) == '2017') echo 'selected '; echo " >
                                2017</option>
                        <option value='2016' "; if(substr($row['Semester_Num'], -4) == '2016') echo 'selected '; echo " >
                                2016    
                        </option>
                        <option value='2015' "; if(substr($row['Semester_Num'], -4) == '2015') echo 'selected '; echo " >
                                2015    
                        </option>
                        <option value='2014' "; if(substr($row['Semester_Num'], -4) == '2014') echo 'selected '; echo " >
                                2014    
                        </option>
                        <option value='2013' "; if(substr($row['Semester_Num'], -4) == '2013') echo 'selected '; echo " >
                                2013    
                        </option>
                        <option value='2012' "; if(substr($row['Semester_Num'], -4) == '2012') echo 'selected '; echo " >
                                2012    
                        </option>
                        <option value='2011' "; if(substr($row['Semester_Num'], -4) == '2011') echo 'selected '; echo " >
                                2011    
                        </option>
                        <option value='2010' "; if(substr($row['Semester_Num'], -4) == '2010') echo 'selected '; echo " >
                                2010    
                        </option>
                        <option value='2009' "; if(substr($row['Semester_Num'], -4) == '2009') echo 'selected '; echo " >
                                2009    
                        </option>
                        <option value='2008' "; if(substr($row['Semester_Num'], -4) == '2008') echo 'selected '; echo " >
                                2008    
                        </option>
                        <option value='2007' "; if(substr($row['Semester_Num'], -4) == '2007') echo 'selected '; echo " >
                                2007    
                        </option>
                                                <option value='2006' "; if(substr($row['Semester_Num'], -4) == '2006') echo 'selected '; echo " >
                                2006    
                        </option>
                                                <option value='2005' "; if(substr($row['Semester_Num'], -4) == '2005') echo 'selected '; echo " >
                                2005    
                        </option>
                                                <option value='2004' "; if(substr($row['Semester_Num'], -4) == '2004') echo 'selected '; echo " >
                                2004    
                        </option>
                                                <option value='2003' "; if(substr($row['Semester_Num'], -4) == '2003') echo 'selected '; echo " >
                                2003    
                        </option>
                                                <option value='2002' "; if(substr($row['Semester_Num'], -4) == '2002') echo 'selected '; echo " >
                                2002    
                        </option>
                                                <option value='2001' "; if(substr($row['Semester_Num'], -4) == '2001') echo 'selected '; echo " >
                                2001    
                        </option>
                                                <option value='2000' "; if(substr($row['Semester_Num'], -4) == '2000') echo 'selected '; echo " >
                                2000    
                        </option>
                                                <option value='1999' "; if(substr($row['Semester_Num'], -4) == '1999') echo 'selected '; echo " >
                                1999    
                        </option>
                                                <option value='1998' "; if(substr($row['Semester_Num'], -4) == '1998') echo 'selected '; echo " >
                                1998    
                        </option>
                                                <option value='1997' "; if(substr($row['Semester_Num'], -4) == '1997') echo 'selected '; echo " >
                                1997    
                        </option>
                                                <option value='1996' "; if(substr($row['Semester_Num'], -4) == '1996') echo 'selected '; echo " >
                                1996    
                        </option>
                                                <option value='1995' "; if(substr($row['Semester_Num'], -4) == '1995') echo 'selected '; echo " >
                                1995    
                        </option>
                                                <option value='1994' "; if(substr($row['Semester_Num'], -4) == '1994') echo 'selected '; echo " >
                                1994    
                        </option>
                                                <option value='1993' "; if(substr($row['Semester_Num'], -4) == '1993') echo 'selected '; echo " >
                                1993    
                        </option>
                                                <option value='1992' "; if(substr($row['Semester_Num'], -4) == '1992') echo 'selected '; echo " >
                                1992    
                        </option>
                                                <option value='1991' "; if(substr($row['Semester_Num'], -4) == '1991') echo 'selected '; echo " >
                                1991    
                        </option>
                                                <option value='1990' "; if(substr($row['Semester_Num'], -4) == '1990') echo 'selected '; echo " >
                                1990    
                        </option>
        </select>
                        </div>
		    		<div class="col-md-1 form-group">
						<input type="submit" value="pdf" class="form-control btn btn-success" style="width:  100%;margin-top: 32px;padding:  4px;"/>
			    	</div>
				</form>
            </div>
        <div class= 'row'><div class= 'row'></div></div>
        <!--<div class='row'>
            <form action='all-record.php' method="POST">
                <div class="col-md-1 form-group"> </div>    
                <div class='row'>
                <div class="col-md-2 form-group">
                            <label for="srch-year" class="control-label">From</label>
                            <select name='semester_no'>
                        <option value='2017' "; if(substr($row['Semester_Num'], -4) == '2017') echo 'selected '; echo " >
                                2017</option>
                        <option value='2016' "; if(substr($row['Semester_Num'], -4) == '2016') echo 'selected '; echo " >
                                2016    
                        </option>
                        <option value='2015' "; if(substr($row['Semester_Num'], -4) == '2015') echo 'selected '; echo " >
                                2015    
                        </option>
                        <option value='2014' "; if(substr($row['Semester_Num'], -4) == '2014') echo 'selected '; echo " >
                                2014    
                        </option>
                        <option value='2013' "; if(substr($row['Semester_Num'], -4) == '2013') echo 'selected '; echo " >
                                2013    
                        </option>
                        <option value='2012' "; if(substr($row['Semester_Num'], -4) == '2012') echo 'selected '; echo " >
                                2012    
                        </option>
                        <option value='2011' "; if(substr($row['Semester_Num'], -4) == '2011') echo 'selected '; echo " >
                                2011    
                        </option>
                        <option value='2010' "; if(substr($row['Semester_Num'], -4) == '2010') echo 'selected '; echo " >
                                2010    
                        </option>
                        <option value='2009' "; if(substr($row['Semester_Num'], -4) == '2009') echo 'selected '; echo " >
                                2009    
                        </option>
                        <option value='2008' "; if(substr($row['Semester_Num'], -4) == '2008') echo 'selected '; echo " >
                                2008    
                        </option>
                        <option value='2007' "; if(substr($row['Semester_Num'], -4) == '2007') echo 'selected '; echo " >
                                2007    
                        </option>
                                                <option value='2006' "; if(substr($row['Semester_Num'], -4) == '2006') echo 'selected '; echo " >
                                2006    
                        </option>
                                                <option value='2005' "; if(substr($row['Semester_Num'], -4) == '2005') echo 'selected '; echo " >
                                2005    
                        </option>
                                                <option value='2004' "; if(substr($row['Semester_Num'], -4) == '2004') echo 'selected '; echo " >
                                2004    
                        </option>
                                                <option value='2003' "; if(substr($row['Semester_Num'], -4) == '2003') echo 'selected '; echo " >
                                2003    
                        </option>
                                                <option value='2002' "; if(substr($row['Semester_Num'], -4) == '2002') echo 'selected '; echo " >
                                2002    
                        </option>
                                                <option value='2001' "; if(substr($row['Semester_Num'], -4) == '2001') echo 'selected '; echo " >
                                2001    
                        </option>
                                                <option value='2000' "; if(substr($row['Semester_Num'], -4) == '2000') echo 'selected '; echo " >
                                2000    
                        </option>
                                                <option value='1999' "; if(substr($row['Semester_Num'], -4) == '1999') echo 'selected '; echo " >
                                1999    
                        </option>
                                                <option value='1998' "; if(substr($row['Semester_Num'], -4) == '1998') echo 'selected '; echo " >
                                1998    
                        </option>
                                                <option value='1997' "; if(substr($row['Semester_Num'], -4) == '1997') echo 'selected '; echo " >
                                1997    
                        </option>
                                                <option value='1996' "; if(substr($row['Semester_Num'], -4) == '1996') echo 'selected '; echo " >
                                1996    
                        </option>
                                                <option value='1995' "; if(substr($row['Semester_Num'], -4) == '1995') echo 'selected '; echo " >
                                1995    
                        </option>
                                                <option value='1994' "; if(substr($row['Semester_Num'], -4) == '1994') echo 'selected '; echo " >
                                1994    
                        </option>
                                                <option value='1993' "; if(substr($row['Semester_Num'], -4) == '1993') echo 'selected '; echo " >
                                1993    
                        </option>
                                                <option value='1992' "; if(substr($row['Semester_Num'], -4) == '1992') echo 'selected '; echo " >
                                1992    
                        </option>
                                                <option value='1991' "; if(substr($row['Semester_Num'], -4) == '1991') echo 'selected '; echo " >
                                1991    
                        </option>
                                                <option value='1990' "; if(substr($row['Semester_Num'], -4) == '1990') echo 'selected '; echo " >
                                1990    
                        </option>
        </select>
                        </div>
                <div class='col-md-1'></div>
                <div class="col-md-2 form-group">
                <label for="srch-year" class="control-label">To</label>
                <select name='semester_no'>
            <option value='2017' "; if(substr($row['Semester_Num'], -4) == '2017') echo 'selected '; echo " >
                    2017</option>
            <option value='2016' "; if(substr($row['Semester_Num'], -4) == '2016') echo 'selected '; echo " >
                    2016    
            </option>
            <option value='2015' "; if(substr($row['Semester_Num'], -4) == '2015') echo 'selected '; echo " >
                    2015    
            </option>
            <option value='2014' "; if(substr($row['Semester_Num'], -4) == '2014') echo 'selected '; echo " >
                    2014    
            </option>
            <option value='2013' "; if(substr($row['Semester_Num'], -4) == '2013') echo 'selected '; echo " >
                    2013    
            </option>
            <option value='2012' "; if(substr($row['Semester_Num'], -4) == '2012') echo 'selected '; echo " >
                    2012    
            </option>
            <option value='2011' "; if(substr($row['Semester_Num'], -4) == '2011') echo 'selected '; echo " >
                    2011    
            </option>
            <option value='2010' "; if(substr($row['Semester_Num'], -4) == '2010') echo 'selected '; echo " >
                    2010    
            </option>
            <option value='2009' "; if(substr($row['Semester_Num'], -4) == '2009') echo 'selected '; echo " >
                    2009    
            </option>
            <option value='2008' "; if(substr($row['Semester_Num'], -4) == '2008') echo 'selected '; echo " >
                    2008    
            </option>
            <option value='2007' "; if(substr($row['Semester_Num'], -4) == '2007') echo 'selected '; echo " >
                    2007    
            </option>
                                    <option value='2006' "; if(substr($row['Semester_Num'], -4) == '2006') echo 'selected '; echo " >
                    2006    
            </option>
                                    <option value='2005' "; if(substr($row['Semester_Num'], -4) == '2005') echo 'selected '; echo " >
                    2005    
            </option>
                                    <option value='2004' "; if(substr($row['Semester_Num'], -4) == '2004') echo 'selected '; echo " >
                    2004    
            </option>
                                    <option value='2003' "; if(substr($row['Semester_Num'], -4) == '2003') echo 'selected '; echo " >
                    2003    
            </option>
                                    <option value='2002' "; if(substr($row['Semester_Num'], -4) == '2002') echo 'selected '; echo " >
                    2002    
            </option>
                                    <option value='2001' "; if(substr($row['Semester_Num'], -4) == '2001') echo 'selected '; echo " >
                    2001    
            </option>
                                    <option value='2000' "; if(substr($row['Semester_Num'], -4) == '2000') echo 'selected '; echo " >
                    2000    
            </option>
                                    <option value='1999' "; if(substr($row['Semester_Num'], -4) == '1999') echo 'selected '; echo " >
                    1999    
            </option>
                                    <option value='1998' "; if(substr($row['Semester_Num'], -4) == '1998') echo 'selected '; echo " >
                    1998    
            </option>
                                    <option value='1997' "; if(substr($row['Semester_Num'], -4) == '1997') echo 'selected '; echo " >
                    1997    
            </option>
                                    <option value='1996' "; if(substr($row['Semester_Num'], -4) == '1996') echo 'selected '; echo " >
                    1996    
            </option>
                                    <option value='1995' "; if(substr($row['Semester_Num'], -4) == '1995') echo 'selected '; echo " >
                    1995    
            </option>
                                    <option value='1994' "; if(substr($row['Semester_Num'], -4) == '1994') echo 'selected '; echo " >
                    1994    
            </option>
                                    <option value='1993' "; if(substr($row['Semester_Num'], -4) == '1993') echo 'selected '; echo " >
                    1993    
            </option>
                                    <option value='1992' "; if(substr($row['Semester_Num'], -4) == '1992') echo 'selected '; echo " >
                    1992    
            </option>
                                    <option value='1991' "; if(substr($row['Semester_Num'], -4) == '1991') echo 'selected '; echo " >
                    1991    
            </option>
                                    <option value='1990' "; if(substr($row['Semester_Num'], -4) == '1990') echo 'selected '; echo " >
                    1990    
            </option>
</select>
            </div>
                <div class="col-md-1 form-group">
						<input type="submit" value="pdf" class="form-control btn btn-success" style="width:  100%;margin-top: 32px;padding:  4px;"/>
			    	</div>
              </div>
            </form>
        </div>-->
				<!-- </div> -->
		</div>
		
	</div>

</div>
</body>
</html>

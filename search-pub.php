<?php
include 'includes/connection.php';
$id = 1;
$result = mysqli_query($connection, "SELECT * FROM tbl_research_paper") or die(mysqli_error($connection));

    echo "<table id='searchtbl' class='table table-striped table-bordered' cellspacing='0' width='100%'>
    <thead>
    <tr>
        <th>Title</th>
        <th>Author</th>
        <th>Publication</th>
        <th>Journal/Conference Name</th>
        <th>Impact Factor</th>
        <th>ISSN Number</th>
        <th>Publication Date</th>
        <th>Attach Publication</th>
        </tr></thead>";
        while($row = mysqli_fetch_array($result)){
        echo "
        <tr>
            <td>{$row['Title']}</td>
            <td>{$row['Ptype']}</td>
            <td>{$row['JC']}</td>
            <td>{$row['JC_Name']}</td>
            <td>{$row['Impact_Factor']}</td>
            <td>{$row['ISSN']}</td>
            <td>{$row['PDate']}</td>
            <td>{$row['PFile']}</td>

        </tr>
        ";
    }
    echo "</table>";


?>
<?php
require('fpdf/fpdf.php');
include 'includes/connection.php';
//$id = $_POST['srch-id'];
$id = 'L1F17PHMD0012';

    $resultcnic = mysqli_query($connection, "SELECT * FROM tbl_candidate WHERE CID = 'L1F17PHMD0012'") or die(mysqli_error($connection)); 
    $rowCNIC= (mysqli_fetch_array($resultcnic,MYSQLI_ASSOC));
    $cnic = $rowCNIC['CNIC_num'];
    $imgCNIC = base64_encode($rowCNIC['CNIC_front']);

    $resultadmission = mysqli_query($connection, "SELECT * FROM tbl_admission_form WHERE CID = 'L1F19PHMD0002'") or die(mysqli_error($connection)); 
    $rowadmission= (mysqli_fetch_array($resultadmission,MYSQLI_ASSOC));
    $admission = $rowadmission['Upload_Form'];

    $resultThesis = mysqli_query($connection, "SELECT * FROM tbl_thesis WHERE CID = 'L1F17PHMD0012'") or die(mysqli_error($connection)); 
    $rowThesis= (mysqli_fetch_array($resultThesis,MYSQLI_ASSOC));
    $Thesis = $rowThesis['thesisfile'];

    $resultSynopsis = mysqli_query($connection, "SELECT * FROM tbl_synopsis WHERE CID = 'L1F17PHMD0012'") or die(mysqli_error($connection)); 
    $rowSynopsis= (mysqli_fetch_array($resultSynopsis,MYSQLI_ASSOC));
    $BASRsynopsis = $rowSynopsis['BASR_Approval_File'];

    $resultComprehencive = mysqli_query($connection, "SELECT * FROM tbl_comprehensive WHERE CID = 'L1F17PHMD0012'") or die(mysqli_error($connection)); 
    $rowComp= (mysqli_fetch_array($resultComprehencive,MYSQLI_ASSOC));
    $comprehensive = $rowComp['Comprehensive_Result'];

    $resultEvaluat = mysqli_query($connection, "SELECT * FROM tbl_evaluation WHERE CID = 'L1F17PHMD0012'") or die(mysqli_error($connection)); 
    $rowEvaluation= (mysqli_fetch_array($resultEvaluat,MYSQLI_ASSOC));
    $apfile = $rowEvaluation['Approval_File'];
    $report = $rowEvaluation['Evaluation_Report'];

    $resultDegreeComp = mysqli_query($connection, "SELECT * FROM tbl_degree_completion WHERE CID = 'L1F17PHMD0012'") or die(mysqli_error($connection)); 
    $rowDegreeComp= (mysqli_fetch_array($resultDegreeComp,MYSQLI_ASSOC));
    $degreefile = $rowDegreeComp['Final_Degree_File'];

    $resultDefence = mysqli_query($connection, "SELECT * FROM tbl_defence_notification WHERE CID = 'L1F17PHMD0012'") or die(mysqli_error($connection)); 
    $rowDefence= (mysqli_fetch_array($resultDefence,MYSQLI_ASSOC));
    $degreefile = $rowDefence['Commitee_File'];
    $BASRfile = $rowDefence['BASR_File'];


class PDF extends FPDF {
 
function Header() {
    //include 'includes/connection.php';
    $this->Cell(190, 10, "PHD Student File ",1, 5, "C");
    //reset Y
    $this->SetY(1);
}
function Footer(){

}
 

}
 
    $pdf=new PDF();
    $pdf->SetMargins(10,10,5);
    $pdf->SetFont('Times','',14);
    $pdf->SetY(10);
    $pdf->AddPage();
    $height = 7;
    $width = 140;
    $pdf->SetY(20);

    $pdf->Cell($width, 15, "Name Of Documents ", 'LR', 0,"C");
    $pdf->Cell(50, 7, "Evidence attached ", 1, 1,"C");
    $pdf->Cell($width, 7, "", 'LRB', 0,"C");
    $pdf->Cell(17, 7, "Yes ", 1, 0,"C");
    $pdf->Cell(17, 7, "No ", 1, 0,"C");
    $pdf->Cell(16, 7, "N/A ", 1, 1,"C");
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Secondary School Certificate or Equivalent",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Mark Sheet of Secondary School Certificate or Equivalent",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "4", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Higher Secondary School Certificate or Equivalent",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "4", 1, 0,"C");
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Mark Sheet of Higher Secondary School Certificate or Equivalent",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "4", 1, 0,"C");
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Bachelors of Equivalent Degree (16 years qualification)",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of Bachelors or Equivalent (16 years qualification)",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "4", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Equivalence Certificate from HEC",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "MS/M.Phil.Equivalent Degree (18 years qualification)",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "4", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of MS/M.Phil or Equivalent (18 years qualification)",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "4", 1, 0,"C");
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Equivalence Certificate from HEC",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "4", 1, 0,"C");
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);

    $pdf->Cell($width, $height, "Admission Form",1, 0);
    
    if($admission != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "CNIC",1, 0);
    
    if($cnic != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "GAT Subject or Equivalent",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Porposal at the time of Admission",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "4", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Allocation of Supervisor (provisionall)",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "4", 1, 0,"C");
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Admission Confirmation Letter",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "4", 1, 0,"C");
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of 1st Semester",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of 2nd Semester",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "4", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of 3rd Semester",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of 4th Semester",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "4", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Comprehencive Examination",1, 0);
    
    if($comprehensive !== "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    } 
    
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Approval of Synopsis from BASR or Equivalent",1, 0);
    if($BASRsynopsis != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Approval of Supervisor allocation",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Semester wise Progress Report",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "4", 1, 0);
    $pdf->Cell(16, $height, "", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Publication of Research Paper in HEC Recognized Journal",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Thesis Submission",1, 0);
    
    if($Thesis != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else
    {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);

    $pdf->Cell($width, $height, "Selection of Foreign Evaluators",1, 0);
    
    if($apfile != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Foreign Evaluators Reports",1, 0);
    if($report != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }

    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Notification of Public Defence",1, 0);
    if($BASRfile !== "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    } 
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Notification of Successful Defence",1, 0);
    if($degreefile !== "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    } 
    
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Notification of Degree Completion",1, 0);
    if($degreefile != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->AddPage();

$pdf->Output();
?>

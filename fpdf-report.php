<?php
require('fpdf/fpdf.php');
include 'includes/connection.php';
$id = $_POST['srch-id'];

//$certificateImg = $certificate;

    $resultGAT = mysqli_query($connection, "SELECT * FROM tbl_gat WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowGAT= (mysqli_fetch_array($resultGAT,MYSQLI_ASSOC));
    $GAT = $rowGAT['Result_Card'];
    $date = $rowGAT['Test_Date'];
    $marks = $rowGAT['Max_Marks'];
    $obt = $rowGAT['Obt_Marks'];


    $resultcnic = mysqli_query($connection, "SELECT * FROM tbl_candidate WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowCNIC= (mysqli_fetch_array($resultcnic,MYSQLI_ASSOC));
    $cnic = $rowCNIC['CNIC_num'];
    $img1CNIC = $rowCNIC['CNIC_front'];
    $img2CNIC = $rowCNIC['CNIC_back'];
    $Name = $rowCNIC['Name'];
    $Fname = $rowCNIC['Father_Name'];
    $CID = $rowCNIC['CID'];
    $email = $rowCNIC['Email'];
    $domicile = $rowCNIC['Domicile'];
    $gender = $rowCNIC['Gender'];

    $resultadmission = mysqli_query($connection, "SELECT * FROM tbl_admission_form WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowadmission= (mysqli_fetch_array($resultadmission,MYSQLI_ASSOC));
    $admission = $rowadmission['Upload_Form'];

    $resultThesis = mysqli_query($connection, "SELECT * FROM tbl_thesis WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowThesis= (mysqli_fetch_array($resultThesis,MYSQLI_ASSOC));
    $Thesis = $rowThesis['thesisfile'];

    $resultSynopsis = mysqli_query($connection, "SELECT * FROM tbl_synopsis WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowSynopsis= (mysqli_fetch_array($resultSynopsis,MYSQLI_ASSOC));
    $BASRsynopsis = $rowSynopsis['BASR_Approval_File'];
    $TitleSynopsis = $rowSynopsis['Title'];
    $AbstractSynopsis = $rowSynopsis['Abstract'];
    $SupervisorSynopsis = $rowSynopsis['Supervisor'];
    $DateSynopsis = $rowSynopsis['Approval_Date'];

    $resultComprehencive = mysqli_query($connection, "SELECT * FROM tbl_comprehensive WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowComp= (mysqli_fetch_array($resultComprehencive,MYSQLI_ASSOC));
    $comprehensive = $rowComp['Comprehensive_Result'];
    $compDate = $rowComp['Exam_Date'];
    $compMax = $rowComp['Max_Marks'];
    $compObt = $rowComp['Obt_Marks'];

    $resultEvaluat = mysqli_query($connection, "SELECT * FROM tbl_evaluation WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowEvaluation= (mysqli_fetch_array($resultEvaluat,MYSQLI_ASSOC));
    $apfile = $rowEvaluation['Approval_File'];
    $report = $rowEvaluation['Evaluation_Report'];

    $resultDegreeComp = mysqli_query($connection, "SELECT * FROM tbl_degree_completion WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowDegreeComp= (mysqli_fetch_array($resultDegreeComp,MYSQLI_ASSOC));
    $degreefile = $rowDegreeComp['Final_Degree_File'];

    $resultDefence = mysqli_query($connection, "SELECT * FROM tbl_defence_notification WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowDefence= (mysqli_fetch_array($resultDefence,MYSQLI_ASSOC));
    $commitee = $rowDefence['Commitee_File'];
    $BASRfile = $rowDefence['BASR_File'];

    $resultSupervisor = mysqli_query($connection, "SELECT * FROM tbl_supervisor WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowSupervisor= (mysqli_fetch_array($resultSupervisor,MYSQLI_ASSOC));
    $approvalSupervisor = $rowSupervisor['BASR_Approval_File'];

    $resultProgress = mysqli_query($connection, "SELECT * FROM tbl_semester_progress_report WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $rowProgress= (mysqli_fetch_array($resultProgress,MYSQLI_ASSOC));
    $progressFile = $rowProgress['Progress_File'];

    $resultResearch = mysqli_query($connection, "SELECT * FROM tbl_research_paper WHERE PID = '$id'") or die(mysqli_error($connection)); 
    $rowRearch= (mysqli_fetch_array($resultResearch,MYSQLI_ASSOC));
    $researchPaper = $rowRearch['PFile'];
    $title = $rowRearch['Title'];
    $name = $rowRearch['JC_Name'];
    $volume = $rowRearch['ISSN'];
    $factor = $rowRearch['Impact_Factor'];
    $jcr = $rowRearch['JC'];

class PDF extends FPDF {
 
    function Header() {
        //include 'includes/connection.php';
        //$this->Cell(190, 10, "PHD Student File ",1, 5, "C");
        //reset Y
        //$this->SetY(1);
    }
    function Footer(){

    }
    

}
 
    $pdf=new PDF();
    
    $pdf->SetMargins(10,10,5);
    $pdf->SetFont('Times','',14);
    $pdf->SetY(10);
    $pdf->AddPage();
    $pdf->Cell(190, 10, "PHD Student File ",1, 5, "C");
    $height = 7;
    $width = 140;
    $pdf->SetY(20);
    $pdf->Cell($width, 15, "Name Of Documents ", 'LR', 0,"C");
    $pdf->Cell(50, 7, "Evidence attached ", 1, 1,"C");
    $pdf->Cell($width, 7, "", 'LRB', 0,"C");
    $pdf->Cell(17, 7, "Yes ", 1, 0,"C");
    $pdf->Cell(17, 7, "No ", 1, 0,"C");
    $pdf->Cell(16, 7, "N/A ", 1, 1,"C");
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Secondary School Certificate or Equivalent",1, 0);
    $resultSSC = mysqli_query($connection, "SELECT * FROM tbl_qualification WHERE CID = '$id' and Degree = 'SSC/Equivalent'") or die(mysqli_error($connection)); 
    $rowSSC= (mysqli_fetch_array($resultSSC,MYSQLI_ASSOC));

    $SCcertificate = $rowSSC['Original_Certificate'];
    $SCsheet = $rowSSC['DMC'];
    $SSCid = $rowSSC['CID'];
    $SSCDegree = $rowSSC['Degree'];
    $SSCYear = $rowSSC['Passing_Year'];
    $SSCSubject = $rowSSC['Major'];
    //echo $SCcertificate;
    if($SCcertificate != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Mark Sheet of Secondary School Certificate or Equivalent",1, 0);
    if($SCsheet != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Higher Secondary School Certificate or Equivalent",1, 0);
    $resultFA = mysqli_query($connection, "SELECT * FROM tbl_qualification WHERE CID = '$id' and Degree = 'FA / FSc. /Equivalent'") or die(mysqli_error($connection)); 
    $rowFA= (mysqli_fetch_array($resultFA,MYSQLI_ASSOC));
    $FAcertificate = $rowFA['Original_Certificate'];
    $FAsheet = $rowFA['DMC'];
    if($FAcertificate != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Mark Sheet of Higher Secondary School Certificate or Equivalent",1, 0);
    if($FAsheet != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Bachelors of Equivalent Degree (16 years qualification)",1, 0);
    $resultBA = mysqli_query($connection, "SELECT * FROM tbl_qualification WHERE CID = '$id' and Degree = 'BA / BSc. /Equivalent'") or die(mysqli_error($connection)); 
    $rowBA= (mysqli_fetch_array($resultBA,MYSQLI_ASSOC));
    
    $BAcertificate = $rowBA['Original_Certificate'];
    $BAsheet = $rowBA['DMC'];
    $BAid = ["CID"];

    if($BAcertificate != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of Bachelors or Equivalent (16 years qualification)",1, 0);
    if($BAsheet != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Equivalence Certificate from HEC",1, 0);
    $resultPHD = mysqli_query($connection, "SELECT * FROM tbl_qualification WHERE CID = '$id' and Degree = 'PHD'") or die(mysqli_error($connection)); 
    $rowPHD= (mysqli_fetch_array($resultPHD,MYSQLI_ASSOC));
    $PHDcertificate = $rowPHD['Original_Certificate'];
    if($PHDcertificate != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "MS/M.Phil.Equivalent Degree (18 years qualification)",1, 0);
    $resultMS = mysqli_query($connection, "SELECT * FROM tbl_qualification WHERE CID = '$id' and Degree = 'MS / M.Phil/Equivalent'") or die(mysqli_error($connection)); 
    $rowMS= (mysqli_fetch_array($resultMS,MYSQLI_ASSOC));
    $MScertificate = $rowMS['Original_Certificate'];
    $MSsheet = $rowMS['DMC'];
    if($MScertificate != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of MS/M.Phil or Equivalent (18 years qualification)",1, 0);
    if($MSsheet != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Equivalence Certificate from HEC",1, 0);
    $resultPost = mysqli_query($connection, "SELECT * FROM tbl_qualification WHERE CID = '$id' and Degree = 'Post-Doc'") or die(mysqli_error($connection)); 
    $rowPost= (mysqli_fetch_array($resultPost,MYSQLI_ASSOC));
    $POSTcertificate = $rowPost['Original_Certificate'];
    //$MSsheet = $rowPost['DMC'];
    if($POSTcertificate != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }

    
    $pdf->SetFont('Times','',12);

    $pdf->Cell($width, $height, "Admission Form",1, 0);
    
    if($admission != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "CNIC",1, 0);
    
    if($cnic != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "GAT Subject or Equivalent",1, 0);
    if($GAT != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Porposal at the time of Admission",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Allocation of Supervisor (provisionall)",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0,"C");
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Admission Confirmation Letter",1, 0);
    $pdf->SetFont('ZapfDingbats','', 10);
    $pdf->Cell(17, $height, "", 1, 0,"C");
    $pdf->Cell(17, $height, "", 1, 0);
    $pdf->Cell(16, $height, "4", 1, 1);
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of FALL Semester",1, 0);
    $resultFALL = mysqli_query($connection, "SELECT * FROM tbl_transcript_details WHERE CID = '$id' and Semester_Num='FALL%'") or die(mysqli_error($connection)); 
    $rowFALL= (mysqli_fetch_array($resultFALL,MYSQLI_ASSOC));
    $semesterFALL = $rowFALL['Semester_Num'];
    //echo $semesterFALL;
    if($semesterFALL != ""){
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of SPRING Semester",1, 0);
    $resultSpring = mysqli_query($connection, "SELECT * FROM tbl_transcript_details WHERE CID = '$id' and Semester_Num='SPRING%'") or die(mysqli_error($connection)); 
    $rowSpring= (mysqli_fetch_array($resultSpring,MYSQLI_ASSOC));
    $semesterSpring = $rowSpring['Semester_Num'];
    if($semesterSpring != ""){
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Transcript of SUMMER Semester",1, 0);
    $resultSummer = mysqli_query($connection, "SELECT * FROM tbl_transcript_details WHERE CID = '$id' and Semester_Num='SUMMER%'") or die(mysqli_error($connection)); 
    $rowSummer= (mysqli_fetch_array($resultSummer,MYSQLI_ASSOC));
    $semesterSummer = $rowSummer['Semester_Num'];
    if($semesterSummer != ""){
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Comprehencive Examination",1, 0);
    
    if($comprehensive !== "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    } 
    
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Approval of Synopsis from BASR or Equivalent",1, 0);
    if($BASRsynopsis != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Approval of Supervisor allocation",1, 0);
    if($approvalSupervisor !== "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    } 
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Semester wise Progress Report",1, 0);
    if($progressFile !== "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    } 
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Publication of Research Paper in HEC Recognized Journal",1, 0);
    if($researchPaper !== "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Thesis Submission",1, 0);
    
    if($Thesis != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else
    {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);

    $pdf->Cell($width, $height, "Selection of Foreign Evaluators",1, 0);
    
    if($apfile != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Foreign Evaluators Reports",1, 0);
    if($report != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }

    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Notification of Public Defence",1, 0);
    if($BASRfile !== "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    } 
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Notification of Successful Defence",1, 0);
    if($commitee !== "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    } 
    
    $pdf->SetFont('Times','',12);
    $pdf->Cell($width, $height, "Notification of Degree Completion",1, 0);
    if($degreefile != "" ) {
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "4", 1, 0,"C");
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    else{
        $pdf->SetFont('ZapfDingbats','', 10);
        $pdf->Cell(17, $height, "", 1, 0);
        $pdf->Cell(17, $height, "4", 1, 0);
        $pdf->Cell(16, $height, "", 1, 1);
    }
    $pdf->SetFont('Times','',12);
    //////////////////////////////////////////////  Images  ////////////////////////////////////////////////////////
    $pg=2;
    $width = 32;$height = 7;
    if($SCcertificate != ""){
        
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        $pdf->Cell(190, 10, "Secondary School Certificate  ".$pg,0, 1, "R");
        $pdf->Cell(190, 0, "", 'LRT', 1,"C");
        $pdf->SetY(25);
        $pdf->Cell($width,$height,"Reg #",1,"R");
        $pdf->Cell($width,$height,"Name",1,"R");
        $pdf->Cell($width,$height,"Father Name",1,"R");
        $pdf->Cell($width,$height,"Degree",1,"R");
        $pdf->Cell($width,$height,"Subject ",1,"R");
        $pdf->Cell($width,$height,"Year Of Passing",1,1);

        $pdf->Cell($width,$height,$SSCid,1,"R");
        $pdf->Cell($width,$height,$Name,1,"R");
        $pdf->Cell($width,$height,$Fname,1,"R");
        $pdf->Cell($width,$height,$SSCDegree,1,"R");
        $pdf->Cell($width,$height,$SSCSubject,1,"R");
        $pdf->Cell($width,$height,$SSCYear,1,1);

        $image1 = "uploads/$SCcertificate";
        $pdf->Image($image1,30 ,50, 150, 200);
        $pg++;
    }
    if($BAcertificate != ""){
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        $pdf->Cell(190, 10, "Bachelors of Equivalent Degree (16 years qualification)  ".$pg,0, 1, "R");
        $pdf->Cell(190, 0, "", 'LRT', 1,"C");
        $pdf->SetY(25);
        $pdf->Cell($width,$height,"Reg #",1,"R");
        $pdf->Cell($width,$height,"Name",1,"R");
        $pdf->Cell($width,$height,"Father Name",1,"R");
        $pdf->Cell($width,$height,"Degree",1,"R");
        $pdf->Cell($width,$height,"Subject ",1,"R");
        $pdf->Cell($width,$height,"Year Of Passing",1,1);
        //$pdf->SetY(30);
        $pdf->Cell($width,$height,$SSCid,1,"R");
        $pdf->Cell($width,$height,$Name,1,"R");
        $pdf->Cell($width,$height,$Fname,1,"R");
        $pdf->Cell($width,$height,$SSCDegree,1,"R");
        $pdf->Cell($width,$height,$SSCSubject,1,"R");
        $pdf->Cell($width,$height,$SSCYear,1,1);
        $image1 = "uploads/$BAcertificate";
        $pdf->Image($image1,30 ,50, 150, 200);
        $pg++;
    }
    //$pdf->SetAutoPageBreak(true,15);
    if($BAsheet != ""){
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        $pdf->Cell(190, 10, "Transcript of Bachelors or Equivalent (16 years qualification) ".$pg,0, 1, "R");
        $pdf->Cell(190, 0, "", 'LRT', 1,"C");
        $pdf->SetY(25);
        $pdf->Cell($width,$height,"Reg #",1,"R");
        $pdf->Cell($width,$height,"Name",1,"R");
        $pdf->Cell($width,$height,"Father Name",1,"R");
        $pdf->Cell($width,$height,"Degree",1,"R");
        $pdf->Cell($width,$height,"Subject ",1,"R");
        $pdf->Cell($width,$height,"Year Of Passing",1,1);
        //$pdf->SetY(30);
        $pdf->Cell($width,$height,$SSCid,1,"R");
        $pdf->Cell($width,$height,$Name,1,"R");
        $pdf->Cell($width,$height,$Fname,1,"R");
        $pdf->Cell($width,$height,$SSCDegree,1,"R");
        $pdf->Cell($width,$height,$SSCSubject,1,"R");
        $pdf->Cell($width,$height,$SSCYear,1,1);
        $image2 = "uploads/$BAsheet";
        $pdf->Image($image2,30 ,50, 150, 200);
        $pg++;
    }
    if($MSsheet != ""){
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        $pdf->Cell(190, 10, "MS/M.Phil.Equivalent Degree (18 years qualification)   ".$pg,0, 1, "R");
        $pdf->Cell(190, 0, "", 'LRT', 1,"C");
        $pdf->SetY(25);
        $pdf->Cell($width,$height,"Reg #",1,"R");
        $pdf->Cell($width,$height,"Name",1,"R");
        $pdf->Cell($width,$height,"Father Name",1,"R");
        $pdf->Cell($width,$height,"Degree",1,"R");
        $pdf->Cell($width,$height,"Subject ",1,"R");
        $pdf->Cell($width,$height,"Year Of Passing",1,1);
        //$pdf->SetY(30);
        $pdf->Cell($width,$height,$SSCid,1,"R");
        $pdf->Cell($width,$height,$Name,1,"R");
        $pdf->Cell($width,$height,$Fname,1,"R");
        $pdf->Cell($width,$height,$SSCDegree,1,"R");
        $pdf->Cell($width,$height,$SSCSubject,1,"R");
        $pdf->Cell($width,$height,$SSCYear,1,1);
        $image2 = "uploads/$MSsheet";
        $pdf->Image($image2,30 ,50, 150, 200);
        $pg++;
    }
    if($MScertificate != ""){
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        $pdf->Cell(190, 10, "Transcript of MS/M.Phil or Equivalent (18 years qualification)   ".$pg,0, 1, "R");
        $pdf->Cell(190, 0, "", 'LRT', 1,"C");
        $pdf->SetY(25);
        $pdf->Cell($width,$height,"Reg #",1,"R");
        $pdf->Cell($width,$height,"Name",1,"R");
        $pdf->Cell($width,$height,"Father Name",1,"R");
        $pdf->Cell($width,$height,"Degree",1,"R");
        $pdf->Cell($width,$height,"Subject ",1,"R");
        $pdf->Cell($width,$height,"Year Of Passing",1,1);
        //$pdf->SetY(30);
        $pdf->Cell($width,$height,$SSCid,1,"R");
        $pdf->Cell($width,$height,$Name,1,"R");
        $pdf->Cell($width,$height,$Fname,1,"R");
        $pdf->Cell($width,$height,$SSCDegree,1,"R");
        $pdf->Cell($width,$height,$SSCSubject,1,"R");
        $pdf->Cell($width,$height,$SSCYear,1,1);
        $image2 = "uploads/$MScertificate";
        $pdf->Image($image2,30 ,50, 150, 200);
        $pg++;
    }
    //Approval of Synopsis
    if($BASRsynopsis != ""){
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        $pdf->Cell(190, 10, "Approval of Synopsis from BASR or Equivalent   ".$pg,0, 1, "R");
        $pdf->Cell(190, 0, "", 'LRT', 1,"C");
        $pdf->SetY(25);
        $pdf->Cell($width,$height,"Reg #",1,"R");
        $pdf->Cell($width,$height,"Name",1,"R");
        $pdf->Cell($width,$height,"Title",1,"R");
        $pdf->Cell($width,$height,"Abstract",1,"R");
        $pdf->Cell($width,$height,"Supervisor ",1,"R");
        $pdf->Cell($width,$height,"Approval Date",1,1);
        //$pdf->SetY(30);
        $pdf->Cell($width,$height,$CID,1,"R");
        $pdf->Cell($width,$height,$Name,1,"R");
        $pdf->Cell($width,$height,$TitleSynopsis,1,"R");
        $pdf->Cell($width,$height,$AbstractSynopsis,1,"R");
        $pdf->Cell($width,$height,$SupervisorSynopsis,1,"R");
        $pdf->Cell($width,$height,$DateSynopsis,1,1);
        $image2 = "uploads/$BASRsynopsis";
        $pdf->Image($image2,30 ,50, 150, 200);
        $pg++;
    }
    if($img1CNIC != ""){
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        $pdf->Cell(190, 10, "CNIC Front   ".$pg,0, 1, "R");
        $pdf->Cell(190, 0, "", 'LRT', 1,"C");
        $pdf->SetY(25);
        $pdf->Cell($width,$height,"Reg #",1,"R");
        $pdf->Cell($width,$height,"Name",1,"R");
        $pdf->Cell($width,$height,"Father Name",1,"R");
        $pdf->Cell($width,$height,"Email",1,"R");
        $pdf->Cell($width,$height,"Gender ",1,"R");
        $pdf->Cell($width,$height,"Domicile",1,1);
        //$pdf->SetY(30);
        $pdf->Cell($width,$height,$CID,1,"R");
        $pdf->Cell($width,$height,$Name,1,"R");
        $pdf->Cell($width,$height,$Fname,1,"R");
        $pdf->Cell($width,$height,$email,1,"R");
        $pdf->Cell($width,$height,$gender,1,"R");
        $pdf->Cell($width,$height,$domicile,1,1);
        $image2 = "uploads/$img1CNIC";
        $pdf->Image($image2,30 ,50, 150, 200);
        $pg++;
    }
    if($img2CNIC != ""){
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        $pdf->Cell(190, 10, "CNIC Back  ".$pg,0, 1, "R");
        $pdf->Cell(190, 0, "", 'LRT', 1,"C");
        $pdf->SetY(25);
        $pdf->Cell($width,$height,"Reg #",1,"R");
        $pdf->Cell($width,$height,"Name",1,"R");
        $pdf->Cell($width,$height,"Father Name",1,"R");
        $pdf->Cell($width,$height,"Email",1,"R");
        $pdf->Cell($width,$height,"Gender ",1,"R");
        $pdf->Cell($width,$height,"Domicile",1,1);
        //$pdf->SetY(30);
        $pdf->Cell($width,$height,$CID,1,"R");
        $pdf->Cell($width,$height,$Name,1,"R");
        $pdf->Cell($width,$height,$Fname,1,"R");
        $pdf->Cell($width,$height,$email,1,"R");
        $pdf->Cell($width,$height,$gender,1,"R");
        $pdf->Cell($width,$height,$domicile,1,1);
        $image2 = "uploads/$img1CNIC";
        $pdf->Image($image2,30 ,50, 150, 200);
        $pg++;
    }
    //NTS 
    if($GAT != ""){
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        $pdf->Cell(190, 10, "GAT Result Card    ".$pg,0, 1, "R");
        $pdf->Cell(190, 0, "", 'LRT', 1,"C");
        $pdf->SetY(25);
        $pdf->Cell($width,$height,"Reg #",1,"R");
        $pdf->Cell($width,$height,"Name",1,"R");
        $pdf->Cell($width,$height,"Father Name",1,"R");
        $pdf->Cell($width,$height,"Obt Marks",1,"R");
        $pdf->Cell($width,$height,"Max Marks ",1,"R");
        $pdf->Cell($width,$height,"Test Date",1,1);

        $pdf->Cell($width,$height,$CID,1,"R");
        $pdf->Cell($width,$height,$Name,1,"R");
        $pdf->Cell($width,$height,$Fname,1,"R");
        $pdf->Cell($width,$height,$obt,1,"R");
        $pdf->Cell($width,$height,$marks,1,"R");
        $pdf->Cell($width,$height,$date,1,1);
        $image2 = "uploads/$GAT";
        $pdf->Image($image2,30 ,50, 150, 200);
        $pg++;
    }
    //Comprehencive Examination
    if($comprehensive != ""){
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        $pdf->Cell(190, 10, "Comprehencive Result Cards    ".$pg,0, 1, "R");
        $pdf->Cell(190, 0, "", 'LRT', 1,"C");
        $pdf->SetY(25);
        $pdf->Cell($width,$height,"Reg #",1,"R");
        $pdf->Cell($width,$height,"Name",1,"R");
        $pdf->Cell($width,$height,"Father Name",1,"R");
        $pdf->Cell($width,$height,"Obt Marks",1,"R");
        $pdf->Cell($width,$height,"Max Marks ",1,"R");
        $pdf->Cell($width,$height,"Test Date",1,1);

        $pdf->Cell($width,$height,$CID,1,"R");
        $pdf->Cell($width,$height,$Name,1,"R");
        $pdf->Cell($width,$height,$Fname,1,"R");
        $pdf->Cell($width,$height,$compObt,1,"R");
        $pdf->Cell($width,$height,$compMax,1,"R");
        $pdf->Cell($width,$height,$compDate,1,1);
        $image2 = "uploads/$comprehensive";
        $pdf->Image($image2,30 ,50, 150, 200);
        $pg++;
    }
   
    $pdf->SetAutoPageBreak(true,15);

    $pdf->SetFont('Times','',12);
   // $pdf->Cell($width, $height, "Notification of Degree Completion",1, 0);

   ///////////////////////////////////////////////   After Images   /////////////////////////////////////////////////////////////////
   
   $pdf->SetMargins(10,10,5);
   $pdf->SetFont('Times','',14);
   $pdf->SetY(10);
   $pdf->AddPage();
   $pdf->Cell(190, 10, "LIST OF PUBLICATIONS",0, 0, "C");
   $pdf->SetFont('Times','',10);
   $pdf->Cell(5, 10, $pg,0, 1, "R");
   $pdf->SetFont('Times','',14);
   $pdf->Cell(67, 0, "", '', 0,"C");
   $pdf->Cell(57, 0, "", 'LRT', 1,"C");
   $pg++;
   $height = 5;
   $width = 140;
   $pdf->SetY(30);

   $pdf->SetFont('Times','',12);
   $pdf->Cell(10, $height, "", '', 0,"C");
    $pdf->Cell(10, $height, "Sr.", 'LRT', 0,"C");
    //$pdf->Cell(10, $height, "No ", 'LR', 0,"C");
    $pdf->Cell(50, $height, "Title ", 'LRT', 0,"L");
    $pdf->Cell(53, $height, "Name of Journal/Conference ", 'LRT', 0,"L");
    $pdf->Cell(30, $height, "Volume No. ", 'LRT', 0,"L");
    $pdf->Cell(15, $height, "Impact ", 'LRT', 0,"L");
    $pdf->Cell(15, $height, "JCR ", 'LRT', 1,"L");
    $pdf->Cell(10, $height, "", '', 0,"C");       //Left margin
    $pdf->Cell(10, $height, "No", 'LRB', 0,"C");
    $pdf->Cell(50, $height, " ", 'LRB', 0,"L");
    $pdf->Cell(53, $height, "", 'LRB', 0,"L");
    $pdf->Cell(30, $height, "", 'LRB', 0,"L");
    $pdf->Cell(15, $height, "", 'LRB', 0,"L");
    $pdf->Cell(15, $height, "", 'LRB', 1,"L");
    $pdf->Cell(10, $height, "", '', 0,"C");       //Left margin
    $pdf->Cell(10, $height, "(A)", 1, 0,"C");
    $pdf->Cell(163, $height, "PUBLISHED", 1, 1,"L");//////////////////////
    $i=1;
    $resultResearch = mysqli_query($connection, "SELECT * FROM tbl_research_paper WHERE PID = '$id'") or die(mysqli_error($connection)); 
    
    while($row = mysqli_fetch_array($resultResearch)){

        $pdf->Cell(10, $height, "", '', 0,"C");
        $pdf->Cell(10, $height, $i, 'LRT', 1,"C");
        $pdf->Cell(50, $height, $row['Title']);
        $pdf->Cell(53, $height, $row["JC_Name"], 'LRT', 0,"L");
        $pdf->Cell(30, $height, $row["ISSN"], 'LRT', 0,"L");
        $pdf->Cell(15, $height, $row["Impact_Factor"], 'LRT', 0,"L");
        $pdf->Cell(15, $height, $row["PDate"], 'LRT', 1,"L");
        $pdf->Cell(10, $height, "", '', 0,"C");       //Left margin

    }
    
    
    //MEMO PAGE
    $pdf->SetMargins(10,10,5);
    $pdf->SetFont('Times','',14);
    $pdf->SetY(10);
    $pdf->AddPage();
    $pdf->Cell(190, 10, "MEMO",0, 0, "C");
    $pdf->SetFont('Times','',10);
    $pdf->Cell(5, 10, $pg,0, 1, "R");
    $pdf->SetFont('Times','',14);
    $pg++;

    ////////////////////////////////////////Thesis Evaluation Report///////////////////////////////////////

    //if($report != ""){
        $pdf->SetMargins(10,10,5);
        $pdf->SetFont('Times','',10);
        $pdf->SetY(10);
        $pdf->AddPage();
        
        $p=1;
        $result = mysqli_query($connection, "SELECT * FROM tbl_evaluation WHERE CID = '$id'") or die(mysqli_error($connection)); 
        $Width = 48; $Hieght = 7;
        while($row = (mysqli_fetch_array($result,MYSQLI_ASSOC))){
            $pdf->Cell(190, 10, "Thesis Evaluation Report ".$pg,0, 1, "R");
            $pdf->Cell(190, 0, "", 'LRT', 1,"C");

            $pdf->SetY(25);
            $pdf->Cell($Width,$Hieght,"Reg #",1,"R");
            $pdf->Cell($Width,$Hieght,"Name",1,"R");
            $pdf->Cell($Width,$Hieght,"Evaluator Name",1,"R");
            $pdf->Cell($Width,$Hieght,"Affeliations",1,1);

            $pdf->Cell($Width,$Hieght,$CID,1,"R");
            $pdf->Cell($Width,$Hieght,$Name,1,"R");
            $pdf->Cell($Width,$Hieght,$row['Evaluator_name'],1,"R");
            $pdf->Cell($Width,$Hieght,$row['Affeliations'],1,1);

            $img = $row["Evaluation_Report"];
            $image2 = "uploads/$img";
            $pdf->SetY(20);
            $pdf->Image($image2,30 ,50, 150, 200);
            $pdf->SetY(250);
            $pdf->Cell(190, 10, $p."of 3",0, 1, "R");
            $p++;
        }
        
        //$image2 = "uploads/$GAT";
        //$pdf->Image($image2,30 ,30, 150, 200);
        $pg++;
    //}
    $pdf->SetFont('Times','',12);
    //$pdf->Cell($width, $height, "Secondary School Certificate or Equivalent",1, 0);
$pdf->Output('I', $id);
?>

<?php

//  var_dump($_POST);
 include 'includes/connection.php';
 $id = $_POST['reg_id'];

 $location = getcwd() . '/uploads/';

 if(isset($_POST['submit'])){
	$email=$_POST['email'];
	$name=$_POST['name'];
	$namepp = $_FILES['profilepicture']['name'];  
	$temp_name  = $_FILES['profilepicture']['tmp_name']; 
	$dob=$_POST['dob'];
	$cnic=$_POST['cnic'];
	$namecnic = $_FILES['cnicimg']['name'];  
	$temp_namecnic  = $_FILES['cnicimg']['tmp_name'];
	$namecnic_b = $_FILES['cnicimg_back']['name'];  
	$temp_namecnic_b  = $_FILES['cnicimg_back']['tmp_name'];
	$fname=$_POST['fname'];
	$gender=$_POST['gender'];
	$mobile=$_POST['mobile'];
	if($_POST['domicile'] == '0'){
		$domicile=$_POST['custom-domicile'];
	}
	else{
		$domicile=$_POST['domicile'];
	}
	if($_POST['religion'] == '0'){
		$religion=$_POST['custom-religion'];
	}
	else {
		$religion=$_POST['religion'];
	}
	
	
	
	$pic_db = "";
	
	if(!empty($namepp)){
		$pic_name = "profilepic/{$id}." . pathinfo($namepp, PATHINFO_EXTENSION);
		if(file_exists($location . $pic_name)) {
			chmod($location . $pic_name,0644); //Change the file permissions if allowed
			unlink($location . $pic_name); //remove the file
		}
		if(move_uploaded_file($temp_name, $location . $pic_name))
		{
			$pic_db = "Picture = '{$pic_name}', ";
		}
		
	}   
	$cnic_db = "";
	
	if(!empty($namecnic)){
		$cnic_name = "cnic/{$id}." . pathinfo($namecnic, PATHINFO_EXTENSION);
		if(file_exists($location . $cnic_name)) {
			// chmod($location . $cnic_name,0755); //Change the file permissions if allowed
			unlink($location . $cnic_name); //remove the file
		}
		if(move_uploaded_file($temp_namecnic, $location . $cnic_name))
		{
			$cnic_db = "CNIC_front = '{$cnic_name}', ";
		}
		// else
			// echo "cnic ";
	}       

	$cnic_db_b = "";
	
	if(!empty($namecnic_b)){
		$cnic_nameb = "cnic/{$id}-back." . pathinfo($namecnic_b, PATHINFO_EXTENSION);
		if(file_exists($location . $cnic_nameb)) {
			// chmod($location . $cnic_name,0755); //Change the file permissions if allowed
			unlink($location . $cnic_nameb); //remove the file
		}
		if(move_uploaded_file($temp_namecnic_b, $location . $cnic_nameb))
		{
			$cnic_db_b = ", CNIC_back = '{$cnic_nameb}' ";
		}
	}    
            
		
            

	$sql = "UPDATE tbl_candidate Set `Name` = '{$name}', Email = '{$email}',Father_Name='{$fname}',Gender='{$gender}', {$pic_db} CNIC_num='{$cnic}',DOB='{$dob}',{$cnic_db} Domicile='{$domicile}',Mobile='{$mobile}',Religion='{$religion}' {$cnic_db_b} where CID = '$id' ";
	
	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";

            	
}
  
////////////////////////////////////////////////////////////
if(isset($_POST['submit_education'])){
	    
	$degree=mysqli_real_escape_string($connection,$_POST['Degree']);
	$year=$_POST['YearOfPassing'];
	//$board=$_POST['Board'];
	if($_POST['Board'] == '0'){
		$board=mysqli_real_escape_string($connection,$_POST['custom-Board']);
	}
	else{
		$board=mysqli_real_escape_string($connection,$_POST['Board']);
	}
	
	$subjects=mysqli_real_escape_string($connection,$_POST['Subjects']);
	$MaxMarks=$_POST['MaxMarks'];
	$ObtMarks=$_POST['ObtainedMarks'];
	/*$dmc=$_POST['DMCPicture'];
	$Original=$_POST['DegreePicture'];*/
	$Distinct=mysqli_real_escape_string($connection,$_POST['Distinction']);
	
	$dmc=$_FILES['DMCPicture']['name'];
	$dmc_tmp = $_FILES['DMCPicture']['tmp_name'];
	$dgr=$_FILES['DegreePicture']['name'];
	$dgr_tmp = $_FILES['DegreePicture']['tmp_name'];

	$deg = array(
		'SSC/Equivalent' => 1,
		'FA / FSc. /Equivalent' => 2,
		'BA / BSc. /Equivalent' => 3,
		'MA / MSc. /Equivalent' => 4,
		'MS / M.Phil/Equivalent' => 5,
		'PhD' => 6,
		'Post-Doc' => 7
	);

	$degid = array_key_exists($degree, $deg) ? $deg[$degree] : "0";
	
	$dmc_db = "";
	$dmc_name = "";
	if(!empty($dmc)){
		$dmc_name = "qualification/{$id}-{$degid}-dmc." . pathinfo($dmc, PATHINFO_EXTENSION);
		if(file_exists($location . $dmc_name)) {
			chmod($location . $dmc_name,0755); //Change the file permissions if allowed
			unlink($location . $dmc_name); //remove the file
		}
		if(move_uploaded_file($dmc_tmp, $location . $dmc_name))
		{
			$dmc_db = ", DMC = '{$dmc_name}' ";
			$dmc_db_i = ",'{$dmc_name}'";
		}
	}  

	$dgr_db = "";
	$dgr_name = "";
	if(!empty($dgr)){
		$dgr_name = "qualification/{$id}-{$degid}-degree." . pathinfo($dgr, PATHINFO_EXTENSION);
		if(file_exists($location . $dgr_name)) {
			chmod($location . $dgr_name,0755); //Change the file permissions if allowed
			unlink($location . $dgr_name); //remove the file
		}
		if(move_uploaded_file($dgr_tmp, $location . $dgr_name))
		{
			$dgr_db = ", Original_Certificate = '{$dgr_name}' ";
			$dgr_db_i = ",'{$dgr_name}' ";
		}
	}  

	$chk = mysqli_query($connection, "SELECT CID FROM tbl_qualification WHERE CID = '$id' AND Degree = '$degree'");

	if(mysqli_fetch_array($chk))
		$sql = "UPDATE tbl_qualification Set Passing_Year = '{$year}',Board='{$board}', Major='{$subjects}', Max_Marks='{$MaxMarks}', Obt_Marks='{$ObtMarks}', Distinction = '{$Distinct}' {$dmc_db} {$dgr_db} where CID = '$id' AND Degree = '{$degree}'";
	else
		$sql = "INSERT INTO tbl_qualification VALUES
				(
					'$id',
					'$degree',
					'$year',
					'$board',
					'$subjects',
					'$MaxMarks',
					'$ObtMarks',
					'$dmc_name',
					'$dgr_name',
					'$Distinct'
				)";

	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED" . mysqli_error($connection) . "<br>" . $sql;
			
	
	
	/*}*/
	
}
if(isset($_POST['submit_gate'])){
	    
	$testDate=$_POST['dob'];
	$MaxMarks=$_POST['MaxMarks'];
	$ObtMarks=$_POST['ObtainedMarks'];
	$resultCard=$_FILES['addmissionForm']['name'];
	$resultCard_tmp = $_FILES['addmissionForm']['tmp_name'];

	$card_db = "";
	if(!empty($resultCard)){
		$card_name = "gat/{$id}." . pathinfo($resultCard, PATHINFO_EXTENSION);
		if(file_exists($location . $card_name)) {
			chmod($location . $card_name,0755); //Change the file permissions if allowed
			unlink($location . $card_name); //remove the file
		}
		if(move_uploaded_file($resultCard_tmp, $location . $card_name))
		{
			$card_db = ", Result_Card = '{$card_name}' ";
		}
		// else
			// echo "cnic ";
	}   
	
	$sql = "UPDATE tbl_gat Set Test_Date = '{$testDate}', Max_Marks = '{$MaxMarks}',Obt_Marks='{$ObtMarks}' {$card_db} where CID = '$id' ";
	
	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";

     
	
}

 if(isset($_POST['submit_transcript'])){

	$semester=$_POST['semester'];
	$year=$_POST['YearOfPassing'];
	$dob=$_POST['dob'];
	$course=$_POST['course'];
	
	$ObtCGPA=$_POST['Obtainedcgpa'];

	$add=$_FILES['addmissionForm']['name'];
	$add_tmp = $_FILES['addmissionForm']['tmp_name'];
	$MaxCGPA = 4;
	$add_db = "";
	$add_name = "";
	if(!empty($add)){
		$add_name = "transcript/{$semester}{$year}-{$id}." . pathinfo($add, PATHINFO_EXTENSION);
		if(file_exists($location . $add_name)) {
			chmod($location . $add_name,0755); //Change the file permissions if allowed
			unlink($location . $add_name); //remove the file
		}
		if(move_uploaded_file($add_tmp, $location . $add_name))
		{
			$add_db = ", Result_Card = '{$add_name}' ";
		}
	} 
 

	$chk = mysqli_query($connection, "SELECT CID FROM tbl_transcript_details WHERE CID = '$id' AND Semester_Num = '$semester {$year}'");
	
	if(mysqli_fetch_array($chk))
		$sql = "UPDATE tbl_transcript_details Set Result_Date= '{$dob}', Max_CGPA = '{$MaxCGPA}',Obt_CGPA='{$ObtCGPA}' {$add_db} where CID = '$id' AND Semester_Num = '{$semester} {$year}'";
	else
		$sql = "INSERT INTO tbl_transcript_details VALUES
				(
					'$id',
					'$semester $year',
					'$dob',
					'$MaxCGPA',
					'$ObtCGPA',
					'$add_name'
				)";
	
	$result = mysqli_query($connection,$sql);
	if($result && !empty($course)) {
		$query = "INSERT INTO tbl_transcript_course VALUES ('{$id}','$semester $year','{$course}')";
		$result = mysqli_query($connection, $query);
	}
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED" .mysqli_error($connection) . $sql;
	
}

if(isset($_POST['transcript_course']))
{
	$course = $_POST['course'];
	$sem_no = $_POST['sem_no'];

	$query = "SELECT * FROM tbl_transcript_details WHERE CID = '$id' AND Semester_Num = '$sem_no'";
	$result = mysqli_query($connection, $query);	
	if(mysqli_num_rows($result) == 0){
		$query = "INSERT INTO tbl_transcript_details (CID, Semester_Num) VALUES ('$id', '$sem_no')";
		$result = mysqli_query($connection, $query);	
	}

	$query = "INSERT INTO tbl_transcript_course VALUES ('{$id}','{$sem_no}','{$course}')";
	$result = mysqli_query($connection, $query);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	die();

}

if(isset($_POST['transcript_course_delete']))
{
	$course = $_POST['course'];
	$sem_no = $_POST['sem_no'];

	$query = "DELETE FROM tbl_transcript_course WHERE CID = '{$id}' AND Semester_Num = '{$sem_no}' AND Course = '{$course}'";
	// echo $query;
	$result = mysqli_query($connection, $query);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	die();

}

if(isset($_POST['add2attempt']))
{
    // die(print_r($_POST));
    $sql = "DELETE FROM tbl_comprehensive WHERE CID = '$id'; INSERT INTO tbl_comprehensive (CID,attempt) VALUES ('$id', '2')";
    
    $result = mysqli_multi_query($connection,$sql);
    if($result)
        echo "SUCCESS";
    else 
        echo "FAILED";
    die();
}


if(isset($_POST['submit_Comprehensive'])){
    
     
    $dob=$_POST['dob'];
    // $MaxMarks=$_POST['MaxMarks'];
    // $ObtMarks=$_POST['ObtainedMarks'];
    $MaxMarks=0;
	$ObtMarks=0;
	
    $no=$_POST['title'];
    $result=$_POST['result'];
    $paper=$_FILES['paper']['name'];
    $paper_tmp=$_FILES['paper']['tmp_name'];
    $solution=$_FILES['solution']['name'];
    $solution_tmp=$_FILES['solution']['tmp_name'];
    
    $add=$_FILES['addmissionForm']['name'];
    $add_tmp = $_FILES['addmissionForm']['tmp_name'];
    
    $add_db = "";
    if(!empty($add)){
        $add_name = "comprehensive/{$id}-result." . pathinfo($add, PATHINFO_EXTENSION);
        if(file_exists($location . $add_name)) {
            chmod($location . $add_name,0755); //Change the file permissions if allowed
            unlink($location . $add_name); //remove the file
        }
        if(move_uploaded_file($add_tmp, $location . $add_name))
        {
            $add_db = ", Comprehensive_Result = '{$add_name}' ";
        }
    }
    
    $paper_db = "";
    if(!empty($paper)){
        $paper_name = "comprehensive/{$id}-paper-{$no}." . pathinfo($paper, PATHINFO_EXTENSION);
        if(file_exists($location . $paper_name)) {
            chmod($location . $paper_name,0755); //Change the file permissions if allowed
            unlink($location . $paper_name); //remove the file
        }
        if(move_uploaded_file($paper_tmp, $location . $paper_name))
        {
            $paper_db = ", Paper{$no} = '{$paper_name}' ";
        }
    }
    
////////////////////////////////////////////////

	$solution_db = "";
    if(!empty($solution)){
        $solution_name = "comprehensive/{$id}-solution-{$no}." . pathinfo($solution, PATHINFO_EXTENSION);
        if(file_exists($location . $solution_name)) {
            chmod($location . $solution_name,0755); //Change the file permissions if allowed
            unlink($location . $solution_name); //remove the file
        }
        if(move_uploaded_file($solution_tmp, $location . $solution_name))
        {
            $solution_db = ", Solution{$no} = '{$solution_name}' ";
        }
    }


    $sql = "UPDATE tbl_comprehensive Set Exam_Date = '{$dob}', Max_Marks= '{$MaxMarks}', Obt_Marks = '{$ObtMarks}', Title{$no} = '1', Result{$no} = '{$result}' {$add_db} {$paper_db} {$solution_db} WHERE CID = '$id' ";
    
    $result = mysqli_query($connection,$sql);
    if($result)
        echo "SUCCESS";
    else 
        echo "FAILED";
    
}


if(isset($_POST['submit_Synopsis'])){
	    
	$dob=$_POST['dob'];
	$titl=$_POST['ThesisTitle'];
	$abstract=$_POST['abstract'];
	$supervisor=$_POST['supervisor'];

	$gpcfile=$_FILES['gpcfile']['name'];
	$dpcfile=$_FILES['dpcfile']['name'];
	$basrfile=$_FILES['basrfile']['name'];
	$gpcfile_tmp=$_FILES['gpcfile']['tmp_name'];
	$dpcfile_tmp=$_FILES['dpcfile']['tmp_name'];
	$basrfile_tmp=$_FILES['basrfile']['tmp_name'];
	
	$synopsis=$_FILES['synopsisfile']['name'];
	$synopsis_tmp = $_FILES['synopsisfile']['tmp_name'];
	     
	$synopsis_db = "";
	if(!empty($synopsis)){
		$synopsis_name = "synopsis/{$id}-synopsis." . pathinfo($synopsis, PATHINFO_EXTENSION);
		if(file_exists($location . $synopsis_name)) {
			chmod($location . $synopsis_name,0755); //Change the file permissions if allowed
			unlink($location . $synopsis_name); //remove the file
		}
		if(move_uploaded_file($synopsis_tmp, $location . $synopsis_name))
		{
			$synopsis_db = ", Synopsis_File = '{$synopsis_name}' ";
		}
	}
	
	$gpcfile_db = "";
	if(!empty($gpcfile)){
		$gpcfile_name = "synopsis/{$id}-gpcfile." . pathinfo($gpcfile, PATHINFO_EXTENSION);
		if(file_exists($location . $gpcfile_name)) {
			chmod($location . $gpcfile_name,0755); //Change the file permissions if allowed
			unlink($location . $gpcfile_name); //remove the file
		}
		if(move_uploaded_file($gpcfile_tmp, $location . $gpcfile_name))
		{
			$gpcfile_db = ", GPC_Approval_file = '{$gpcfile_name}' ";
		}
	}
	
	$dpcfile_db = "";
	if(!empty($dpcfile)){
		$dpcfile_name = "synopsis/{$id}-dpcfile." . pathinfo($dpcfile, PATHINFO_EXTENSION);
		if(file_exists($location . $dpcfile_name)) {
			chmod($location . $dpcfile_name,0755); //Change the file permissions if allowed
			unlink($location . $dpcfile_name); //remove the file
		}
		if(move_uploaded_file($dpcfile_tmp, $location . $dpcfile_name))
		{
			$dpcfile_db = ", DPC_Approcal_File = '{$dpcfile_name}' ";
		}
	}
	
	$basrfile_db = "";
	if(!empty($basrfile)){
		$basrfile_name = "synopsis/{$id}-basrfile." . pathinfo($basrfile, PATHINFO_EXTENSION);
		if(file_exists($location . $basrfile_name)) {
			chmod($location . $basrfile_name,0755); //Change the file permissions if allowed
			unlink($location . $basrfile_name); //remove the file
		}
		if(move_uploaded_file($basrfile_tmp, $location . $basrfile_name))
		{
			$basrfile_db = ", BASR_Approval_File = '{$basrfile_name}' ";
		}
	} 

	$sql = "UPDATE tbl_synopsis Set Approval_Date = '{$dob}', Title= '{$titl}', Abstract = '{$abstract}',Supervisor='{$supervisor}' {$synopsis_db} {$gpcfile_db} {$basrfile_db} {$dpcfile_db} where CID = '$id' ";
	
	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	
}

if(isset($_POST['submit_supervisor'])){

	$sname=$_POST['supervisorname'];
	$aff=$_POST['Affiliations'];
	$gpcfile=$_FILES['gpc_file']['name'];
	$dpcfile=$_FILES['bos_file']['name'];
	$basrfile=$_FILES['basr_file']['name'];
	$gpcfile_tmp=$_FILES['gpc_file']['tmp_name'];
	$dpcfile_tmp=$_FILES['bos_file']['tmp_name'];
	$basrfile_tmp=$_FILES['basr_file']['tmp_name'];
	
	$app_file=$_FILES['app_file']['name'];
	$app_file_tmp = $_FILES['app_file']['tmp_name'];
		
	$app_file_db = "";
	if(!empty($app_file)){
		$app_file_name = "supervisor/{$id}-app_file." . pathinfo($app_file, PATHINFO_EXTENSION);
		if(file_exists($location . $app_file_name)) {
			chmod($location . $app_file_name,0755); //Change the file permissions if allowed
			unlink($location . $app_file_name); //remove the file
		}
		if(move_uploaded_file($app_file_tmp, $location . $app_file_name))
		{
			$app_file_db = ", Approval_file	 = '{$app_file_name}' ";
		}
	}
	
	$gpcfile_db = "";
	if(!empty($gpcfile)){
		$gpcfile_name = "supervisor/{$id}-gpcfile." . pathinfo($gpcfile, PATHINFO_EXTENSION);
		if(file_exists($location . $gpcfile_name)) {
			chmod($location . $gpcfile_name,0755); //Change the file permissions if allowed
			unlink($location . $gpcfile_name); //remove the file
		}
		if(move_uploaded_file($gpcfile_tmp, $location . $gpcfile_name))
		{
			$gpcfile_db = ", GPC_Approval_File = '{$gpcfile_name}' ";
		}
	}
	
	$dpcfile_db = "";
	if(!empty($dpcfile)){
		$dpcfile_name = "supervisor/{$id}-bosfile." . pathinfo($dpcfile, PATHINFO_EXTENSION);
		if(file_exists($location . $dpcfile_name)) {
			chmod($location . $dpcfile_name,0755); //Change the file permissions if allowed
			unlink($location . $dpcfile_name); //remove the file
		}
		if(move_uploaded_file($dpcfile_tmp, $location . $dpcfile_name))
		{
			$dpcfile_db = ", BOS_Approval_File = '{$dpcfile_name}' ";
		}
	}
	
	$basrfile_db = "";
	if(!empty($basrfile)){
		$basrfile_name = "supervisor/{$id}-basrfile." . pathinfo($basrfile, PATHINFO_EXTENSION);
		if(file_exists($location . $basrfile_name)) {
			chmod($location . $basrfile_name,0755); //Change the file permissions if allowed
			unlink($location . $basrfile_name); //remove the file
		}
		if(move_uploaded_file($basrfile_tmp, $location . $basrfile_name))
		{
			$basrfile_db = ", BASR_Approval_File = '{$basrfile_name}' ";
		}
	} 

	$sql = "UPDATE tbl_supervisor Set Name = '{$sname}', Affiliations= '{$aff}' {$app_file_db} {$gpcfile_db} {$basrfile_db} {$dpcfile_db} where CID = '$id' ";
	
	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";

	
}

if(isset($_POST['submit_addmissionForm']))
{

	$app_file=$_FILES['addmissionForm']['name'];
	$app_file_tmp = $_FILES['addmissionForm']['tmp_name'];
		
	$app_file_db = "";
	if(!empty($app_file)){
		$app_file_name = "admission/{$id}." . pathinfo($app_file, PATHINFO_EXTENSION);
		if(file_exists($location . $app_file_name)) {
			chmod($location . $app_file_name,0755); //Change the file permissions if allowed
			unlink($location . $app_file_name); //remove the file
		}
		if(move_uploaded_file($app_file_tmp, $location . $app_file_name))
		{
			$sql = "UPDATE tbl_admission_form Set Upload_Form = '{$app_file_name}' where CID = '$id'";
			$result = mysqli_query($connection,$sql);
			if($result)
				echo "SUCCESS";
			else 
				echo "FAILED";
			die();
		}
	}
	echo "SUCCESS";

}

if(isset($_POST['add_course']))
{
	$course = $_POST['course'];

	$query = "INSERT INTO tbl_pre_req VALUES ('{$id}','{$course}')";
	$result = mysqli_query($connection, $query);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	die();

}

if(isset($_POST['audit_course']))
{
	$course = $_POST['course'];

	$query = "INSERT INTO tbl_audit_courses VALUES ('{$id}','{$course}')";
	$result = mysqli_query($connection, $query);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	die();

}

if(isset($_POST['del_pre_course']))
{
	$course = $_POST['course'];

	$query = "DELETE FROM tbl_pre_req WHERE CID = '{$id}' AND Course_ID = '{$course}'";
	$result = mysqli_query($connection, $query);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	die();

}

if(isset($_POST['del_audit_course']))
{
	$course = $_POST['course'];

	$query = "DELETE FROM tbl_audit_courses WHERE CID = '{$id}' AND Course_ID = '{$course}'";
	$result = mysqli_query($connection, $query);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	die();

}


if(isset($_POST['submit_address'])){
	    
	$p_address = $_POST['Permanent_Address'];
	if($_POST['Permanent_City'] == '0'){
		$p_city = $_POST['custom-city'];
	}
	else{
		$p_city = $_POST['Permanent_City'];
	}	
	$p_phone = $_POST['Permanent_Phone'];
	$val = $_POST['sameAs'];
	$val1='sameAsPernanetForGuadian';
	$val2 = 'sameAsCurrentForGuadian';

	if(isset($_POST['sameAsPernanetForCurrent'])){
		$c_address = $_POST['Permanent_Address'];
		$c_city = $_POST['Permanent_City'];
		$c_phone = $_POST['Permanent_Phone'];
	}
	else{
		$c_address = $_POST['Current_Address'];
	
		if($_POST['Current_District']=='0'){
			$c_city = $_POST['custom-curr-city'];
		}
		else{
			$c_city = $_POST['Current_District'];
		}
		
		
		$c_phone = $_POST['Current_Phone'];
	}
	if($val == $val1){
		$g_address = $_POST['Permanent_Address'];
		$g_city = $_POST['Permanent_City'];
		$g_phone = $_POST['Permanent_Phone'];
	}
	else if($val == $val2){
		$g_address = $_POST['Current_Address'];
		$g_city = $_POST['Current_District'];
		$g_phone = $_POST['Current_Phone'];
	}
	else{
		$g_address = $_POST['Guadian_Address'];
		if($_POST['Guadian_Districts']=='0'){
			$g_city = $_POST['custom-guard-city'];
		}
		else{
			$g_city = $_POST['Guadian_District'];
		}
		
		
		$g_phone = $_POST['Guadian_Phone'];
	}
	
	
	$sql = "UPDATE tbl_address Set Perma_Address = '{$p_address}', Perma_City= '{$p_city}', Perma_Phone = '{$p_phone}', Cur_Address = '{$c_address}', Cur_City= '{$c_city}', Cur_Phone = '{$c_phone}', Guard_Address = '{$g_address}', Guard_City= '{$g_city}', Guard_Phone = '{$g_phone}' where CID = '$id' ";
	
	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	
}


if(isset($_POST['submit_thesis'])){
	    
	$dob=$_POST['dob'];
	$ThesisTitle=$_POST['ThesisTitle'];

	$gpcfile=$_FILES['gpcMeeting']['name'];
	$gpcfile_tmp=$_FILES['gpcMeeting']['tmp_name'];
	
	$app_file=$_FILES['addmissionForm']['name'];
	$app_file_tmp = $_FILES['addmissionForm']['tmp_name'];
		
	$app_file_db = "";
	if(!empty($app_file)){
		$app_file_name = "thesis/{$id}-thesis." . pathinfo($app_file, PATHINFO_EXTENSION);
		if(file_exists($location . $app_file_name)) {
			chmod($location . $app_file_name,0755); //Change the file permissions if allowed
			unlink($location . $app_file_name); //remove the file
		}
		if(move_uploaded_file($app_file_tmp, $location . $app_file_name))
		{
			$app_file_db = ", thesisfile	 = '{$app_file_name}' ";
		}
	}
	
	$gpcfile_db = "";
	if(!empty($gpcfile)){
		$gpcfile_name = "thesis/{$id}-gpc." . pathinfo($gpcfile, PATHINFO_EXTENSION);
		if(file_exists($location . $gpcfile_name)) {
			chmod($location . $gpcfile_name,0755); //Change the file permissions if allowed
			unlink($location . $gpcfile_name); //remove the file
		}
		if(move_uploaded_file($gpcfile_tmp, $location . $gpcfile_name))
		{
			$gpcfile_db = ", gpcfile = '{$gpcfile_name}' ";
		}
	}

	$sql = "UPDATE tbl_thesis Set Date = '{$dob}', title= '{$ThesisTitle}' {$gpcfile_db} {$app_file_db} where CID = '$id' ";
	
	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED" ;
	
}


if(isset($_POST['submit_evaluation'])){
		
	$Evaluator = $_POST['evaluator'];
	$evaluator_name=$_POST['evaluation'];
	$Affeliations=$_POST['Affeliations'];

	$gpcfile=$_FILES['evaluationreport']['name'];
	$gpcfile_tmp=$_FILES['evaluationreport']['tmp_name'];

	$app_file=$_FILES['aprovalreport']['name'];
	$app_file_tmp = $_FILES['aprovalreport']['tmp_name'];
		
	$app_file_db = "";
	if(!empty($app_file)){
		$app_file_name = "evaluation/{$id}-app_file." . pathinfo($app_file, PATHINFO_EXTENSION);
		if(file_exists($location . $app_file_name)) {
			chmod($location . $app_file_name,0755); //Change the file permissions if allowed
			unlink($location . $app_file_name); //remove the file
		}
		if(move_uploaded_file($app_file_tmp, $location . $app_file_name))
		{
			$app_file_db = ", Approval_file	 = '{$app_file_name}' ";
		}
	}
	
	$gpcfile_db = "";
	if(!empty($gpcfile)){
		$gpcfile_name = "evaluation/{$id}-gpcfile." . pathinfo($gpcfile, PATHINFO_EXTENSION);
		if(file_exists($location . $gpcfile_name)) {
			chmod($location . $gpcfile_name,0755); //Change the file permissions if allowed
			unlink($location . $gpcfile_name); //remove the file
		}
		if(move_uploaded_file($gpcfile_tmp, $location . $gpcfile_name))
		{
			$gpcfile_db = ", Evaluation_Report = '{$gpcfile_name}' ";
		}
	}


	$sql = "UPDATE tbl_evaluation Set Evaluator = '{$Evaluator}', Evaluator_name = '{$evaluator_name}', Affeliations= '{$Affeliations}' {$gpcfile_db} {$app_file_db} where CID = '$id' ";
	
	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	
}

if(isset($_POST['submit_publication'])){
	    
	$Title=$_POST['title'];
	$Author=$_POST['author'];
	$Type=$_POST['PublicationType'];
	$Conf_Name=$_POST['con_name'];
	$Impact_fact=$_POST['impactFactor'];
	$ISSN=$_POST['issnNum'];
	$P_date=$_POST['dob'];
	$resultCard=$_FILES['attchfile']['name'];
	$resultCard_tmp = $_FILES['attchfile']['tmp_name'];
	
	$card_db = "";
	if(!empty($resultCard)){
		$file_name = "publication/{$id}." . pathinfo($resultCard, PATHINFO_EXTENSION);
		if(file_exists($location . $file_name)) {
			chmod($location . $file_name,0755);
			unlink($location . $file_name); 
		}
		if(move_uploaded_file($resultCard_tmp, $location . $file_name))
		{
			$card_db = ", Result_Card = '{$file_name}' ";
		}
	}  

	$sql = "UPDATE tbl_research_paper Set Titl = '{$Title}', Author= '{$Author}',publication_Type= '{$Type}', Conference_Name = '{$Conf_Name}', Impact_Factor= '{$Impact_fact}', ISSN_Number= '{$ISSN}' , Publication_Date= '{$P_date}' {$card_db} where CID = '$id' ";
	
	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	
}


if(isset($_POST['submit_notification'])){
	    
	$dob=$_POST['dob'];
	$gpcfile=$_FILES['basrfile']['name'];
	$gpcfile_tmp=$_FILES['basrfile']['tmp_name'];

	$app_file=$_FILES['addmissionForm']['name'];
	$app_file_tmp = $_FILES['addmissionForm']['tmp_name'];
		
	$app_file_db = "";
	if(!empty($app_file)){
		$app_file_name = "public_notification/{$id}-Commitee_File." . pathinfo($app_file, PATHINFO_EXTENSION);
		if(file_exists($location . $app_file_name)) {
			chmod($location . $app_file_name,0755); //Change the file permissions if allowed
			unlink($location . $app_file_name); //remove the file
		}
		if(move_uploaded_file($app_file_tmp, $location . $app_file_name))
		{
			$app_file_db = ", Commitee_File	 = '{$app_file_name}' ";
		}
	}
	
	$gpcfile_db = "";
	if(!empty($gpcfile)){
		$gpcfile_name = "public_notification/{$id}-basr." . pathinfo($gpcfile, PATHINFO_EXTENSION);
		if(file_exists($location . $gpcfile_name)) {
			chmod($location . $gpcfile_name,0755); //Change the file permissions if allowed
			unlink($location . $gpcfile_name); //remove the file
		}
		if(move_uploaded_file($gpcfile_tmp, $location . $gpcfile_name))
		{
			$gpcfile_db = ", BASR_File = '{$gpcfile_name}' ";
		}
	}

	$sql = "UPDATE tbl_defence_notification Set Date = '{$dob}' {$gpcfile_db} {$app_file_db} where CID = '$id' ";
	
	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	
}
if(isset($_POST['submit_degree'])){
     
    $dob=$_POST['dob'];
    $hec_t=isset($_POST['hec_t']);
    $hec_d=isset($_POST['hec_d']);
    $gpcfile=$_FILES['trnsfile']['name'];
    $gpcfile_tmp=$_FILES['trnsfile']['tmp_name'];
    $gpcfile_b=$_FILES['trnsfile_back']['name'];
    $gpcfile_b_tmp=$_FILES['trnsfile_back']['tmp_name'];

    $app_file=$_FILES['addmissionForm']['name'];
    $app_file_tmp = $_FILES['addmissionForm']['tmp_name'];
    $app_file_b=$_FILES['addmissionForm_back']['name'];
    $app_file_b_tmp = $_FILES['addmissionForm_back']['tmp_name'];
        
    $app_file_db = "";
    if(!empty($app_file)){
        $app_file_name = "degree/{$id}-final_file." . pathinfo($app_file, PATHINFO_EXTENSION);
        if(file_exists($location . $app_file_name)) {
            chmod($location . $app_file_name,0755); //Change the file permissions if allowed
            unlink($location . $app_file_name); //remove the file
        }
        if(move_uploaded_file($app_file_tmp, $location . $app_file_name))
        {
            $app_file_db = ", Final_Degree_File  = '{$app_file_name}' ";
        }
    }       
    $app_file_b_db = "";
    if(!empty($app_file_b) && $hec_d){
        $app_file_b_name = "degree/{$id}-final_file_b." . pathinfo($app_file_b, PATHINFO_EXTENSION);
        if(file_exists($location . $app_file_b_name)) {
            chmod($location . $app_file_b_name,0755); //Change the file permissions if allowed
            unlink($location . $app_file_b_name); //remove the file
        }
        if(move_uploaded_file($app_file_b_tmp, $location . $app_file_b_name))
        {
            $app_file_b_db = ", Final_Degree_File_Back   = '{$app_file_b_name}' ";
        }
    }
    
    $gpcfile_db = "";
    if(!empty($gpcfile)){
        $gpcfile_name = "degree/{$id}-Transcript_File." . pathinfo($gpcfile, PATHINFO_EXTENSION);
        if(file_exists($location . $gpcfile_name)) {
            chmod($location . $gpcfile_name,0755); //Change the file permissions if allowed
            unlink($location . $gpcfile_name); //remove the file
        }
        if(move_uploaded_file($gpcfile_tmp, $location . $gpcfile_name))
        {
            $gpcfile_db = ", Transcript_File = '{$gpcfile_name}' ";
        }
    }
    $gpcfile_b_db = "";
    if(!empty($gpcfile_b) && $hec_t){
        $gpcfile_b_name = "degree/{$id}-Transcript_File_b." . pathinfo($gpcfile_b, PATHINFO_EXTENSION);
        if(file_exists($location . $gpcfile_b_name)) {
            chmod($location . $gpcfile_b_name,0755); //Change the file permissions if allowed
            unlink($location . $gpcfile_b_name); //remove the file
        }
        if(move_uploaded_file($gpcfile_b_tmp, $location . $gpcfile_b_name))
        {
            $gpcfile_b_db = ", Transcript_File_Back = '{$gpcfile_b_name}' ";
        }
    }
    if(!$hec_d){
        $app_file_b_db = ", Final_Degree_File_Back   = '' ";
    }
    if(!$hec_t){
        $gpcfile_b_db = ", Transcript_File_Back = ''";
    }

    $sql = "UPDATE tbl_degree_completion Set Date = '{$dob}' {$gpcfile_db} {$app_file_db} {$app_file_b_db} {$gpcfile_b_db} where CID = '$id' ";
    $result = mysqli_query($connection,$sql);
    if($result)
        echo "SUCCESS";
    else 
        echo "FAILED";
    
}



/*
if(isset($_POST['submit_degree'])){
	    
	$dob=$_POST['dob'];
	$gpcfile=$_FILES['trnsfile']['name'];
	$gpcfile_tmp=$_FILES['trnsfile']['tmp_name'];

	$app_file=$_FILES['addmissionForm']['name'];
	$app_file_tmp = $_FILES['addmissionForm']['tmp_name'];
		
	$app_file_db = "";
	if(!empty($app_file)){
		$app_file_name = "degree/{$id}-final_file." . pathinfo($app_file, PATHINFO_EXTENSION);
		if(file_exists($location . $app_file_name)) {
			chmod($location . $app_file_name,0755); //Change the file permissions if allowed
			unlink($location . $app_file_name); //remove the file
		}
		if(move_uploaded_file($app_file_tmp, $location . $app_file_name))
		{
			$app_file_db = ", Final_Degree_File	 = '{$app_file_name}' ";
		}
	}
	
	$gpcfile_db = "";
	if(!empty($gpcfile)){
		$gpcfile_name = "degree/{$id}-Transcript_File." . pathinfo($gpcfile, PATHINFO_EXTENSION);
		if(file_exists($location . $gpcfile_name)) {
			chmod($location . $gpcfile_name,0755); //Change the file permissions if allowed
			unlink($location . $gpcfile_name); //remove the file
		}
		if(move_uploaded_file($gpcfile_tmp, $location . $gpcfile_name))
		{
			$gpcfile_db = ", Transcript_File = '{$gpcfile_name}' ";
		}
	}

	$sql = "UPDATE tbl_degree_completion Set Date = '{$dob}' {$gpcfile_db}  {$app_file_db} where CID = '$id' ";
	
	$result = mysqli_query($connection,$sql);
	if($result)
		echo "SUCCESS";
	else 
		echo "FAILED";
	
}*/

?>

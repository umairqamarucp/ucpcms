<?php
include 'includes/connection.php';

    $id = $_GET['id'];
    $result1 = mysqli_query($connection, "SELECT * FROM tbl_candidate where CID = '$id'") or die(mysqli_error($connection)); 
    $row1 = mysqli_fetch_array($result1);
    
    $result2 = mysqli_query($connection, "SELECT * FROM tbl_address where CID = '$id'") or die(mysqli_error($connection)); 
    $row2 = mysqli_fetch_array($result2);

    $result3 = mysqli_query($connection, "SELECT * FROM tbl_qualification where CID = '$id'") or die(mysqli_error($connection)); 
    
    $result4 = mysqli_query($connection, "SELECT * FROM tbl_gat WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $row4 = mysqli_fetch_array($result4);

    $result5 = mysqli_query($connection, "SELECT * FROM tbl_transcript_details WHERE CID = '$id'") or die(mysqli_error($connection)); 
    //$row5 = mysqli_fetch_array($result5);

    $result6 = mysqli_query($connection, "SELECT * FROM tbl_comprehensive WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $row6 = mysqli_fetch_array($result6);

    $result7 = mysqli_query($connection, "SELECT * FROM tbl_synopsis WHERE CID = '$id'") or die(mysqli_error($connection)); 
    $row7 = mysqli_fetch_array($result7);

    $result8 = mysqli_query($connection, "SELECT * FROM tbl_supervisor WHERE CID = '$id'") or die(mysqli_error($connection));
    $row8 = mysqli_fetch_array($result8);

    $result9 = mysqli_query($connection, "SELECT * FROM tbl_thesis WHERE CID = '$id'") or die(mysqli_error($connection));
    $row9 = mysqli_fetch_array($result9);

    $result10 = mysqli_query($connection, "SELECT * FROM tbl_evaluation WHERE CID = '$id'") or die(mysqli_error($connection));
    $row10 = mysqli_fetch_array($result10);

    $result11 = mysqli_query($connection, "SELECT * FROM tbl_admission_form where CID='$id'") or die(mysqli_error($connection));
    $row11 = mysqli_fetch_array($result11);

    $result12 = mysqli_query($connection, "SELECT p.Course_ID Course_ID, c.Course_Name Course_Name FROM tbl_pre_req p JOIN tbl_courses c ON p.Course_ID = c.Course_ID  where CID='$id'") or die(mysqli_error($connection));
    
    $result13 = mysqli_query($connection, "SELECT p.Course_ID Course_ID, c.Course_Name Course_Name FROM tbl_audit_courses p JOIN tbl_courses c ON p.Course_ID = c.Course_ID where CID='{$id}'") or die(mysqli_error($connection));
  
    $result14 = mysqli_query($connection, "SELECT * FROM tbl_defence_notification WHERE CID = '$id'") or die(mysqli_error($connection));
     $row14 = mysqli_fetch_array($result14);
    
     $result15 = mysqli_query($connection, "SELECT * FROM tbl_degree_completion WHERE CID = '$id'") or die(mysqli_error($connection));
        $row15 = mysqli_fetch_array($result15);
?>


<!DOCTYPE html>
<html lang="en">
<head>
	  <title>WAD | QUIZ</title>
	  <!--<link rel="icon" href="images/favicon.ico"/>-->
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta http-equiv="cache-control" content="no-cache" />
	  <meta http-equiv="pragma" content="no-cache" />
	  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" crossorigin="anonymous">


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
	  <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
      <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">	</script>
      
      <style>
          .class--program{
              width:150px !important;
              border-left:0px;
              border-right:0px;
              border-top:0px;
              background-color:white !important;
              text-align:center;
              }
              tbody{
                  display:table;
                  width: 100%;
              }
	.btn-primary{
		background-color: white;
		border-color: black;
		color:black;
		}
.btn-danger {
    color: #fff;
    background-color: #dc3545;
    border-color: #dc3545;
}
.btn-success {
    color: #fff;
    background-color: #28a745;
    border-color: #28a745;
}
/* .page {
        width: 210mm;
        min-height: 297mm;
        padding: 20mm;
        margin: 10mm auto;
        border: 1px #D3D3D3 solid;
        border-radius: 5px;
        background: white;
        box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
    } */
    .subpage {
        padding: 1cm;
        border: 5px red solid;
        height: 257mm;
        outline: 2cm #FFEAEA solid;
    }
    
    @page {
        size: A4;
        margin: 0;
    }
    @media print {
        html, body {
            width: 210mm;
            height: 297mm;        
        }
        .page {
            margin: 0;
            border: initial;
            border-radius: initial;
            width: initial;
            min-height: initial;
            box-shadow: initial;
            background: initial;
            page-break-after: always;
        }
    }
      </style>
</head>
<body style="margin-bottom:100px">
    <div class="container page" id="checklist">
        <div class="panel panel-danger">
            <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Check List</Strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                        <table class="table table-stripes">
                        <thead>
                        <tr>
                            <th>Please make sure that you have attached the following documents:</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                            while($row3 = mysqli_fetch_array($result3))
                            {
                                $arrResult[$row3['Degree']] =  $row3;
                            }
                            // echo $row3['Degree'];
                        ?>
                            <tr>
                            <td>
                                <div class="col-md-12">Matriculation/O-level Certificate</div>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($arrResult['SSC/Equivalent']['Degree']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$arrResult['SSC/Equivalent']['Degree']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">Matriculation/O-level Detailed Marks Certificate</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($arrResult['SSC/Equivalent']['Degree']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$arrResult['SSC/Equivalent']['Degree']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">Intermediate/A-level Certificate</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($arrResult['FA / FSc. /Equivalent']['Degree']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$arrResult['FA / FSc. /Equivalent']['Degree']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">Intermediate/A-level Detailed Marks Certificate</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($arrResult['FA / FSc. /Equivalent']['Degree']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$arrResult['FA / FSc. /Equivalent']['Degree']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">Bachelor's Degree</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($arrResult['BA / BSc. /Equivalent']['Degree']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$arrResult['BA / BSc. /Equivalent']['Degree']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">Bachelor's Detailed Marks Certificate</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($arrResult['BA / BSc. /Equivalent']['Degree']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$arrResult['BA / BSc. /Equivalent']['Degree']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">Master's Degree</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($arrResult['MA / MSc. /Equivalent']['Degree']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$arrResult['MA / MSc. /Equivalent']['Degree']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">Master's Detailed Marks Certificate</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($arrResult['MA / MSc. /Equivalent']['Degree']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$arrResult['MA / MSc. /Equivalent']['Degree']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">Any Other Diploma</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($row3['FA / FSc. /Equivalent']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$row3['FA / FSc. /Equivalent']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">Provisional Certificate(For students awaiting results)/Copy of Roll No Slip</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($row3['FA / FSc. /Equivalent']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$row3['FA / FSc. /Equivalent']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">National ID Card/B-Form</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary <?php if($row1['CNIC_front']) echo "btn-success";?>'>Yes</button>
                                        <button class='btn btn-primary <?php if(!$row1['CNIC_front']) echo "btn-danger";?>'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                            <tr>
                            <td><div class="col-md-12">03 Recent photographs with white background</div></td>
                            <td>
                                <div class='col-md-12'>
                                    <div class='row justify-content-around'>
                                        <button class='btn btn-primary'>Yes</button>
                                        <button class='btn btn-primary'>No</button>
                                    </div>
                                </div>
                            </td>
                            </tr>
                        </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- Check List Ended -->
    <div class="container page" id="personal_info">
        <div class="panel panel-danger">
            <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Application Form</Strong></h3>
            </div>
            <div class="panel-body">
            <div class="col-md-12">
                <div class="row justify-content-center">
                    <div>
                        <p>Program Applied for: </p>
                    </div> 
                    <?php 
                        if (strpos($id, 'MSDS') !== false) {
                            echo '<input class="form-control class--program" disabled value="MSDS">';
                        }
                        else if (strpos($id, 'PHDC') !== false) {
                            echo '<input class="form-control class--program" disabled value="PHDC">';
                        }
                        ?>
                </div>
            </div><!-- col-md-12 -->
            <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Personal Information</Strong></h3>
            </div>
            
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-8">
                        <label for="personal_info_name">Name</label>
                        <input type="text" class="form-control" id="personal_info_name" disabled value="<?php echo $row1['Name'];?>">
                        <label for="personal_info_gender">Gender</label>
                        <?php 
                            if($row1['Gender'] === "Male")
                                echo '<div id="personal_info_gender"><input type="radio" name="personal_radio" value="Male" checked>Male <input type="radio" name="personal_radio" value="Female">Female</div>';
                            else if($row1['Gender'] === "Female")
                                echo '<div id="personal_info_gender"><input type="radio" name="personal_radio" value="Male">Male <input type="radio" name="personal_radio" value="Female" checked>Female</div>';
                        ?>
                    </div>
                    <div class="col-md-4">
                        <?php
                            echo "<img src='uploads/{$row1['Picture']}' class='img-thumbnail rounded' alt='Profile Picture' style=\"width: auto; height:220px\">";
                        ?>                       
                    </div>
                </div>
            </div> <!-- col-md-12 -->
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-6">
                        <label for="personal_cnic">CNIC</label>
                        <input type="text" class="form-control" id="personal_cnic" disabled value="<?php echo $row1['CNIC_num'];?>">
                        <label for="personal_dob">Date of Birth</label>
                        <input type="text" class="form-control" id="personal_dob" disabled value="<?php echo $row1['DOB'];?>">
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                        <div class="col-md-6">
                            <label for="personal_pob">Place of Birth</label>
                            <input type="text" class="form-control" id="personal_pob" disabled value="<?php echo $row2['Perma_City'];?>">
                            <label for="personal_domicile">Domicile</label>
                            <input type="text" class="form-control" id="personal_domicile" disabled value="<?php echo $row1['Domicile'];?>">
                        </div>
                        <div class="col-md-6">
                            <label for="personal_religion">Religion</label>
                            <input type="text" class="form-control" id="personal_religion" disabled value="<?php echo $row1['Religion'];?>">
                            <label for="personal_marital">Marital Status</label>
                            <input type="text" class="form-control" id="personal_marital" disabled value="<?php echo "Single";?>">
                        </div>
                    </div>
                    </div>

                </div>
            </div><!-- col-md-12 -->
            <div class="col-md-12">
                <div class="row">
                <div class="col-md-8">
                    
                        <label for="father_name">Father Name</label>
                        <input type="text" class="form-control" id="father_name" disabled value="<?php echo $row1['Father_Name'];?>">
                    
                        <label for="father_cnic">Father CNIC</label>
                        <input type="text" class="form-control" id="father_cnic" disabled value="<?php echo $row1['Father_Name'];?>">
                
                </div>
                <div class="col-md-4">
                    
                        <label for="father_phone">Phone</label>
                        <input type="text" class="form-control" id="father_phone" disabled value="<?php echo $row1['Father_Name'];?>">
                    
                        <label for="father_email">Email</label>
                        <input type="text" class="form-control" id="father_email" disabled value="<?php echo $row1['Father_Name'];?>">
                    
                </div>
                </div>
            </div><!-- col-md-12 -->
            <div class="col-md-12">
                <div class="row"> 
                    <div class="col-md-12">  
                        <label for="perm_address">Permanent Address</label>
                        <input type="text" class="form-control" id="perm_address" disabled value="<?php echo $row2['Perma_Address']." ". $row2['Perma_City'];?>">
                        <label for="cur_address">Current Address (If different from above)</label>
                        <input type="text" class="form-control" id="cur_address" disabled value="<?php echo $row2['Cur_Address']." ". $row2['Cur_City'];?>">
                    </div>
                    <div class="col-md-12">
                        <label for="std_email">Email</label>
                        <input type="text" class="form-control" id="std_email" disabled value="<?php echo $row1['Email'];?>">
                        <label for="std_cell">Cell</label>
                        <input type="text" class="form-control" id="std_cell" disabled value="<?php echo $row1['Mobile'];?>">
                    </div>
                </div>
            </div><!-- col-md-12 -->
            <div class="col-md-12">
            <label for="personal_info">Personal Information Doc: </label>
            <?php
                
                    echo "<img src='uploads/{$row1['CNIC_front']}' id='personal_info' class='img-thumbnail rounded' alt='Personal Information' style=\"width: auto; height:auto\">";
                ?>
            </div><!-- col-md-12 -->
        </div>
     </div>
    </div> <!-- Personal Info Ended-->
    <div class="container page" id="academic_background">
        <div class="panel panel-danger">
            <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Academic Background</Strong></h3>
            </div>
            <div class="panel-body">
                <table class="table table-responsive table-striped">
                    <tr>
                    <th>Degree</th>
                    <?php 
                    $myquery1 = "SELECT Degree FROM tbl_qualification WHERE CID='$id'";  
                    $myresult1 = mysqli_query($connection, $myquery1);
                        while($myrow1 = mysqli_fetch_array($myresult1)){
                            echo "<td>".$myrow1['Degree']."</td>";
                    }?>
                    </tr>
                    <tr>
                    <th>Year of Passing</th>
                    <?php 
                    $myquery2 = "SELECT Passing_Year FROM tbl_qualification WHERE CID='$id'";  
                    $myresult2 = mysqli_query($connection, $myquery2);
                        while($myrow2 = mysqli_fetch_array($myresult2)){
                            echo "<td>".$myrow2['Passing_Year']."</td>";
                    }?>
                    </tr>
                    <tr>
                    <th>Board/University</th>
                    <?php 
                    $myquery3 = "SELECT Board FROM tbl_qualification WHERE CID='$id'";  
                    $myresult3 = mysqli_query($connection, $myquery3);
                        while($myrow3 = mysqli_fetch_array($myresult3)){
                            echo "<td>".$myrow3['Board']."</td>";
                    }?>
                    </tr>
                    <tr>
                    <th>Major</th>
                    <?php 
                    $myquery4 = "SELECT Major FROM tbl_qualification WHERE CID='$id'";  
                    $myresult4 = mysqli_query($connection, $myquery4);
                        while($myrow4 = mysqli_fetch_array($myresult4)){
                            echo "<td>".$myrow4['Major']."</td>";
                    }?>
                    </tr>
                    <tr>
                    <th>Max Marks</th>
                    <?php 
                    $myquery5 = "SELECT Max_Marks FROM tbl_qualification WHERE CID='$id'";  
                    $myresult5 = mysqli_query($connection, $myquery5);
                        while($myrow5 = mysqli_fetch_array($myresult5)){
                            echo "<td>".$myrow5['Max_Marks']."</td>";
                    }?>
                    </tr>
                    <tr>
                    <th>Obtained Marks</th>
                    <?php 
                    $myquery6 = "SELECT Obt_Marks FROM tbl_qualification WHERE CID='$id'";  
                    $myresult6 = mysqli_query($connection, $myquery6);
                        while($myrow6 = mysqli_fetch_array($myresult6)){
                            echo "<td>".$myrow6['Obt_Marks']."</td>";
                    }?>
                    </tr>
                    <tr>
                    <th>Distinction</th>
                    <?php 
                    $myquery7 = "SELECT Distinction FROM tbl_qualification WHERE CID='$id'";  
                    $myresult7 = mysqli_query($connection, $myquery7);
                        while($myrow7 = mysqli_fetch_array($myresult7)){
                            echo "<td>".$myrow7['Distinction']."</td>";
                    }?>
                    </tr>


                </table>
            </div>
        </div>
    </div> <!-- Academic Background Ended -->
    <div class="container page" id="gat">
        <div class="panel panel-danger">
            <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>GAT Test</Strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <table class="table table-responsive table-striped">
                                <tr>
                                    <th>Gat Test Date</th>
                                    <td><?php echo $row4['Test_Date'];?></td>
                                </tr>
                                <tr>
                                    <th>Max Marks</th>
                                    <td><?php echo $row4['Max_Marks'];?></td>
                                </tr>
                                <tr>
                                    <th>Obtained Marks</th>
                                    <td><?php echo $row4['Obt_Marks'];?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <?php
                                echo "<img src='uploads/{$row4['Result_Card']}' class='img-thumbnail rounded' alt='No GAT Result' style=\"width: auto; height:220px\">";
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Gat Result -->
    <div class="container page" id="transcript_detail">
        <div class="panel panel-danger">
            <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Transcript</Strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="row">
                    <?php while($row5 = mysqli_fetch_array($result5)){?>
                        <div class="col-md-6">
                            <table class="table table-responsive table-striped">
                                <tr>
                                    <th>Semester</th>
                                    <td><?php echo $row5['Semester_Num'];?></td>
                                </tr>
                                <tr>
                                    <th>Result Declaration Date</th>
                                    <td><?php echo $row5['Result_Date'];?></td>
                                </tr>
                                <tr>
                                    <th>Max CGPA</th>
                                    <td><?php echo $row5['Max_CGPA'];?></td>
                                </tr>
                                <tr>
                                    <th>Obtained CGPA</th>
                                    <td><?php echo $row5['Obt_CGPA'];?></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <?php
                                echo "<img src='uploads/{$row5['Result_Card']}' class='img-thumbnail rounded' alt='No Transcript' style=\"width: auto; height:220px\">";
                            ?>
                        </div>
                    <?php }?>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- Transcript Detail -->
    <div class="container" id="documents">
        <div class="panel panel-danger">
            <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Documents</Strong></h3>
            </div>
            <div class="panel-body">
                <div class="col-md-12">
                <?php
                    $query42 = "SELECT DMC, Original_Certificate FROM tbl_qualification WHERE CID='$id'";
                    $result42 = mysqli_query($connection, $query42);
                    while($row42=mysqli_fetch_array($result42)){    
                        // echo $row42['DMC']."/".$row42['Original_Certificate'];
                        echo "<div class='page'><img src='uploads/{$row42['DMC']}' class='img-thumbnail rounded col-md-12' alt='No DMC' style=\"width: auto; height:auto\"></div>";
                        echo "<div class='page'><img src='uploads/{$row42['Original_Certificate']}' class='img-thumbnail rounded col-md-12' alt='No Original Certificate' style=\"width: auto; height:auto\"></div>";

                    }
                ?>
                </div>

                <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Approval Of Synopsis</Strong></h3>
                </div>
                <div class="col-md-12">
                <?php
                    echo "<div class='page'><img src='uploads/{$row7['Synopsis_File']}' class='img-thumbnail rounded col-md-12' alt='No DMC' style=\"width: auto; height:auto\"></div>";
                    echo "<div class='page'><img src='uploads/{$row7['GPC_Approval_file']}' class='img-thumbnail rounded col-md-12' alt='No iamge' style=\"width: auto; height:auto\"></div>";
                    echo "<div class='page'><img src='uploads/{$row7['DPC_Approcal_File']}' class='img-thumbnail rounded col-md-12' alt='No image' style=\"width: auto; height:auto\"></div>";
                    echo "<div class='page'><img src='uploads/{$row7['BASR_Approval_File']}' class='img-thumbnail rounded col-md-12' alt='No image' style=\"width: auto; height:auto\"></div>";                    
                ?>
                </div>

                <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Approval of Supervisor</Strong></h3>
                </div>
                <div class="col-md-12">
                <?php
                    echo "<div class='page'><img src='uploads/{$row8['Approval_file']}' class='img-thumbnail rounded col-md-12' alt='No DMC' style=\"width: auto; height:auto\"></div>";
                    echo "<div class='page'><img src='uploads/{$row8['GPC_Approval_File']}' class='img-thumbnail rounded col-md-12' alt='No iamge' style=\"width: auto; height:auto\"></div>";
                    echo "<div class='page'><img src='uploads/{$row8['BOS_Approval_File']}' class='img-thumbnail rounded col-md-12' alt='No image' style=\"width: auto; height:auto\"></div>";
                    echo "<div class='page'><img src='uploads/{$row8['BASR_Approval_File']}' class='img-thumbnail rounded col-md-12' alt='No image' style=\"width: auto; height:auto\"></div>";                    
                ?>
                </div>

                <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Thesis Submission</Strong></h3>
                </div>
                <div class="col-md-12">
                <?php
                    echo "<div class='page'><img src='uploads/{$row9['gpcfile']}' class='img-thumbnail rounded col-md-12' alt='No DMC' style=\"width: auto; height:auto\"></div>";
                    echo "<div class='page'><img src='uploads/{$row9['thesisfile']}' class='img-thumbnail rounded col-md-12' alt='No iamge' style=\"width: auto; height:auto\"></div>";
                    
                ?>
                </div>

                <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Thesis Evaluation Report</Strong></h3>
                </div>
                <div class="col-md-12">
                <?php
                    echo "<div class='page'><img src='uploads/{$row10['Evaluation_Report']}' class='img-thumbnail rounded col-md-12' alt='No DMC' style=\"width: auto; height:auto\"></div>";
                    echo "<div class='page'><img src='uploads/{$row10['Approval_File']}' class='img-thumbnail rounded col-md-12' alt='No iamge' style=\"width: auto; height:auto\"></div>";
                    
                ?>
                </div>

                <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Public Defence Notification</Strong></h3>
                </div>
                <div class="col-md-12">
                <?php
                    echo "<div class='page'><img src='uploads/{$row14['BASR_File']}' class='img-thumbnail rounded col-md-12' alt='No DMC' style=\"width: auto; height:auto\"></div>";
                    echo "<div class='page'><img src='uploads/{$row14['Commitee_File']}' class='img-thumbnail rounded col-md-12' alt='No iamge' style=\"width: auto; height:auto\"></div>";
                    
                ?>
                </div>

                <div class='panel-heading' style='background-color:#983415;border:none' >
                <h3 style='text-align:center;color:white;'><Strong>Degree Completion</Strong></h3>
                </div>
                <div class="col-md-12">
                <?php
                    echo "<div class='page'><img src='uploads/{$row15['Transcript_File']}' class='img-thumbnail rounded col-md-12' alt='No DMC' style=\"width: auto; height:auto\"></div>";
                    echo "<div class='page'><img src='uploads/{$row15['Final_Degree_File']}' class='img-thumbnail rounded col-md-12' alt='No iamge' style=\"width: auto; height:auto\"></div>";
                    
                ?>
                </div>
            </div>
        </div>
    </div><!-- documents -->
</body>
</html>
<?php
include 'includes/connection.php';
$arr = 1;

$result1 = mysqli_query($connection, "SELECT * FROM tbl_candidate where CID = $arr || Email = $arr || Name = $arr || Father_Name = $arr || DOB = $arr || CNIC_num = $arr || Gender = $arr || Mobile = $arr|| Domicile = $arr || Religion = $arr") or die(mysqli_error($connection));
$row1 = mysqli_fetch_array($result1);
$result2 = mysqli_query($connection, "SELECT * FROM tbl_address where CID = $arr || Perma_Address = $arr || Perma_City = $arr || Perma_Phone = $arr || Cur_Address = $arr || Cur_City = $arr || Cur_Phone = $arr || Guard_Address = $arr|| Guard_City = $arr || Guard_Phone = $arr") or die(mysqli_error($connection));
$row2 = mysqli_fetch_array($result2);
$result3 = mysqli_query($connection, "SELECT * FROM tbl_qualification where CID = $arr || Degree = $arr || Passing_Year = $arr || Board = $arr || Major = $arr || Max_Marks = $arr || Obt_Marks = $arr || DMC = $arr|| Original_Certificate = $arr || Distinction = $arr") or die(mysqli_error($connection));
$row3 = mysqli_fetch_array($result3);
$result4 = mysqli_query($connection, "SELECT * FROM tbl_admission_form where CID = $arr ") or die(mysqli_error($connection));
$row4 = mysqli_fetch_array($result4);
$result5 = mysqli_query($connection, "SELECT * FROM tbl_gat where CID = $arr || Test_Date = $arr || Max_Marks = $arr || Obt_Marks = $arr ") or die(mysqli_error($connection));
$row5 = mysqli_fetch_array($result5);
$result6 = mysqli_query($connection, "SELECT * FROM tbl_transcript_details where CID = $arr || Semester_Num = $arr || Result_Date = $arr || Max_CGPA = $arr || Obt_CGPA = $arr ") or die(mysqli_error($connection));
$row6 = mysqli_fetch_array($result6);
$result7 = mysqli_query($connection, "SELECT * FROM tbl_comprehensive where CID = $arr || Exam_Date = $arr || Max_Marks = $arr || Obt_Marks = $arr") or die(mysqli_error($connection));
$row7 = mysqli_fetch_array($result7);
$result8 = mysqli_query($connection, "SELECT * FROM tbl_synopsis where CID = $arr || Approval_Date = $arr || Title = $arr || Abstract = $arr || Supervisor = $arr") or die(mysqli_error($connection));
$row8 = mysqli_fetch_array($result8);
$result9 = mysqli_query($connection, "SELECT * FROM tbl_supervisor where CID = $arr || Name = $arr || Affiliations = $arr ") or die(mysqli_error($connection));
$row9 = mysqli_fetch_array($result9);
$result10 = mysqli_query($connection, "SELECT * FROM tbl_thesis where CID = $arr || Date = $arr || title = $arr") or die(mysqli_error($connection));
$row10 = mysqli_fetch_array($result10);
$result11 = mysqli_query($connection, "SELECT * FROM tbl_evaluation where CID = $arr || Evaluator_name = $arr || Affeliations = $arr") or die(mysqli_error($connection));
$row11 = mysqli_fetch_array($result11);
$result12 = mysqli_query($connection, "SELECT * FROM tbl_defence_notification where CID = $arr || Date = $arr ") or die(mysqli_error($connection));
$row12 = mysqli_fetch_array($result12);
$result13 = mysqli_query($connection, "SELECT * FROM tbl_degree_completion where CID = $arr || Date = $arr ") or die(mysqli_error($connection));
$row13 = mysqli_fetch_array($result13);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	  <title>WAD | QUIZ</title>
	  <!--<link rel="icon" href="images/favicon.ico"/>-->
	  <meta charset="utf-8">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	  <meta http-equiv="cache-control" content="no-cache" />
	  <meta http-equiv="pragma" content="no-cache" />
	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

	  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	  <link href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css"/>
	  <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">	</script>
</head>
<body>
	<div class='container' style="margin-top:  75px;">
    <div class="panel panel-danger">
      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Personal Information </Strong></h3>
        </div>
      <div class="panel-body">
        <div class='form-group col-md-6'>
            <label for='control-label '>Email:</label>
            <?php
            echo "<input type=''  class='form-control' name='email' placeholder='' value=\"{$row1['Email']}\" readonly/>";
            ?>
        </div>
        <div class='form-group col-md-6'>
               <label for='control-label '>Name:</label>
               <?php
               echo "<input type='' class='form-control' name='email' placeholder='' value=\"{$row1['Name']}\"  readonly/>";
               ?>
        </div>
        <div class='form-group col-md-6'>
            <label for='control-label '>Profile Picture</label>
            <?php
               echo "<img src='uploads/{$row1['Picture']}' class='form-img' alt='Profile Picture' style=\"width: 100%;\">";
            ?>
        </div>
        <div class='form-group col-md-6'>
            <label for='control-label '><span style='color:red'>*</span> Date of Birth</label>
            <?php
               echo "<input type=''  class='form-control' name='email' placeholder='' value=\"{$row1['DOB']}\" readonly/>";
               ?>
        </div>
        <div class='form-group col-md-6'>
            <label for='control-label '><span style='color:red'>*</span> CNIC/Form-B</label>
            <?php
               echo "<input type=''  class='form-control' name='email' placeholder='' value=\"{$row1['CNIC_num']}\" readonly/>";
               ?>
         </div>
        <div class='col-md-12'>
            <div class='form-group col-md-6'>
                <label class='control-label '><span style='color:red'>*</span>CNIC/Form-B (Front)</label>
                
                <?php
                     echo "<img src='uploads/{$row1['CNIC_front']}' alt='No CNIC Uploaded' class='form-img' style=\"width: 100%;\">";
               ?>
          </div>
            <div class='form-group col-md-6'>
                <label class='control-label '><span style='color:red'>*</span>CNIC/Form-B (Back)</label>
                
                <?php
                     echo "<img src='uploads/{$row1['CNIC_back']}' alt='No CNIC Uploaded' class='form-img' style=\"width: 100%;\">";
               ?>
            </div>
        </div>
        <div class='form-group col-md-12'>
            <label class='control-label '><span style='color:red'>*</span> Father Name</label>
            <?php
               echo "<input type=''  class='form-control' name='email' placeholder='' value=\"{$row1['Father_Name']}\" readonly/>";
               ?>
            </div>
        <div class='form-group col-md-6'>
            <label for='control-label '>Gender</label>
            <?php
               echo "<input type=''  class='form-control' name='email' placeholder='' value=\"{$row1['Gender']}\" readonly/>";
               ?>
        </div>
        <div class='form-group col-md-6'>
               <label for='control-label '>Mobile Number</label>
               <?php
               echo "<input type=''  class='form-control' name='email' placeholder='' value=\"{$row1['Mobile']}\" readonly/>";
               ?>
        </div>
        <div class='form-group col-md-6'>
            <label for='control-label '>Domicile</label>
            <?php
               echo "<input type=''  class='form-control' name='email' placeholder='' value=\"{$row1['Domicile']}\" readonly/>";
               ?>
        </div>
        <div class='form-group col-md-6'>
               <label for='control-label '>Religion</label>
               <?php
               echo "<input type=''  class='form-control' name='email' placeholder='' value=\"{$row1['Religion']}\" readonly/>";
               ?>
        </div>
      </div>
      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Permanent Address </Strong></h3>
        </div>
      <div class="panel-body">
      <section class='content'>
        <div class='row'>
            <div class='col-xs-12'>
                  <!-- Widget: user widget -->
                <div class='box box-widget widget-user'>
                    <div class='box-footer'>
                        <div class='row'>
                            <div class='col-xs-12'>
                                <div class='box-body table-responsive'>
                                    
                                    <fieldset>
                                        <legend></legend>       
                                        <div class='form-group'>
                                                <div class='row'>
                                                <label class='control-label col-sm-2'><span style='color:red'>*</span>Address</label>
                                                <div class='col-sm-5'>
                                                
                                                <textarea name='Permanent_Address' id='Permanent_Address' class='form-control' readonly><?php
                                                echo "{$row2['Perma_Address']}";
                                                ?></textarea>
                                                </div>
                                                </div>
                                                <br>
                                                <div class='row'>
                                                <label class='control-label col-sm-2'>City</label>
                                                <div class='col-sm-5'>
                                                    <?php
                                                    echo "<input type='text' class='form-control' name='name' id='name' value=\"{$row2['Perma_City']}\" readonly/>";
                                                    ?>
                                                </div>
                                                </div>
                                                <br>
                                                <div class='row'>
                                                <label class='control-label col-sm-2'><span style='color:red'></span>Phone No (If Any)</label>
                                                <div class='col-sm-5'>
                                                <?php
                                                    echo "<input type='text' class='form-control' name='name' id='name' value=\"{$row2['Perma_Phone']}\" readonly/>";
                                                    ?>
                                                    </div>
                                                </div>
                                        </div>
                
                                    </fieldset>
                                    <fieldset>
                                        <legend>Current Address</legend>
                                        <div class='form-group'>
                                                <div class='row'>
                                                <label class='control-label col-sm-2'><span style='color:red'>*</span>Address</label>
                                                <div class='col-sm-5'>
                                                   
                                                    <textarea name='Current_Address' id='Current_Address' class='form-control' readonly><?php
                                                    echo "{$row2['Cur_Address']}";
                                                    ?></textarea>
                                                </div>
                                                </div>
                                                <br>
                                            <div class='row'>
                                                <label class='control-label col-sm-2'>City</label>
                                                <div class='col-sm-5'>
                                                    <?php
                                                echo "<input type='text' class='form-control' name='name' id='name' value=\"{$row2['Cur_City']}\" readonly/>";
                                                ?>
                                                </div>
                                                </div>
                                                <br>
                                                <div class='row'>
                                                <label class='control-label col-sm-2'><span style='color:red'>*</span>Phone No (If any)</label>
                                                <div class='col-sm-5'>
                                                <?php
                                                echo "<input type='text' class='form-control' name='name' id='name' value=\"{$row2['Cur_Phone']}\" readonly/>";
                                                ?>
                                                     </div>
                                                </div>
                                            </div>
                                        
                                    </fieldset>
                                    <fieldset>
                                        <legend>
                                            Guardian's Address
                                        </legend>
                                            <div class='form-group'>
                                            <div class='row'>
                                            <label class='control-label col-sm-2'><span style='color:red'>*</span>Address</label>
                                            <div class='col-sm-5'>
                                                <textarea name='Guadian_Address' id='Guadian_Address' class='form-control' readonly><?php
                                                    echo "{$row2['Guard_Address']}";
                                                    ?></textarea>
                                            </div>
                                            </div>
                                            <br>
                                        <div class='row'>
                                            <label class='control-label col-sm-2'>City</label>
                                            <div class='col-sm-5'>  
                                            <?php
                                                echo "<input type='text' class='form-control' name='name' id='name' value=\"{$row2['Guard_City']}\" readonly/>";
                                                ?>
                                            </div>
                                        </div><br>
                                        <div class='row'>
                                        <label class='control-label col-sm-2'><span style='color:red'>*</span>Phone No (If any)</label>
                                        <div class='col-sm-5'>
                                            <?php
                                            echo "<input name='Guadian_Phone' id='Guadian_Phone'  value=\"{$row2['Guard_Phone']}\" class='form-control' readonly/>";
                                            ?>
                                            
                                        </div>
                                        </div>
                                        </div>
                                    </fieldset>
                                    <div class='form-group'>
             
            </div>              
        </div>
    </div>
</div>
</div><!-- /.widget-user -->
</div>
        </div>
        </section><!-- /.content -->

      </div>
      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong> Candidate Qualification Details</Strong></h3>
        </div>
      <div class="panel-body">
      <section class='content'>
            <h2 style='color:red'></h2>
        <div class='row'>
            <div class='col-xs-12'>
              <!-- Widget: user widget -->
            <div class='box box-widget widget-user'>
             
                <div class='box-footer'>
                    <div class='row'>
                        <div class='col-xs-12'>
                            <div class='box-body table-responsive'>
                                  <div class='form-group'>
                                      
                <?php 
              while($row3 = mysqli_fetch_array($result3)){
                echo "<div class='form-group'>
                <div class='row'>
                <label class='control-label col-sm-2'>Degree</label>
                <div class='col-sm-5'>
                <input type='text' class='form-control' name='name' id='name' value=\"{$row3['Degree']}\" readonly/>
               
                </div>
              </div>
            </div> 
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Year of Passing</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control' name='name' id='name' value=\"{$row3['Passing_Year']}\" readonly/>
                </div>
              </div>
            </div>
                                      
           <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Board/University</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control' name='name' id='name' value=\"{$row3['Board']}\" readonly/>
                </div>
              </div>
            </div>
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Major (i.e. Subjects)</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control' name='name' id='name' value=\"{$row3['Major']}\" readonly/>
                </div>
              </div>
            </div>            
             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control' name='name' id='name' value=\"{$row3['Max_Marks']}\" readonly/>
                </div>
            </div>
            <br>
                                      
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <input type='text' class='form-control' name='name' id='name' value=\"{$row3['Obt_Marks']}\" readonly/>
                </div>
            </div>
            <br>     
           <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Details Marks Certificate</label>
                
                    <div class='col-sm-5'>
                        <img src='uploads/{$row3['DMC']}' alt='No CNIC Uploaded' class='form-img' style=\"width: 100%;\">
                    </div>
                     
              </div>
            </div>
                                
            <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Degree/Original Certificate</label>
                
                    <div class='col-sm-5'>
                       <img src='uploads/{$row3['Original_Certificate']}' alt='No CNIC Uploaded' class='form-img' style=\"width: 100%;\">
                            
                        </div>
              </div>
            </div>
			<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Distinction</label>
                <div class='col-sm-5'>
                    <input class='form-control' name='name' id='name' value=\"{$row3['Distinction']}\" readonly/>
            </div>
              </div>
            </div> 
            <div class='form-group' id='DistinctionCertGrp' style='display:  none'>
              <div class='row'>
              <label class='control-label col-sm-2'>Distinction Certificate</label>
					      <div class='col-sm-5'>
						          <input type='file' class='form-control'  name='DistinctionCert'  id='DistinctionCert'/>
						         </div>

                                 </div>
                                 </div> <br> "; }
                ?>      
                           
                        
        </div>
    </div>
</div>
</div><!-- /.widget-user -->
</div>
        </div>
</section>
      </div>
      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Upload Addmission Form </Strong></h3>
        </div>
      <div class="panel-body">
      <div class='form-group'>
                <div class='row'>
                        <label class='control-label col-sm-3'>Addmission Form</label>
                
                        <div class='col-sm-5'>
                          
                            <?php
                     echo "<img src='uploads/{$row11['Upload_Form']}' alt='No CNIC Uploaded' class='form-img' style=\"width: 100%;\">";
               ?>
                            </div>
                        
                </div>
            </div>
            <hr>
<h3> Pre-Req Courses List<small></small>
          </h3><hr>
          <div class='col-sm-12'>
          <?php
          echo "<table class='table'>
                <tr>
                        <th>Course ID</th>
                        <th>Course Name</th>
                </tr>
                ";
              while($row12 = mysqli_fetch_array($result12)){
                  
                  echo "<tr>
                                <td>{$row12['Course_ID']}</td>
                                <td>{$row12['Course_Name']}</td>
                        </tr>";
                       
                    }
            echo "</table>";
             ?>
            </div>
            <hr>
<h3> Audit Courses List<small></small>
          </h3><hr>
          <div class='col-sm-12'>
          <?php
          echo "<table class='table'>
                <tr>
                        <th>Course ID</th>
                        <th>Course Name</th>
                </tr>
                ";
              while($row13 = mysqli_fetch_array($result13)){
                  
                  echo "<tr>
                                <td>{$row13['Course_ID']}</td>
                                <td>{$row13['Course_Name']}</td>
                        </tr>";
                       
                    }
            echo "</table>";
             ?>
       
            
            </div>
      </div>
      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>GAT General/Subject Details </Strong></h3>
        </div>
      <div class="panel-body">
          
      <hr><div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> GAT Test Date</label>
                <div class='col-sm-5'>
                <?php
                    echo "<input type='text' class='form-control' name='name' id='name' value=\"{$row4['Test_Date']}\" readonly/>";
                ?>
                </div>  
              </div>
            </div>
<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                <?php
                    echo "<input type='text' class='form-control' name='name' id='name' value=\"{$row4['Max_Marks']}\" readonly/>";
                ?>
                   </div>
            </div>
                         <br>             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='number' id='ObtainedMarks' step='any' class='form-control' value=\"{$row4['Obt_Marks']}\" readonly/>";
                    ?>
                </div>
            </div>
<br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>GAT Result</label>
                
                        <div class='col-sm-5'>
                            <?php
                            echo "<img class='form-img' src='uploads/{$row4['Result_Card']}' alt='No GAT Result' style=\"width: 100%;\">";
                            ?>
                           </div>

              </div>
            </div>
           
      </div>
      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Transcript Details</Strong></h3>
        </div>
      <div class="panel-body">
      <hr>
      <?php 
    
    while($row5 = mysqli_fetch_array($result5)){
               echo " <div class='form-group'>
               <div class='row'>
               <label class='control-label col-sm-2'>SEMESTER</label>
                
        <div class='col-sm-5'>
            <input type='text' class='form-control' name='name' id='name' value=\"{$row5['Semester_Num']}\" readonly/>
        </div>
    </div>
</div>  
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Result Declaration Date</label>
                <div class='col-sm-5'>
                    <input type='date' class='form-control' name='dob' value='{$row5['Result_Date']}' id='dob' readonly/>
                </div>  
              </div>
</div>
<div class='row'>
            <label class='control-label col-sm-2'><span style='color:red'>*</span>Max CGPA</label>
            <div class='col-sm-5'>
                <input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' value=\"{$row5['Max_CGPA']}\"readonly />
            </div>
</div>
                         <br>             
 <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained GPA</label>
                <div class='col-sm-5'>
                    <input type='number' id='ObtainedMarks' step='any' class='form-control' value=\"{$row5['Obt_CGPA']}\" readonly/>
                </div>
</div>
<br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Result Card</label>
                
                <div class='col-sm-5'>
                <img class='form-img' src='uploads/{$row5['Result_Card']}' alt='NO Result Card' style=\"width: 100%;\">
                </div>
         </div>
         </div>
   ";
    }
   ?>
</div>

      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Comprehensive Examination Result </Strong></h3>
        </div>
      <div class="panel-body">
      <hr><div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Date Exam Conducted</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='date' class='form-control' name='dob' value=\"{$row6['Exam_Date']}\" readonly  />";
                    ?>
                </div>  
              </div>
            </div>
<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Max Marks</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='number' id='MaxMarks' step='any' class='form-control' name='MaxMarks' value=\"{$row6['Max_Marks']}\" readonly />";
                    ?>
                </div>
            </div>
                         <br>             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Obtained Marks</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='number' id='ObtainedMarks' step='any' class='form-control' required='required' value=\"{$row6['Obt_Marks']}\" readonly/>";
                    ?>
                </div>
            </div>
<br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Question Paper 1</label>
                
                        <div class='col-sm-5'>
                        <?php
                    echo "<img class='form-img' src='uploads/{$row6['Paper1']}' alt='No Paper1' style=\"width: 100%;\">";
                    ?>                      
                        </div>
              </div>
              </div>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Question Paper 2</label>
                
                        <div class='col-sm-5'>
                        <?php
                    echo "<img class='form-img' src='uploads/{$row6['Paper2']}' alt='No Paper2' style=\"width: 100%;\">";
                    ?>  
                        </div>
              </div>
              </div>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Question Paper 3</label>
                
                        <div class='col-sm-5'>
                        <?php
                    echo "<img class='form-img' src='uploads/{$row6['Paper3']}' alt='No Paper3' style=\"width: 100%;\">";
                    ?>  
                                                 
                        </div>
              </div>
</div>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Comprehensive Result</label>
                
                        <div class='col-sm-5'>
                           
                            <?php
                    echo "<img class='form-img' src='uploads/{$row6['Comprehensive_Result']}' alt='No comprehensive result' style=\"width: 100%;\">";
                    ?>                      
                        </div>
                         
               
              </div>
            </div>
            
      </div>
      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Approval of Synopsis </Strong></h3>
        </div>
      <div class="panel-body">
      <hr><div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Approval Date</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='date' class='form-control' name='dob'  value=\"{$row7['Approval_Date']}\" readonly />";
                    ?>
                </div>  
              </div>
            </div>
	<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Synopsis Title</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='text' id='ThesisTitle' step='any' class='form-control'  value=\"{$row7['Title']}\" readonly/>";
                    ?>
                </div>
            </div>
                         <br>             
            <div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Abstract</label>
                <div class='col-sm-5'>
                    <textarea id='abstract' step='any' class='form-control' name='abstract' readonly ><?php
                        echo "{$row7['Abstract']}";
                        ?></textarea>
                </div>
            </div>
	<br>
	<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Supervisor:</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='text' id='supervisor' step='any' class='form-control'     value=\"{$row7['Supervisor']}\" readonly/>";
                    ?>
                </div>
            </div>
	<br>
	<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Synopsis File</label>
                
                        <div class='col-sm-5'>
                            <?php
                            echo "<img src='uploads/{$row7['Synopsis_File']}' class='form-img' alt='No Image' style=\"width: 100%;\">";
                            ?>                         
                        </div>
                         
               
              </div>
    </div><br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>GPC Approval File</label>
                
                        <div class='col-sm-5'>
                            <?php
                            echo "<img src='uploads/{$row7['GPC_Approval_file']}' class='form-img' alt='No Image' style=\"width: 100%;\">";
                            ?>                         
                        </div>
                         
               
              </div>
</div><br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>DPC Approval File</label>
                
                        <div class='col-sm-5'>
                            <?php
                        echo "<img src='uploads/{$row7['DPC_Approcal_File']}' class='form-img' alt='No Image' style=\"width: 100%;\">";
                        ?>                         
                        </div>
                         
               
              </div>
</div><br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>BASR Approval File</label>
                
                        <div class='col-sm-5'>
                            <?php
                            echo "<img src='uploads/{$row7['BASR_Approval_File']}' class='form-img' alt='No Image' style=\"width: 100%;\">";
                            ?>                     
                        </div>
                         
               
              </div>
      </div>
</div>

      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Approval of Supervisor  </Strong></h3>
        </div>

     
      <div class="panel-body">
      <hr><div class='form-group'>
	<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Supervisor Name:</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='text' id='supervisorname' class='form-control'value=\"{$row8['Name']}\"  readonly/>";
                    ?>
                </div>
            </div>
	<br>
	<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Affeliations:</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='text' id='Affiliations' step='any' class='form-control' name='Affiliations' value=\"{$row8['Affiliations']}\" readonly />";
                    ?>
                </div>
            </div>
	<br>
	<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Approval File</label>
                
                        <div class='col-sm-5'>
                            <?php
                            echo "<img src='uploads/{$row8['Approval_file']}' class='form-img' alt='No Image' style=\"width: 100%;\">";
                            ?>  
                        </div>
               
              </div>
    </div><br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>GPC Approval File</label>
                
                        <div class='col-sm-5'>  
                            <?php
                            echo "<img src='uploads/{$row8['GPC_Approval_File']}' class='form-img' alt='No Image' style=\"width: 100%;\">";
                            ?>            
                                         
                        </div>
                         
               
              </div>
    </div><br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>BOS Approval File</label>
                
                        <div class='col-sm-5'>
                            <?php
                            echo "<img src='uploads/{$row8['BOS_Approval_File']}' class='form-img' alt='No Image' style=\"width: 100%;\">";
                            ?>                         
                        </div>
                </div>
               
   </div><br>
<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>BASR Approval File</label>
                
                        <div class='col-sm-5'>                       
                            <?php
                            echo "<img src='uploads/{$row8['BASR_Approval_File']}' class='form-img' alt='No Image' style=\"width: 100%;\">";
                            ?>   
                        </div>
                         
               
              </div>
      </div>
</div>
                </div>
      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Thesis Submissions </Strong></h3>
        </div>
      <div class="panel-body">
      <h3>  <small></small>
          </h3><hr>
          <div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> <span style='color:red'>*</span> Thesis Submission Date</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='date' class='form-control' name='dob' value=\"{$row9['Date']}\" id='dob' required readonly />";
                    ?>
                </div>  
              </div>
            </div>
	<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Thesis Title</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='text' id='ThesisTitle' step='any' class='form-control' name='ThesisTitle' value=\"{$row9['title']}\" readonly/>";
                    ?>
                    
                </div>
            </div>
        <br>            
	<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>GPC Meeting File</label>
                
                        <div class='col-sm-5'>
                        <?php
                        echo "<img class='form-img' src='uploads/{$row9['gpcfile']}' alt='No Image' style=\"width: 100%;\">";
                        ?>    
                        </div>
                </div>
               
              </div>
        <br>      
	<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Thesis File</label>
                
                        <div class='col-sm-5'>
                        <?php
                    echo "<img class='form-img' src='uploads/{$row9['thesisfile']}' alt='No Image' style=\"width: 100%;\">";
                    ?>   
                        </div>
                         
               
              </div>
            </div>
      </div>

      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Thesis Evaluation Reports </Strong></h3>
        </div>
      <div class="panel-body">
      <h3> <small></small>
          </h3><hr><div class='form-group'>
	<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Local/Foreign Evaluator Name:</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='text' id='evaluation' step='any' class='form-control' value=\"{$row10['Evaluator_name']}\" readonly/>";
                    ?>
                </div>
            </div>
	<br>
	<div class='row'>
                <label class='control-label col-sm-2'><span style='color:red'>*</span>Affeliations:</label>
                <div class='col-sm-5'>
                    <?php
                    echo "<input type='text' id='Affeliations' step='any' class='form-control' name='Affeliations' value=\"{$row10['Affeliations']}\" readonly/>";
                    ?>
                </div>
            </div>
	<br>
		<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'> Evaluation Report</label>
                
                        <div class='col-sm-5'>
                        
                            <?php
                    echo "<img class='form-img' src='uploads/{$row10['Evaluation_Report']}' alt='No Image' style=\"width: 100%;\">";
                    ?>   
                        </div>
                </div>
              </div><br>
	<div class='form-group'>
              <div class='row'>
                <label class='control-label col-sm-2'>Approval File</label>
                
                        <div class='col-sm-5'>
                         
                            <?php
                    echo "<img class='form-img' src='uploads/{$row10['Approval_File']}' alt='No Image' style=\"width: 100%;\">";
                    ?>   
                        </div>
                         
               
              </div>

            </div>
                </div>
        </div>

      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Public Defence Notification </Strong></h3>
        </div>
      <div class="panel-body">
      <h3>  <small></small>
                      </h3><hr><div class='form-group'>
                          <div class='row'>
                            <label class='control-label col-sm-2'> <span style='color:red'>*</span> Notification Date</label>
                            <div class='col-sm-5'>
                            <?php
                                echo "<input type='date' class='form-control' name='dob' value=\"{$row14['Date']}\" readonly  />";
                                ?>
                            </div>  
                          </div>
                        </div>
                   
                    <br>            
                    <div class='form-group'>
                          <div class='row'>
                            <label class='control-label col-sm-2'>BASR Approval File</label>
                            
                                    <div class='col-sm-5'>
                                    <?php
                                        echo "<img class='form-img' src='uploads/{$row14['BASR_File']}' alt='No Image' style=\"width: 100%;\">";
                                        ?>                      
                                    </div>
                                     
                           
                          </div>
                    <br>      
                    <div class='form-group'>
                          <div class='row'>
                            <label class='control-label col-sm-2'>Commitee File</label>
                            
                                    <div class='col-sm-5'>
                                    <?php
                                        echo "<img class='form-img' src='uploads/{$row14['Commitee_File']}' alt='No Image' style=\"width: 100%;\">";
                                        ?>                            
                                    </div>
                                     
                           
                          </div>
                        </div>
      </div>
      <div class='panel-heading' style='background-color:#983415;border:none' >
	  <h3 style='text-align:center;color:white;'><Strong>Degree Completion</Strong></h3>
        </div>
      <div class="panel-body">
      <h3>  <small></small>
              </h3><hr><div class='form-group'>
                  <div class='row'>
                    <label class='control-label col-sm-2'> <span style='color:red'>*</span> Degree Completion Date</label>
                    <div class='col-sm-5'>
                    <?php
                    echo "<input type='text' id='evaluation' step='any' class='form-control' value=\"{$row15['Date']}\" readonly/>";
                    ?>
                    </div>  
                  </div>
                </div>
           
            <br>            
            <div class='form-group'>
                  <div class='row'>
                    <label class='control-label col-sm-2'>Transcript File</label>
                    
                            <div class='col-sm-5'>
                            <?php
                                        echo "<img class='form-img' src='uploads/{$row15['Transcript_File']}' alt='No Image' style=\"width: 100%;\">";
                                        ?>                           
                            </div>
                             
                   
                  </div>
            <br>      
            <div class='form-group'>
                  <div class='row'>
                    <label class='control-label col-sm-2'>Final Degree File</label>
                    
                            <div class='col-sm-5'>
                            <?php
                                echo "<img class='form-img' src='uploads/{$row15['Final_Degree_File']}' alt='No Image' style=\"width: 100%;\">";
                                ?>                          
                            </div>
                             
                   
                  </div>
                </div>
      </div>

    </div>
	</div>
</body>

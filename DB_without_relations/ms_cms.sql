-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2018 at 09:23 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ms_cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_address`
--

CREATE TABLE `tbl_address` (
  `CID` varchar(20) NOT NULL,
  `Perma_Address` varchar(150) NOT NULL,
  `Perma_City` varchar(20) NOT NULL,
  `Perma_Phone` varchar(15) NOT NULL,
  `Cur_Address` varchar(150) NOT NULL,
  `Cur_City` varchar(20) NOT NULL,
  `Cur_Phone` varchar(15) NOT NULL,
  `Guard_Address` varchar(150) NOT NULL,
  `Guard_City` varchar(20) NOT NULL,
  `Guard_Phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admission_form`
--

CREATE TABLE `tbl_admission_form` (
  `CID` varchar(20) NOT NULL,
  `Upload_Form` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_audit_courses`
--

CREATE TABLE `tbl_audit_courses` (
  `CID` varchar(20) NOT NULL,
  `Course_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_candidate`
--

CREATE TABLE `tbl_candidate` (
  `CID` varchar(20) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Name` varchar(100) NOT NULL,
  `Father_Name` varchar(100) NOT NULL,
  `Picture` blob NOT NULL,
  `DOB` date NOT NULL,
  `CNIC_num` int(15) NOT NULL,
  `CNIC_front` blob NOT NULL,
  `CNIC_back` blob NOT NULL,
  `Gender` text NOT NULL,
  `Mobile` varchar(100) NOT NULL,
  `Domicile` text NOT NULL,
  `Religion` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comprehensive`
--

CREATE TABLE `tbl_comprehensive` (
  `CID` varchar(20) NOT NULL,
  `Exam_Date` date NOT NULL,
  `Max_Marks` double NOT NULL,
  `Obt_Marks` double NOT NULL,
  `Paper1` blob NOT NULL,
  `Paper2` blob NOT NULL,
  `Paper3` blob NOT NULL,
  `Paper4` blob NOT NULL,
  `Comprehensive_Result` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_courses`
--

CREATE TABLE `tbl_courses` (
  `Course_ID` varchar(20) NOT NULL,
  `Course_Name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gat`
--

CREATE TABLE `tbl_gat` (
  `CID` varchar(20) NOT NULL,
  `Test_Date` date NOT NULL,
  `Max_Marks` double NOT NULL,
  `Obt_Marks` double NOT NULL,
  `Result_Card` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pre_req`
--

CREATE TABLE `tbl_pre_req` (
  `CID` varchar(20) NOT NULL,
  `Course_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_qualification`
--

CREATE TABLE `tbl_qualification` (
  `CID` varchar(20) NOT NULL,
  `Degree` varchar(100) NOT NULL,
  `Passing_Year` int(11) NOT NULL,
  `Board` varchar(100) NOT NULL,
  `Major` text NOT NULL,
  `Max_Marks` double NOT NULL,
  `Obt_Marks` double NOT NULL,
  `DMC` blob NOT NULL,
  `Original_Certificate` blob NOT NULL,
  `Distinction` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_semester_progress_report`
--

CREATE TABLE `tbl_semester_progress_report` (
  `CID` varchar(20) NOT NULL,
  `Semester` varchar(20) NOT NULL,
  `Year` varchar(10) NOT NULL,
  `Report_Date` date NOT NULL,
  `Student_Progress` int(11) NOT NULL,
  `Progress_File` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supervisor`
--

CREATE TABLE `tbl_supervisor` (
  `CID` varchar(20) NOT NULL,
  `Name` varchar(20) NOT NULL,
  `Affiliations` varchar(100) NOT NULL,
  `Approval_file` blob NOT NULL,
  `GPC_Approval_File` blob NOT NULL,
  `BOS_Approval_File` blob NOT NULL,
  `BASR_Approval_File` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_synopsis`
--

CREATE TABLE `tbl_synopsis` (
  `CID` varchar(20) NOT NULL,
  `Approval_Date` date NOT NULL,
  `Title` varchar(50) NOT NULL,
  `Abstract` varchar(200) NOT NULL,
  `Supervisor` varchar(20) NOT NULL,
  `Synopsis_File` blob NOT NULL,
  `GPC_Approval_file` blob NOT NULL,
  `DPC_Approcal_File` blob NOT NULL,
  `BASR_Approval_File` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transcript_details`
--

CREATE TABLE `tbl_transcript_details` (
  `CID` varchar(20) NOT NULL,
  `Semester_Num` int(11) NOT NULL,
  `Result_Date` date NOT NULL,
  `Max_CGPA` double NOT NULL,
  `Obt_CGPA` double NOT NULL,
  `Result_Card` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_address`
--
ALTER TABLE `tbl_address`
  ADD KEY `CID` (`CID`);

--
-- Indexes for table `tbl_candidate`
--
ALTER TABLE `tbl_candidate`
  ADD PRIMARY KEY (`CID`),
  ADD KEY `CID` (`CID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
